<?php
/**
 * Created by PhpStorm.
 * User: snilli
 * Date: 9/30/18
 * Time: 4:11 PM
 */

namespace app\components;

use Mpdf\Mpdf;
use Mpdf\Config\ConfigVariables;
use Mpdf\Config\FontVariables;
use yii\helpers\Url;

class MpdfManage
{
    public function newPdf($html, $header, $margin_top = 10, $margin_left = 10, $margin_right = 10, $margin_bottom = 10, $tag_hr = true) {
        $params = [
            'defaultConfig' => (new ConfigVariables())->getDefaults(),
            'defaultFontConfig' => (new FontVariables())->getDefaults(),
            'SetFontTHsarabun' => [
                'R' => 'THSarabunNew.ttf',
                'B' => 'THSarabunNew Bold.ttf',
            ]
        ];

        $fontDirs = $params['defaultConfig']['fontDir'];
        $fontData = $params['defaultFontConfig']['fontdata'];

        $mpdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                Url::base() . 'fonts/',
            ]),
            'fontdata' => $fontData + ['thsarabun' => $params['SetFontTHsarabun']],
            'default_font_size' => 14,
            'default_font' => 'thsarabun',
            'mode' => 'utf-8',
            'format' => 'A4',
            'orientation' => 'p',
            'margin_top' => $margin_top,
            'margin_left' => $margin_left,
            'margin_right' => $margin_right,
            'margin_bottom' => $margin_bottom,
        ]);

        $stylesheet = file_get_contents(Url::base() . 'css/bootstrap.css');
        $stylesheet .= file_get_contents(Url::base() . 'css/carbooking-report.css');
        $mpdf->WriteHTML($stylesheet, 1);
        // $mpdf->SetHTMLHeader("<div id='header'>$header</div>");
        // $mpdf->SetHTMLFooter("<div id='footer' class='text-right'> หน้า {PAGENO}/{nb}</div>");

        $mpdf->WriteHTML($html, 2);
        $mpdf->Output();
    }
}