<?php

namespace app\components;

use yii\base\Component;

class MessageManage extends Component
{
    public $create_at = 'ทำรายการเมื่อ';
    public $create_by = 'ทำรายการโดย';
    public $update_at = 'แก้ไขรายการเมื่อ';
    public $update_by = 'แก้ไขรายการโดย';

    public $create_button = 'สร้างรายการ';
    public $update_button = 'แก้ไขรายการ';
    public $delete_button = 'ลบรายการ';
    public $confirm = 'คุณแน่ใจใช่ไหมที่จะทำการลบรายการนี้';
    public $detail = 'รายละเอียด';

    public $car = 'รถ';
    public $car_miles_last_check = 'เลขไมค์เช็คสภาพ';
    public $car_insurance_date = 'วันต่อประกัน';
    public $car_act_date = 'วันต่อ พ.ร.บ.';

    public $car_model = 'รุ่น';
    public $car_license = 'ทะเบียนรถ';
    public $car_brand = 'ยี่ห้อ';

    public $car_booking = 'จองรถ';
    public $cb_location_name = 'สถานที่';
    public $cb_category_name = 'วัตถุประสงค์การใช้';
    public $cb_datetime_start = 'เวลาจองรถ';
    public $cb_datetime_end = 'เวลาคืนรถ';
    public $cb_miles_start = 'เลขไมล์รถก่อนใช้';
    public $cb_miles_end = 'เลขไมล์รถตอนคืน';
    public $cb_status = 'สถานะการจอง';
    public $cb_passengers = 'ผู้โดยสาร';

    public $traffic_ticket = 'ใบสั่ง';
    public $tt_accuse_at = 'กระทำผิดเมื่อ';
    public $payment_at = 'จ่ายค่าปรับเมื่อ';
    public $tt_location = 'สถานที่เกิดเหตุ';
    public $tt_price = 'ค่าเสียหาย';
    public $allegation_type_name = 'ประเภทความผิด';
    public $tt_due_pay_at = 'วันที่กำหนดจ่าย';
    public $tt_tid = 'เลขที่ใบสั่ง';

    public $car_fixed = 'ประวัติการซ่อมรถ';
    public $garage = 'อู่ซ่อมรถ';
    public $car_fixed_type = 'ประเภทการซ่อมรถ';
    public $miles = 'ระยะไมค์';
    public $cf_price = 'ราคาซ่อมรถ';
    public $cf_sender_fixed_by = 'ผู้นำส่งซ่อม';
    public $cf_fix_date = 'วันส่งซ่อม';

    public $cf_fixed_date = 'วันซ่อมเสร็จ';

    public $dash_board = 'Dash board';
    public $report = 'รายงาน';
    //------------------------------------------------------------------------
    //for attendance

    public $user_name = 'ชื่อพนักงาน';

    public $clockIn = 'เวลาเข้างาน';
    public $clockOut = 'เวลาออกงาน';
    public $clock_date = 'วันที่';
    public $overtime = 'งานล่วงเวลา';
    public $clockIn_status = 'สถานะการลงเวลา';
    public $clock_description = 'รายละเอียดการทำงาน';
    public $work_note = 'หมายเหตุ';

    public $depart_name = 'ชื่อแผนก';

    public $leave_type = 'ประเภทการลา';
    public $leave_start = 'วันที่ลา';
    public $leave_end = 'วันที่สิ้นสุดการลางาน';
    public $leave_description = 'รายละเอียดการลา';
    public $leave_approve_status = 'สถานะการอนุมัติ';
    public $leave_approve_by = 'อนุมัติโดย';

    public $outside_start = 'วันที่เดินทางไป';
    public $outside_end = 'วันที่เดินทางกลับ';


}