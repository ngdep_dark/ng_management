<?php

namespace app\components;

use yii\base\Component;

class DbManage extends Component
{
    public function select2Save ($model_loaded, $model_loaded_index, $new_model, $findone_model, $getRelation)
    {
        // Prepare new model for save select2 value and find unique value of model then send to my function.
        // My function will check unique value ? if not will save then link value id back to model and return model back.

        // my function need 5 parameter is
        // 1. model (return back),
        // 2. model attribute,
        // 3. new model for save select2 valus and
        // 4. find_one model for check unique value
        // 5. relation name
//        if (!is_numeric($model_loaded->$model_loaded_index)) {
//            if (!isset($findone_model)) {
//                $new_model->name = $model_loaded->$model_loaded_index;
//                if($new_model->save()) {
//                    $model_loaded->link($getRelation, $new_model);
//                    return $model_loaded;
//                }
//            } else {
//                $model_loaded->link($getRelation, $findone_model);
//                return $model_loaded;
//            }
//        } else return $model_loaded;
        if (!is_numeric($model_loaded->$model_loaded_index)) {
            if (!isset($findone_model)) {
                $new_model->name = $model_loaded->$model_loaded_index;
                if($new_model->save()) {
                    $model_loaded->$model_loaded_index = $new_model->id;
                    return $model_loaded;
                }
            } else {
                $model_loaded->$model_loaded_index = $findone_model->id;
                return $model_loaded;
            }
        } else return $model_loaded;
    }

}