<?php

namespace app\components;

use yii\base\Component;

class DatetimeManage extends Component
{
    protected $month = [
        'มกราคม',
        'กุมภาพันธ์',
        'มีนาคม',
        'เมษายน',
        'พฤษภาคม',
        'มิถุนายน',
        'กรกฎาคม',
        'สิงหาคม',
        'กันยายน',
        'ตุลาคม',
        'พฤศจิกายน',
        'ธันวาคม'
    ];

    protected $short_month = [
        'ม.ค.',
        'ก.พ.',
        'ม.ค.',
        'เม.ย.',
        'พ.ค.',
        'มิ.ย.',
        'ก.ค.',
        'ส.ค.',
        'ก.ย.',
        'ต.ค.',
        'พ.ย.',
        'ธ.ค.'
    ];

    //get input only datetime
    public function dtFormat($datetime, $short = false, $date_only = false)
    {
        $year = substr($datetime, '0', '4');
        $month = intval(substr($datetime, '5', '2'));
        $day = intval(substr($datetime, '8', '2'));
        $time = substr($datetime, '11', '5');

        $short == true ? $year = substr($year + 543, '2', '2') : $year = intval($year) + 543;
        $short == true ? $month = $this->short_month[$month - 1] : $month = $this->month[$month - 1];

        $output = $day . ' ' . $month . ' ' . $year . ' ';
        return $date_only === false ? $output . $time : $output;
    }

    //get input only timestamp
    public function tsFormat($timestamp, $short = false, $date_only = false)
    {
        $ts = date('Y-m-d H:i:s', $timestamp);
        return $this->dtFormat($ts, $short, $date_only);
    }

    public function changeThaiFormateDate($date)
    {
        $dateArray = explode('/', $date);
        $dateRightFormat = '';

        for ($x = count($dateArray) - 1; $x >= 0; $x--) {
            $dateRightFormat .= $dateArray[$x];
            if ($x != 0) $dateRightFormat .= '-';
        }
        return $dateRightFormat;
    }

    public function dateName($day)
    {
        if ($day == 'Monday') {
            return 'จันทร์';
        } elseif ($day == 'Tuesday') {
            return 'อังคาร';
        } elseif ($day == 'Wednesday') {
            return 'พุธ';
        } elseif ($day == 'Thursday') {
            return 'พฤหัสบดี';
        } elseif ($day == 'Friday') {
            return 'ศุกร์';
        } elseif ($day == 'Saturday') {
            return 'เสาร์';
        } else {
            return 'อาทิตย์';
        }
    }

}