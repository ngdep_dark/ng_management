<?php
use Mpdf\Config\FontVariables;
use Mpdf\Config\ConfigVariables;
return [
    'bsDependencyEnabled' => false,
    'bsVersion' => '3.4.1', 
    'adminEmail' => 'admin@example.com',
    'defaultConfig' => (new ConfigVariables())->getDefaults(),
    'defaultFontConfig' => (new FontVariables())->getDefaults(),
    'SetTHSarabun' => [
        'R' => 'THSarabunNew.ttf',
        'B' => 'THSarabunNew Bold.ttf',
    ]
];