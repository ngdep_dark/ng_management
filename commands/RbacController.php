<?php
namespace app\commands;

use app\models\User;
use Yii;
use yii\helpers\Console;
use yii\console\Controller;

class RbacController extends Controller {

    public function actionInit(){
        $auth = Yii::$app->authManager;
        $auth->removeAll();
        Console::output('Removing All! RBAC.....');
        $admin = $auth->createRole('Admin');
        $admin->description = 'ผู้ดูแลระบบ';
        $auth->add($admin);

        $manage = $auth->createRole("manage");
        $manage->description = 'จัดการข้อมูลพนักงาน';
        $auth->add($manage);

        $att = $auth->createRole('att');
        $att->type=2;
        $att ->description = 'หน้าลงเวลา';
        $auth->add($att);

        $store = $auth->createRole('store');
        $store->type=2;
        $store ->description = 'สต๊อกสินค้า';
        $auth->add($store);
        //เพิ่มเข้าใน admin
        $auth->addChild($admin,$att);
        $auth->addChild($admin,$store);
        $auth->addChild($admin,$manage);
        $auth->addChild($manage, $att);
        $auth->assign($admin, 1);
        $auth->assign($admin, 2);
        $auth->assign($admin, 6);
        $auth->assign($manage, 4);
        $auth->assign($store, 7);
        $auth->assign($manage, 12);
        $auth->assign($manage, 13);
        $auth->assign($admin, 3);
        $auth->assign($att, 5);
        $auth->assign($att, 9);
        $auth->assign($att, 10);
        $auth->assign($att, 11);
        $auth->assign($att, 14);
        $auth->assign($att, 15);
        $auth->assign($att, 16);
        $auth->assign($att, 17);
        $auth->assign($att, 18);
        $auth->assign($att, 19);
        $auth->assign($att, 20);
        $auth->assign($att, 26);
        $auth->assign($att, 27);
        $auth->assign($att, 28);
        $auth->assign($att, 29);
        $auth->assign($att, 30);

        Console::output('Success! RBAC roles has been added');
    }

}
?>