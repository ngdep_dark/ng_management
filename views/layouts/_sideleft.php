<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <div id="sidebar-menu">
            <ul>
                <li>
                    <a href="<?=\yii\helpers\Url::to(['/store/default/index'])?>" class="waves-effect">
                        <i class="mdi mdi-home"></i>
                        <span> Store
                            <span class="badge badge-primary pull-right">1</span>
                        </span>
                    </a>
                </li>

                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect">
                        <i class="mdi mdi-av-timer"></i>
                        <span> ข้อมูลการลงเวลา </span>
                        <span class="pull-right">
                            <i class="mdi mdi-plus"></i>
                        </span>
                    </a>
                    <ul class="list-unstyled">
                        <li>
                            <a href="<?=\yii\helpers\Url::to(['/attd/attendance/index'])?>">
                                <i class="mdi mdi-alarm-plus"></i>
                                ลงเวลา
                            </a>
                        </li>
                        <li>
                            <?php if(Yii::$app->getUser()->can('Admin')||Yii::$app->getUser()->can('schedule')):?>
                            <a href="<?=\yii\helpers\Url::to(['/attd/schedule/index'])?>">
                                <i class="mdi mdi-calendar-check"></i>
                                สรุปตารางงาน
                            </a>
                            <?php endif;?>
                            <a href="<?=\yii\helpers\Url::to(['/attd/schedule/index'])?>">
                                <i class="mdi mdi-repeat-once"></i>
                                รายงาน
                            </a>
                        </li>
                        <li>
                            <a href="<?=\yii\helpers\Url::to(['/attd/shift/index'])?>">
                                <i class="mdi mdi-beach"></i>
                                จัดการโปรไฟล์ลงเวลา
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect">
                        <i class="mdi mdi-car"></i>
                        <span> ข้อมูลการใช้รถ </span>
                        <span class="pull-right">
                            <i class="mdi mdi-plus"></i>
                        </span>
                    </a>
                    <ul class="list-unstyled">
                        <li>
                            <a href="<?=\yii\helpers\Url::to(['/car-booking/car-booking/'])?>">
                                <i class="mdi mdi-book"></i>
                                <span> จองรถ </span>
                            </a>
                            <a href="<?=\yii\helpers\Url::to(['/car-booking/car-booking/overall-approve-booking'])?>">
                                <i class="mdi mdi-playstation"></i>
                                <span> อนุมัติ </span>
                            </a>
                        </li>
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect">
                                <i class="mdi mdi-wrench"></i>
                                <span> ตั้งค่า </span>
                                <span class="pull-right">
                                    <i class="mdi mdi-plus"></i>
                                </span>
                            </a>
                            <ul class="list-unstyled">
                                <li>
                                    <a href="<?=\yii\helpers\Url::to(['/car-booking/car'])?>"> รถ </a>
                                </li>
                                <li>
                                    <a href="<?=\yii\helpers\Url::to(['/car-booking/car-brand'])?>"> ยี่ห้อรถ </a>
                                </li>
                                <li>
                                    <a href="<?=\yii\helpers\Url::to(['/car-booking/car-booking-category'])?>">
                                        ประเภทการใช้รถ </a>
                                </li>
                                <li>
                                    <a href="<?=\yii\helpers\Url::to(['/car-booking/car-booking-location'])?>">
                                        ที่หมายปลายทาง </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect">
                        <i class="mdi mdi-layers"></i>
                        <span> ตั้งค่าระบบ</span>
                        <span class="pull-right">
                            <i class="mdi mdi-plus"></i>
                        </span>
                    </a>
                    <ul class="list-unstyled">
                        <li>

                            <a href="<?=\yii\helpers\Url::to(['/auth/index'])?>">
                                <i class="mdi mdi-account-key"></i>
                                สิทธิ์การใช้งาน</a>
                        </li>

                        <li>
                            <a href="<?=\yii\helpers\Url::to([''])?>">
                                <i class="mdi mdi-account-multiple"></i>
                                ข้อมูลผู้ใช้งาน</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>