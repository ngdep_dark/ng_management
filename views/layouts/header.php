<?php
use yii\helpers\Html;
use \app\modules\attd\models\AttendanceGroupDescription;
use yii\helpers\ArrayHelper;
use app\modules\attd\models\Leave;
use \app\modules\attd\models\AttendanceOutsideService;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">APP</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">
                <!-- Messages: style can be found in dropdown.less-->
                <!-- <li class="dropdown messages-menu"> -->
                <!--                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">-->
                <!--                        <i class="fa fa-envelope-o"></i>-->
                <!--                        <span class="label label-success">4</span>-->
                <!--                    </a>-->
                <!--                    <ul class="dropdown-menu">-->
                <!--                        <li class="header">You have 4 messages</li>-->
                <!--                        <li>-->
                <!--                            <!-- inner menu: contains the actual data -->
                <!--                            <ul class="menu">-->
                <!--                                <li><!-- start message -->
                <!--                                    <a href="#">-->
                <!--                                        <div class="pull-left">-->
                <!--                                            <img src="-->
                <?//= $directoryAsset ?>
                <!--/img/user2-160x160.jpg" class="img-circle"-->
                <!--                                                 alt="User Image"/>-->
                <!--                                        </div>-->
                <!--                                        <h4>-->
                <!--                                            Support Team-->
                <!--                                            <small><i class="fa fa-clock-o"></i> 5 mins</small>-->
                <!--                                        </h4>-->
                <!--                                        <p>Why not buy a new awesome theme?</p>-->
                <!--                                    </a>-->
                <!--                                </li>-->
                <!--                                <!-- end message -->
                <!--                                <li>-->
                <!--                                    <a href="#">-->
                <!--                                        <div class="pull-left">-->
                <!--                                            <img src="-->
                <?//= $directoryAsset ?>
                <!--/img/user3-128x128.jpg" class="img-circle"-->
                <!--                                                 alt="user image"/>-->
                <!--                                        </div>-->
                <!--                                        <h4>-->
                <!--                                            AdminLTE Design Team-->
                <!--                                            <small><i class="fa fa-clock-o"></i> 2 hours</small>-->
                <!--                                        </h4>-->
                <!--                                        <p>Why not buy a new awesome theme?</p>-->
                <!--                                    </a>-->
                <!--                                </li>-->
                <!--                                <li>-->
                <!--                                    <a href="#">-->
                <!--                                        <div class="pull-left">-->
                <!--                                            <img src="-->
                <?//= $directoryAsset ?>
                <!--/img/user4-128x128.jpg" class="img-circle"-->
                <!--                                                 alt="user image"/>-->
                <!--                                        </div>-->
                <!--                                        <h4>-->
                <!--                                            Developers-->
                <!--                                            <small><i class="fa fa-clock-o"></i> Today</small>-->
                <!--                                        </h4>-->
                <!--                                        <p>Why not buy a new awesome theme?</p>-->
                <!--                                    </a>-->
                <!--                                </li>-->
                <!--                                <li>-->
                <!--                                    <a href="#">-->
                <!--                                        <div class="pull-left">-->
                <!--                                            <img src="-->
                <?//= $directoryAsset ?>
                <!--/img/user3-128x128.jpg" class="img-circle"-->
                <!--                                                 alt="user image"/>-->
                <!--                                        </div>-->
                <!--                                        <h4>-->
                <!--                                            Sales Department-->
                <!--                                            <small><i class="fa fa-clock-o"></i> Yesterday</small>-->
                <!--                                        </h4>-->
                <!--                                        <p>Why not buy a new awesome theme?</p>-->
                <!--                                    </a>-->
                <!--                                </li>-->
                <!--                                <li>-->
                <!--                                    <a href="#">-->
                <!--                                        <div class="pull-left">-->
                <!--                                            <img src="-->
                <?//= $directoryAsset ?>
                <!--/img/user4-128x128.jpg" class="img-circle"-->
                <!--                                                 alt="user image"/>-->
                <!--                                        </div>-->
                <!--                                        <h4>-->
                <!--                                            Reviewers-->
                <!--                                            <small><i class="fa fa-clock-o"></i> 2 days</small>-->
                <!--                                        </h4>-->
                <!--                                        <p>Why not buy a new awesome theme?</p>-->
                <!--                                    </a>-->
                <!--                                </li>-->
                <!--                            </ul>-->
                <!--                        </li>-->
                <!--                        <li class="footer"><a href="#">See All Messages</a></li>-->
                <!--                    </ul>-->
                <!--                </li>-->
                <?php
                $leaveCount = null;
                $overtimeCount = null;
                $groupLeader = AttendanceGroupDescription::find()
                    ->where(['user_id' => Yii::$app->user->id])
                    ->andWhere(['group_leader' => true])
                    ->one();
                if (!empty($groupLeader)){
                    $groupDescription = AttendanceGroupDescription::find()
                        ->select('user_id')
                        ->where(['group_leader' => false])
                        ->andWhere(['group_id' => $groupLeader->group_id])
                        ->groupBy('user_id')
                        ->asArray()
                        ->all();

                    $EmployeeId = ArrayHelper::map($groupDescription, 'user_id', 'user_id');

                    $leaveQuery = Leave::find()
                        ->where(['user_id' => $EmployeeId])
                        ->andWhere(['approve_status' => 1])
                        ->all();
                    $leaveCount = count($leaveQuery);

                    $overtimeQuery = AttendanceOutsideService::find()
                        ->where(['user_id' => $EmployeeId])
                        ->andWhere(['status' => 1])
                        ->all();
                    $overtimeCount = count($overtimeQuery);
                }
                ?>
                <?php if (!empty($groupLeader)): ?>
                <li class="dropdown notifications-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="label label-warning"><?= $leaveCount + $overtimeCount ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have <?= $leaveCount + $overtimeCount ?> notifications</li>
                        <li>
                            <!--                             inner menu: contains the actual data -->
                            <ul class="menu">
                                <li>

                                    <a href="/attd/approve/approve#collapse1">
                                        <i class="fa fa-users text-aqua"></i> มีคำขออนุมัติลางานใหม่ <?= $leaveCount ?>
                                        รายการ
                                    </a>
                                </li>
                                <!-- <li>
                                    <a href="/attd/approve/approve#collapse1">
                                        <i class="fa fa-warning text-yellow"></i> มีคำขอทำงานล่วงเวลาใหม่
                                        <?= $overtimeCount ?> รายการ
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-users text-red"></i> 5 new members joined
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-user text-red"></i> You changed your username
                                    </a>
                                </li> -->
                            </ul>
                        </li>
                        <li class="footer"><a href="#">View all</a></li>
                    </ul>
                </li>
                <?php endif; ?>
                <!-- Tasks: style can be found in dropdown.less -->
                <li class="dropdown tasks-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-flag-o"></i>
                        <span class="label label-danger">9</span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header">You have 9 tasks</li>
                        <li>
                            <!-- inner menu: contains the actual data -->
                            <ul class="menu">
                                <li>
                                    <!-- Task item -->
                                    <a href="#">
                                        <h3>
                                            Design some buttons
                                            <small class="pull-right">20%</small>
                                        </h3>
                                        <div class="progress xs">
                                            <div class="progress-bar progress-bar-aqua" style="width: 20%"
                                                role="progressbar" aria-valuenow="20" aria-valuemin="0"
                                                aria-valuemax="100">
                                                <span class="sr-only">20% Complete</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- end task item -->
                                <li>
                                    <!-- Task item -->
                                    <a href="#">
                                        <h3>
                                            Create a nice theme
                                            <small class="pull-right">40%</small>
                                        </h3>
                                        <div class="progress xs">
                                            <div class="progress-bar progress-bar-green" style="width: 40%"
                                                role="progressbar" aria-valuenow="20" aria-valuemin="0"
                                                aria-valuemax="100">
                                                <span class="sr-only">40% Complete</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- end task item -->
                                <li>
                                    <!-- Task item -->
                                    <a href="#">
                                        <h3>
                                            Some task I need to do
                                            <small class="pull-right">60%</small>
                                        </h3>
                                        <div class="progress xs">
                                            <div class="progress-bar progress-bar-red" style="width: 60%"
                                                role="progressbar" aria-valuenow="20" aria-valuemin="0"
                                                aria-valuemax="100">
                                                <span class="sr-only">60% Complete</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- end task item -->
                                <li>
                                    <!-- Task item -->
                                    <a href="#">
                                        <h3>
                                            Make beautiful transitions
                                            <small class="pull-right">80%</small>
                                        </h3>
                                        <div class="progress xs">
                                            <div class="progress-bar progress-bar-yellow" style="width: 80%"
                                                role="progressbar" aria-valuenow="20" aria-valuemin="0"
                                                aria-valuemax="100">
                                                <span class="sr-only">80% Complete</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <!-- end task item -->
                            </ul>
                        </li>
                        <li class="footer">
                            <a href="#">View all tasks</a>
                        </li>
                    </ul>
                </li>
                <!-- User Account: style can be found in dropdown.less -->

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="https://app.nextschool.io/assets/5e80296f/images/logo.png" class="user-image"
                            alt="User Image" />
                        <span class="hidden-xs"><?= @Yii::$app->user->identity->username?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle"
                                alt="User Image" />

                            <p>
                                Alexander Pierce - Web Developer
                                <small>Member since Nov. 2012</small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <li class="user-body">
                            <div class="col-xs-4 text-center">
                                <a href="#">Followers</a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="#">Sales</a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="#">Friends</a>
                            </div>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <?= Html::a('Change Password',
                                    ['/user/change-password', 'id' => Yii::$app->user->id],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                )?>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Sign out',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>