<?php
$admin = Yii::$app->user->can('Admin');
?>
<aside class="main-sidebar">
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="https://app.nextschool.io/assets/5e80296f/images/logo.png" class="img-circle" alt="User Image"
                    width="160" />
            </div>
            <div class="pull-left info">
                <p><?= @Yii::$app->user->identity->username ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..." />
                <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i
                            class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'เมนูใช้งาน', 'options' => ['class' => 'header']],
                    [
                        'label' => 'ข้อมูลติดต่อใน Office',
                        'icon' => 'address-book',
                        'url' => '/site/contact-list',
                    ],
                    [
                        'label' => 'ปฏิทิน',
                        'icon' => 'calendar',
                        'url' => '#',
                        'visible' => false,
                        'items' => [
                            [
                                'label' => 'ปฏิทิน', 
                                'icon' => 'calendar', 
                                'url' => ['/']
                            ],
                            [
                                'label' => 'รายการงานในอาทิตย์นี้', 
                                'icon' => 'calendar', 
                                'url' => ['/site/appointment-list']
                            ],
                            [
                                'label' => 'คิวงานตัวเอง', 
                                'icon' => 'calendar', 
                                'url' => ['/appointment/my-appointment', 'id' => Yii::$app->user->id]
                            ],
                            [
                                'label' => 'ลงเวลานัดหมาย', 
                                'icon' => 'pencil', 
                                'url' => ['/appointment']
                            ],
                            [
                                'label' => 'ตั้งค่า',
                                'icon' => 'wrench',
                                'url' => '/appointment-type',
                            ],
                        ],
                    ],
                    [
                        'label' => 'Store',
                        'icon' => 'product-hunt',
                        'url' => '#',
                        'visible' => Yii::$app->user->can('store'),
                        'items' => [
                            ['label' => 'ข้อมูลสินค้า', 'icon' => 'list', 'url' => ['/store/product/index'],],
                            ['label' => 'บันทึการใช้งาน', 'icon' => 'exchange', 'url' => ['/store/'],],
                            ['label' => 'เพิ่มสินค้าเข้า & เบิกสินค้า', 'icon' => 'cart-plus', 'url' => ['/store/items/create'],],
                            ['label' => 'คำนวนการสั่งซื้อ', 'icon' => 'calculator', 'url' => ['/store/default/calculator'],],
                            ['label' => 'รายงาน', 'icon' => 'file-text', 'url' => ['/store/report/index'],],
                            [
                                'label' => 'ข้อมูลสินค้า',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'ประเภทสินค้า', 'icon' => 'file-code-o', 'url' => ['/store/types/index'],],
                                    ['label' => 'ข้อมูลโรงเรียน', 'icon' => 'file-code-o', 'url' => ['/store/school/index'],],
                                ],
                            ],
                        ],
                    ],
                    [
                        'label' => 'ข้อมูลการลงเวลาเข้า-ออก',
                        'icon' => 'clock-o',
                        'url' => '#',
                        'items' => [
                            [
                                'label' => 'Dashboard', 'icon' => 'dashboard', 'url' => ['/attd/dashboard/index']
                            ],
                            [
                                'label' => 'ข้อมูลพนักงาน',
                                'icon' => 'user',
                                'items' => [
                                    ['label' => 'ข้อมูลส่วนตัว', 'icon' => 'user', 'url' => ['/attd/schedule/index', 'id' => Yii::$app->user->id],],
                                    ['label' => 'ข้อมูลพนักงาน', 'icon' => 'user', 'url' => ['/attd/schedule/overall-employee'], 'visible' => Yii::$app->user->can('manage'),],
                                ]
                            ],
                            [
                                'label' => 'การลงเวลา',
                                'icon' => 'clock-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'การลงเวลาย้อนหลัง', 'icon' => 'clock-o', 'url' => ['/attd/attendance/index'], 'visible' => Yii::$app->user->can('manage')],
                                    ['label' => 'บันทึกการลา', 'icon' => 'bed', 'url' => ['/attd/leave/index'],],
                                    ['label' => 'ทำงานนอกสถานที่', 'icon' => 'car', 'url' => ['/attd/attendance-outside-service/index'],],
                                ],
                            ],
                            [
                                'label' => 'การอนุมัติ',
                                'icon' => 'thumbs-up',
                                'url' => '#',
                                'items' => [
//                                    ['label' => 'รายการรออนุมัติ', 'icon' => 'dashboard', 'url' => ['/attd/approve/approve'],],
                                    ['label' => 'รายการที่อนุมัติแล้ว', 'icon' => 'dashboard', 'url' => ['/attd/approve/overall-approve'], 'visible' => Yii::$app->user->can('Admin')],
                                ],
                                'visible' => Yii::$app->user->can("manage")
                            ],
                            [
                                'label' => 'รายงาน',
                                'icon' => 'print',
                                'url' => ['/attd/report/index'],
                                'visible' => Yii::$app->user->can("manage")
                            ],
                            [
                                'label' => 'ตั้งค่า',
                                'icon' => 'gears',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'จัดการกะลงเวลา', 'icon' => 'dashboard', 'url' => ['/attd/shift/index'],],
                                    ['label' => 'ตั้งค่าวันหยุด', 'icon' => 'dashboard', 'url' => ['/attd/holiday/index'],],
                                ],
                                'visible' => Yii::$app->user->can("Admin")
                            ],
                        ],
                    ],
                    [
                        'label' => 'ข้อมูลการใช้รถ',
                        'icon' => 'calendar-check-o',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Dash board', 'icon' => 'dashboard', 'url' => ['/car-booking/dash-board']],
                            ['label' => 'จองรถ', 'icon' => 'book', 'url' => ['/car-booking/car-booking']],
//                            ['label' => 'อนุมัติ', 'icon' => 'calendar-check-o', 'url' => ['/car-booking/car-booking/overall-approve-booking'], 'visible' => $admin],
                            ['label' => 'ใบสั่ง', 'icon' => 'ticket', 'url' => ['/car-booking/traffic-ticket'], 'visible' => $admin],
                            ['label' => 'รายงาน', 'icon' => 'file-text', 'url' => ['/car-booking/report']],
                            [
                                'label' => 'ตั้งค่า',
                                'icon' => 'cog',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'รถ', 'icon' => 'car', 'url' => ['/car-booking/car']],

//                                    [
//                                        'label' => 'เกี่ยวกับรถ',
//                                        'icon' => 'car',
//                                        'url' => '#',
//                                        'items' => [
//                                            ['label' => 'ยี่ห้อรถ', 'icon' => 'flag-checkered', 'url' => ['/car-booking/car-brand']],
//                                            ['label' => 'ประเภทการใช้รถ ', 'icon' => 'tags', 'url' => ['/car-booking/car-booking-category']],
//                                            ['label' => 'ที่หมายปลายทาง ', 'icon' => 'map-o', 'url' => ['/car-booking/car-booking-location']],
//                                        ]
//                                    ],
//                                    [
//                                        'label' => 'เกี่ยวกับการซ่อมรถ',
//                                        'icon' => 'wrench',
//                                        'url' => '#',
//                                        'items' => [
//                                            ['label' => 'อู่ซ่อมรถ', 'icon' => 'wrench', 'url' => ['/car-booking/garage']],
//                                            ['label' => 'ประเภทการซ่อมรถ', 'icon' => 'tags', 'url' => ['/car-booking/car-fixed-type']],
//                                        ]
//                                    ],
//                                    ['label' => 'ประเภทความผิด', 'icon' => 'balance-scale', 'url' => ['/car-booking/allegation-type']],
                                ],
                                'visible' => $admin
                            ],
                        ],
                    ],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => 'ตั้งต่าระบบ',
                        'icon' => 'share',
                        'url' => '#',
                        'visible' => $admin,
                        'items' => [
                            ['label' => 'สิทธิ์การใช้งาน', 'icon' => 'file-code-o', 'url' => ['/auth/index'], 'visible' => Yii::$app->user->can("Admin")],
                            ['label' => 'ข้อมูลผู้ใช้งาน', 'icon' => 'dashboard', 'url' => ['/user/index'], 'visible' => Yii::$app->user->can("Admin")],
//                            ['label' => 'ข้อมูลกลุ่มผู้ใช้งาน', 'icon' => 'group', 'url' => ['/attd/attendance-group-description/index'],],
                        ],
                    ],
                    ['label' => 'เครื่องมือ', 'icon' => 'wrench', 'url' => '/tools/site/',],
                ],
            ]
        ) ?>
    </section>
</aside>