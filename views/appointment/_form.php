<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DateTimePicker;
use kartik\widgets\Select2;
use kartik\widgets\DepDrop;
/* @var $this yii\web\View */
/* @var $model app\models\Appointment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appointment-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'school_id')
    ->widget(Select2::classname(), [
        'data' => $model->schoolData,
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>

    <?= $form->field($model, 'appointment_type_id')
    ->widget(Select2::classname(), [
        'data' => $model->appointmentData,
        'options' => ['placeholder' => 'Select a state ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);?>

    <?= $form->field($model, 'workers')->widget(Select2::classname(), [
            'data' => $model->workersData,
            'options' => ['placeholder' => 'เลือกผู้โดยสาร', 'multiple' => true],
    ]);?>

    <?= $form->field($model, 'appointment_start')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => 'วันเวลาเริ่ม'],
        'readonly' => true,
        'pluginOptions' => [
            'autoclose' => true,
            'minuteStep' => 30,
        ]
    ]);?>

    <?= $form->field($model, 'appointment_end')->widget(DateTimePicker::classname(), [
        'options' => ['placeholder' => 'วันเวลาจบ'],
        'readonly' => true,
        'pluginOptions' => [
            'autoclose' => true,
            'minuteStep' => 30,
        ]
    ]);?>

    <?= $form->field($model, 'description')->textarea(['rows' => 4]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php 
$this->registerJs("
    $(document).ready(function() {
        $('#appointment-description').summernote({
        height: 150,   //set editable area's height
        codemirror: { // codemirror options
            theme: 'monokai'
        },
        toolbar: [
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough']],
            ['fontsize', ['fontsize']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['view', ['fullscreen', 'codeview']],
        ],
        });
    });
");