<?php
use yii\grid\GridView;

$this->title = 'รายละเอียดงาน';
$this->params['breadcrumbs'][] = ['label' => 'รายละเอียดงาน'];
?>
<div class="my-appointment">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">รายละเอียดงาน</h3>
                </div>
                <div class="box-body">
                    <?= GridView::widget([
                        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            [
                                'attribute' => 'appointment.schoolName',
                                'label' => 'สถานที่',
                                // 'value' => function ($model) {
                                    // return $model->userName;
                                // }
                            ], 
                            [
                                'attribute' => 'appointment.appointmentType.name',
                                'label' => 'ประเภทงาน',
                            ], 
                            [
                                'attribute' => 'appointment.appointment_start',
                                'label' => 'เริ่มงาน',
                            ], 
                            [
                                'attribute' => 'appointment.appointment_end',
                                'label' => 'เลิกงาน',
                            ]
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>