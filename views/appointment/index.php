<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AppointmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'การนัดหมาย';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appointment-index">
    <p>
        <?= Html::a('เพิ่มการนัดหมาย', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'school_id',
                'value' => 'schoolName',
                'filter'=>$schoolName,
            ],
            [
                'attribute' => 'appointment_type_id',
                'value' => 'appointmentType.name',
                'filter' => $appointmentType
            ],
            'appointment_start',
            'appointment_end',
            //'created_at',
            //'updated_at',
            //'created_by',
            //'updated_by',

            [
                'class' => 'yii\grid\ActionColumn',
                // 'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
</div>