<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Appointment */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Appointments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="appointment-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="row">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'label' => 'school_id',
                    'value' => $model->schoolName,
                ],
                [
                    'label' => 'appointment_type_id',
                    'value' => $model->appointmentType->name,
                ],
                'appointment_start',
                'appointment_end',
                [
                    'label' => 'สร้างเมื่อ',
                    'value' => Yii::$app->datetimeManage->dtFormat(date('Y-m-d H:i:s', $model->created_at)),
                ],
                [
                    'label' => 'แก้ไขเมื่อ',
                    'value' => Yii::$app->datetimeManage->dtFormat(date('Y-m-d H:i:s', $model->updated_at)),
                ],
                [
                    'label' => 'สร้างโดย',
                    'value' => User::find()->select('nickname')->where(['id' => $model->created_by])->scalar()
                ],
                [
                    'label' => 'แก้ไขโดย',
                    'value' => User::find()->select('nickname')->where(['id' => $model->updated_by])->scalar()
                ],
                [
                    'label' => 'Worker',
                    'value' => function($model) {
                        $nameList = "";
                        foreach($model->appointmentDetails as $appointmentDetail) {
                            $nameList .= $appointmentDetail->user->nickname . ' ';
                        }
                        return $nameList;
                    }
                ]
            ],
        ]) ?>
    </div>
</div>