<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-primary">
            <div class="panel-body"><h4 class="m-t-0 m-b-30">ข้อมูลผู้ใช้งาน</h4>
                <div class="user-index">
                    <div class="text-right">
                        <?= Html::a('<i class="fa fa-user-md"></i>  สร้างผู้ใช้งาน', ['create'], ['class' => 'btn btn-success']) ?>
                    </div>
                    <?php Pjax::begin(); ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel'=>$searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            // 'id',
                            'full_name',
                            //'auth_key',
                            //'password_hash',
                            //'password_reset_token',
                            // 'email:email',
                            [
                                'attribute' => 'status',
                                'format' => 'html',
                                //'filter'=>$searchModel->itemStatus,
                                'value' => function ($model) {
                                    return $model->statusName == 'Active' ? '<span class="text-success">' . $model->statusName . '</span>' : $model->statusName;
                                }
                            ],
                            // 'created_at',
                            // 'updated_at',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'template' => '{update} {cancel}',
                                'buttons' => [
                                    'cancel' => function ($url, $model, $key) {
                                        return $model->id != 1 ? Html::a('<i class="glyphicon glyphicon-lock text-danger"></i>', ['cancel', 'id' => $model->id], [
                                            'class' => '',
                                            'data' => [
                                                'confirm' => 'ต้องการปิดใช้านใช้ หรือไม่ ?',
                                                'method' => 'post',
                                            ],
                                        ]) : null;
                                    },
                                ]
                            ],
                        ],
                    ]); ?>
                    <?php Pjax::end(); ?></div>
            </div>
        </div>
    </div>
</div>


