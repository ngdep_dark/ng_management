<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\attd\models\Shift;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
//$model->roles='Admin';
$datetime_end = date("Y-m-d", strtotime("-18 year"));;
?>

<div class="user-form">

    <div class="user-form">

        <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">ข้อมูลส่วนตัว</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'nickname')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model,'dob')->widget(DatePicker::className(),[
                                    'pluginOptions' => [
                                        'endDate' => $datetime_end,
                                        'autoclose'=>true,
                                        'format' => 'yyyy-mm-dd'
                                    ]
                                ]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">ข้อมูลใช้งาน</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
                            </div>
                            <div class="col-md-6">
                                <?= $form->field($model, 'status')->radioList($model->getItemStatus())->label('สถานะการใช้งาน')?>
                            </div>

                            <div class="col-md-6">
                                <?= $form->field($model, 'roles')->checkboxList($model->getAllRoles()) ?>
                            </div>

                        </div>
                        <div class="row">


                            <div class="col-md-6">
                                <?php
                                if($model->isNewRecord == 'create') $model->role_shift=1;
                                ?>
                                <!--
                                <= $form->field($model, 'role_shift')->radioList(ArrayHelper::map(Shift::find()->all(),'id','name')) ?>
                                -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Save') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>