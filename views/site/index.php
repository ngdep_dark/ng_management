<?php

/* @var $this yii\web\View */
$this->title = 'ปฎิทินการนัดหมาย';
$this->registerCss(".fc-day-grid-event > .fc-content { white-space: normal; }");

$colorDetail = '<div class="row">';
$eventSources = json_encode($eventSources);
foreach ($appointmentType as $type) {
    $colorDetail .= '
    <div class="col-xs-3">
    <div style="float: left; width: 22px; height: 22px; background: '.$type['color'].';"></div>
    <div style="display: inline; margin-left: 20px;">
    '.$type['name'].'
    </div>
    </div>
    ';
}

$appointmentType = json_encode($appointmentType);
$colorDetail .= '</div>';

?>

<div id="type" class="row"></div>

<div id="calendar"></div>

<?php
$this->registerJs(<<<JS
$(document).ready(function() {
    var appointmentType = $appointmentType;
    var {eventSources} = $eventSources;
    var appointmentTypeValue = [];
    var eventSourcesNotCheck = [];

    const handleCheckboxType = (color) => {
        appointmentTypeValue.map((item,index) => {
            if (item.color === color) {
                appointmentTypeValue[index].check = !appointmentTypeValue[index].check
            }
        })
        $('#calendar').fullCalendar('removeEvents');
        eventSources.map((event) => {
            const { check } = appointmentTypeValue.find((type) => type.color === event.color)

            if (check) {
                $('#calendar').fullCalendar('addEventSource', event);
            }
        })
        $('#calendar').fullCalendar('rerenderEvents' );
    }
    
    appointmentType.map((type) => {
        appointmentTypeValue.push({
            'color': type.color,
            'check': true,
        })
        $("#type").append("<div class='col-xs-6 col-md-3 col-xl-2'><input type='checkbox' id='"+type.color+"' value='"+type.color+"' name='type' checked ><label for='"+type.color+"'><div style='margin-left: 10px; margin-right: 10px; float: left; width: 22px; height: 22px; background: "+type.color+";'></div>"+type.name+"</label></div>")
    })


    $("input[name='type']").change((e) => {
        const checkboxValue = e.target.value
        handleCheckboxType(checkboxValue)
    });
    
    var eventMouseover = function (data, event, view) {
        tooltip = '<div class="tooltiptopicevent" style="width:auto;height:auto;background:#fcfdff;position:absolute;z-index:10001;padding:10px 10px 10px 10px ;  line-height: 200%;">' + data.description + '</div>';

        $("body").append(tooltip);
        $(this).mouseover(function (e) {
            $(this).css('z-index', 10000);
            $('.tooltiptopicevent').fadeIn('500');
            $('.tooltiptopicevent').fadeTo('10', 1.9);
        }).mousemove(function (e) {
            $('.tooltiptopicevent').css('top', e.pageY + 10);
            $('.tooltiptopicevent').css('left', e.pageX + 20);
        });
    }
    
    var eventMouseout = function (data, event, view) {
        $(this).css('z-index', 8);
        $('.tooltiptopicevent').remove();
    }
    
    var fullCalendar = {
        locale: 'th',
        eventSources,
        eventMouseover,
        eventMouseout,
    }

    $('#calendar').fullCalendar(fullCalendar);


    // setInterval(async () => {
    //     var data = await fetch('/appointment/get-appointment')
    //     var {eventSources} = await data.json();
    //     $('#calendar').fullCalendar('removeEvents');
    //     eventSources.map(item => {
    //         $('#calendar').fullCalendar('addEventSource', item);
    //     })
    //     $('#calendar').fullCalendar('rerenderEvents' );
    // }, 1000 * 60 * 10);
});

JS
);
?>