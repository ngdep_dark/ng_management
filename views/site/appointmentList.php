<?php

/* @var $this yii\web\View */
$this->title = 'รายการงานในอาทิตย์นี้';
$this->registerCss("td.fc-list-item-title.fc-widget-content { font-size: 22px; }");
$eventSources = json_encode($eventSources);
$colorDetail = '<div style="margin-bottom: 20px;">';

foreach ($appointmentType as $type) {
    $colorDetail .= '
        <div class="col-xs-6 col-md-3 col-xl-2">
            <div style="float: left; width: 22px; height: 22px; background: '.$type['color'].';"></div>
            <div style="display: inline; margin-left: 20px;">
            '.$type['name'].'
            </div>
        </div>
    ';
}

$colorDetail .= '</div>'
?>
<?=$colorDetail?>
<div id="calendar"></div>

<?php
$this->registerJs(<<<JS
$(document).ready(async function() {
    // var data = await fetch('/appointment/get-appointment?list=true')
    // var {eventSources} = await data.json();
    var { eventSources } = $eventSources;

    $('#calendar').fullCalendar({
        defaultView: 'listWeek',
        locale: 'th',
        eventSources
    });

    // setInterval(async () => {
    //     var data = await fetch('/appointment/get-appointment')
    //     var {eventSources} = await data.json();
    //     $('#calendar').fullCalendar('removeEvents');
    //     eventSources.map(item => {
    //         $('#calendar').fullCalendar('addEventSource', item);
    //     })
    //     $('#calendar').fullCalendar('rerenderEvents' );
    // },1000 * 60 * 10);
});

JS
);
?>