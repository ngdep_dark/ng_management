<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\AuthAssignment;

$this->title = 'กลุ่มผู้ใช้งาน';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="panel panel-primary">
        <div class="panel-body"><h4 class="m-t-0 m-b-30">ข้อมูลผู้ใช้งาน</h4>
            <div class="row">
                <div class="col-md-12">
                    <div class="auth-index">
                        <?= Html::a('สร้างกลุ่มใช้งาน', ['create'], ['class' => 'btn btn-primary']) ?>
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                'description:ntext',
                                [
                                    'label' => 'ประเภทกลุ่ม',
                                    'value' => function ($model) {
                                        return $model->name != 'Customer' && $model->name != 'Admin' ? 'กำหนดเอง' : 'ระบบกำหนดให้';
                                    }
                                ],
                                ['class' => 'yii\grid\ActionColumn',
                                    'contentOptions' => [
                                        'noWrap' => true
                                    ],
                                    'template' => '  {update} {view} {delete}',
                                    'buttons' => [
                                        'view' => function ($url, $model, $key) {
                                            return Yii::$app->user->can('Admin') ? Html::a('<i class="glyphicon glyphicon-cog text-danger" ></i>', $url) : null;
                                        },
                                        'update' => function ($url, $model, $key) {
                                            return Yii::$app->user->can('Admin') ? Html::a('<i class="glyphicon glyphicon-pencil text-info"></i>', $url) : null;
                                        },
                                        'delete' => function ($url, $model, $key) {
                                            $Asm = AuthAssignment::findOne(['item_name' => $model->name]);
                                            if ($Asm['item_name'] === $model->name) {
                                            } else {
                                                return Yii::$app->user->can('Admin') ? Html::a('<i class="glyphicon glyphicon-trash text-error"></i>', ['delete', 'id' => $model->name], [
                                                    'class' => '',
                                                    'data' => [
                                                        'confirm' => 'Are you sure you want to delete this item?',
                                                        'method' => 'post',
                                                    ],
                                                ]) : null;
                                            }
                                        }
                                    ]
                                ]
                            ]
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



