<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveFor */
?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-primary">
            <div class="panel-body">
                <h4 class="m-t-0 m-b-30">สร้างกลุ่มผู้ใช้งาน</h4>
                <div class="auth-form">
                    <?php $form = ActiveForm::begin(); ?>
                    <?php if($model->type == 2):?>
                    <?php echo $form->field($model, 'name')->textInput() ?>
                    <?php endif;?>
                    <?= $form->field($model, 'description')->textInput() ?>
                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? 'บันทึก' : 'บันทึกแก้ไข', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        <?= Html::a('ยกเลิก', ['index'], ['class' => 'btn btn-danger']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>

