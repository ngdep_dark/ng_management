<?php
use yii\helpers\Html;
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">สิทธิในการใช้งานของกลุ่ม : <?=$model->description?></h3>
    </div>
    <div class="panel-body">
<?php if(!empty($model)): ?>
            <?php $form = \yii\widgets\ActiveForm::begin(); ?>
            <h4> <code>เลือกสิทธ์ใช้งาน</code></h4>
            <?= $form->field($model, 'permissions')->checkboxList($model->getAllPermissions())->label(false) ?>
            <div class="form-group text-right">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'บันทึกข้อมูล', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-success']) ?>
                <?= Html::a('cancel', ['index'], ['class' => 'btn btn-danger']) ?>
            </div>

            <?php \yii\widgets\ActiveForm::end(); ?>


<?php endif;?>

    </div>
</div>