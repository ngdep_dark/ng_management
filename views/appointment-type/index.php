<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Appointment;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AppointmentTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ประเภทการนัดหมาย';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="appointment-type-index">

    <p>
        <?= Html::a('เพิ่มประเภทการนัดหมาย', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'attribute' => 'color',
                'format' => 'html',
                'value' => function($model) {
                    return '<div style="background-color: '.$model->color.'; width: 25px; height: 25px;"></div>';
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        $query = Appointment::find()
                            ->select('appointment_type_id')
                            ->from('appointment')
                            ->where(['appointment_type_id' => $model->id])
                            ->scalar();
                        return $model->id == $query ?
                            null :
                            Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete', 'id' => $model->id], [
                                'data' => [
                                    'confirm' => Yii::$app->messageManage->confirm,
                                    'method' => 'post',
                                ],
                            ]);
                    },
                ]
            ],
        ],
        
    ]); ?>
</div>