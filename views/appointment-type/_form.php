<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use kartik\widgets\ColorInput

/* @var $this yii\web\View */
/* @var $model app\models\AppointmentType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="appointment-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'color')->widget(ColorInput::classname(), [
                'size' => 'lg',
                'options' => ['placeholder' => 'Select color ...'],
            ]);?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>