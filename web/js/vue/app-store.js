Vue.component("v-select", VueSelect.VueSelect);
var cal_stock = new Vue({
    el: "#cal-stock",
    data: {
        amount: null,
        percent: null,
        total: 0,
        Order_id: '',
        rawProduct: [],
        DataProduct: [],
        inputProduct: [{
            product: null,
            amount: null,
            balance: null,
            takeAmount: null,
            takeOrder: null
        }]
    },
    filters: {
        formatNumber(number) {
            console.log(number)
            if (number != undefined) {
                return new Intl.NumberFormat('en-IN', {
                    maximumSignificantDigits: 10
                }).format(number)
            }
        }
    },
    watch: {},
    computed: {
        SelectProduct() {
            let that = this;
            return that.DataProduct.filter(value => {
                if (value.status) {
                    return value;
                }
            });
        },
        SumAmount() {
            let amount = parseInt(this.amount);
            let percent = parseInt((this.percent * this.amount) / 100);
            this.total = amount + percent;
            if (isNaN(this.total)) {
                return 0;
            }
            return this.total;
        }
    },
    created() {
        this.getProduct();

    },
    methods: {

        Export() {
            let datas = {
                amount: this.amount,
                percent: this.percent,
                sumALl: this.SumAmount,
                dataCal: this.inputProduct
            }
            console.log('log', datas.length)
            axios.post("/store/default/export", {
                data: datas
            }).then(function (res) {
                if (res.status == 200) {
                    console.log(res)
                    window.location.href = "/store/report/report?id=" + res.data
                }
            });


        },
        TakeOut(balance, use) {
            var total = parseInt(this.SumAmount);
            let sumCal = balance - total;
            if (sumCal < 0 && use != undefined) {
                var sumAll = sumCal * -1 / use;
                return sumAll.toFixed()
            }
        },
        SumBalance(amount, SumPre) {
            let total = amount - SumPre;
            if (total == NaN) {
                return ''
            } else {
                return total
            }
        },
        Product() {
            console.log("in");
            let that = this;
            that.inputProduct.filter(item => {
                if (item.product != null) {
                    item.id = item.product.id;
                    item.use = item.product.use;
                    item.amount = item.product.balance;
                    item.balance = item.product.use * item.product.balance;
                    item.total_use = 0;
                    item.take_is_out = 0;
                }
            });
        },
        Remove(item) {
            this.inputProduct.splice(this.inputProduct.indexOf(item), 1)
            console.log(item)
        },
        getProduct() {
            let that = this;
            axios.get("/store/app/get-product").then(function (res) {
                if (res.status == 200) {
                    that.DataProduct = res.data;
                    that.rawProduct = res.data;
                    // console.log(res.data)
                }
            });
        },
        addForm() {
            var data = {
                product: null,
                amount: null
            };
            this.inputProduct.push(data);
        }
    }
});

var app = new Vue({
    el: "#app-take",
    data: {
        checked_card: 5,
        options: [],
        dataTake: [],
        select: [],
        selectCheck: [],
        allSelected: false,
        isLoading: false,
        searchOrder: "",
        dataOrder: [],
        SchoolName: "",
        Order_id: "",
        SelectSchool: null,
        CradGoods: false,
        amount: null,
        cardBad: false,
        cardBadselect: null,
        selectManual: false,
        cardReplace: false,
        searchReplace: "",
        causes: "",
        replace_id : "",
        replace : [],
        replace_details :[]
    },

    created() {
        this.getDataTake();
    },
    computed: {
        checkOrder() {
            var that = this;
            var select = that.select;
            if (that.select.length != 0) {
                for (var d in this.dataTake) {
                    console.log(this.dataTake[d])
                }
                for (var i in select) {


                }
            }
            // for(var i in select ){
            //     console.log(select[i])
            // }
        },
        //check พอไม่
        CheckSelect() {
            var data = [];
            let vm = this;
            let dataTake = vm.dataTake
            for (var d in dataTake) {
                data.push(this.dataTake[d])
            }
            if (data.length > 0) {
                console.log('เลือก =' + this.select.length)
                var loopData = data.filter(val => {
                    console.log(val.id)
                    this.select.map(item => {
                        val.id != item
                    })
                });
                // console.log(loopData)
                if (!this.selectManual) {
                    if (this.select.length != data.length) {
                        return true
                    } else {
                        return false
                    }
                } else {
                    return false
                }

                //console.log(data.length)
            }

        },
        filterTake() {
            var that = this;
            var select = that.select;
            var data = [];
            if (that.select.length != 0) {
                // for (i in select) {
                //   var object = that.dataTake.filter(item => item.id === select[i]).map(item => data.push(item));
                // }
                //return data
            }
            return this.dataTake;
        },
        btnGood() {
            if (
                this.SelectSchool != null &&
                this.amount != null &&
                this.amount != undefined &&
                this.amount != " "
            ) {
                return true;
            } else {
                return false;
            }
        },

    },
    methods: {
        cancelTake(item) {
            console.log(item);
            axios.get("store/app/cancel-take?id=" + item.item_id).then(function (res) {
                if (res.data.status == "ok") {
                    // toastr.info("คืนรายการเบิกสำเร็จ !");
                    // setTimeout(() => {
                    //     location.reload();
                    // }, 1000);
                }
            });
        },
        selectId() {
            console.log('select');
            this.selectManual = true;
            // toastr.info("ปิดการคำนวน Auto แล้ว !");
        },
        checkInputOrder(data) {
            var that = this;
            let cardCount = data.length
            if (cardCount > 0) {
                var dataTake = this.dataTake;
                //var inputAmount = cardCount;
                var select = that.select;
                var selectNew = [];
                for (var i in dataTake) {
                    var amount = dataTake[i].amount * dataTake[i].use;
                    var take_amount = cardCount + parseInt(dataTake[i].take_amount);

                    console.log('amount s= ' + take_amount)
                    var ids = parseInt(dataTake[i].id);
                    if (amount < take_amount) {
                        that.select = that.select.filter(item => item != ids);
                    } else {
                        selectNew.push(ids);
                        that.select = selectNew;
                    }
                }
            }
        },
        //check card good && bad
        selectCheckAmount() {
            // check take
            var that = this;
            var dataTake = this.dataTake;
            var inputAmount = parseInt(this.amount);
            var select = that.select;
            var selectNew = [];
            for (var i in dataTake) {
                if(dataTake[i].lock_cal_products ===1){
                    var amount = dataTake[i].amount;
                }else {
                    var amount = dataTake[i].amount * dataTake[i].use;
                }
                var take_amount = inputAmount + parseInt(dataTake[i].take_amount);
                console.log('take_amount = '+ take_amount)
                console.log('amount = ' + take_amount)
                var ids = parseInt(dataTake[i].id);
                if (amount < take_amount) {
                    that.select = that.select.filter(item => item != ids);
                } else {
                    selectNew.push(ids);
                    that.select = selectNew;
                }
            }
        },
        removeTake(data) {
            console.log(data)
            axios.get("store/app/remove-take?id=" + data.item_id).then(function (res) {
                if (res.data.status == "ok") {
                    toastr.info("ยกเลิกรายการเบิกสำเร็จ !");
                    setTimeout(() => {
                        location.reload();
                    }, 1000);
                }
            });
        },
        saveCardBad() {
            var that = this;
            if (that.amount != 0 && that.amount != " ") {
                if (that.select.length > 0) {
                    axios.post("/store/app/save-bad", {
                            data: {
                                checked_card: that.checked_card,
                                school: that.SelectSchool,
                                amount: that.amount,
                                select: that.select,
                                causes: that.causes,
                                cardBadselect: that.cardBadselect
                            }
                        }).then(function (res) {
                            if (res.data.status == "ok") {
                                if(that.cardBadselect ==='0'){
                                    toastr.warning("บันทึกบัตรTest " + that.amount + " ใบ", "บันทึกบัตรTest");
                                }
                                toastr.success("จำนวน " + that.amount + " ใบ", "บันทึกสำเร็จ");
                                console.log(that.cardBadselect);
                                setTimeout(() => {
                                    location.reload();
                                }, 1000);
                            }
                        });
                } else {
                    toastr.warning("เพื่อบันทึกการใช้งาน...", "กรุณาเลือกรายการเบิก");
                }
            } else {
                toastr.warning("กรุณาใส่จำนวน..");
            }
        },
        saveCardGood() {
            var that = this;
            if (that.amount != 0 && that.amount != " ") {
                if (that.select.length > 0) {
                    if (this.btnGood) {
                        console.log("save");
                        axios
                            .post("/store/app/save-good", {
                                data: {
                                    checked_card: that.checked_card,
                                    school: that.SelectSchool,
                                    amount: that.amount,
                                    select: that.select
                                }
                            })
                            .then(function (res) {
                                console.log(res)
                                if (res.data.status == "ok") {
                                    toastr.success("บันทึกสำเร็จ");
                                    setTimeout(() => {
                                        location.reload();
                                    }, 1500);
                                }
                            });
                    }
                } else {
                    toastr.warning("เพื่อบันทึกการใช้งาน...", "กรุณาเลือกรายการเบิก");
                }
            } else {
                toastr.warning("กรุณาใส่จำนวน..");
            }
        },
        getOrder() {
            let that = this;
            if (that.select.length > 0) {
                if (that.searchOrder.length >= 3) {
                    axios
                        .get(
                            "http://139.59.240.55:4000/auto-print/get-order?id=" +
                            that.searchOrder
                        ).then(function (res) {
                            if (res.status == 200) {
                                that.Order_id = res.data["order_id"];
                                that.SchoolName = res.data["item"];
                                that.dataOrder = res.data["item_details"];
                                that.checkInputOrder(res.data["item_details"])
                            }
                        });
                }
            } else {
                toastr.warning("เพื่อบันทึกการใช้งาน...", "กรุณาเลือกรายการเบิก");
            }
        },
        getDataReplace() {
            let that = this;
            console.log(that.searchReplace)
            if (that.select.length > 0) {
                if (that.searchReplace.length >= 3) {
                    axios
                        .get(
                            "http://139.59.240.55:4000/auto-print/get-replace?id=" +
                            that.searchReplace
                        ).then(function (res) {
                            if (res.status == 200) {
                                console.log()
                                if(res.data.status =="ok"){
                                    let item =res.data["item"];
                                    that.replace_id = res.data["replace_id"];
                                    that.replace = res.data["item"];
                                    that.SchoolName = item.school;
                                    that.replace_details = res.data["item_details"];
                                    that.checkInputOrder(res.data["item_details"])
                                }
                            }
                        });
                }
            } else {
                toastr.warning("เพื่อบันทึกการใช้งาน...", "กรุณาเลือกรายการเบิก");
            }
        },
        //saveRe
        saveRepalce() {
            let that = this;
            axios
                .post("/store/app/save-replace", {
                    select: that.select,
                    checked_card: that.checked_card,
                    order_id: that.replace_id,
                    SchoolName: that.SchoolName,
                    dataOrder: that.replace_details
                })
                .then(function (res) {
                    if (res.data.status == "ok") {
                        toastr.success(
                            "จำนวน " + that.replace_details.length + "ใบ",
                            "บันทึกสำเร็จ"
                        );
                        setTimeout(() => {
                            location.reload();
                        }, 1000);
                    }
                });
        },
        saveOrder() {
            let that = this;
            axios
                .post("/store/app/save-order", {
                    select: that.select,
                    checked_card: that.checked_card,
                    order_id: that.Order_id,
                    SchoolName: that.SchoolName,
                    dataOrder: that.dataOrder
                })
                .then(function (res) {
                    if (res.data.status == "ok") {
                        toastr.success(
                            "จำนวน " + that.dataOrder.length + "ใบ",
                            "บันทึกสำเร็จ"
                        );
                        setTimeout(() => {
                            location.reload();
                        }, 1000);
                    }
                });
        },
        selectAll() {
            var that = this;
            that.CradGoods = false;
            that.cardReplace = false
            that.cardBad = false;
            this.allSelected = true;
            if (this.allSelected) {
                if (that.select.length == 0) {
                    for (var i in that.dataTake) {
                        var id = that.dataTake[i].id;
                        that.select.push(id);
                    }
                }
            }
        },
        //repalce
        CardReplacesEvent() {
            var that = this;
            that.allSelected = false;
            that.cardReplace = true;
            that.cardBad = false;
            // that.select = [];
            that.SchoolName = [];
            that.dataOrder = [];
            if (that.select.length == 0) {
                for (var i in that.dataTake) {
                    var id = that.dataTake[i].id;
                    that.select.push(id);
                }
            }
        },
        cardGood() {

            var that = this;
            that.allSelected = false;
            that.CradGoods = true;
            that.cardBad = false;
            that.cardReplace = false
            // that.select = [];
            that.SchoolName = [];
            that.dataOrder = [];
            if (that.select.length == 0) {
                for (var i in that.dataTake) {
                    var id = that.dataTake[i].id;
                    that.select.push(id);
                }
            }
        },
        cardBadBtn() {
            var that = this;
            that.allSelected = false;
            that.CradGoods = false;
            that.cardReplace = false
            that.cardBad = true;
            if (that.select.length == 0) {
                for (var i in that.dataTake) {
                    var id = that.dataTake[i].id;
                    that.select.push(id);
                }
            }
        },
        checkSelect(id) {
            var classData = " ";
            var data = this.select;
            for (var key in data) {
                if (data[key] == id) return (classData = "bg-green");
            }
            return classData;
        },

        getDataTake() {
            var that = this;
            axios.get("/store/app/school").then(function (res) {
                that.options = res.data;
            });
            console.log('get')
            axios.get("/store/default/get-take").then(function (res) {
                // handle success
                //if (res.data != undefined && res.data.length > 0) {
                setTimeout(() => {
                    that.isLoading = true;
                    that.dataTake = res.data
                }, 100);
                //}
                //console.log(res.data[0]);
            });
        }
    }
});