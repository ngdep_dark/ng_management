var app = angular.module("appApp", []);
app.controller("appCtrl", function ($scope, $http) {
    $scope.product_id = null;
    $scope.amount = null;
    $scope.add = true;
    $scope.take = false;
    $scope.dataCheck = null;
    $scope.Checkbtn = false;
    $scope.BalanceTake = false;
    $scope.checkItemUse=false;
    //get data
    $http.get("/store/items/data-amount")
        .then(function (response) {
            $scope.dataCheck = response.data
        });

    $scope.Check = function (key) {
        if ($scope.dataCheck != null && $scope.dataCheck != undefined) {
            var dataSend = $scope.dataCheck.filter(items => items.id == key)
            return dataSend
        }
    }
    ;
    $scope.CheckTake = function (model) {
        if (model < $scope.amount) {
            return $scope.Checkbtn = true;
        } else {
            return $scope.Checkbtn = false;
        }
    }
    $scope.addProduct = function () {
        $scope.add = true;
        $scope.take = false;
    }
    $scope.takeProduct = function () {
        $scope.add = false;
        $scope.take = true;

    }
    $scope.balanceProduct = function () {
        var product_id = $scope.product_id;
        if(product_id !=null && product_id !=undefined){
           $http.get("/store/items/balance?id="+product_id)
               .then(function (res) {
                   if(res.data == 0){
                       $scope.BalanceTake = true;
                   } else {
                    $scope.BalanceTake = false;
                   }
            });
            $http.get("/store/app/item-use?id="+product_id)
                .then(function (res) {
                    console.log(res.data)
                      $scope.checkItemUse=res.data;
                });
       }
       //เช็คการใช้งาน
    }
});