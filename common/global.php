<?php

use yii\helpers\VarDumper;

function dump($input)
{
    VarDumper::dump($input, 10, true);
    exit;
}

function datetimeManage()
{
    return Yii::$app->datetimeManage;
}