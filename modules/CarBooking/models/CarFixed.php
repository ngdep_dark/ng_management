<?php

namespace app\modules\CarBooking\models;

use app\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "car_fixed".
 *
 * @property int $id
 * @property int $car_id
 * @property int $car_fixed_type_id
 * @property int $garage_id
 * @property int $price
 * @property int $sender_fixed_by
 * @property string $fix_date
 * @property string $fixed_date
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 * @property int $last_check_miles
 * @property Car $car
 * @property CarFixedType $carFixedType
 * @property Garage $garage
 * @property User $senderFixedBy
 */
class CarFixed extends \yii\db\ActiveRecord
{
    public $detail;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_fixed';
    }

    public function behaviors(){
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['car_id', 'car_fixed_type_id', 'garage_id', 'sender_fixed_by'], 'required'],
            [['car_id', 'car_fixed_type_id', 'garage_id', 'sender_fixed_by', 'created_at', 'created_by', 'updated_at', 'updated_by', 'detail'], 'default', 'value' => null],
            [['car_id', 'sender_fixed_by', 'created_at', 'created_by', 'updated_at', 'updated_by', 'last_check_miles'], 'integer'],
            [['fix_date', 'fixed_date'], 'safe'],
            [['car_id'], 'exist', 'skipOnError' => true, 'targetClass' => Car::className(), 'targetAttribute' => ['car_id' => 'id']],
            [['car_fixed_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarFixedType::className(), 'targetAttribute' => ['car_fixed_type_id' => 'id']],
            [['garage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Garage::className(), 'targetAttribute' => ['garage_id' => 'id']],
            [['sender_fixed_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['sender_fixed_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'car_id' => Yii::$app->messageManage->car_license,
            'car_fixed_type_id' => Yii::$app->messageManage->car_fixed_type,
            'garage_id' => Yii::$app->messageManage->garage,
//            'price' => Yii::$app->messageManage->cf_price,
            'sender_fixed_by' => Yii::$app->messageManage->cf_sender_fixed_by,
            'fix_date' => Yii::$app->messageManage->cf_fix_date,
            'fixed_date' => Yii::$app->messageManage->cf_fixed_date,
            'created_at' => Yii::$app->messageManage->create_at,
            'created_by' => Yii::$app->messageManage->create_by,
            'updated_at' => Yii::$app->messageManage->update_at,
            'updated_by' => Yii::$app->messageManage->update_by,
            'last_check_miles' => Yii::$app->messageManage->car_miles_last_check,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::className(), ['id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarFixedType()
    {
        return $this->hasOne(CarFixedType::className(), ['id' => 'car_fixed_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGarage()
    {
        return $this->hasOne(Garage::className(), ['id' => 'garage_id']);
    }

    public function getCarFixedDetail()
    {
        return $this->hasMany(CarFixedDetail::className(), ['car_fixed_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSenderFixedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'sender_fixed_by']);
    }

    public function getCreateBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    // start query for select2
    public function getCarFixedTypeData()
    {
        return $query = ArrayHelper::map(CarFixedType::find(['id','name'])->orderBy('id')->all(), 'id', 'name');
    }

    public function getGarageData()
    {
        return $query = ArrayHelper::map(Garage::find(['id','name'])->orderBy('id')->all(), 'id', 'name');
    }

    public function getSenderFixedByData()
    {
        return $query = ArrayHelper::map(User::find(['id','full_name'])->orderBy('id')->all(), 'id', 'full_name');
    }
}
