<?php

namespace app\modules\CarBooking\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\modules\CarBooking\models\Car;

/**
 * This is the model class for table "car_booking".
 *
 * @property int $id
 * @property string $datetime_start
 * @property string $datetime_end
 * @property int $miles_start
 * @property int $miles_end
 * @property int $status
 * @property int $car_id
 * @property int $car_booking_location_id
 * @property int $car_booking_category_id
 * @property string $detail
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 *
 * @property Car $car
 * @property CarBookingCategory $carBookingCategory
 * @property CarBookingLocation $carBookingLocation
 * @property CarBookingPassenger[] $carBookingPassengers
 */
class CarBooking extends \yii\db\ActiveRecord
{
    public $passengers;
    public $datetime_range;

    public $datetime;

    public $file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_booking';
    }

    public function behaviors(){
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['datetime_start', 'datetime_end', 'passengers'], 'required'],
//            [['datetime_start'],'validateDatetimeStart', 'on' => 'create'],
            [['datetime_end'],'validateDatetimeEnd', 'params' => $this->datetime_start, 'on' => ['create','return']],
            [['datetime_range'], 'validateDatetimeOverlaps', 'on' => 'create'],
            [['miles_start', 'miles_end'], 'validateAdminEditBooking', 'on' => 'edit'],
            [['car_id'], 'validateUseCar', 'on' => 'create'],
            [['miles_start', 'miles_end'], 'validateReturnCar', 'on' => 'return'],
            [['miles_start', 'miles_end', 'status', 'car_id', 'car_booking_location_id', 'car_booking_category_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'default', 'value' => null],
            [['miles_start', 'status', 'car_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['miles_start', 'miles_end'], 'required', 'on' => ['update', 'return']],
            // [['miles_end'], 'integer', 'min' => $this->miles_start, 'on' => ['create', 'update', 'return']],
            [['car_id', 'car_booking_location_id', 'car_booking_category_id'], 'required'],
            [['detail'], 'string', 'max' => 255],
            [['car_id'], 'exist', 'skipOnError' => true, 'targetClass' => Car::className(), 'targetAttribute' => ['car_id' => 'id']],
            [['car_booking_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarBookingCategory::className(), 'targetAttribute' => ['car_booking_category_id' => 'id']],
            [['car_booking_location_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarBookingLocation::className(), 'targetAttribute' => ['car_booking_location_id' => 'id']],
            [['file'], 'file', 'skipOnEmpty' => false ,'on' => ['return']],
        ];
    }

    // validate datetime start booking car
    public function validateDatetimeStart($attribute)
    {
        $datetimeStart = strtotime($this->$attribute);
        $datetimeCurrent = time();

        $currentTimeMessage = Yii::$app->formatter->asDateTime(new \DateTime('NOW'), 'php:d-m-Y H:i');
        if ($this->status == 1) {
            if ($datetimeStart < $datetimeCurrent) {
                $this->addError($attribute,'ช่วงเวลาที่จะจองรถต้องมากกว่าเวลาปัจจุบัน' . ' ' .$currentTimeMessage);
            }
        }
    }
    // validate datetime end booking car
    public function validateDatetimeEnd($attribute)
    {
        $datetimeStart = strtotime($this->datetime_start);
        $datetimeEnd = strtotime($this->$attribute);
        $datetimeCurrent = time();

        $currentTimeMessage = Yii::$app->formatter->asDateTime(new \DateTime('NOW'), 'php:Y-m-d H:i');
        $startRangeTimeMessage = Yii::$app->formatter->asDateTime($datetimeStart, 'php:d-m-Y H:i');

        if ($datetimeEnd <= $datetimeStart) {
            $this->addError($attribute, 'ช่วงเวลาใช้คืนรถต้องมากกว่าเวลายืม ' . ' ' .$startRangeTimeMessage);
        }
    }

    public function validateDatetimeOverlaps()
    {
        $query = CarBooking::find()
            ->where(['(:datetime_start, :datetime_end) OVERLAPS (datetime_start, datetime_end)' => true])
            ->andWhere(['car_id' => $this->car_id])
            ->andWhere(['status' => 2])
            ->addParams([
                ':datetime_start' => $this->datetime_start,
                ':datetime_end' => $this->datetime_end
            ])
            ->orderBy('id')
            ->all();

        if (!empty($query) && !is_null($query) && $this->status == 2) {
            $this->addError('datetime_range','*** รถคันนี้ไม่ว่างในช่วงเวลาทีท่านเลือก กรุณาเลือกช่วงเวลาอื่น ***');
            return true;
        }
    }

    public function validateUseCar()
    {
        $query = CarBooking::find()
            ->where(['miles_end' => null])
            ->andWhere(['car_id' => $this->car_id])
            ->andWhere(['status' => 2])
            ->orderBy('id')
            ->all();
        
        if (!empty($query) && !is_null($query) && $this->status == 2) {
            $this->addError('car_id','*** รถคันนี้มีคนเอาไปใช้อยู่ กรุณาคืนรถก่อน ***');
            return true;
        }
    }

    public function validateReturnCar()
    {
        $query = Car::findOne($this->car_id);
        $miles_start = intval($this->miles_start);
        $miles_end = intval($this->miles_end);

        if($miles_start < $query->miles) {
            $this->addError('miles_start','*** เลขไมค์คืนรถต้องมากกว่า เลขไมค์รถปัจจุบัน ' . ' ' . $query->miles .' ' . '  ***');
            return true;
        }
        
        if($miles_end < $query->miles && $miles_end < $miles_start) {
            $this->addError('miles_end','*** เลขไมค์คืนรถต้องมากกว่า เลขไมค์รถปัจจุบัน ' . ' ' . $query->miles .' ' . ' และต้องมากกว่าเลขไมค์ก่อนใช้  ***');
            return true;
        }
    }

    public function validateAdminEditBooking()
    {
        $miles_start = intval($this->miles_start);
        $miles_end = intval($this->miles_end);

        if ($miles_start > $miles_end) {
            $this->addError('miles_start','*** เลขไมค์ก่อนใช้ต้องน้อยกว่าเลขไมค์คืนรถ  ***');
            $this->addError('miles_end','*** เลขไมค์คืนรถต้องมากกว่าเลขไมค์ก่อนใช้  ***');
            return true;
        }
    }

    public function uploadImage()
    {
        if ($this->validate()) {
            $this->file->saveAs('uploads/' . $this->file->baseName . '.' . $this->file->extension);
            return true;
        } else {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'datetime_start' => Yii::$app->messageManage->cb_datetime_start,
            'datetime_end' => Yii::$app->messageManage->cb_datetime_end,
            'miles_start' => Yii::$app->messageManage->cb_miles_start,
            'miles_end' => Yii::$app->messageManage->cb_miles_end,
            'status' => Yii::$app->messageManage->cb_status,
            'car_id' => Yii::$app->messageManage->car_license,
            'car_booking_location_id' => Yii::$app->messageManage->cb_location_name,
            'car_booking_category_id' => Yii::$app->messageManage->cb_category_name,
            'passengers' => Yii::$app->messageManage->cb_passengers,
//            'driver' => 'คนขับรถ',
            'detail' => Yii::$app->messageManage->detail,
            'created_at' => Yii::$app->messageManage->create_at,
            'created_by' => Yii::$app->messageManage->create_by,
            'updated_at' => Yii::$app->messageManage->update_at,
            'updated_by' => Yii::$app->messageManage->update_at,
            'datetime' => 'ช่วงเวลา'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCar()
    {
        return $this->hasOne(Car::className(), ['id' => 'car_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarBookingCategory()
    {
        return $this->hasOne(CarBookingCategory::className(), ['id' => 'car_booking_category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarBookingLocation()
    {
        return $this->hasOne(CarBookingLocation::className(), ['id' => 'car_booking_location_id']);
    }

    public function getCreateBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuerypassengers
     */
    public function getCarBookingPassengers()
    {
        return $this->hasMany(CarBookingPassenger::className(), ['car_booking_id' => 'id']);
    }

    public function getTrafficTickets()
    {
        return $this->hasMany(TrafficTicket::className(), ['car_booking_id' => 'id']);
    }

    // return passenger name only
    public function getPassengersName($passengers = null)
    {
        if (is_null($passengers) or empty($passengers)) {
            $passengersArray = $this->getCarBookingPassengers()->asArray()->all();
            $passengers = ArrayHelper::getColumn($passengersArray, 'user_id');
        }

        $passengersName = null;
        if(isset($passengers) && !is_null($passengers)) {
            foreach ($passengers as $passenger) {

                $passengerModel = new CarBookingPassenger();
                $passengerModel->user_id = $passenger;

                $passengersName .= ', ' . $passengerModel->user->full_name;
            }
        }
        return $passengersName = substr($passengersName, -abs(strlen($passengersName) -2));
    }
    // use in view page because that model save attribute passengers at create page.
    // i get attribute passengers
    // use this for convert key = id passenger to user_id for send to getPassengerName
    public function setPassengersName()
    {
//        var_dump($this->passengers); exit();
        $getPassengersValue = array_values($this->passengers);
        return $this->getPassengersName($getPassengersValue);
    }
    
    public function getStatusName()
    {
        return ArrayHelper::getValue($this->getStatusData(),$this->status);
    }

    // start query for select2
    public function getCarData()
    {
        return $query = ArrayHelper::map(Car::find(['id','license_id'])->orderBy('id')->all(), 'id', 'license_id');
    }

    public function getLocationData()
    {
        return $query = ArrayHelper::map(CarBookingLocation::find(['id','name'])->orderBy('id')->all(), 'id', 'name');
    }

    public function getCategoryData()
    {
        return $query = ArrayHelper::map(CarBookingCategory::find(['id','name'])->orderBy('id')->all(), 'id', 'name');
    }

    public function getPassengersData()
    {
        return $query = ArrayHelper::map(User::find(['id','full_name'])->orderBy('id')->all(), 'id', 'full_name');
    }

    public function getStatusData()
    {
        return [
//            3 => 'ไม่อนุมัติ',
//            1 => 'รอการอนุมัติ',
            2 => 'อนุมัติ',
            4 => 'ยกเลิกรายการ',
        ];
    }
    // end query for select2

    // query for select2 multiple value
    public function getPassengersValue(){
        return ArrayHelper::map($this->carBookingPassengers,'id','user_id');
    }

    // for query need result return
    public function compareOverlabsBooking($datetime_start,$datetime_end) {

        $model = new CarBooking();
        $cars_list = $model->carData;

        $query = CarBooking::find()
            ->where(['(:datetime_start, :datetime_end) OVERLAPS (datetime_start, datetime_end)' => true])
            ->andWhere(['status' => 2])
            ->andWhere(['car_id' => array_keys($cars_list)])
            ->groupBy(['id', 'car_id'])
            ->addParams([
                ':datetime_start' => $datetime_start,
                ':datetime_end' => $datetime_end
            ])
            ->all();

        $data = [];

        if (!is_null($query)) {
            $id_list = array_keys(ArrayHelper::map($query,'id','id'));
            foreach ($id_list as $key => $id) {

                $model = $this->findModel($id);

                $data[$key][] = $model->car->license_id;
                $data[$key][] = $model->carBookingLocation->name;
                $data[$key][] = $model->carBookingCategory->name;
                $data[$key][] = $model->setPassengersName($model->passengers);
                $data[$key][] = Yii::$app->datetimeManage->dtFormat($datetime = $model->datetime_start, true) . ' - ' . Yii::$app->datetimeManage->dtFormat($datetime = $model->datetime_end, true);
            }
        }

        return $data;
    }
}