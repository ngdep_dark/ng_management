<?php

namespace app\modules\CarBooking\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use app\models\User;

/**
 * This is the model class for table "traffic_ticket".
 *
 * @property int $id
 * @property int $car_booking_id
 * @property string $tid
 * @property string $accuse_at
 * @property string $due_pay_at
 * @property int $car_booking_location_id
 * @property int $allegation_type_id
 * @property int $price
 * @property string $payment_at
 * @property string $detail
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 *
 * @property AllegationType $allegationType
 * @property CarBooking $carBooking
 * @property CarBookingLocation $carBookingLocation
 */
class TrafficTicket extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'traffic_ticket';
    }

    public function behaviors(){
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tid', 'accuse_at', 'price', 'car_booking_location_id', 'car_id', 'allegation_type_id', 'due_pay_at'], 'required'],
            [['car_booking_location_id', 'allegation_type_id', 'price', 'created_at', 'created_by', 'updated_at', 'updated_by', 'detail'], 'default', 'value' => null],
            [['created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['payment_at'], 'safe'],
            [['price'], 'integer', 'min' => 0],
            [['tid', 'detail'], 'string', 'max' => 255],
            [['tid'], 'unique'],
            [['car_booking_id'], 'integer'],
            [['car_id'], 'validateBooking', 'on' => 'create'],
            [['car_id'], 'exist', 'skipOnError' => true, 'targetClass' => Car::className(), 'targetAttribute' => ['car_id' => 'id']],
            [['allegation_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => AllegationType::className(), 'targetAttribute' => ['allegation_type_id' => 'id']],
            [['car_booking_location_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarBookingLocation::className(), 'targetAttribute' => ['car_booking_location_id' => 'id']],
        ];
    }

    public function validateBooking() {
        $query = CarBooking::find()
            ->where(['car_id' => $this->car_id])
            ->andWhere(['(:datetime_start, :datetime_end) OVERLAPS (datetime_start, datetime_end)' => true])
            ->andWhere(['status' => 2])
            ->addParams([
                ':datetime_start' => $this->accuse_at,
                ':datetime_end' => date('Y-m-d H:i:s',strtotime($this->accuse_at) + 24*60*60 -1)
            ])
//            ->asArray()
            ->all();

        if (!empty($query)) {
            $this->addError('car_id', 'เลือกรายการจองรถที่อาจจะเป็นรายการที่ทำความผิด');
            return $query;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'car_id' => Yii::$app->messageManage->car,
            'car_booking_id' => Yii::$app->messageManage->car_booking,
            'tid' => Yii::$app->messageManage->tt_tid,
            'accuse_at' => Yii::$app->messageManage->tt_accuse_at,
            'due_pay_at' => Yii::$app->messageManage->tt_due_pay_at,
            'car_booking_location_id' => Yii::$app->messageManage->cb_location_name,
            'allegation_type_id' => Yii::$app->messageManage->allegation_type_name,
            'price' => Yii::$app->messageManage->tt_price,
            'payment_at' => Yii::$app->messageManage->payment_at,
            'detail' => Yii::$app->messageManage->detail,
            'created_at' => Yii::$app->messageManage->create_at,
            'created_by' => Yii::$app->messageManage->create_by,
            'updated_at' => Yii::$app->messageManage->update_at,
            'updated_by' => Yii::$app->messageManage->update_by,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllegationType()
    {
        return $this->hasOne(AllegationType::className(), ['id' => 'allegation_type_id']);
    }

    public function getCarBooking()
    {
        return $this->hasOne(CarBooking::className(), ['id' => 'car_booking_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarBookingLocation()
    {
        return $this->hasOne(CarBookingLocation::className(), ['id' => 'car_booking_location_id']);
    }

    public function getCar()
    {
        return $this->hasOne(Car::className(), ['id' => 'car_id']);
    }

    // start query for select2
    public function getLocationData()
    {
        return $query = ArrayHelper::map(CarBookingLocation::find(['id','name'])->orderBy('id')->all(), 'id', 'name');
    }

    public function getAllegationTypeData()
    {
        return $query = ArrayHelper::map(AllegationType::find(['id','name'])->orderBy('id')->all(), 'id', 'name');
    }

    public function getCarData()
    {
        return $query = ArrayHelper::map(Car::find(['id','license_id'])->orderBy('id')->all(), 'id', 'license_id');
    }

    public function getCreateBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}