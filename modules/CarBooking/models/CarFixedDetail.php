<?php

namespace app\modules\CarBooking\models;

use Yii;

/**
 * This is the model class for table "car_fixed_detail".
 *
 * @property int $id
 * @property int $car_fixed_id
 * @property string $name
 * @property int $amount
 * @property int $price
 *
 * @property CarFixed $carFixed
 */
class CarFixedDetail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_fixed_detail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['car_fixed_id'], 'required'],
            [['car_fixed_id', 'amount', 'price'], 'default', 'value' => null],
            [['car_fixed_id', 'amount'], 'integer'],
            [['price'], 'integer', 'integerOnly' => false,],
            [['name'], 'string', 'max' => 255],
            [['car_fixed_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarFixed::className(), 'targetAttribute' => ['car_fixed_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'car_fixed_id' => 'Car Fixed ID',
            'name' => 'Name',
            'amount' => 'Amount',
            'price' => 'Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarFixed()
    {
        return $this->hasOne(CarFixed::className(), ['id' => 'car_fixed_id']);
    }
}
