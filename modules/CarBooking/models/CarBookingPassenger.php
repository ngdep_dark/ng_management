<?php

namespace app\modules\CarBooking\models;

use app\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "car_booking_passenger".
 *
 * @property int $id
 * @property int $car_booking_id
 * @property int $user_id
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 *
 * @property CarBooking $carBooking
 * @property User $user
 */
class CarBookingPassenger extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_booking_passenger';
    }

    public function behaviors(){
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['car_booking_id', 'user_id'], 'required'],
            [['car_booking_id', 'user_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'default', 'value' => null],
            [['car_booking_id', 'user_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['car_booking_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarBooking::className(), 'targetAttribute' => ['car_booking_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'car_booking_id' => 'Car Booking ID',
            'user_id' => 'User ID',
            'created_at' => Yii::$app->messageManage->create_at,
            'created_by' => Yii::$app->messageManage->create_by,
            'updated_at' => Yii::$app->messageManage->update_at,
            'updated_by' => Yii::$app->messageManage->update_at,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarBooking()
    {
        return $this->hasOne(CarBooking::className(), ['id' => 'car_booking_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
