<?php

namespace app\modules\CarBooking\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CarBookingSearch represents the model behind the search form of `app\modules\CarBooking\models\CarBooking`.
 */
class CarBookingStatementSearch extends CarBooking
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'miles_start', 'miles_end', 'status', 'car_id', 'car_booking_location_id', 'car_booking_category_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['datetime_start', 'datetime_end', 'detail'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CarBooking::find();

        // add conditions that should always apply here
        $query->leftJoin('"car_booking_passenger"','car_booking.id = car_booking_passenger.car_booking_id');
        $query->where(['car_booking_passenger.user_id' => Yii::$app->user->id]);
        $query->orderBy('car_booking.datetime_start');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'datetime_start' => $this->datetime_start,
            'datetime_end' => $this->datetime_end,
            'miles_start' => $this->miles_start,
            'miles_end' => $this->miles_end,
            'status' => $this->status,
            'car_id' => $this->car_id,
            'car_booking_location_id' => $this->car_booking_location_id,
            'car_booking_category_id' => $this->car_booking_category_id,
        ]);

        return $dataProvider;
    }
}
