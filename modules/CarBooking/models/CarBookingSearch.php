<?php

namespace app\modules\CarBooking\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CarBookingSearch represents the model behind the search form of `app\modules\CarBooking\models\CarBooking`.
 */
class CarBookingSearch extends CarBooking
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'miles_start', 'miles_end', 'status', 'car_id', 'car_booking_location_id', 'car_booking_category_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['datetime_start', 'datetime_end', 'datetime', 'passengers', 'detail'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
//        $permission = Yii::$app->user->can('Admin');

        $query = CarBooking::find()
            ->leftJoin('"car_booking_passenger"','car_booking.id = car_booking_passenger.car_booking_id');
//        if (!$permission) $query->andFilterWhere(['car_booking_passenger.user_id' => Yii::$app->user->id]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['datetime_start'=>SORT_DESC, 'datetime_end' => SORT_DESC]]
        ]);

        $dataProvider->sort->attributes['datetime'] = [
            'asc' => ['datetime_start' => SORT_ASC, 'datetime_end' => SORT_ASC],
            'desc' => ['datetime_start' => SORT_DESC, 'datetime_end' => SORT_DESC],
//            'label' => 'ช่วงเวลา',
            'default' => SORT_DESC
        ];

        $dataProvider->sort->attributes['passengers'] = [
            'asc' => ['"car_booking_passenger"."user_id"' => SORT_ASC],
            'desc' => ['"car_booking_passenger"."user_id"' => SORT_DESC],
            'default' => SORT_ASC
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            '"car_booking"."id"' => $this->id,
            '"car_booking"."datetime_start"' => $this->datetime_start,
            '"car_booking"."datetime_end"' => $this->datetime_end,
            '"car_booking"."miles_start"' => $this->miles_start,
            '"car_booking"."miles_end"' => $this->miles_end,
            '"car_booking"."status"' => $this->status,
            '"car_booking"."car_id"' => $this->car_id,
            '"car_booking"."car_booking_location_id"' => $this->car_booking_location_id,
            '"car_booking"."car_booking_category_id"' => $this->car_booking_category_id,
            '"car_booking"."created_at"' => $this->created_at,
            '"car_booking"."created_by"' => $this->created_by,
            '"car_booking"."updated_at"' => $this->updated_at,
            '"car_booking"."updated_by"' => $this->updated_by,
            '"car_booking_passenger"."user_id"' => $this->passengers,
        ]);

        if(!is_null($this->datetime) && !empty($this->datetime)) {
            $query->andFilterWhere(['(:datetime_start, :datetime_end) OVERLAPS (datetime_start, datetime_end)' => true])
                ->addParams([
                    ':datetime_start' => $this->datetime,
                    ':datetime_end' => $this->datetime
                ]);
        }


        return $dataProvider;
    }
}
