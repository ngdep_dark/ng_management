<?php

namespace app\modules\CarBooking\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\CarBooking\models\TrafficTicket;

/**
 * TrafficTicketSearch represents the model behind the search form of `app\modules\CarBooking\models\TrafficTicket`.
 */
class TrafficTicketSearch extends TrafficTicket
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'car_booking_location_id', 'allegation_type_id', 'price', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['tid', 'accuse_at', 'due_pay_at', 'payment_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TrafficTicket::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'accuse_at' => SORT_DESC,
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
//            'car_booking_id' => $this->car_booking_id,
            'accuse_at' => $this->accuse_at,
            'due_pay_at' => $this->due_pay_at,
            'car_booking_location_id' => $this->car_booking_location_id,
            'allegation_type_id' => $this->allegation_type_id,
            'price' => $this->price,
            'payment_at' => $this->payment_at,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['ilike', 'tid', $this->tid]);

        return $dataProvider;
    }
}