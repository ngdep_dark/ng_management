<?php

namespace app\modules\CarBooking\models;

use app\models\User;
use Yii;

/**
 * This is the model class for table "car_booking_location".
 *
 * @property int $id
 * @property string $name
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 *
 * @property CarBooking[] $carBookings
 * @property TrafficTicket[] $trafficTickets
 */
class CarBookingLocation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_booking_location';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'created_by', 'updated_at', 'updated_by'], 'default', 'value' => null],
            [['created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => Yii::$app->messageManage->cb_location_name,
            'created_at' => Yii::$app->messageManage->create_at,
            'created_by' => Yii::$app->messageManage->create_by,
            'updated_at' => Yii::$app->messageManage->update_at,
            'updated_by' => Yii::$app->messageManage->update_at,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarBookings()
    {
        return $this->hasMany(CarBooking::className(), ['car_booking_location_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrafficTickets()
    {
        return $this->hasMany(TrafficTicket::className(), ['car_booking_location_id' => 'id']);
    }

    public function getCreateBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
