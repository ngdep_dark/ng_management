<?php

namespace app\modules\CarBooking\models;

use app\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "car_fixed_type".
 *
 * @property int $id
 * @property string $name
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 *
 * @property CarFixed[] $carFixeds
 */
class CarFixedType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car_fixed_type';
    }

    public function behaviors(){
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'created_by', 'updated_at', 'updated_by'], 'default', 'value' => null],
            [['created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => Yii::$app->messageManage->car_fixed_type,
            'created_at' => Yii::$app->messageManage->create_at,
            'created_by' => Yii::$app->messageManage->create_by,
            'updated_at' => Yii::$app->messageManage->update_at,
            'updated_by' => Yii::$app->messageManage->update_by,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarFixeds()
    {
        return $this->hasMany(CarFixed::className(), ['car_fixed_type_id' => 'id']);
    }

    public function getCreateBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
