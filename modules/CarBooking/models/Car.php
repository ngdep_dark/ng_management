<?php

namespace app\modules\CarBooking\models;

use app\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "car".
 *
 * @property int $id
 * @property string $license_id
 * @property int $car_brand_id
 * @property string $car_model
 * @property int $miles
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 * @property int $miles_last_check
 * @property string $insurance_date
 * @property string $car_act_date
 *
 * @property CarBrand $carBrand
 * @property CarBooking[] $carBookings
 * @property CarFixed[] $carFixeds
 */
class Car extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'car';
    }

    public function behaviors(){
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className()
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['car_brand_id', 'license_id', 'car_model', 'miles', 'insurance_date', 'car_act_date', 'insurance_date', 'car_act_date'], 'required'],
            [['miles_last_check'], 'required', 'on' => 'create'],
            [['car_brand_id', 'miles', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'default', 'value' => null],
            [['miles', 'created_at', 'created_by', 'updated_at', 'updated_by', 'miles_last_check'], 'integer'],
//            [['miles_last_check'], 'default', 'value' => function ($model, $attribute) {
//                return $model->miles + 10000;
//            }],
            [['license_id', 'car_model'], 'string', 'max' => 255],
            [['license_id'], 'unique'],
            [['car_brand_id'], 'exist', 'skipOnError' => true, 'targetClass' => CarBrand::className(), 'targetAttribute' => ['car_brand_id' => 'id']],
        ];
    }

    public function validateInsuranceDate() {
        $insuranceDateExp = new \DateTime($this->insurance_date);
//        $plus1Year = new \DateInterval('P1Y');
//        $insuranceDateExp->add($plus1Year);

        $nowDate = date_create('now');

        $interval = date_diff($nowDate, $insuranceDateExp);

        $diffDays = $interval->format('%R%a');

        if (intval($diffDays) <=0) {
            return 'ประกัน หมดแล้ว กรุณาต่อรีบไปต่อ';
        } else if (intval($diffDays) <= 30) {
            return 'ประกันใกล้หมดแล้วแล้ว กรุณาต่อประกันก่อนวันที่ ' . Yii::$app->datetimeManage->dtFormat($datetime = $insuranceDateExp->format('Y-m-d'), false, true);
        } else {
            return null;
        }
    }

    public function validateCarActDate() {
        $carActExp = new \DateTime($this->car_act_date);
//        $plus1Year = new \DateInterval('P1Y');
//        $carActExp->add($plus1Year);

        $nowDate = date_create('now');

        $interval = date_diff($nowDate, $carActExp);

        $diffDays = $interval->format('%R%a');

        if (intval($diffDays) <=0) {
            return 'พ.ร.บ. หมดแล้ว กรุณาต่อรีบไปต่อ';
        } else if (intval($diffDays) <= 30) {
            return 'พ.ร.บ. ใกล้หมดแล้ว กรุณาต่อ พ.ร.บ. ก่อนวันที่ ' . Yii::$app->datetimeManage->dtFormat($datetime = $carActExp->format('Y-m-d'), false, true);
        } else {
            return null;
        }
    }

    public function validateMilesLastCarCheck() {

        $milesLastCarCheck = $this->miles_last_check;

        if ($this->miles > $milesLastCarCheck) {
            return 'ระยะไมล์เกินกำหนดตรวจเช็คแล้ว กรุณาต่อรีบไปเข้าศูนย์';
        } else if ($this->miles > $milesLastCarCheck * 0.9) {
            return 'ใกล้ถึงระยะตรวจสอบแล้วควรรีบนำไปตรวจเช็คสภาพ ' . 'เลขไมค์ที่ต้องนำไปตรวจสภาพ ' . $this->miles_last_check . ' เลขไมค์ปัจจุบัน ' . $this->miles;
        } else {
            return null;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'license_id' => Yii::$app->messageManage->car_license,
            'car_brand_id' => Yii::$app->messageManage->car_brand,
            'car_model' => Yii::$app->messageManage->car_model,
            'miles' => Yii::$app->messageManage->miles,
            'miles_last_check' => Yii::$app->messageManage->car_miles_last_check,
            'insurance_date' => Yii::$app->messageManage->car_insurance_date,
            'car_act_date' => Yii::$app->messageManage->car_act_date,
            'created_at' => Yii::$app->messageManage->create_at,
            'created_by' => Yii::$app->messageManage->create_by,
            'updated_at' => Yii::$app->messageManage->update_at,
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarBrand()
    {
        return $this->hasOne(CarBrand::className(), ['id' => 'car_brand_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarBookings()
    {
        return $this->hasMany(CarBooking::className(), ['car_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarFixeds()
    {
        return $this->hasMany(CarFixed::className(), ['car_id' => 'id']);
    }

    public function getCreateBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    // query for select2
    public function getCarBrandData()
    {
        return $query = ArrayHelper::map(CarBrand::find(['id','name'])->orderBy('id')->all(), 'id', 'name');
    }
}
