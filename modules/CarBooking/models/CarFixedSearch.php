<?php

namespace app\modules\CarBooking\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\CarBooking\models\CarFixed;

/**
 * CarFixedSearch represents the model behind the search form of `app\modules\CarBooking\models\CarFixed`.
 */
class CarFixedSearch extends CarFixed
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'car_id', 'car_fixed_type_id', 'garage_id', 'sender_fixed_by', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['fix_date', 'fixed_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CarFixed::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'car_id' => $this->car_id,
            'car_fixed_type_id' => $this->car_fixed_type_id,
            'garage_id' => $this->garage_id,
//            'price' => $this->price,
            'sender_fixed_by' => $this->sender_fixed_by,
            'fix_date' => $this->fix_date,
            'fixed_date' => $this->fixed_date,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        return $dataProvider;
    }
}
