<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\BookingCar\models\CarBrand */

$this->title = 'สร้าง' . Yii::$app->messageManage->car_brand;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->car_brand, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-brand-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
