<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\BookingCar\models\CarBrand */

$this->title = 'แก้ไข' . Yii::$app->messageManage->car_brand . ': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->car_brand, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="car-brand-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
