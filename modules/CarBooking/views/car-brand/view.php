<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\BookingCar\models\CarBrand */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->car_brand, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$query = (new \yii\db\Query())
    ->select('car_brand.id')
    ->from('car_booking')
    ->leftJoin('car','car_booking.car_id = car.id')
    ->leftJoin('car_brand','car.car_brand_id = car_brand.id')
    ->where(['car_brand.id' => $model->id])
    ->scalar();
?>
<div class="car-brand-view">
    <p>
        <?= Html::a(Yii::$app->messageManage->update_button, ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= $model->id != $query ? Html::a(Yii::$app->messageManage->delete_button, ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::$app->messageManage->confirm,
                'method' => 'post',
            ],
        ]) : null ;?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//                'id',
            'name',
            [
                'attribute' => 'created_at',
                'value' => Yii::$app->datetimeManage->tsFormat($datetime = $model->created_at)
            ],
            [
                'label' => 'สร้างโดย',
                'attribute' => 'createBy.full_name',
            ],
            [
                'attribute' => 'updated_at',
                'value' => Yii::$app->datetimeManage->tsFormat($datetime = $model->updated_at)
            ],
            [
                'label' => Yii::$app->messageManage->update_by,
                'attribute' => 'updatedBy.full_name',
            ],
        ],
    ]) ?>
</div>
