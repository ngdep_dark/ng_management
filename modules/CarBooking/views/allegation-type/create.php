<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\CarBooking\models\AllegationType */

$this->title = 'สร้าง' . Yii::$app->messageManage->allegation_type_name;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->allegation_type_name, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="allegation-type-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
