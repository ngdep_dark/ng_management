<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\CarBooking\models\AllegationType */

$this->title = 'แก้ไข' . Yii::$app->messageManage->allegation_type_name . ': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->allegation_type_name, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="allegation-type-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
