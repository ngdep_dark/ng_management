<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::$app->messageManage->cb_category_name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-booking-category-index">

    <p>
        <?= Html::a('สร้าง' . Yii::$app->messageManage->cb_category_name, ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'name',
//            'created_at',
//            'created_by',
//            'updated_at',
            //'updated_by',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                        $query = (new \yii\db\Query())
                            ->select('car_booking_category_id')
                            ->from('car_booking')
                            ->where(['car_booking_category_id' => $model->id])
                            ->scalar();
                        return $model->id == $query ?
                            null :
                            Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete', 'id' => $model->id], [
                                'data' => [
                                    'confirm' => Yii::$app->messageManage->confirm,
                                    'method' => 'post',
                                ],
                            ]);
                    },
                ]
            ],
        ],
    ]); ?>
</div>
