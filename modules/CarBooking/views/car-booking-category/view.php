<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\BookingCar\models\CarBookingCategory */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->cb_category_name, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$query = (new \yii\db\Query())
    ->select('car_booking_category_id')
    ->from('car_booking')
    ->where(['car_booking_category_id' => $model->id])
    ->scalar();
?>
<div class="car-booking-category-view">

    <p>
        <?= Html::a(Yii::$app->messageManage->update_button, ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= $model->id != $query ? Html::a(Yii::$app->messageManage->delete_button, ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::$app->messageManage->confirm,
                'method' => 'post',
            ],
        ]) : null ;?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//                'id',
            'name',
            [
                'attribute' => 'created_at',
                'value' => Yii::$app->datetimeManage->tsFormat($datetime = $model->created_at)
            ],
            [
                'label' => 'สร้างโดย',
                'attribute' => 'createBy.full_name',
            ],
            [
                'attribute' => 'updated_at',
                'value' => Yii::$app->datetimeManage->tsFormat($datetime = $model->updated_at)
            ],
            [
                'label' => Yii::$app->messageManage->update_by,
                'attribute' => 'updatedBy.full_name',
            ],
        ],
    ]) ?>

</div>
