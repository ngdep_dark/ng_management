<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\BookingCar\models\Car */

$this->title = 'แก้ไข' . Yii::$app->messageManage->car . ': ' . $model->license_id;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->car, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->license_id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="car-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
