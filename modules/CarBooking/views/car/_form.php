<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\BookingCar\models\Car */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-form">
    <br><br>
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <?= $form->field($model, 'license_id')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="input-group">
                <?= $form->field($model, 'car_brand_id')->widget(Select2::classname(), [
                    'language' => 'th',
                    'data' => $model->carBrandData,
                    'options' => ['placeholder' => 'เลือกยี่ห้อรถ'],
                    'pluginOptions' => [
                        'tags' => true,
                        'allowClear' => true
                    ],
                ]);?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <?= $form->field($model, 'car_model')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <?= $form->field($model, 'miles')->textInput(['type' => 'number']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <?= $form->field($model, 'insurance_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => Yii::$app->messageManage->car_insurance_date],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd/mm/yyyy'
                ]
            ]);
            ?>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <?= $form->field($model, 'car_act_date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => Yii::$app->messageManage->car_act_date],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd/mm/yyyy'
                ]
            ]);
            ?>
        </div>
    </div>

    <?php if ($model->scenario == 'create') : ?>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <?= $form->field($model, 'miles_last_check')->textInput(['type' => 'number']) ?>
            </div>
        </div>
    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
