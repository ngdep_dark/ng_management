<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\modules\BookingCar\models\Car */

$this->title = $model->license_id;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->car, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$query = (new \yii\db\Query())
    ->select('car_id')
    ->from('car_booking')
    ->where(['car_id' => $model->id])
    ->scalar();

$car_id = $model->id;
?>
<div class="car-view">

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
            <p>
                <?= Html::a(Yii::$app->messageManage->update_button, ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= $model->id != $query ? Html::a(Yii::$app->messageManage->delete_button, ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => Yii::$app->messageManage->confirm,
                        'method' => 'post',
                    ],
                ]) : null ;?>
            </p>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title text-center">รายละเอียดรถ</h3>
                </div>
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
//                'id',
                        'license_id',
                        'carBrand.name',
                        'car_model',
                        'miles',
                        'miles_last_check',
                        [
                            'attribute' => 'insurance_date',
                            'value' => Yii::$app->datetimeManage->dtFormat($datetime = $model->insurance_date,false,true)
                        ],
                        [
                            'attribute' => 'car_act_date',
                            'value' => Yii::$app->datetimeManage->dtFormat($datetime = $model->car_act_date,false,true)
                        ],
                        [
                            'attribute' => 'created_at',
                            'value' => Yii::$app->datetimeManage->tsFormat($datetime = $model->created_at)
                        ],
                        [
                            'label' => Yii::$app->messageManage->create_by,
                            'attribute' => 'createBy.full_name',
                        ],
                        [
                            'attribute' => 'updated_at',
                            'value' => Yii::$app->datetimeManage->tsFormat($datetime = $model->updated_at)
                        ],
                        [
                            'label' => Yii::$app->messageManage->update_by,
                            'attribute' => 'updatedBy.full_name',
                        ],
                    ],
                ]) ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
            <p>
                <?= Html::a(Yii::$app->messageManage->create_button.Yii::$app->messageManage->car_fixed, ['car-fixed/create', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
            </p>
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title text-center">รายละเอียดประวัติการซ่อม</h3>
                </div>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        [
                            'attribute' => 'car_id',
                            'value' => 'car.license_id'
                        ],
                        [
                            'attribute' => 'car_fixed_type_id',
                            'value' => 'carFixedType.name'
                        ],
                        [
                            'attribute' => 'garage_id',
                            'value' => 'garage.name'
                        ],
                        [
                            'attribute' => 'sender_fixed_by',
                            'value' => 'senderFixedBy.full_name'
                        ],
                        [
                            'value' => function($model, $key, $index){
                                $detailData = $model->carFixedDetail;
                                $detail = array_map(function($model) {
                                    return $model['amount'] * $model['price'];
                                }, $detailData);
                                return array_sum($detail) . ' บาท';
                            },
                            'label' => 'ค่าใช้จ่าย'
                        ],
                        //'fix_date',
                        //'fixed_date',
                        //'created_at',
                        //'created_by',
                        //'updated_at',
                        //'updated_by',
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view} {update} {delete}',
                            'buttons' => [
                                'view' => function ($url, $model, $key) {
                                    return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', ['car-fixed/view', 'id' => $model->id]);
                                },
                                'update' => function ($url, $model, $key) {
                                    return Html::a('<i class="glyphicon glyphicon-pencil"></i>', ['car-fixed/update', 'id' => $model->id]);
                                },
                                'delete' => function ($url, $model, $key) use ($car_id) {
                                    return Html::a('<i class="glyphicon glyphicon-trash"></i>', ['car-fixed/delete', 'id' => $model->id], [
                                        'data' => [
                                            'confirm' => Yii::$app->messageManage->confirm,
                                            'method' => 'post',
                                            'params' => ['id' => $car_id]
                                        ],
                                    ]);
                                },
                            ]
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>

</div>
