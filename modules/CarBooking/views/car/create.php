<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\BookingCar\models\Car */

$this->title = 'สร้าง' . Yii::$app->messageManage->car;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->car, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
