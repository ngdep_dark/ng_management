<?php
/* @var $this yii\web\View */

use app\modules\CarBooking\models\CarBooking;
use kartik\daterange\DateRangePicker;
use kartik\widgets\Select2;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'การใช้รถตามช่วงเวลา';
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->report, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerCss("
    .float-inline{
        float : right
    }
");

function findCarBooking($id)
{
    if ($id) {
        return CarBooking::findOne($id);
    } else {
        return new CarBooking();
    }
}

?>
<div class="car-booking-report">
    <div class="clearfix">
        <?=Html::beginForm(['period-use-car'], 'post', ['data-pjax' => '', 'class' => 'pull-right form-inline']);?>
        <div class="float-inline">
            <?=Html::submitButton('<i class="fa fa-search"></i>', ['class' => 'btn btn-primary', 'id' => 'submit'])?>
        </div>

        <div class="float-inline">
            <?=DateRangePicker::widget([
                'name' => 'date_range',
                'presetDropdown' => true,
                'hideInput' => true,
            ]);?>
        </div>

        <div class="float-inline">
            <?php echo Select2::widget([
                'name' => 'car_id',
                'hideSearch' => true,
                'data' => CarBooking::getCarData(),
                'options' => [
                    'placeholder' => 'กรุณาเลือกรถ',
                ],
            ]); ?>
        </div>

        <?=Html::endForm()?>
        <br><br>
        <div class="row">
            <div class="col-md-pull-1 col-lg-push-1 col-md-10 col-lg-10">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">รายงานการใช้รถ</h3>
                    </div>
                    <div class="panel-body">
                        <?=GridView::widget([
                            'tableOptions' => ['class' => 'table table-striped table-bordered text-center'],
                            'dataProvider' => $dataProvider,
                            'showOnEmpty' => false,
                            'columns' => [
                                ['class' => \yii\grid\SerialColumn::className()],
                                [
                                    'attribute' => 'datetime_start',
                                    'label' => Yii::$app->messageManage->cb_datetime_start,
                                    'value' => function ($model, $key, $index) {
                                        $findModel = findCarBooking($model['id']);
                                        return Yii::$app->datetimeManage->dtFormat($findModel->datetime_start);
                                    },
                                ],
                                [
                                    'attribute' => 'datetime_start',
                                    'label' => Yii::$app->messageManage->cb_datetime_end,
                                    'value' => function ($model, $key, $index) {
                                        $findModel = findCarBooking($model['id']);
                                        return Yii::$app->datetimeManage->dtFormat($findModel->datetime_end);
                                    },
                                ],
                                [
                                    'attribute' => 'car_id',
                                    'label' => Yii::$app->messageManage->car_license,
                                    'value' => function ($model, $key, $index) {
                                        $findModel = findCarBooking($model['id']);
                                        return $findModel->car->license_id;
                                    },
                                ],
                                [
                                    'attribute' => 'car_booking_location_id',
                                    'label' => Yii::$app->messageManage->cb_location_name,
                                    'value' => function ($model, $key, $index) {
                                        $findModel = findCarBooking($model['id']);
                                        return $findModel->carBookingLocation->name;
                                    },
                                ],
                                [
                                    'attribute' => 'car_booking_category_id',
                                    'label' => Yii::$app->messageManage->cb_category_name,
                                    'value' => function ($model, $key, $index) {
                                        $findModel = findCarBooking($model['id']);
                                        return $findModel->carBookingCategory->name;
                                    },
                                ],
                                [
                                    'attribute' => 'car_booking_category_id',
                                    'label' => Yii::$app->messageManage->cb_passengers,
                                    'value' => function ($model, $key, $index) {
                                        $findModel = findCarBooking($model['id']);
                                        return $findModel->passengersName;
                                    },
                                ],
                                [
                                    'attribute' => 'car_id',
                                    'label' => 'ใช้ไป',
                                    'value' => function ($model, $key, $index) {
                                        $findModel = findCarBooking($model['id']);
                                        return $findModel->miles_end - $findModel->miles_start . ' ไมค์';
                                    },
                                ],
                            ],
                        ]);?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->registerJs(<<<JS
    $("#submit").prop('disabled', true);
    function checkValue() {
        if ($("#w0").val() !== '' && $("#w1").val() !== '') {
            $("#submit").prop('disabled', false);
        }
    }
    $("#w0").change(function() {
        console.log(123456);
        checkValue();
    });
    $("#w1").change(function() {
        console.log(654321);
        checkValue();
    });
JS
);