<div>
    <table class="table table-bordered table-condensed table-striped">
        <thead>
        <tr>
            <td><?= Yii::$app->messageManage->tt_tid ?></td>
            <td><?= Yii::$app->messageManage->tt_accuse_at ?></td>
            <td><?= Yii::$app->messageManage->tt_due_pay_at ?></td>
            <td><?= Yii::$app->messageManage->payment_at ?></td>
            <td><?= Yii::$app->messageManage->tt_price ?></td>
            <td><?= Yii::$app->messageManage->cb_passengers ?></td>
            <td>ประเภทการหัก</td>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $item => $value) : ?>
            <tr>
                <td><?= $value["tid"] ?></td>
                <td><?= Yii::$app->datetimeManage->dtFormat($value["accuse_at"]) ?></td>
                <td><?= Yii::$app->datetimeManage->dtFormat($value["due_pay_at"], false, true) ?></td>
                <td><?= $value["payment_at"] !== null ? Yii::$app->datetimeManage->dtFormat($value["payment_at"], false, true) : 'ยังไม่ได้จ่าย' ?></td>
                <td><?= $value["price"] ?></td>
                <td><?= !empty($value['passengers']) ? implode(", ", $value['passengers']) : 'ไม่มีการจองรถ'?></td>
                <td><?= $value["status"] ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>