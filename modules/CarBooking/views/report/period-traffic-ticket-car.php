<?php
/* @var $this yii\web\View */
use kartik\daterange\DateRangePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = ' ใบสั่งตามช่วงเวลา';
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->report, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerCss("
    .float-inline{
        float : right
    }
");

?>
<div class="car-booking-report">
    <div class="clearfix">

        <?php $form = ActiveForm::begin();?>
        <div class="float-inline">
            <?=Html::submitButton('<i class="fa fa-search"></i>', ['class' => 'btn btn-primary'])?>
        </div>

        <div class="float-inline">
            <?=DateRangePicker::widget([
                'name' => 'date_range',
                'presetDropdown' => true,
                'hideInput' => true,
            ]);?>
        </div>

        <?php $form = ActiveForm::end();?>
    </div>
</div>