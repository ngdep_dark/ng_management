<?php
use yii\helpers\ArrayHelper;
?>

<div>
    <table class="table table-bordered table-condensed table-striped">
        <thead>
        <tr>
            <td>ชื่อ</td>
            <td>จากรายการจอง/ครั้ง</td>
            <td>จากรายการจอง/บาท</td>
            <td>เหมาะรวมสัปดาห์/ครั้ง</td>
            <td>เหมาะรวมสัปดาห์/บาท</td>
            <td>จำนวนค่าปรับ/บาท</td>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($data as $item => $value) : ?>
            <tr>
                <td><?= $item ?></td>
                <td><?= $value["normal"] ?></td>
                <td><?= $value["normal_price"] ?></td>
                <td><?= $value["abnormal"] ?></td>
                <td><?= $value["abnormal_price"] ?></td>
                <td><?= $value["price"] ?></td>
            </tr>
        <?php endforeach; ?>
        <tr>
            <td>รวม</td>
            <td></td>
            <td><?= array_sum(ArrayHelper::getColumn($data, 'normal_price')) ?></td>
            <td></td>
            <td><?= array_sum(ArrayHelper::getColumn($data, 'abnormal_price')) ?></td>
            <td><?= array_sum(ArrayHelper::getColumn($data, 'price')) ?></td>
        </tr>
        </tbody>
    </table>
</div>