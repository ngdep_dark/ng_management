<?php
/* @var $this yii\web\View */
use app\modules\CarBooking\models\CarBooking;
use kartik\daterange\DateRangePicker;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

$this->title = 'รายงานการใช้รถรายคน';
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->report, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerCss("
    .float-inline{
        float : right
    }
");

?>
<div class="car-booking-report">
    <div class="clearfix">
        <?=Html::beginForm(['period-use-car-per-person'], 'post', ['class' => 'pull-right form-inline']);?>
        <div class="float-inline">
            <?=Html::submitButton('<i class="fa fa-search"></i>', ['class' => 'btn btn-primary', 'id' => 'submit'])?>
        </div>

        <div class="float-inline">
            <?=DateRangePicker::widget([
            'name' => 'date_range',
            'presetDropdown' => true,
            'hideInput' => true,
        ]);?>
        </div>
    </div>
</div>
</div>