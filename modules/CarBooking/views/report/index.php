<?php
$this->title = Yii::$app->messageManage->report;
$this->params['breadcrumbs'][] = $this->title;
/* @var $this yii\web\View */

$this->registerCss("
    .padding-border {
        padding: 10px 20px;
    }
")
?>
<div class="car-booking-report">
    <div class="clearfix">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="padding-border box-title text-olive"><i class="fa fa-file-pdf-o"></i> <small></small>
                            รายงานการใช้รถ</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="box-group">
                            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                            <div class="row fontawesome-icon-list">
                                <div class="padding-border fa-hover col-md-4 col-sm-4 col-xs-12"><a
                                        href="/car-booking/report/period-use-car"><i class="fa fa-file-pdf-o"></i>
                                        การใช้รถตามช่วงเวลา</a></div>
                                <div class="padding-border fa-hover col-md-4 col-sm-4 col-xs-12"><a
                                        href="/car-booking/report/period-traffic-ticket-car"><i
                                            class="fa fa-file-pdf-o"></i> ใบสั่งตามช่วงเวลา</a></div>
                                <div class="padding-border fa-hover col-md-4 col-sm-4 col-xs-12"><a
                                        href="/car-booking/report/period-cost-traffic-ticket-person"><i
                                            class="fa fa-file-pdf-o"></i> การหักเงินเดือนตามใบสั่งรายคน</a></div>
                                <div class="padding-border fa-hover col-md-4 col-sm-4 col-xs-12"><a
                                        href="/car-booking/report/period-use-car-per-person"><i
                                            class="fa fa-file-pdf-o"></i>
                                        รายงานการใช้รถรายคน</a></div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>