<?php
/* @var $this yii\web\View */
use kartik\daterange\DateRangePicker;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = $title;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->report, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->registerCss("
    .float-inline{
        float : right
    }
");

?>
    <div class="car-booking-report">
        <div class="clearfix">

            <?php $form = ActiveForm::begin(); ?>
            <div class="float-inline">
                <?= Html::submitButton('<i class="fa fa-search"></i>', ['class' => 'btn btn-primary']) ?>
            </div>

            <div class="float-inline">
                <?= DateRangePicker::widget([
                    'name'=>'date_range',
                    'presetDropdown'=>true,
                    'hideInput'=>true,
                ]);
                ?>
            </div>

            <?php $form = ActiveForm::end(); ?>
        </div>
    </div>
<?php
//$this->registerJs(<<<JS
//    $("#submit").prop('disabled', true);
//    function checkValue() {
//        if ($("#w0").val() !== '' && $("#w1").val() !== '') {
//            $("#submit").prop('disabled', false);
//        }
//    }
//    $("#w0").change(function() {
//        console.log(123456);
//        checkValue();
//    });
//    $("#w1").change(function() {
//        console.log(654321);
//        checkValue();
//    });
//JS
//);