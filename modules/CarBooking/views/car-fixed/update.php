<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\CarBooking\models\CarFixed */

$this->title = 'แก้ไข' . Yii::$app->messageManage->car_fixed . ': ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->car, 'url' => ['/car-booking/car']];
$this->params['breadcrumbs'][] = ['label' => $model->car->license_id, 'url' => ['/car-booking/car/view', 'id' => $model->car_id]];
//$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->car_fixed, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="car-fixed-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
