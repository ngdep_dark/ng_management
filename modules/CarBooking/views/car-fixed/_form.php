<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\CarBooking\models\CarFixed */
/* @var $form yii\widgets\ActiveForm */
$this->registerJsFile('@web/js/angular.min.js', ['depends' => [\yii\web\JqueryAsset::className(), \dmstr\web\AdminLteAsset::className()]]);
$this->registerJsFile('@web/js/fixed-car.js', ['depends' => [\yii\web\JqueryAsset::className(), \dmstr\web\AdminLteAsset::className()]]);
$newRecord = $model->isNewRecord;
//var_dump((new \yii\db\Query())->select('last_check_miles')->from('car_fixed')->where(['car_id' => $model->car_id])->orderBy(['last_check_miles' => SORT_DESC])->one());
?>
    <div class="car-fixed-form">
        <div ng-app="carBooking" ng-controller="carFixed" ng-cloak>
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'car_id')->hiddenInput()->label(false) ?>
            <?= $form->field($model, 'id')->hiddenInput(['id' => 'carFixedId'])->label(false) ?>

            <div class="row">
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4" id="type">
                    <?= $form->field($model, 'car_fixed_type_id')->widget(Select2::classname(), [
                        'language' => 'th',
                        'data' => $model->carFixedTypeData,
                        'options' => ['placeholder' => 'เลือก' . Yii::$app->messageManage->car_fixed_type,],
                        'pluginOptions' => [
                            'tags' => true,
                            'allowClear' => true
                        ],
                    ]);?>
                </div>

                <div id="garage" class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <?= $form->field($model, 'garage_id')->widget(Select2::classname(), [
                        'language' => 'th',
                        'data' => $model->garageData,
                        'options' => [
//                        'placeholder' => 'เลือก' . Yii::$app->messageManage->garage,
                            'placeholder' => ''
                        ],
                        'pluginOptions' => [
                            'tags' => true,
                            'allowClear' => true
                        ],
                    ]);?>
                </div>
                <div id="sender-fixed" class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <!--            $form->field($model, 'price')->textInput(['type' => 'number']) -->
                    <?= $form->field($model, 'sender_fixed_by')->widget(Select2::classname(), [
                        'language' => 'th',
                        'data' => $model->senderFixedByData,
                        'options' => ['placeholder' => 'เลือก' . Yii::$app->messageManage->cf_sender_fixed_by],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);?>
                </div>
            </div>

            <div class="row">
                <div class="col-xs-4 col-sm-4 col-md-6 col-lg-6">
                    <?= $form->field($model, 'fix_date')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => Yii::$app->messageManage->cf_fix_date],
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd/mm/yyyy'
                        ]
                    ]);
                    ?>
                </div>
                <div class="col-xs-4 col-sm-4 col-md-6 col-lg-6">
                    <?= $form->field($model, 'fixed_date')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => Yii::$app->messageManage->cf_fixed_date],
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd/mm/yyyy'
                        ]
                    ]);
                    ?>
                </div>
            </div>

            <div class="panel">

                <table class="table table-hover table-responsive text-center" width="100%">
                    <thead>
                    <tr>
                        <th width="10%">No.</th>
                        <th width="25%">รายการ</th>
                        <th width="25%">จำนวน</th>
                        <th width="25%">ราคา</th>
                        <th width="15%">รวม</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="(index, model) in carFixedDetail" class="form-group">
                        <td style="padding-top: 15px;">
                            {{index+1}}
                        </td>
                        <td class="form-group">
                            <input type="text" class="form-control" ng-model="model.name" name="CarFixed[detail][{{index}}][CarFixedDetail][name]" placeholder="รายการ" required>
                        </td>
                        <td class="form-group">
                            <input type="number" class="form-control" ng-model="model.amount" ng-change="sumPriceFunc()" name="CarFixed[detail][{{index}}][CarFixedDetail][amount]" placeholder="จำนวน" min="0" max="10000" required>
                        </td>
                        <td class="form-group">
                            <input type="text" class="form-control" ng-model="model.price" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01" ng-change="sumPriceFunc()" name="CarFixed[detail][{{index}}][CarFixedDetail][price]" min="0" max="100000" placeholder="ราคา" float-only>

                        </td>
                        <td>
                            {{model.amount * model.price}}
                        </td>

                    </tr>
                    <tr>
                        <td colspan="4">รวมทั้งสิ้น</td>
                        <td>{{sumPrice}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <button type="button" class="btn btn-primary" ng-click="AddRow()">เพิ่มรายการ</button>
            <button type="button" class="btn btn-warning" ng-click="DelRow()">ลบรายการ</button>

            <?= Html::submitButton('บันทึกการทำรายการ', ['class' => 'btn btn-success']) ?>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
<?php
$this->registerJs(<<<JS

function datepickerValNull(id) {
    var idVal = $(id).val();
    if (idVal === '<span class="not-set">(ไม่ได้ตั้ง)</span>') {
        $(id).val(null)
    }
}

datepickerValNull('#carfixed-fix_date');
datepickerValNull('#carfixed-fixed_date');

//
//var value;
//function visibleCarFixedType() {
//    console.log($("input[name='CarFixed[type]']"));
//
//    if (value === 1) {
//        value = 2;
//    } else {
//        value = 1;
//    }
//
//    var carFixedType = $("#type");
//    var garage = $("#garage");
//    var senderFixed = $("#sender-fixed");
//
//    if (value === 1) {
//        carFixedType.hide();
//        garage.removeClass("col-xs-4 col-sm-4 col-md-4 col-lg-4");
//        garage.addClass("col-xs-6 col-sm-6 col-md-6 col-lg-6");
//        senderFixed.removeClass("col-xs-4 col-sm-4 col-md-4 col-lg-4");
//        senderFixed.addClass("col-xs-6 col-sm-6 col-md-6 col-lg-6");
//
//    } else {
//        carFixedType.show();
//        garage.removeClass("col-xs-6 col-sm-6 col-md-6 col-lg-6");
//        garage.addClass("col-xs-4 col-sm-4 col-md-4 col-lg-4");
//        senderFixed.removeClass("col-xs-6 col-sm-6 col-md-6 col-lg-6");
//        senderFixed.addClass("col-xs-4 col-sm-4 col-md-4 col-lg-4");
//    }
//}
//
//visibleCarFixedType();
//
//$("#type1").click(function() {
//    visibleCarFixedType();
//});
//
//$("#type2").click(function() {
//    visibleCarFixedType();
//});
//
//
//
JS
);