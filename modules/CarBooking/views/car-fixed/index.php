<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\CarBooking\models\CarFixedSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::$app->messageManage->car_fixed;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-fixed-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'car_id',
                'value' => 'car.license_id'
            ],
            [
                'attribute' => 'car_fixed_type_id',
                'value' => 'carFixedType.name'
            ],
            [
                'attribute' => 'garage_id',
                'value' => 'garage.name'
            ],
            //'sender_fixed_by',
            //'fix_date',
            //'fixed_date',
            //'created_at',
            //'created_by',
            //'updated_at',
            //'updated_by',

//                ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
