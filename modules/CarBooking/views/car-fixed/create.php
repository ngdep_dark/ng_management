<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\CarBooking\models\CarFixed */

$this->title = 'สร้าง' . Yii::$app->messageManage->car_fixed;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->car, 'url' => ['/car-booking/car']];
$this->params['breadcrumbs'][] = ['label' => $model->car->license_id, 'url' => ['/car-booking/car/view', 'id' => $model->car_id]];
//$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->car_fixed, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-fixed-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
