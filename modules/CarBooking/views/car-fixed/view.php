<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\CarBooking\models\CarFixed */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->car, 'url' => ['/car-booking/car']];
$this->params['breadcrumbs'][] = ['label' => $model->car->license_id, 'url' => ['/car-booking/car/view', 'id' => $model->car_id]];
//$this->params['breadcrumbs'][] = ['label' => 'Car Fixeds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-fixed-view">

    <div class="container">
        <p>
            <?= Html::a(Yii::$app->messageManage->update_button, ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::$app->messageManage->delete_button, ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::$app->messageManage->confirm,
                    'method' => 'post',
                    'params' => ['id' => $model->car_id]
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
//                'id',
                'car.license_id',
                'carFixedType.name',
                'garage.name',
                'senderFixedBy.full_name',
                'fix_date',
                'fixed_date',
                [
                    'attribute' => 'created_at',
                    'value' => Yii::$app->datetimeManage->tsFormat($datetime = $model->created_at)
                ],
                [
                    'label' => Yii::$app->messageManage->create_by,
                    'attribute' => 'createBy.full_name',
                ],
                [
                    'attribute' => 'updated_at',
                    'value' => Yii::$app->datetimeManage->tsFormat($datetime = $model->updated_at)
                ],
                [
                    'label' => Yii::$app->messageManage->update_by,
                    'attribute' => 'updatedBy.full_name',
                ],
            ],
        ]) ?>
    </div>

</div>
