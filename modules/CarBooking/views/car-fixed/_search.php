<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\CarBooking\models\CarFixedSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="car-fixed-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'car_id') ?>

    <?= $form->field($model, 'car_fixed_type_id') ?>

    <?= $form->field($model, 'garage_id') ?>

    <?= $form->field($model, 'price') ?>

    <?php // echo $form->field($model, 'sender_fixed_by') ?>

    <?php // echo $form->field($model, 'fix_date') ?>

    <?php // echo $form->field($model, 'fixed_date') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
