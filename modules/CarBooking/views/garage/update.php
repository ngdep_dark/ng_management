<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\CarBooking\models\Garage */

$this->title = 'แก้ไข' . Yii::$app->messageManage->garage . ': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->garage, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="garage-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
