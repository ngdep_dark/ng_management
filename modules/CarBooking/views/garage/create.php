<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\CarBooking\models\Garage */

$this->title = 'สร้าง' . Yii::$app->messageManage->garage;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->garage, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="garage-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
