<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\CarBooking\models\Garage */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->garage, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$query = (new \yii\db\Query())
    ->select('garage_id')
    ->from('car_fixed')
    ->where(['garage_id' => $model->id])
    ->scalar();

?>
<div class="garage-view">

    <p>
        <?= Html::a(Yii::$app->messageManage->update_button, ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= $model->id != $query ? Html::a(Yii::$app->messageManage->delete_button, ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::$app->messageManage->confirm,
                'method' => 'post',
            ],
        ]) : null ;?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//                'id',
            'name',
            [
                'attribute' => 'created_at',
                'value' => Yii::$app->datetimeManage->tsFormat($datetime = $model->created_at)
            ],
            [
                'label' => Yii::$app->messageManage->create_by,
                'attribute' => 'createBy.full_name',
            ],
            [
                'attribute' => 'updated_at',
                'value' => Yii::$app->datetimeManage->tsFormat($datetime = $model->updated_at)
            ],
            [
                'label' => Yii::$app->messageManage->update_by,
                'attribute' => 'updatedBy.full_name',
            ],
        ],
    ]) ?>

</div>
