<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\CarBooking\models\TrafficTicket */

$this->title = 'แก้ไข' . Yii::$app->messageManage->traffic_ticket . ': ' . $model->tid;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->traffic_ticket, 'url' => ['/car-booking/traffic-ticket']];
//$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->car_booking, 'url' => ['/car-booking/car-booking']];
//$this->params['breadcrumbs'][] = ['label' => $model->car_booking_id, 'url' => ['/car-booking/car-booking/view', 'id' => $model->car_booking_id]];
//$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->traffic_ticket, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->tid, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="traffic-ticket-update">

    <?= $this->render('_form', [
        'model' => $model,
        'query' => $query
    ]) ?>

</div>
