<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\CarBooking\models\TrafficTicket */

$this->title = $model->tid;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->traffic_ticket, 'url' => ['/car-booking/traffic-ticket']];
//$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->car_booking, 'url' => ['/car-booking/car-booking']];
//$this->params['breadcrumbs'][] = ['label' => $model->car_booking_id, 'url' => ['/car-booking/car-booking/view', 'id' => $model->car_booking_id]];
//$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->traffic_ticket, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="traffic-ticket-view">

    <div class="container">
        <p>
            <?= Html::a(Yii::$app->messageManage->update_button, ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
//                'id',
//                'car_booking_id',
                'tid',
                [
                    'attribute' => 'accuse_at',
                    'value' => function ($model, $widget) {
                        return Yii::$app->datetimeManage->dtFormat($datetime = $model->accuse_at, false, false);
                    },
                ],
                [
                    'attribute' => 'due_pay_at',
                    'value' => function ($model, $widget) {
                        return Yii::$app->datetimeManage->dtFormat($datetime = $model->due_pay_at, false, true);
                    },
                ],
                'carBookingLocation.name',
                'allegationType.name',
                'price',
                [
                    'attribute' => 'payment_at',
                    'value' => function ($model, $widget) {
                        return $model->payment_at == null ? 'ยังไม่ได้จ่าย' : $model->payment_at;
                    },
                ],
                [
                    'attribute' => 'created_at',
                    'value' => Yii::$app->datetimeManage->tsFormat($datetime = $model->created_at)
                ],
                [
                    'label' => Yii::$app->messageManage->create_by,
                    'attribute' => 'createBy.full_name',
                ],
                [
                    'attribute' => 'updated_at',
                    'value' => Yii::$app->datetimeManage->tsFormat($datetime = $model->updated_at)
                ],
                [
                    'label' => Yii::$app->messageManage->update_by,
                    'attribute' => 'updatedBy.full_name',
                ],
                'detail',

            ],
        ]) ?>
    </div>

</div>