<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\CarBooking\models\TrafficTicket */

$this->title = 'สร้าง' . Yii::$app->messageManage->traffic_ticket;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->traffic_ticket, 'url' => ['/car-booking/traffic-ticket']];
//$this->params['breadcrumbs'][] = ['label' => $model->car_booking_id, 'url' => ['/car-booking/car-booking/view', 'id' => $model->car_booking_id]];
//$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->traffic_ticket, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="traffic-ticket-create">

    <?= $this->render('_form', [
        'model' => $model,
        'query' => $query
    ]) ?>

</div>
