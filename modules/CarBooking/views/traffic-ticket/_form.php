<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use kartik\widgets\DateTimePicker;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\CarBooking\models\TrafficTicket */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="traffic-ticket-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <?= $form->field($model, 'tid')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <?= $form->field($model, 'car_id')->widget(Select2::classname(), [
                'language' => 'th',
                'data' => $model->carData,
                'options' => ['placeholder' => 'เลือกรถที่ต้องการ'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);?>
            <?php if ($query != null) : ?>
            <div class="row text-center">
                <div class="panel panel-primary">
                    <div class="form-group">
                        <table class="table table-striped" width="100%">
                            <thead>
                                <td>เลือก</td>
                                <td>สถานที่</td>
                                <td>ช่วงเวลา</td>
                            </thead>
                            <tbody>
                                <?php foreach ($query as $item) : ?>
                                <tr>
                                    <td>
                                        <?= Html::radio('TrafficTicket[car_booking_id]', 'null',[
                                                'value' => $item->id
                                            ]) ?>
                                    </td>
                                    <td>
                                        <?= $item->carBookingLocation->name ?>
                                    </td>
                                    <td>
                                        <?= Yii::$app->datetimeManage->dtFormat($datetime = $item->datetime_start,true,false) . '<br>' . Yii::$app->datetimeManage->dtFormat($datetime = $item->datetime_end,true,false); ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                                <tr>
                                    <td>
                                        <?= Html::radio('TrafficTicket[car_booking_id]', 'true',[
                                            'value' => "0"
                                        ]) ?>
                                    </td>
                                    <td>
                                        ไม่ตรง
                                    </td>
                                    <td>
                                        -
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <?= $form->field($model, 'car_booking_location_id')->widget(Select2::classname(), [
                'language' => 'th',
                'data' => $model->locationData,
                'options' => ['placeholder' => 'เลือก' . Yii::$app->messageManage->cb_location_name],
                'pluginOptions' => [
                    'tags' => true,
                    'allowClear' => true
                ],
            ]);?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <?= $form->field($model, 'allegation_type_id')->widget(Select2::classname(), [
                'language' => 'th',
                'data' => $model->allegationTypeData,
                'options' => ['placeholder' => 'เลือก' . Yii::$app->messageManage->allegation_type_name],
                'pluginOptions' => [
                    'tags' => true,
                    'allowClear' => true
                ],
            ]);?>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <?= $form->field($model, 'accuse_at')->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => Yii::$app->messageManage->tt_accuse_at],
                'readonly' => true,
                'pluginOptions' => [
                    'autoclose' => true,
                    'minuteStep' => 10,
                ]
            ]);?>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <?= $form->field($model, 'due_pay_at')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => Yii::$app->messageManage->tt_due_pay_at],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd/mm/yyyy'
                ]
            ]);?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model, 'price')->textInput(['type' => 'number']) ?>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <?= $form->field($model, 'payment_at')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => Yii::$app->messageManage->payment_at],
                'pluginOptions' => [
                    'autoclose'=>true,
                    'format' => 'dd/mm/yyyy'
                ]
            ]);
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?= $form->field($model, 'detail')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>