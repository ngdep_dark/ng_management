<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\CarBooking\models\TrafficTicketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::$app->messageManage->traffic_ticket;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="traffic-ticket-index">
    <p>
        <?= Html::a('เพิ่ม' . Yii::$app->messageManage->traffic_ticket, ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'tid',
            [
                'attribute' => 'accuse_at',
                'value' => function ($model, $index, $widget) {
                    return Yii::$app->datetimeManage->dtFormat($datetime = $model->accuse_at, false, false);
                }
            ],
            [
                'attribute' => 'due_pay_at',
                'value' => function ($model, $index, $widget) {
                    return Yii::$app->datetimeManage->dtFormat($datetime = $model->due_pay_at, false, true);
                }
            ],
            [
                'attribute' => 'car_booking_location_id',
                'value' => 'carBookingLocation.name',
            ],
            [
                'attribute' => 'allegation_type_id',
                'value' => 'allegationType.name',
            ],
            //'price',
            //'payment_at',
            //'created_at',
            //'created_by',
            //'updated_at',
            //'updated_by',

//                [
//                    'class' => 'yii\grid\ActionColumn'
//                ],

            [
                'class' => 'yii\grid\ActionColumn',
            ],
        ],
    ]); ?>
</div>
