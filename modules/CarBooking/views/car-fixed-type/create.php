<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\CarBooking\models\CarFixedType */

$this->title = 'สร้าง' . Yii::$app->messageManage->car_fixed_type;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->car_fixed_type, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-fixed-type-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
