<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::$app->messageManage->car_fixed_type;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="car-fixed-type-index">
    <p>
        <?= Html::a('สร้าง' . Yii::$app->messageManage->car_fixed_type, ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'name',
//            'created_at',
//            'created_by',
//            'updated_at',
            //'updated_by',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete}',
                'buttons' => [
                    'delete' => function ($url, $model, $key) use($id) {
                        $query = (new \yii\db\Query())
                            ->select('car_fixed_type_id')
                            ->from('car_fixed')
                            ->where(['car_fixed_type_id' => $model->id])
                            ->scalar();
                        return $model->id == $query || $model->id == $id ?
                            null :
                            Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete', 'id' => $model->id], [
                                'data' => [
                                    'confirm' => Yii::$app->messageManage->confirm,
                                    'method' => 'post',
                                ],
                            ]);
                    },
                ]
            ],
        ],
    ]); ?>
</div>
