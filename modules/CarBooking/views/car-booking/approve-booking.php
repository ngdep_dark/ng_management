<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\bootstrap\ActiveForm;
use app\modules\CarBooking\models\CarBooking;

/* @var $this yii\web\View */
/* @var $model app\modules\BookingCar\models\CarBooking */

$this->title = 'อนุมัติการจองรถ';
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->car_booking, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'ภาพรวมการอนุมัติ', 'url' => ['overall-approve-booking']];
$this->params['breadcrumbs'][] = $this->title;

$id = implode(',',ArrayHelper::map($data,'id','id'));

$this->registerCss("
    .padding-top9{
        padding-top: 9px;
    }
");
?>
    <div class="car-booking-approve">

        <p>รวมทั้งสิ้น <?= count($data) ?> รายการ
            <span class="text-red"> *** ตัดสินใจเลือก 1 รายการที่ต้องการอนุมัติให้ใช้รถ ***</span>
        </p>

        <?php $form = ActiveForm::begin(); ?>

        <?= Html::textInput('all_id', $id,['id' => 'getId', 'readonly' => true, 'style' => 'display: none-img']) ?>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success', 'disabled' => true]) ?>
        </div>

        <div class="row">
            <?php foreach ($data as $items => $value) : ?>
                <?php
                $model = CarBooking::findOne($value['id']);

                $value['ทะบียนรถ'] = $model->car->license_id;
                $value['สถานที่'] = $model->carBookingLocation->name;
                $value['วัตถุประสงค์'] = $model->carBookingCategory->name;
                $value['ผู้โดยสาร'] = $model->passengersName;
                $value['วันที่จอง'] = $value['datetime_start'];
                $value['วันที่คืน'] = $value['datetime_end'];
                $value['ผู้บันทึก'] = $model->createBy->full_name;
                $value['เมื่อ'] = date('Y-m-d H:i:s', $value['created_at']);
                $value['รายละเอียด'] = $value['detail'];

                unset(
                    $value['datetime_start'],
                    $value['datetime_end'],
                    $value['miles_start'],
                    $value['miles_end'],
                    $value['status'],
                    $value['car_id'],
                    $value['car_booking_location_id'],
                    $value['car_booking_category_id'],
                    $value['detail'],
                    $value['created_at'],
                    $value['created_by'],
                    $value['updated_at'],
                    $value['updated_by']
                );
                ?>
                <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="radio text-center">
                                <?= Html::radio('id',null, ['value' => $value['id'], 'id' => $value['id']]) ?>
                                <?= Html::label('อนุมัติรายการนี้', $value['id'])?>
                            </div>
                        </div>
                        <div class="panel-body" id="">
                            <table class="table table-striped">
                                <?php foreach ($value as $contents => $contentValue) :?>
                                <?php if ($contents !== 'รายละเอียด') : ?>

                                    <tr>
                                        <td class="text-center" ><?= $contents ?></td>
                                        <?php if($contents === 'วันที่จอง' || $contents === 'วันที่คืน') : ?>
                                            <td class="text-center" ><?= Yii::$app->datetimeManage->dtFormat($datetime = $contentValue, $short = true) ?></td>
                                        <?php else: ?>
                                            <td class="text-center" ><?= $contentValue ?></td>
                                        <?php endif; ?>
                                    </tr>

                                <?php else: ?>
                            </table>
                            <?= Html::label($contents, null,['class' => ['col-md-12','padding-top9', 'text-center']])?>
                            <div class="col-md-12">
                                <?= Html::textarea(null, $contentValue, ['class' => ['form-control'], 'id' => $contents, 'readonly' => true, 'rows' => 6]) ?>
                            </div>
                            <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
<?php
$this->registerJs(<<<JS
$(':input[type="submit"]').prop('disabled', true)

$('input[name=id]').change(function() {
    $(':input[type="submit"]').prop('disabled', false)
})
JS
);