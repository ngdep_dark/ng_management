<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\widgets\DateTimePicker;
use kartik\file\FileInput;
/* @var $this yii\web\View */
/* @var $model app\modules\BookingCar\models\CarBooking */

$this->title = 'คืนรถ';
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->car_booking, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-booking-return-car">
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <?=$form->field($model, 'file')->widget(FileInput::classname(), [
            'pluginOptions' => [
                'showCaption' => false,
                'showRemove' => false,
                'showUpload' => false,
                'browseClass' => 'btn btn-primary btn-block',
                'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                'browseLabel' => 'Select Photo',
                'allowedFileExtensions' => ["bmp", "jpg", "jpeg", "png"],
                'maxFileSize' => 4096,
            ],
            'options' => ['accept' => 'image/*'],
        ]);?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
            <?= $form->field($model, 'datetime_end')->widget(DateTimePicker::classname(), [
                'options' => ['placeholder' => 'เวลาที่ต้องการคืน'],
                'readonly' => true,
                'pluginOptions' => [
                    'autoclose' => true,
                    'minuteStep' => 10,
                ]
            ]);?>
        </div>
    </div>

    <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'datetime_start')->hiddenInput()->label(false) ?>

    <div class="row">


        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <?= $form->field($model, 'miles_end')->textInput() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?= $form->field($model, 'detail')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success','style'=> 'margin-top: 25px']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>