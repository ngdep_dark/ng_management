<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\BookingCar\models\CarBooking */

$this->title = 'แก้ไข' . Yii::$app->messageManage->car_booking . ': ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->car_booking, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
$currentUrl = Yii::$app->request->url;
?>
<div class="car-booking-update">
    <?= $this->render('_form', [

        'model' => $model,
        'url' => $currentUrl,
//        'scenario' => $scenario
    ]) ?>
</div>
