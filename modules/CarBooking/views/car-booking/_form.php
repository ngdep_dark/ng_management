<?php

use yii\helpers\Html;
use kartik\widgets\DateTimePicker;
use kartik\widgets\Select2;
use kartik\widgets\ActiveForm;
use kartik\widgets\DepDrop;

/* @var $this yii\web\View */
/* @var $model app\modules\CarBooking\models\CarBooking */
/* @var $form yii\widgets\ActiveForm */

//$currentUrl = explode("/",$url);
//$index = count($currentUrl) -1;

$scenario = $model->scenario;
$status = $model->status;

$admin = Yii::$app->user->can('Admin');
$model->datetime_range = 1;

$now = date_create();
$datetime_start = date_format($now, 'Y-m-d H:i:s');

$future_10min = date_create('10 min');
$datetime_end = date_format($future_10min, 'Y-m-d H:i:s');

$query = (new \yii\db\Query())
    ->from('car_booking')
    ->where(['status' => 2])
    ->andWhere(['miles_end' => null])
    ->andWhere(['car_id' => $model->car_id])
    ->orderBy('datetime_start')
    ->scalar();
?>

<div class="container-fluid">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'datetime_range')->hiddenInput()->label(false) ?>

    <?php if ($scenario === 'create') :?>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <?= $form->field($model, 'datetime_start')->widget(DateTimePicker::classname(), [
                        'options' => ['placeholder' => 'เวลาที่ต้องการจอง'],
                        'readonly' => true,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'startDate' => $datetime_start,
                            'minuteStep' => 10,
                        ]
                    ]);?>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <?= $form->field($model, 'datetime_end')->widget(DateTimePicker::classname(), [
                        'options' => ['placeholder' => 'เวลาที่ต้องการคืน'],
                        'readonly' => true,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'startDate' => $datetime_end,
                            'minuteStep' => 10,
                        ]
                    ]);?>
        </div>
    </div>
    <?php endif; ?>
    <!-- if ($scenario === 'update' || !(strtotime($model->datetime_start) >= strtotime('now'))) -->

    <?php if ($scenario === 'update') :?>
    <?php if ($admin) : ?>
    <div class="row">
        <?= $form->field($model, 'datetime_end')->hiddenInput([
                        'value' => date('Y-m-d H:i', strtotime('now'))
                    ])->label(false) ?>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <?= $form->field($model, 'miles_end')->textInput() ?>
        </div>
    </div>
    <?php endif; ?>
    <?php endif; ?>

    <div class="row">
        <div class="row text-center text-red">
            <div class="col-md-push-1 col-md-5">
                <div id="insurance"></div>
                <div id="car_act"></div>
                <div id="check_car"></div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="input-group">
                <?= $form->field($model, 'car_id')->widget(Select2::classname(), [
                        'language' => 'th',
                        'data' => $model->carData,
                        'options' => [
                            'placeholder' => 'เลือกรถที่ต้องการ',
                            'id' => 'car_id'
                        ],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);?>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="input-group">
                <?= $form->field($model, 'car_booking_location_id')->widget(Select2::classname(), [
                        'language' => 'th',
                        'data' => $model->locationData,
                        'options' => [
                            'placeholder' => 'เลือกสถานที่ที่ต้องการไป',
                        ],
                        'pluginOptions' => [
                            'tags' => true,
                            'allowClear' => true
                        ],
                    ]) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="input-group">
                <?= $form->field($model, 'car_booking_category_id')->widget(Select2::classname(), [
                        'language' => 'th',
                        'data' => $model->categoryData,
                        'options' => ['placeholder' => 'เลือกประเภทการใช้รถ'],
                        'pluginOptions' => [
                            'tags' => true,
                            'allowClear' => true
                        ],
                    ]);?>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="input-group">
                <?= $form->field($model, 'passengers')->widget(Select2::classname(), [
                            'data' => $model->passengersData,
                            'options' => ['placeholder' => 'เลือกผู้โดยสาร', 'multiple' => true],
                        ]);
                    ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4"></div>
        <?php if (!$model->isNewRecord && $admin) :?>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <?=$form->field($model, 'status',[
                        'template' =>
                            '<div>
                                        {label}
                                        <br>
                                        {input}
                                    </div>',
                    ])->radioButtonGroup($model->statusData);?>
        </div>
        <?php endif; ?>

        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4"></div>
    </div>

    <?= $form->field($model, 'detail')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>