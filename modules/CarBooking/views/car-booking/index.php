<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use kartik\widgets\DateTimePicker;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = Yii::$app->messageManage->car_booking;
$this->params['breadcrumbs'][] = $this->title;

$counter = 0;

$booking_id_used = [];

foreach ($model->carData as $car => $carName) {
    $query = (new \yii\db\Query())
        ->select(['id', 'datetime_start'])
        ->from('car_booking')
        ->where(['status' => 2])
        ->andWhere(['miles_end' => null])
        ->andWhere(['car_id' => $car])
        ->orderBy('datetime_start')
        ->one();
    $booking_id_used[] = $query;
}
$timeStampNow = strtotime('now');

?>

<div class="car-booking-index">
    <div class="container-fluid">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'responsive' => true,
            'responsiveWrap' => false,
//            'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
//            'headerRowOptions' => ['class' => 'kartik-sheet-style'],
//            'filterRowOptions' => ['class' => 'kartik-sheet-style'],
            'toolbar' =>  [
                ['content' => Html::a('จองรถ', ['create'], ['class' => 'btn btn-success', 'style' => 'padding: 6px 30px'])],
                '{export}',
                '{toggleData}',
            ],
            'rowOptions' => function($model, $key, $index, $column){
                if ($model->status == 1) return ['class' => 'active'];
                elseif ($model->status == 2) return ['class' => 'success'];
                elseif ($model->status == 4) return ['class' => 'info'];
                else return ['class' => 'danger'];
            },
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute'=>'datetime',
                    'format' => 'html',
                    'value' => function ($model, $index, $widget) {
                        return Yii::$app->datetimeManage->dtFormat($datetime = $model->datetime_start) . '<br>' . Yii::$app->datetimeManage->dtFormat($datetime = $model->datetime_end);
//                        return $model->datetime_start . '<br>' . $datetime = $model->datetime_end;

                    },
                    'filterType' => GridView::FILTER_DATETIME,
                    'filterWidgetOptions' => [
                        'pluginOptions' => [
                            'autoclose' => true,
                            'todayHighlight' => true,
                        ]
                    ],
                    'width' => '200px',
                    'hAlign' => 'center',
                ],
                [
                    'attribute' => 'passengers',
                    'filter'=> $searchModel->passengersData,
                    'value' => function ($model, $key, $index, $column) {
                        return $model->passengersName;
                    },
                    'hAlign' => 'center',
                ],
                [
                    'attribute' => 'car_id',
                    'value' => 'car.license_id',
                    'filter'=> $searchModel->carData,
                    'hAlign' => 'center',
                ],
                [
                    'attribute' => 'car_booking_location_id',
                    'value' => 'carBookingLocation.name',
                    'filter'=> $searchModel->locationData,
                    'hAlign' => 'center',
                ],
                [
                    'attribute' => 'car_booking_category_id',
                    'value' => 'carBookingCategory.name',
                    'filter'=> $searchModel->categoryData,
                    'hAlign' => 'center',
                ],
                [
                    'attribute' => 'status',
                    'value' => 'statusName',
                    'filter'=> $searchModel->statusData,
                    'hAlign' => 'center',
                ],
                [
                    'attribute' => 'miles_start',
                    'value' => function($model, $key, $index, $column) {
                        return $model->miles_start == null ? 'ยังไม่ได้คืน' : $model->miles_start;
                    },
                    'hAlign' => 'center',
                ],
                [
                    'attribute' => 'miles_end',
                    'value' => function($model, $key, $index, $column) {
                        return $model->miles_end == null ? 'ยังไม่ได้คืน' : $model->miles_end;
                    },
                    'hAlign' => 'center',
                ], 
                [
                    'label' => 'ระยะทางที่ใช้',
                    'value' => function($model, $key, $index, $column) {
                        return $model->miles_end == null ? 'ยังไม่ได้คืน' : $model->miles_end - $model->miles_start;
                    },
                    'hAlign' => 'center',
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {return_car} {cancel_car} {admin-edit-booking} {delete}',
                    'buttons' => [
                        'return_car' => function ($url, $model, $key) use ($booking_id_used, $timeStampNow) {
                            $count = 0;
                            foreach ($booking_id_used as $item) {
                                if ($item['id'] == $model->id) {
                                    if ($timeStampNow >= strtotime($item['datetime_start'])) {
                                        $count++;
                                    }
                                }
                            }
                            if ($count == 1) return Html::a('<i class="fa fa-key text-success"></i>', ['return-car', 'id' => $model->id]);
                        },
                        'cancel_car' => function ($url, $model, $key) use ($timeStampNow) {
//                                $count = 0;
//                                foreach ($booking_id_used as $item) {
//                                    if ($item['id'] == $model->id) $count++;
//                                }
                            return $timeStampNow >= strtotime($model->datetime_start) && $model->miles_end == null ?
                                Html::a('<i class="fa fa-close text-danger"></i>', ['cancel-car', 'id' => $model->id], [
                                    'data' => [
                                        'confirm' => 'ต้องการยกเลิกการจองนี้ใช่หรือไม่ ?',
                                        'method' => 'post',
                                    ]
                                ]) : null;
                        },
                        'delete' => function ($url, $model, $key) use ($timeStampNow) {
                            return (strtotime($model->datetime_start) >= $timeStampNow) && $model->miles_end == null ?
                                Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete', 'id' => $model->id], [
                                    'data' => [
                                        'confirm' => 'ต้องการลบรายการนี้ใช่หรือไม่ ?',
                                        'method' => 'post',
                                    ],
                                ]) : null;
                        },
                        'admin-edit-booking' => function ($url, $model, $key) {
                            $admin = Yii::$app->user->can('Admin');
                            // return Html::a('<i class="fa fa-key text-success"></i>', ['return-car', 'id' => $model->id]);
                            return  $model->miles_end !== null && $admin ?
                                Html::a('<i class="fa fa-wrench"></i>', ['admin-edit-booking', 'id' => $model->id]) : null;
                        },
                    ]
                ],
            ],
            'panel' => [
                'type' => GridView::TYPE_PRIMARY,
            ],
        ]); ?>
    </div>
</div>