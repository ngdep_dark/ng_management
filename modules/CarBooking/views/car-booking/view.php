<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\BookingCar\models\CarBooking */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->car_booking, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$query = (new \yii\db\Query())
    ->from('car_booking')
    ->where(['status' => 2])
    ->andWhere(['miles_end' => null])
    ->andWhere(['car_id' => $model->car_id])
    ->orderBy('datetime_start')
    ->scalar();

$car_booking_id = $model->id;
?>
<div class="car-booking-view">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6">
            <P>
                <?= Html::a(Yii::$app->messageManage->update_button, ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= $model->id == $query && !((strtotime($model->datetime_start) >= strtotime('now'))) ?
                    Html::a('คืนรถ', ['return-car', 'id' => $model->id], [
                        'class' => 'btn btn-success',
                    ]) : null ;?>
                <?= strtotime('now') >= strtotime($model->datetime_start) && $model->miles_end == null ?
                    Html::a('ยกเลิกรายการ', ['cancel-car', 'id' => $model->id], [
                        'class' => 'btn btn-warning',
                        'data' => [
                            'confirm' => 'ต้องการยกเลิกการจองใช่หรือไม่ ?',
                            'method' => 'post',
                        ]
                    ]) : null ;?>
                <?= (strtotime($model->datetime_start) >= strtotime('now')) && $model->miles_end == null ?
                    Html::a(Yii::$app->messageManage->delete_button, ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => Yii::$app->messageManage->confirm,
                            'method' => 'post',
                        ],
                    ]) : null ;?>
            </P>
            <div>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title text-center">รายละเอียดการจองรถ</h3>
                    </div>

                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            [
                                'attribute' => 'datetime_start',
                                'value' => Yii::$app->datetimeManage->dtFormat($datetime = $model->datetime_start)
                            ],
                            [
                                'attribute' => 'datetime_end',
                                'value' => Yii::$app->datetimeManage->dtFormat($datetime = $model->datetime_end)
                            ],
                            [
                                'attribute' => 'miles_start',
                                'value' => function($model, $widget) {
                                    return $model->miles_start == null ? 'ยังไม่ได้คืน' : $model->miles_start;
                                }
                            ],
                            [
                                'attribute' => 'miles_end',
                                'value' => function($model, $widget) {
                                    return $model->miles_end == null ? 'ยังไม่ได้คืน' : $model->miles_end;
                                }
                            ],
                            'car.license_id',
                            'carBookingLocation.name',
                            'carBookingCategory.name',
                            [
                                'attribute' => 'status',
                                'label' => 'สถานะ',
                                'value' => $model->statusName,
                            ],
                            [
                                'attribute' => 'passengers',
                                'value' => function ($model, $widget) {
                                    return $model->setPassengersName();
                                },
                            ],
                            [
                                'attribute' => 'created_at',
                                'value' => Yii::$app->datetimeManage->tsFormat($datetime = $model->created_at)
                            ],
                            [
                                'label' => Yii::$app->messageManage->create_by,
                                'attribute' => 'createBy.full_name',
                            ],
                            [
                                'attribute' => 'updated_at',
                                'value' => Yii::$app->datetimeManage->tsFormat($datetime = $model->updated_at)
                            ],
                            [
                                'label' => Yii::$app->messageManage->update_by,
                                'attribute' => 'updatedBy.full_name',
                            ],
                            'detail',

                        ],
                    ]) ?>
                </div>
            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-6">
            <?php if ($model->return_img) : ?>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title text-center">รูปเลขไมล์คืนรถ</h3>
                </div>
                <div class="panel-body">
                    <img src="<?=$model->return_img?>" width="100%" height="100%">
                </div>
            </div>
            <?php endif; ?>

        </div>
    </div>