<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'สถานะการจองรถของท่าน';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-booking-index">
    <div class="container">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'rowOptions' => function($model, $key, $index, $column){
                if ($model->status == 1) return ['class' => 'active'];
                elseif ($model->status == 2) return ['class' => 'success'];
                elseif ($model->status == 4) return ['class' => 'info'];
                elseif ($model->status == 5) return ['class' => 'warning'];
                else return ['class' => 'danger'];
            },
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'datetime_start',
                'datetime_end',
                [
                    'attribute' => 'car_id',
                    'value' => 'car.license_id',
                    'filter'=> $searchModel->carData,
                ],
                [
                    'attribute' => 'car_booking_location_id',
                    'value' => 'carBookingLocation.name',
                    'filter'=> $searchModel->locationData,
                ],
                [
                    'attribute' => 'car_booking_category_id',
                    'value' => 'carBookingCategory.name',
                    'filter'=> $searchModel->categoryData,
                ],
                [
                    'attribute' => 'status',
                    'value' => 'statusName',
                    'filter'=> $searchModel->statusData,
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{return_car} {cancel_car}',
                    'buttons' => [
                        'return_car' => function ($url, $model, $key) {
                            $query = (new \yii\db\Query())
                                ->from('car_booking')
                                ->where(['status' => 2])
                                ->andWhere(['miles_end' => null])
                                ->scalar();
                            return $model->id == $query ?
                                Html::a('<i class="fa fa-key text-success"></i>', ['return-car', 'id' => $model->id], [
                                    'data' => [
                                        'confirm' => 'ต้องการลบรายการนี้ใช่หรือไม่ ?',
                                        'method' => 'post',
                                    ]
                                ]) :
                                null;
                        },
                        'cancel_car' => function ($url, $model, $key) {
                            $query = (new \yii\db\Query())
                                ->from('car_booking')
                                ->where(['status' => 2])
                                ->andWhere(['miles_end' => null])
                                ->scalar();
                            return $model->id == $query ?
                                Html::a('<i class="fa fa-close text-danger"></i>', ['cancel-car', 'id' => $model->id], [
                                    'data' => [
                                        'confirm' => 'ต้องการลบรายการนี้ใช่หรือไม่ ?',
                                        'method' => 'post',
                                    ]
                                ]) :
                                null;
                        },
                    ]
                ],
            ],
        ]); ?>
    </div>
</div>
