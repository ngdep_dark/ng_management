<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\BookingCar\models\CarBooking */

$this->title = 'สร้าง' . Yii::$app->messageManage->car_booking;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->car_booking, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$currentUrl = Yii::$app->request->url;

$counter = 0;
?>

<div class="car-booking-create">
    <?php if ($overlaps) : ?>
    <?= yii\grid\GridView::widget([
            'dataProvider' => $carCurrentStatusProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'license_id',
                    'label' => Yii::$app->messageManage->car_license,
                ],
                [
                    'attribute' => 'location',
                    'label' => Yii::$app->messageManage->cb_location_name,
                ],
                [
                    'attribute' => 'category',
                    'label' => Yii::$app->messageManage->cb_category_name,
                ],
                [
                    'attribute' => 'passengers',
                    'label' => Yii::$app->messageManage->cb_passengers,
                ],
                [
                    'attribute' => 'date_used',
                    'label' => 'วันที่ใช้รถ',
                ],
            ]
        ]);?>
    <?php endif; ?>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>