<?php


/* @var $this yii\web\View */
/* @var $model app\modules\BookingCar\models\CarBooking */

$this->title = 'อนุมัติการจองรถตามวันที่';
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->car_booking, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

use yii\helpers\Html;

$count_data = count($data);

$this->registerCss("
    .center{
        text-align: center
    }
");

$first_time = 0;

?>

<div class="car-booking-overall-approve-booking">
    <div class="container">
        <?php if ($count_data) : ?>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11">
                <div class="panel-group" id="car">
                    <!--start car-->
                    <?php foreach ($data as $car_name => $carValue) : ?>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <?= $car_name ?>
                                </h3>
                            </div>
                            <div class="panel-body">

                                <div class="panel-group" id="date<?= $car_name . key($carValue) ?>">
                                    <!--start car-->
                                    <?php foreach ($carValue as $dateItems => $dateValue) : ?>
                                        <div class="panel panel-success">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse"
                                                       data-parent="#date<?= $car_name . key($carValue) ?>"
                                                       href="#<?= $car_name . $dateItems ?>">
                                                        <?= $dateItems ?>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="<?= $car_name . $dateItems ?>"
                                                 class="panel-collapse collapse <?= $first_time == 0 ? 'in' : null ?>">
                                                <?php $first_time++ ?>
                                                <!--start table-->
                                                <table class="table table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th class="center">#</th>
                                                        <th class="center">เวลาที่จอง</th>
                                                        <th class="center">เวลาที่คืน</th>
                                                        <th class="center">สถานที่</th>
                                                        <th class="center">วัตถุประสงค์</th>
                                                        <th class="center">ผู้โดยสาร</th>
                                                        <th class="center"></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach ($dateValue as $items => $value) : ?>
                                                        <tr>
                                                            <td class="center"><?= $items + 1 ?></td>
                                                            <td class="center"><?= substr($value['datetime_start'], '10') ?></td>
                                                            <td class="center"><?= Yii::$app->datetimeManage->dtFormat($value['datetime_end'], true) ?></td>
                                                            <td class="center"><?= $value['location'] ?></td>
                                                            <td class="center"><?= $value['category'] ?></td>
                                                            <td class="center"><?= $value['passengers'] ?></td>
                                                            <td class="center">
                                                                <?php
                                                                $model = \app\modules\CarBooking\models\CarBooking::findOne($value['id']);
                                                                $query = \app\modules\CarBooking\models\CarBooking::find()
                                                                    ->where(['(:datetime_start, :datetime_end) OVERLAPS (datetime_start, datetime_end)' => true])
                                                                    ->andWhere(['car_id' => $model->car_id])
                                                                    ->andWhere(['status' => 1])
                                                                    ->addParams([
                                                                        ':datetime_start' => $model->datetime_start,
                                                                        ':datetime_end' => $model->datetime_end
                                                                    ])
                                                                    ->orderBy('id')
                                                                    ->asArray()
                                                                    ->all();

                                                                $findId = count(array_values(\yii\helpers\ArrayHelper::map($query,'id','id')));
                                                                ?>

                                                                <?= $findId > 1 ?
                                                                    Html::a(
                                                                        '<span class="glyphicon glyphicon-eye-open"></span>',
                                                                        [
                                                                            '/car-booking/car-booking/approve-booking',
                                                                            'id' => $value['id']
                                                                        ]) :
                                                                    null
                                                                ?>

                                                                <?= $findId == 1 ?
                                                                    Html::a(
                                                                        '<i class="text-success fa fa-check"></i>',
                                                                        [
                                                                            '/car-booking/car-booking/one-approve-booking',
                                                                            'id' => $value['id'],
                                                                        ],
                                                                        [
                                                                            'data' => [
                                                                                'confirm' => 'ต้องการอนุมัติรายการนี้ใช่หรือไม่ ?',
                                                                                'method' => 'post',
                                                                            ]
                                                                        ]) :
                                                                    null
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                    </tbody>
                                                </table>
                                                <!--table end-->
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                    <?php $first_time = 0; ?>
                                    <!--end car-->
                                </div>

                            </div>
                        </div>
                    <?php endforeach; ?>
                    <!--end car-->
                </div>
            </div>
        </div>
    </div>
    <?php else : ?>
        <h1 style="color: red; text-align: center">ไม่มีรายการจองในขณะนี้</h1>
    <?php endif; ?>
</div>
</div>