<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\BookingCar\models\CarBooking */

$this->title = 'แก้ไขเลขไมค์ย้อนหลัง';
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->car_booking, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-booking-return-car">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>


    <div class="row">
        <?php if ($model->return_img) : ?>
        <div class="col-xs-6 col-xs-offset-3">
            <div class="panel panel-default">
                <div class="panel-body">
                    <img src="<?=$model->return_img?>" alt="" width="100%" height="100%">
                </div>
            </div>
        </div>
        <?php endif; ?>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <?= $form->field($model, 'miles_start')->textInput() ?>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <?= $form->field($model, 'miles_end')->textInput() ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success','style'=> 'margin-top: 25px']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>