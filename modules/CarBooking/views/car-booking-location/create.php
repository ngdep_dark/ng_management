<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\BookingCar\models\CarBookingLocation */

$this->title = 'สร้าง' . Yii::$app->messageManage->cb_location_name;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->cb_location_name, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-booking-location-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
