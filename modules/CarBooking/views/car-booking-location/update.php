<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\BookingCar\models\CarBookingLocation */

$this->title = 'แก้ไข' . Yii::$app->messageManage->cb_location_name . ': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->messageManage->cb_location_name, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'แก้ไข';
?>
<div class="car-booking-location-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
