<?php
use yii\widgets\Pjax;
/* @var $this yii\web\View */
$this->title = Yii::$app->messageManage->dash_board;
?>
<div class="dash-board-calendar">
    <?php Pjax::begin(); ?>
    <?= \yii\helpers\Html::a("Refresh", [
            '/car-booking/dash-board'],
            [
                'id' => 'refreshButton',
                'style' => 'display: none-img'
            ])
        ?>

    <div class="row">
        <div class="col-md-push-1 col-lg-push-1 col-xs-12 col-sm-12 col-md-5 col-lg-5">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">สถานะปัจจุบัน</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                    <div class="box-body">
                        <?= yii\grid\GridView::widget([
                                'tableOptions' => ['class' => 'table table-striped table-bordered text-center'],
                                'dataProvider' => $carCurrentStatusProvider,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
                                    [
                                        'attribute' => 'license_id',
                                        'label' => Yii::$app->messageManage->car_license,
                                    ],
                                    [
                                        'attribute' => 'location',
                                        'label' => Yii::$app->messageManage->cb_location_name,
                                    ],
                                    [
                                        'attribute' => 'category',
                                        'label' => Yii::$app->messageManage->cb_category_name,
                                    ],
                                    [
                                        'attribute' => 'passengers',
                                        'label' => Yii::$app->messageManage->cb_passengers,
                                    ],
                                    [
                                        'attribute' => 'date_used',
                                        'label' => 'วันที่ใช้รถ',
                                    ],
                                ]

                            ]);?>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-md-push-1 col-lg-push-1 col-xs-12 col-sm-12 col-md-5 col-lg-5">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">สถานะการตรวจใช้งานรถ</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                    <div class="box-body">
                        <?= yii\grid\GridView::widget([
                                'dataProvider' => $carCheckStatusProvider,
                                'tableOptions' => ['class' => 'table table-striped table-bordered text-center'],
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
                                    [
                                        'attribute' => 'license_id',
                                        'label' => 'ทะบียนรถ',
                                        'headerOptions' => [
                                            'class' => 'text-center'
                                        ]
                                    ],
//                                    [
//                                        'attribute' => 'insurance',
//                                        'value' => function($model,$key,$index) {
//                                            return $model['insurance'] === true ?
//                                                '<i class="text-danger fa fa-close"></i>' :
//                                                '<i class="text-success fa fa-check"></i>';
//                                        },
//                                        'format' => 'html',
//                                        'label' => 'สถานะประกัน',
//                                        'headerOptions' => [
//                                            'class' => 'text-center'
//                                        ]
//                                    ],
//                                    [
//                                        'attribute' => 'car_act',
//                                        'value' => function($model,$key,$index) {
//                                            return $model['car_act'] === true ?
//                                                '<i class="text-danger fa fa-close"></i>' :
//                                                '<i class="text-success fa fa-check"></i>';
//                                        },
//                                        'format' => 'html',
//                                        'label' => 'สภานะ พ.ร.บ.',
//                                        'headerOptions' => [
//                                            'class' => 'text-center'
//                                        ]
//                                    ],
//                                    [
//                                        'attribute' => 'check_car',
//                                        'value' => function($model,$key,$index) {
//                                            return $model['check_car'] === true ?
//                                                '<i class="text-danger fa fa-close"></i>' :
//                                                '<i class="text-success fa fa-check"></i>';
//                                        },
//                                        'format' => 'html',
//                                        'label' => 'ระยะตรวจเช็คเข้าศูนย์',
//                                        'headerOptions' => [
//                                            'class' => 'text-center'
//                                        ]
//                                    ],
                                    [
                                        'attribute' => 'insurance',
                                        'value' => function($model,$key,$index) {
                                            return \Yii::$app->datetimeManage->dtFormat($model['insurance'],true ,true);
                                        },
                                        'format' => 'html',
                                        'label' => 'สถานะประกัน',
                                        'encodeLabel' => false,
                                        'headerOptions' => [
                                            'class' => 'text-center'
                                        ],
                                        "contentOptions" =>  function ($model, $key, $index, $column) {
                                            $insuranceDateExp = new \DateTime($model['insurance']);
                                            $nowDate = date_create('now');
                                            $interval = date_diff($nowDate, $insuranceDateExp);
                                            $diffDays = $interval->format('%R%a');
                                            if (intval($diffDays) <=0) {
                                                return ['style' => 'color: red'];
                                            } elseif (intval($diffDays) <= 30) {
                                                return ['style' => 'color: #ff993f'];
                                            } else {
                                                return ['style' => 'color: black'];
                                            }
                                        }
                                    ],
                                    [
                                        'attribute' => 'car_act',
                                        'value' => function($model,$key,$index) {
                                            return \Yii::$app->datetimeManage->dtFormat($model['car_act'], true, true);
                                        },
                                        'format' => 'html',
                                        'label' => 'สภานะ พ.ร.บ.',
                                        'headerOptions' => [
                                            'class' => 'text-center'
                                        ],
                                        "contentOptions" =>  function ($model, $key, $index, $column) {
                                            $carActExp = new \DateTime($model['car_act']);
                                            $nowDate = date_create('now');
                                            $interval = date_diff($nowDate, $carActExp);
                                            $diffDays = $interval->format('%R%a');
                                            if (intval($diffDays) <=0) {
                                                return ['style' => 'color: red'];
                                            } elseif (intval($diffDays) <= 30) {
                                                return ['style' => 'color: #ff993f'];
                                            } else {
                                                return ['style' => 'color: black'];
                                            }
                                        }
                                    ],
                                    [
                                        'attribute' => 'check_car',
                                        'value' => function($model,$key,$index) {
                                            return $model['check_car'];
                                        },
                                        'format' => 'html',
                                        'label' => 'ระยะตรวจเช็คเข้าศูนย์',
                                        'headerOptions' => [
                                            'class' => 'text-center'
                                        ],
                                        "contentOptions" =>  function ($model, $key, $index, $column) {
                                            $milesLastCarCheck = $model['check_car'];

                                            $car_miles = \app\modules\CarBooking\models\Car::find()->select('miles')->where(['license_id' => $key])->scalar();
                                            if ($car_miles > $milesLastCarCheck) {
                                                return ['style' => 'color: red'];
                                            } else if ($car_miles > $milesLastCarCheck - 1000) {
                                                return ['style' => 'color: #ff993f'];
                                            } else {
                                                return ['style' => 'color: black'];
                                            }
                                        }
                                    ],
                                ]
                            ]);
                            ?>
                    </div>
                </form>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-push-1 col-xs-12 col-sm-12 col-md-12 col-lg-5">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">ปฏิทินการใช้งาน</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                    <div class="box-body">
                        <?= yii2fullcalendar\yii2fullcalendar::widget([
                            'options' => [
                                'fixedWeekCount' => false,
                                
                                'lang' => 'th',
                                'defaultDate' => '2010-03-12'
                            ],
                            'events' => \yii\helpers\Url::to(['/car-booking/car-booking/json-calendar'])
                        ]);?>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-md-push-1 col-lg-push-1 col-xs-12 col-sm-12 col-md-12 col-lg-5">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">แผนภาพแสดงการใช้งานรถ/คัน/เดือน</h3>
                </div>
                <!-- /.box-header -->
                <form role="form">
                    <div class="box-body">
                        <?= \dosamigos\chartjs\ChartJs::widget([
                                'type' => 'bar',
                                'options' => [
                                    'height' => 200,
                                    'width' => 350,
                                ],
                                'data' => [
                                    'labels' => ['ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'],
                                    'datasets' => $milesPerMonthEachChart
                                ]
                            ]);
                            ?>
                    </div>
                    <!-- /.box-body -->
                </form>
            </div>
        </div>
        <div class="col-md-push-1 col-lg-push-1 col-xs-12 col-sm-12 col-md-12 col-lg-5">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">ยอดใบสั่งที่ยังไม่ได้จ่าย</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form role="form">
                    <div class="box-body">
                        <?= yii\grid\GridView::widget([
                                'tableOptions' => ['class' => 'table table-striped table-bordered text-center'],
                                'dataProvider' => $trafficTicketNotPay,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
                                    [
                                        'attribute' => 'count',
                                        'label' => 'จำนวน/ใบ',
                                    ],
                                    [
                                        'attribute' => 'sum',
                                        'label' => 'ยอดค่าใช้จ่ายทั้งหมด',
                                    ]
                                ]

                            ]);?>
                    </div>
                    <!-- /.box-body -->
                </form>
            </div>
        </div>

    </div>
    <?php Pjax::end(); ?>
</div>
<?php
$this->registerJs(<<<JS
$(document).ready(function() {
    setInterval(function(){
        $("#refreshButton").click(); 
    }, 300000);
});
JS
);