<?php

namespace app\modules\CarBooking\controllers;

use app\modules\CarBooking\models\Car;
use app\modules\CarBooking\models\CarBooking;
use app\modules\CarBooking\models\TrafficTicket;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class DashBoardController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $carCheckStatusProvider = $this->carCheckStatusProvider();
        $carCurrentStatusProvider = $this->carCurrentStatusProvider();
        $milesPerMonthEachChart = $this->milesPerMonthEachCar();
        $trafficTicketNotPay = $this->trafficTicketNotPay();

        return $this->render('index', [
            'carCheckStatusProvider' => $carCheckStatusProvider,
            'carCurrentStatusProvider' => $carCurrentStatusProvider,
            'milesPerMonthEachChart' => $milesPerMonthEachChart,
            'trafficTicketNotPay' => $trafficTicketNotPay
        ]);
    }

    private function carCheckStatusProvider() {
        $carCheckStatusQuery = Car::find()->all();
        $carCheckStatusData = [];
        foreach ($carCheckStatusQuery as $items => $value) {
            $carCheckStatusData[$value->license_id]['license_id'] = $value->license_id;
//            $carCheckStatusData[$value->license_id]['insurance'] = !is_null($value->validateInsuranceDate()) ? true : false;
//            $carCheckStatusData[$value->license_id]['car_act'] = !is_null($value->validateCarActDate()) ? true : false;
//            $carCheckStatusData[$value->license_id]['check_car'] = !is_null($value->validateMilesLastCarCheck()) ? true : false;
            $carCheckStatusData[$value->license_id]['insurance'] = $value->insurance_date;
            $carCheckStatusData[$value->license_id]['car_act'] = $value->car_act_date;
            $carCheckStatusData[$value->license_id]['check_car'] = $value->miles_last_check;
        }

        return $carCheckStatusProvider = new ArrayDataProvider([
            'allModels' => $carCheckStatusData,
            'sort' => [
                'attributes' => ['license_id', 'insurance', 'car_act', 'check_car'],
            ],

        ]);
    }

    private function trafficTicketNotPay() {
        $trafficNotPayQuery = (new Query())
            ->select(['COUNT(id)', 'SUM(price)'])
            ->from('traffic_ticket')
            ->where(['payment_at' => null])
            ->all();

        return new ArrayDataProvider([
            'allModels' => $trafficNotPayQuery
        ]);
    }

    public function carCurrentStatusProvider ($datetimeStart = null, $datetimeEnd = null) {
        if ($datetimeStart == null && $datetimeEnd == null) {
            $datetimeStart = \Yii::$app->formatter->asDateTime(new \DateTime('NOW'), 'php:Y-m-d H:i:s');
            $datetimeEnd = \Yii::$app->formatter->asDateTime(new \DateTime('NOW'), 'php:Y-m-d H:i:s');
        }

        $carCurrentStatusSubQuery = (new Query())
            ->select(['id', 'car_id'])
            ->from('car_booking')
            ->where(['(:datetime_start, :datetime_end) OVERLAPS (datetime_start, datetime_end)' => true], [
                ':datetime_start' => $datetimeStart,
                ':datetime_end' => $datetimeEnd
            ])
            ->andWhere(['status' => 2]);

        $carCurrentStatusQuery = (new Query())
            ->select(['car.id car_id', 'car_use.id booking_id'])
            ->from('car')
            ->join('left outer join', ['car_use' => $carCurrentStatusSubQuery], 'car.id=car_use.car_id')
            ->all();

        $carCurrentStatusData = [];

        foreach ($carCurrentStatusQuery as $items => $value) {
            $car = Car::findOne($value['car_id']);
            $carCurrentStatusData[$items]['license_id'] = $car->license_id;

            if (!is_null($value['booking_id'])) {
                $car_booking = CarBooking::findOne($value['booking_id']);
                $carCurrentStatusData[$items]['location'] = $car_booking->carBookingLocation->name;
                $carCurrentStatusData[$items]['category'] = $car_booking->carBookingCategory->name;
                $carCurrentStatusData[$items]['passengers'] = $car_booking->passengersName;
                $carCurrentStatusData[$items]['date_used'] = \Yii::$app->datetimeManage->dtFormat($car_booking->datetime_start,true) . ' - ' . \Yii::$app->datetimeManage->dtFormat($car_booking->datetime_end,true);
            } else {
                $carCurrentStatusData[$items]['location'] = '-';
                $carCurrentStatusData[$items]['category'] = '-';
                $carCurrentStatusData[$items]['passengers'] = '-';
                $carCurrentStatusData[$items]['date_used'] = '-';
            }
        }

        return $carCurrentStatusProvider = new ArrayDataProvider([
            'allModels' => $carCurrentStatusData,
            'sort' => [
                'attributes' => ['license_id', 'location', 'category', 'passengers', 'date_used'],
            ],
        ]);
    }

    private function milesPerMonthEachCar () {
        $query = CarBooking::find()
            ->select([
                'Extract (MONTH FROM datetime_start) - 1 AS month',
                'Extract(YEAR FROM datetime_start) AS year',
                'Sum(miles_end - miles_start) AS miles',
                'car_id'
            ])
            ->where(['is not', 'miles_end', null])
            ->andWhere(['Extract(YEAR FROM datetime_start)' => date('Y',strtotime('now'))])
            ->groupBy(['car_id', 'month', 'year'])
            ->orderBy([
                'car_id' => SORT_ASC,
                'month'  => SORT_ASC,
                'year'  => SORT_ASC,
            ])
            ->asArray()
            ->all();
        $query = ArrayHelper::map($query,'month', 'miles', 'car_id');

//        $list_month_name = ['ม.ค.','ก.พ.','ม.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'];

        $color = ['red','blue','green','yellow','orange'];

        $carPerMonth = [];

        foreach ($query as $car => $value) {
            for ($i = 0; $i < 12; $i++) {
                $carPerMonth[$car][$i] = 0;
                foreach ($value as $month => $miles) {
                    if ($i === $month) {
                        $carPerMonth[$car][$i] += $miles;
                    }
                }
            }
        }
        

        // foreach ($query as $item => $value) {
        //     ksort($query[$item],1);
        // }

        $carName = CarBooking::getCarData();

        $count = 0;
        foreach ($carPerMonth as $item => $value) {
            $data[] = [
                'label' => $carName[$item],
                'backgroundColor' => $color[$count],
                'borderColor' => 'yellow',
                'borderWidth' => 1,
                'data' => $value
            ];
            $count++;
        }

        return $data;
    }
}