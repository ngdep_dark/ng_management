<?php

namespace app\modules\CarBooking\controllers;

use Yii;
use app\modules\CarBooking\models\Car;
use app\modules\CarBooking\models\TrafficTicket;
use app\modules\CarBooking\models\CarBooking;
use app\modules\CarBooking\models\CarBookingSearch;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use app\modules\CarBooking\models\CarBookingCategory;
use app\modules\CarBooking\models\CarBookingLocation;
use app\modules\CarBooking\models\CarBookingPassenger;

use yii\web\Response;
use yii2fullcalendar\models\Event;

use yii\web\UploadedFile;


use Google_Client;
use Google_Service_Drive;
use Google_Service_Drive_DriveFile;
/**
 * CarBookingController implements the CRUD actions for CarBooking model.
 */
class CarBookingController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionChildAccount() {
        $model = new CarBooking();

        $list = $model->getPassengersData();
        $data = [];
        if (isset($_POST['depdrop_parents'])) {

            $id = end($_POST['depdrop_parents']);
            if ($id != null && count($list) > 0) {
//                unset($list[$id]);
                foreach ($list as $item => $value) {
                    if ($item != $id) {
                        $data[$item]['id'] = $item;
                        $data[$item]['name'] = $value;
                    }
                }

                echo Json::encode(['output' => $data, 'selected'=>'']);
                return;
            }
        }
        echo Json::encode(['output' => '', 'selected'=>'']);
    }

    public function actionJsonCalendar(){

        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = CarBooking::find()
            ->where(['status' => 2])
            ->orWhere(['and',['status' => 3], ['>', 'miles_end', 0]])
            ->all();

        $events = [];

        if (!is_null($model)) {
            foreach ($model AS $item){
                $Event = new Event();
                $Event->id = $item->id;
                $Event->title = $item->car->license_id. ' ' .$item->carBookingCategory->name. ' ' .$item->carBookingLocation->name;
                $Event->start = $item->datetime_start;
                $Event->end = $item->datetime_end;

                $events[] = $Event;
            }
        }
        return $events;
    }
    /**
     * Lists all CarBooking models.
     * @return mixed
     */

    public function actionIndex()
    {
        $model = new CarBooking();
        $model->scenario = 'index';

        $now = date_create();
        $datetime = date_format($now, 'Y-m-d H:i:s');

        $post = Yii::$app->request->post();

        if ($post) {
            $post_datetime = $post['CarBooking']['datetime'];
            if (!empty($post_datetime)) {
                $datetime = $post['CarBooking']['datetime'].':00';
            }
        }

        $data = $this->compareOverlabsBooking($datetime_start = $datetime, $datetime_end = $datetime);

        $searchModel = new CarBookingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CarBooking model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionView($id)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => TrafficTicket::find()
                ->where(['car_booking_id' => $id]),
        ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new CarBooking model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $overlaps = false;
        $data = null;
        $scenario = 'create';
        $model = new CarBooking();
        $model->scenario = 'create';
        $model->status = 2;
        $post = Yii::$app->request->post();

        if ($post && $model->load($post)) {

            $model->validateUseCar();
            $overlaps = $model->validateDatetimeOverlaps();
            $locationModel = new CarBookingLocation();
            $location = CarBookingLocation::findOne(['name' => $model->car_booking_location_id]);
            $model = Yii::$app->dbManage->select2Save($model, 'car_booking_location_id', $locationModel, $location, 'carBookingLocation');

            $categoryModel = new CarBookingCategory();
            $category = CarBookingCategory::findOne(['name' => $model->car_booking_category_id]);
            $model = Yii::$app->dbManage->select2Save($model, 'car_booking_category_id', $categoryModel, $category, 'carBookingCategory');
            if ($model->save()) {
                $this->passengersCreateSave($model);
            }
        }

        $carCurrentStatusProvider = DashBoardController::carCurrentStatusProvider($model->datetime_start, $model->datetime_end);

        return $this->render('create', [
            'model' => $model,
            'overlaps' => $overlaps,
            'carCurrentStatusProvider' => $carCurrentStatusProvider
        ]);
    }

    /**
     * Updates an existing CarBooking model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $query = CarBooking::find()
            ->where(['status' => 2])
            ->andWhere(['miles_end' => null])
            ->andWhere(['car_id' => $model->car_id])
            ->orderBy('datetime_start')
            ->scalar();

        if ((strtotime($model->datetime_start) >= strtotime('now'))) {
            $model->scenario = 'create';
        }
        else if ($model->miles_end !== null) {
            $model->scenario = 'default';
        }
        else if ($query == $model->id) {
            $model->scenario = 'update';
        }

        $post = Yii::$app->request->post();

        if ($post && $model->load($post)) {

            if (empty($model->miles_end)) $model->miles_end = null;

            $locationModel = new CarBookingLocation();
            $location = CarBookingLocation::findOne(['name' => $model->car_booking_location_id]);
            $model = Yii::$app->dbManage->select2Save($model, 'car_booking_location_id', $locationModel, $location, 'carBookingLocation');

            $categoryModel = new CarBookingCategory();
            $category = CarBookingCategory::findOne(['name' => $model->car_booking_category_id]);
            $model = Yii::$app->dbManage->select2Save($model, 'car_booking_category_id', $categoryModel, $category, 'carBookingCategory');

            if ($model->save()) {

                if ($model->miles_end) {

                    $queryOverlab = CarBooking::find()
                        ->where(['(:datetime_start, :datetime_end) OVERLAPS (datetime_start, datetime_end)' => true])
                        ->andWhere(['status' => 2])
                        ->andWhere(['<>', 'id', $model->id])
                        ->groupBy(['id', 'car_id'])
                        ->addParams([
                            ':datetime_start' => $model->datetime_start,
                            ':datetime_end' => $model->datetime_end
                        ])
                        ->asArray()
                        ->all();
                    // modify status of other booking to cancel if datetime_end more than booking end time
                    if ($queryOverlab) {
                        $id_list = array_keys(ArrayHelper::map($queryOverlab,'id','id'));

                        foreach ($id_list as $id) {
                            $carBookingOverlabModel = CarBooking::findOne($id);
                            $carBookingOverlabModel->status = 4;
                            $carBookingOverlabModel->save(false);
                        }
                    }

                    $queryUpdateLowMilesStart = CarBooking::find()
                        ->where(['>=', 'datetime_start', $model->datetime_start])
                        ->andWhere(['status' => [1,2]])
                        ->andWhere(['<>','id', $model->id])
                        ->andWhere(['<=', 'miles_start', $model->miles_end])
                        ->orderBy('datetime_start')
                        ->all();

                    if ($queryUpdateLowMilesStart) {
                        foreach ($queryUpdateLowMilesStart as $queryItem) {
                            $queryItem->miles_start = $model->miles_end;
                            $queryItem->save(false);
                        }
                    }
                }

                $this->passengersUpdateSave($model);
            }
        }

        return $this->render('update', [
            'model' => $model,
//            'scenario' => $scenario
        ]);
    }

    public function actionReturnCar($id)
    {
        // condition for model validate have
        // 1. should fill model by find id and send to form
        // 2. when listen from form set scenario name like 'no' => ??? in model validate
        $model = $this->findModel($id);
        $model->scenario = 'return';
        $model->datetime_end = null;

        $post = Yii::$app->request->post();
        
       

        
        if ($post && $model->load($post)) {
            $model->validateReturnCar();
            $car_miles = (new Query())
                ->select('miles')
                ->from('car')
                ->where(['id' => $model->car_id,])
                ->scalar();

            $model->miles_start = $car_miles;

            $model->file = UploadedFile::getInstance($model, 'file');

            $client = $this->connectGoogleDrive();
            $service = new Google_Service_Drive($client);

            $folderId = '1blFcZSwZBEYEvSC3qwysr-2ypxr-NFiW';
            $fileMetadata = new Google_Service_Drive_DriveFile([
                'name' => $model->id.'.'.$model->file->extension,
                'parents' => [$folderId]
            ]);
            
            $content = file_get_contents($model->file->tempName);
            $file = $service->files->create($fileMetadata, [
                'data' => $content,
                'mimeType' => $model->file->type,
                'uploadType' => 'multipart',
                'fields' => 'id'
            ]);
            // printf("File ID: %s\n", $file->id);
            $model->return_img = 'https://drive.google.com/thumbnail?id='.$file->id.'&sz=w800-h640';
            
            
            if($model->save()) {
                $carModel = Car::findOne($model->car_id);
                $carModel->miles = $model->miles_end;
                if ($carModel->save()) {

                    // $queryUpdateMilesStart = CarBooking::find()
                    //     ->where(['>=', 'datetime_start', $model->datetime_start])
                    //     ->andWhere(['<>', 'status', 4])
                    //     ->andWhere(['car_id' => $model->car_id])
                    //     ->andWhere(['<>','id', $model->id])
                    //     ->asArray()
                    //     ->all();

                    // $all_id = array_column($queryUpdateMilesStart,'id');

                    // if (isset($all_id) && !is_null($all_id)) {
                    //     foreach ($all_id as $id) {
                    //         $updateModel = CarBooking::findOne($id);
                    //         $updateModel->miles_start = $model->miles_end;
                    //         $updateModel->save(false);
                    //     }
                    // }
                    // 5
                    return $this->redirect(['index']);
                }
            }
        }

        return $this->render('return-car', [
            'model' => $model,
        ]);
    }

    public function actionAdminEditBooking($id)
    {
        // condition for model validate have
        // 1. should fill model by find id and send to form
        // 2. when listen from form set scenario name like 'no' => ??? in model validate
        $model = $this->findModel($id);
        $model->scenario = 'edit';

        $post = Yii::$app->request->post();
            
        if ($post && $model->load($post)) {
            if($model->save()) {
                $findBookingInNear = CarBooking::find()
                ->where(['car_id' => $model->car_id,])
                ->andWhere(['>', 'datetime_start', $model->datetime_start])
                ->asArray()
                ->all();
            
                if (empty($findBookingInNear)) {
                    $carModel = Car::findOne($model->car_id);
                    $carModel->miles = $model->miles_end;
                    $carModel->save();
                } 
                
                return $this->redirect(['index']);
            }
        }

        return $this->render('admin-edit-booking', [
            'model' => $model,
        ]);
    }

    public function actionOneApproveBooking($id)
    {
        $model = $this->findModel($id);
        $model->status = 2;
        if ($model->save()) {
            return $this->redirect(['index']);
        }
    }

    public function actionCancelCar($id)
    {
        $model = $this->findModel($id);
        $model->status = 4;
        if ($model->save()) {
            return $this->redirect(['index']);
        }
    }

    /**
     * Deletes an existing CarBooking model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    private function passengersCreateSave($model){
        // for add new passenger to car booking_log
        foreach ($model->passengers as $passenger) {
            $passengerSearch = CarBookingPassenger::find()
                ->where([
                    'car_booking_id' => $model->id,
                    'user_id' => $passenger
                ])->scalar();
            // check have passengerPost ? no insert
            if ($passengerSearch == null) {
                $passengerModel = new CarBookingPassenger;
                $passengerModel->user_id = $passenger;
                $passengerModel->link('carBooking', $model);
                $passengerModel->save();
            }
            // count same value passenger == 0 del
        }
        return $this->redirect(['view', 'id' => $model->id]);
    }

    private function passengersUpdateSave($model){
        $passengerCount = 0;
        // for add new passenger to car booking_log
        foreach ($model->carBookingPassengers as $passengerDB) {
            foreach ($model->passengers as $passenger) {
                $passengerSearch = CarBookingPassenger::find()
                    ->where([
                        'car_booking_id' => $model->id,
                        'user_id' => $passenger
                    ])->scalar();
                // check have passengerPost ? no insert
                if ($passengerSearch == null) {
                    $passengerModel = new CarBookingPassenger;
                    $passengerModel->user_id = $passenger;
                    $passengerModel->link('carBooking', $model);
                    $passengerModel->save();
                }
                // count same value passenger == 0 del
                if ($passengerDB->user_id == $passenger) $passengerCount++;
            }

            if ($passengerCount == 0) {
                if (($modelDel = CarBookingPassenger::findOne($passengerDB)) !== null) {
                    $modelDel->delete();
                }
            }
            $passengerCount = 0;
        }

        return $this->redirect(['view', 'id' => $model->id]);
    }

    private function compareOverlabsBooking($datetime_start,$datetime_end) {

        $model = new CarBooking();
        $cars_list = $model->carData;

        $query = CarBooking::find()
            ->where(['(:datetime_start, :datetime_end) OVERLAPS (datetime_start, datetime_end)' => true])
            ->andWhere(['status' => 2])
            ->andWhere(['car_id' => array_keys($cars_list)])
            ->groupBy(['id', 'car_id'])
            ->addParams([
                ':datetime_start' => $datetime_start,
                ':datetime_end' => $datetime_end
            ])
            ->all();

        $data = [];

        if (!is_null($query)) {
            $id_list = array_keys(ArrayHelper::map($query,'id','id'));
            foreach ($id_list as $key => $id) {

                $model = $this->findModel($id);

                $data[$key][] = $model->car->license_id;
                $data[$key][] = $model->carBookingLocation->name;
                $data[$key][] = $model->carBookingCategory->name;
                $data[$key][] = $model->setPassengersName($model->passengers);
                $data[$key][] = Yii::$app->datetimeManage->dtFormat($datetime = $model->datetime_start, true) . ' - ' . Yii::$app->datetimeManage->dtFormat($datetime = $model->datetime_end, true);
            }
        }
        return $data;
    }

    public function actionOverallApproveBooking()
    {
        $query = (new Query())
            ->select([
                'id',
                'to_char(datetime_start, \'YYYY-MM-DD\') AS "date"',
                'datetime_start',
                'datetime_end',
                'car_id AS "car"',
                'car_booking_location_id AS "location"',
                'car_booking_category_id AS "category"'
            ])
            ->from('car_booking')
            ->where(['status' => 1])
            ->all();

        $current_car = null;
        $current_date = null;
        $data = [];

        foreach ($query as $items => $value) {
            $model = $this->findModel($value['id']);

            $car = $model->car->license_id;
            $date = $value['date'];
            $value['location'] = $model->carBookingLocation->name;
            $value['category'] = $model->carBookingCategory->name;
            $value['passengers'] = $model->passengersName;

            unset(
                $value['date'],
                $value['car']
            );

            $data[$car][$date][] = $value;
        }

        return $this->render('overall-approve-booking', [
            'data' => $data,
        ]);
    }

    public function actionApproveBooking($id)
    {
        $post = Yii::$app->request->post();

        if ($post) {
            $id = $post['id'];
            $all_id = explode(',',$post['all_id']);

            foreach ($all_id as $item => $value) {
                $status = 3;

                if ($value == $id) $status = 2;

                $model = CarBooking::findOne($value);
                $model->status = $status;

                $model->save(false);
            }
            return $this->redirect(['overall-approve-booking']);
        }

        $model = $this->findModel($id);

        $query = CarBooking::find()
            ->where(['(:datetime_start, :datetime_end) OVERLAPS (datetime_start, datetime_end)' => true])
            ->andWhere(['car_id' => $model->car_id])
            ->andWhere(['status' => 1])
            ->addParams([
                ':datetime_start' => $model->datetime_start,
                ':datetime_end' => $model->datetime_end
            ])
            ->orderBy('id')
            ->asArray()
            ->all();

        $findId = array_values(ArrayHelper::map($query,'id','id'));

        $data = CarBooking::find()
            ->where(['id' => $findId])
            ->asArray()
            ->all();


        return $this->render('approve-booking', [
            'data' => $data,
        ]);
    }

    private function connectGoogleDrive() {
        $client = new Google_Client();
        $client->setApplicationName('Google Drive API PHP Quickstart');
        $client->setScopes(Google_Service_Drive::DRIVE_FILE);
        $path = Yii::getAlias('@webroot/');
        $client->setAuthConfig($path.'credentials.json');

        $client->setAccessType('offline');
        $client->setPrompt('select_account consent');

        $tokenPath = $path.'token.json';
        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);
        }

        if ($client->isAccessTokenExpired()) {
            if ($client->getRefreshToken()) {
                $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
            } else {
                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();
                printf("Open the following link in your browser:\n%s\n", $authUrl);
                print 'Enter verification code: ';
                $authCode = trim(fgets(STDIN));
    
                // Exchange authorization code for an access token.
                $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);
                $client->setAccessToken($accessToken);
    
                // Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new Exception(join(', ', $accessToken));
                }
            }
        }
        return $client;
    }

    /**
     * Finds the CarBooking model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CarBooking the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {

        if (($model = CarBooking::findOne($id)) !== null) {
            $model->passengers = $model->passengersValue;
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}