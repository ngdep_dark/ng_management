<?php

namespace app\modules\CarBooking\controllers;

use app\models\User;
use app\modules\CarBooking\models\CarBooking;
use app\modules\CarBooking\models\TrafficTicket;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ReportController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionPeriodUseCar()
    {
        $post = Yii::$app->request->post();

        $query = [];

        if ($post) {
            $date_range = explode(' - ', $post['date_range']);

            $query = CarBooking::find()
                ->select('id')
                ->where(['car_id' => $post['car_id']])
                ->andWhere(['>=', 'datetime_start', $date_range[0]])
                ->andWhere(['<=', 'datetime_end', $date_range[1]])
                ->andWhere(['is not', 'miles_end', null])
                ->andWhere('status = 2 OR status = 3')
                ->orderBy('datetime_start')
                ->asArray()
                ->all();
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query,
        ]);

        return $this->render('period-use-car', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPeriodUseCarPerPerson()
    {
        $post = Yii::$app->request->post();
        $query = [];
        if ($post) {
            empty($post['date_range']) ? $post['date_range'] = date('Y-m-d') . ' - ' . date('Y-m-d') : $post['date_range'];
            $date_range = explode(' - ', $post['date_range']);
            
            $query = (new Query())
                ->select([
                    'u.full_name',
                    'c.license_id as car',
                    'cbc.name as cate_name',
                    'cbl.name as loca_name',
                    'cb.datetime_start',
                    'cb.datetime_end',
                    'cb."miles_end" - cb."miles_start" AS miles',
                ])
                ->from(['cbp' => 'car_booking_passenger'])
                ->leftJoin(['cb' => 'car_booking'], 'cbp.car_booking_id = cb.id')
                ->leftJoin(['c' => 'car'], 'cb.car_id = c.id')
                ->leftJoin(['u' => '"user"'], 'cbp.user_id = u.id')
                ->leftJoin(['cbc' => 'car_booking_category'], 'cb.car_booking_category_id = cbc.id')
                ->leftJoin(['cbl' => 'car_booking_location'], 'cb.car_booking_location_id = cbl.id')
                ->where(['>=', 'cb.datetime_start', $date_range[0]])
                ->andWhere(['<=', 'cb.datetime_end', $date_range[1]])
                ->andWhere(['is not', 'cb.miles_end', null])
                ->andWhere('cb.status = 2 OR cb.status = 3')
                ->orderBy(['u.full_name' => SORT_ASC])
                ->addOrderBy(['cb.datetime_start' => SORT_ASC])
                ->all();

               
                if ($query) {


                    header('Content-Encoding: UTF-8');
                    header('Content-type: text/csv; charset=UTF-8');
                    header("Cache-Control: no-store, no-cache");
                    header('Content-Disposition: attachment; filename="content.csv"');
                    header("Pragma: no-cache");
                    header("Expires: 0");
        
                    $fp = fopen('php://output', 'w');
                    $header = ["ชื่อ", "ทะเบียน", "ประเภทการใช้งาน", "วัตถุประสงค์", "เวลาเริ่มใช้", "เวลาคืน", "ไมค์"];
                    fputcsv($fp, $header);
                    foreach($query AS $values){
                        fputcsv($fp, $values);
                    }
                    fclose($fp);
                    exit();
                } else {
                    
                    return $this->redirect(['period-use-car-per-person']);
                }
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query,
        ]);

        return $this->render('period-use-car-per-person', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPeriodTrafficTicketCar()
    {
        $post = Yii::$app->request->post();

        if ($post && !empty($post['date_range'])) {
            $date_range = explode(' - ', $post['date_range']);

            $dataTrafficTicket = $this->getDataPeriodTrafficTicket($post['date_range']);

            $html = $this->renderPartial('pdf_period-traffic-ticket-car', [
                'data' => $dataTrafficTicket,
            ]);

            $date_start = Yii::$app->datetimeManage->dtFormat($date_range[0]);
            $date_end = Yii::$app->datetimeManage->dtFormat($date_range[1]);
            $header = 'รายการใบสั่ง ตั้งแต่วันที่ ' . $date_start . ' - ' . $date_end;

            return Yii::$app->mpdfManage->newPdf($html, $header, $top = 20);

        }

        return $this->render('date-range', [
            'title' => 'ใบสั่งตามช่วงเวลา',
        ]);
    }

    public function actionPeriodCostTrafficTicketPerson()
    {
        $post = Yii::$app->request->post();

        if ($post && !empty($post['date_range'])) {
            $date_range = explode(' - ', $post['date_range']);
            $dataTrafficTicket = $this->getDataPeriodTrafficTicket($post['date_range']);

            $data = [];
            foreach ($dataTrafficTicket as $dataItem => $dataValue) {
                unset($dataTrafficTicket[$dataItem]['tid']);
                unset($dataTrafficTicket[$dataItem]['payment_at']);
                unset($dataTrafficTicket[$dataItem]['accuse_at']);
                unset($dataTrafficTicket[$dataItem]['price']);

                $countPassenger = count($dataValue['passengers']);
                $avgCost = 0;
                if ($countPassenger) {
                    $avgCost = intval($dataValue['price'] / $countPassenger);
                } else {
                    $avgCost = $dataValue['price'];
                }
                $dataTrafficTicket[$dataItem]['avg_cost'] = intval($avgCost);

                foreach ($dataValue as $keyItem => $keyValue) {
                    if ($keyItem === 'passengers') {
                        foreach ($keyValue as $item => $value) {
                            if (!array_key_exists($value, $data)) {
                                $data[$value]['price'] = 0;
                                $data[$value]['normal'] = 0;
                                $data[$value]['normal_price'] = 0;
                                $data[$value]['abnormal'] = 0;
                                $data[$value]['abnormal_price'] = 0;
                            }
                            $data[$value]['price'] += $avgCost;
                            if ($dataValue['status'] === 'ปกติ') {
                                $data[$value]['normal'] += 1;
                                $data[$value]['normal_price'] += $avgCost;
                            } else {
                                $data[$value]['abnormal'] += 1;
                                $data[$value]['abnormal_price'] += $avgCost;
                            }
                        }
                    }
                }
            }

            $html = $this->renderPartial('pdf_period-cost-traffic-ticket-person', [
                'data' => $data,
            ]);

            $date_start = Yii::$app->datetimeManage->dtFormat($date_range[0]);
            $date_end = Yii::$app->datetimeManage->dtFormat($date_range[1]);
            $header = 'การหักเงินเดือนตามใบสั่งรายคน ตั้งแต่วันที่ ' . $date_start . ' - ' . $date_end;

            return Yii::$app->mpdfManage->newPdf($html, $header, $top = 20);
        }

        return $this->render('date-range', [
            'title' => 'การหักเงินเดือนตามใบสั่งรายคน',
        ]);
    }

    private function getDataPeriodTrafficTicket($date_range)
    {
        $date_range = explode(' - ', $date_range);

        $queryMatchBooking = TrafficTicket::find()
            ->select(['id', 'tid', 'car_booking_id', 'accuse_at', 'due_pay_at', 'payment_at', 'price'])
            ->where(['not', ['car_booking_id' => null]])
            ->andWhere(['between', 'accuse_at', $date_range[0], $date_range[1]])
            ->orderBy('accuse_at')
            ->asArray()
            ->all();

        $queryNotMatchBooking = TrafficTicket::find()
            ->select(['id', 'tid', 'accuse_at', 'due_pay_at', 'payment_at', 'price'])
            ->where(['car_booking_id' => null])
            ->andWhere(['between', 'accuse_at', $date_range[0], $date_range[1]])
            ->orderBy('accuse_at')
            ->asArray()
            ->all();
        //find passengers in booking
        foreach ($queryMatchBooking as $item => $value) {
            $queryMatchBooking[$item]['passengers'] = $this->getPassengersForCostTrafficTicket($value["car_booking_id"]);
            $queryMatchBooking[$item]['status'] = 'ปกติ';
            unset($queryMatchBooking[$item]['car_booking_id']);

        }
        //find passengers in week
        foreach ($queryNotMatchBooking as $item => $value) {
            $startDayOfWeek = $this->getStartDayOfWeek($value['accuse_at']);
            $endDayOfWeek = date('Y-m-d', strtotime('+7 days', strtotime($startDayOfWeek)));
            //query find all booking in week
            $query = CarBooking::find()
                ->select(['id'])
                ->andWhere(['between', 'datetime_start', $startDayOfWeek, $endDayOfWeek])
                ->asArray()
                ->all();

            $bookingInWeek = ArrayHelper::getColumn($query, 'id');

            $arrayPassengersInWeek = [];
            //get all passenger in week
            foreach ($bookingInWeek as $bookInWeekitem => $bookInWeakValue) {
                $arrayPassengersInWeek[] = $this->getPassengersForCostTrafficTicket($bookInWeakValue);
            }
            $tempPassenger = [];
            foreach ($arrayPassengersInWeek as $passengerInWeekItem => $passengerInWeekValue) {
                foreach ($passengerInWeekValue as $passengersItem => $passengersValue) {
                    $tempPassenger[] = $passengersValue;
                }
                $tempPassenger = array_unique($tempPassenger);
            }
            $queryNotMatchBooking[$item]['passengers'] = $tempPassenger;
            $queryNotMatchBooking[$item]['status'] = 'หักทั้งสัปดาห์';
        }

        return array_merge($queryMatchBooking, $queryNotMatchBooking);
    }

    private function getStartDayOfWeek($date)
    {
        $year = substr($date, 0, 4);
        $week = date('W', strtotime($date));

        return date("Y-m-d", strtotime("{$year}-W{$week}-1")); //Returns the date of sunday in week
    }

    private function getPassengersForCostTrafficTicket($id)
    {
        $result = CarBooking::findOne($id);
        $passengersArray = $result->getCarBookingPassengers()->asArray()->all();
        $passengers = ArrayHelper::getColumn($passengersArray, 'user_id');

        $passengersName = [];

        foreach ($passengers as $passengerID) {
            $passengersName[] = User::findOne($passengerID)->full_name;
        }

        return $passengersName;
    }

}