<?php

namespace app\modules\CarBooking\controllers;

use app\modules\CarBooking\models\CarBrand;
use app\modules\CarBooking\models\CarFixed;
use app\modules\CarBooking\models\CarFixedDetail;
use Yii;
use app\modules\CarBooking\models\Car;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * CarController implements the CRUD actions for Car model.
 */
class CarController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [],
                'actions' => [
                    'car-validate' => [
                        'Origin' => ['*'],
                        'Access-Control-Request-Method' => ['POST'],
                        'Access-Control-Request-Headers' => ['*'],
                        'Access-Control-Allow-Credentials' => null,
                        'Access-Control-Max-Age' => 3600,
                        'Access-Control-Expose-Headers' => [],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (in_array($action->id, ['car-validate'])) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }
    /**
     * Lists all Car models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Car::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Car model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => CarFixed::find()->where(['car_id' => $id]),
        ]);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Car model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Car();
        $model->scenario = 'create';

        $post = Yii::$app->request->post();

        if ($post && $model->load($post)) {
            $this->actionStatement($model);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Car model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();

        if ($post && $model->load($post)) {
            $this->actionStatement($model);
        }

        $model->insurance_date = Yii::$app->formatter->asDate($model->insurance_date, 'dd/MM/yyyy');
        $model->car_act_date = Yii::$app->formatter->asDate($model->car_act_date, 'dd/MM/yyyy');

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Car model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Car model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Car the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Car::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private function actionStatement($model){
        $model->insurance_date = Yii::$app->datetimeManage->changeThaiFormateDate($model->insurance_date);
        $model->car_act_date = Yii::$app->datetimeManage->changeThaiFormateDate($model->car_act_date);

        $carBrandModel = new CarBrand();
        $car_brand = CarBrand::findOne(['name' => $model->car_brand_id]);
        $model = Yii::$app->dbManage->select2Save($model, 'car_brand_id', $carBrandModel, $car_brand, 'carBrand');
        if ($model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
    }

    public function actionCarValidate()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $post = Yii::$app->request->post();
        $model = $this->findModel($post['id']);

        $insuranceAlert = $model->validateInsuranceDate();
        $carActAlert = $model->validateCarActDate();
        $lastMilesCarCheckAlert = $model->validateMilesLastCarCheck();

        return [
            'insurance' => $insuranceAlert,
            'car_act' => $carActAlert,
            'check_car' => $lastMilesCarCheckAlert
        ];
    }
}
