<?php

namespace app\modules\CarBooking\controllers;

use app\modules\CarBooking\models\AllegationType;
use app\modules\CarBooking\models\CarBooking;
use app\modules\CarBooking\models\CarBookingLocation;
use Yii;
use app\modules\CarBooking\models\TrafficTicket;
use app\modules\CarBooking\models\TrafficTicketSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TrafficTicketController implements the CRUD actions for TrafficTicket model.
 */
class TrafficTicketController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TrafficTicket models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TrafficTicketSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TrafficTicket model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TrafficTicket model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TrafficTicket();
        $model->scenario = 'create';
        $query = null;

        $post = Yii::$app->request->post();

        if ($post && $model->load($post)) {

//            $model->accuse_at = Yii::$app->datetimeManage->changeThaiFormateDate($model->accuse_at);
            $model->due_pay_at = Yii::$app->datetimeManage->changeThaiFormateDate($model->due_pay_at);
            $model->payment_at = Yii::$app->datetimeManage->changeThaiFormateDate($model->payment_at);

            $query = $model->validateBooking();
            $locationModel = new CarBookingLocation();
            $location = CarBookingLocation::findOne(['name' => $model->car_booking_location_id]);
            $model = Yii::$app->dbManage->select2Save($model, 'car_booking_location_id', $locationModel, $location, 'carBookingLocation');
//
            $allegationTypeModel = new AllegationType();
            $allegationType = AllegationType::findOne(['name' => $model->allegation_type_id]);
            $model = Yii::$app->dbManage->select2Save($model, 'allegation_type_id', $allegationTypeModel, $allegationType, 'allegationType');

            if ($model->car_booking_id === null) {
                if ($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            } else {
                if ($model->save(false)) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'query' => $query
        ]);
    }

    /**
     * Updates an existing TrafficTicket model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $query = null;

        $post = Yii::$app->request->post();

        if ($post && $model->load($post)) {

//            $model->accuse_at = Yii::$app->datetimeManage->changeThaiFormateDate($model->accuse_at);
            $model->due_pay_at = Yii::$app->datetimeManage->changeThaiFormateDate($model->due_pay_at);
            $model->payment_at = Yii::$app->datetimeManage->changeThaiFormateDate($model->payment_at);

            $query = $model->validateBooking();
            $locationModel = new CarBookingLocation();
            $location = CarBookingLocation::findOne(['name' => $model->car_booking_location_id]);
            $model = Yii::$app->dbManage->select2Save($model, 'car_booking_location_id', $locationModel, $location, 'carBookingLocation');

            $allegationTypeModel = new AllegationType();
            $allegationType = AllegationType::findOne(['name' => $model->allegation_type_id]);
            $model = Yii::$app->dbManage->select2Save($model, 'allegation_type_id', $allegationTypeModel, $allegationType, 'allegationType');
            if (!$model->errors) {
                if($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            } else if ($model->errors && $model->car_booking_id) {
                if ($model->save(false)) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }

        // $model->accuse_at = Yii::$app->formatter->asDate($model->accuse_at, 'dd/MM/yyyy');
        // $model->due_pay_at = Yii::$app->formatter->asDate($model->due_pay_at, 'dd/MM/yyyy');
        // if ($model->payment_at) $model->payment_at = Yii::$app->formatter->asDate($model->payment_at, 'dd/MM/yyyy');

        return $this->render('update', [
            'model' => $model,
            'query' => $query
        ]);
    }

    /**
     * Deletes an existing TrafficTicket model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TrafficTicket model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TrafficTicket the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TrafficTicket::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}