<?php

namespace app\modules\CarBooking\controllers;

use app\modules\CarBooking\models\Car;
use app\modules\CarBooking\models\CarFixedDetail;
use app\modules\CarBooking\models\CarFixedType;
use app\modules\CarBooking\models\Garage;
use Yii;
use app\modules\CarBooking\models\CarFixed;
use app\modules\CarBooking\models\CarFixedSearch;
use yii\db\Query;
use yii\web\Response;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CarFixedController implements the CRUD actions for CarFixed model.
 */
class CarFixedController extends Controller
{
    public $enableCsrfValidation = false;
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [],
                'actions' => [
                    'car-fixed-detail' => [
                        'Origin' => ['*'],
                        'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                        'Access-Control-Request-Headers' => ['*'],
                        'Access-Control-Allow-Credentials' => null,
                        'Access-Control-Max-Age' => 3600,
                        'Access-Control-Expose-Headers' => [],
                    ],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (in_array($action->id, ['car-fixed-detail'])) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    /**
     * Lists all CarFixed models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CarFixedSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CarFixed model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CarFixed model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $carPeriodCheck = $this->findCarFixedTypeStatus();

        if (!$carPeriodCheck) {
            $typeModel = new CarFixedType();
            $typeModel->name = 'ตรวจสภาพตามระยะทาง';
            $typeModel->save();
        }

        $get = Yii::$app->request->get();

        $model = new CarFixed();
        $model->car_id = $get['id'];

        $post = Yii::$app->request->post();

        $this->actionStatement($post, $model);

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CarFixed model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();

        $this->actionStatement($post, $model);

        $model->fix_date = Yii::$app->formatter->asDate($model->fix_date, 'dd/MM/yyyy');
        $model->fixed_date = Yii::$app->formatter->asDate($model->fixed_date, 'dd/MM/yyyy');
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CarFixed model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $post = Yii::$app->request->post();

        $url = Yii::$app->getUrlManager()->createUrl(['car-booking/car/view', 'id' => $post['id']]);

        $model = $this->findModel($id);

        $carPeriodCheck = $this->findCarFixedTypeStatus();

        if ($model->car_fixed_type_id == $carPeriodCheck) {
            $carModel = Car::findOne($model->car_id);
            $carFixedLastTime = (new Query())->select('last_check_miles')->from('car_fixed')->where(['car_id' => $model->car_id])->orderBy(['last_check_miles' => SORT_DESC])->one();
            $carModel->miles_last_check = $carFixedLastTime['last_check_miles'];


//            $carModel->miles_last_check -= 10000;
//            if ($carModel->miles_last_check < $carModel->miles) {
//                $carModel->miles_last_check += 10000;
//            }
            $carModel->save();
        }
        $model->delete();

        return $this->redirect($url);
    }

    public function actionStatement($post, $model)
    {
        $carPeriodCheck = $this->findCarFixedTypeStatus();

        $create = $model->isNewRecord;

        if ($post && $model->load($post)) {
            $carFixedLastTime = (new Query())->select('last_check_miles')->from('car_fixed')->where(['car_id' => $model->car_id])->orderBy(['last_check_miles' => SORT_DESC])->one();

            $oldStatus = $model->getOldAttribute('car_fixed_type_id');

            $model->fix_date = !empty($model->fix_date) ? Yii::$app->datetimeManage->changeThaiFormateDate($model->fix_date) : null;
            $model->fixed_date = !empty($model->fixed_date) ? Yii::$app->datetimeManage->changeThaiFormateDate($model->fixed_date) : null;

            $carFixedTypeModel = new CarFixedType();
            $carFixedType = CarFixedType::findOne(['name' => $model->car_fixed_type_id]);
            $model = Yii::$app->dbManage->select2Save($model, 'car_fixed_type_id', $carFixedTypeModel, $carFixedType, 'carFixedType');

            $garageModel = new Garage();
            $garage = Garage::findOne(['name' => $model->garage_id]);
            $model = Yii::$app->dbManage->select2Save($model, 'garage_id', $garageModel, $garage, 'garage');

            if ($model->save()) {
                //new record type = เอาไปซ่อมศูนย์
                if ($model->car_fixed_type_id == $carPeriodCheck && $oldStatus === null) {
                    $carModel = Car::findOne($model->car_id);
                    $model->last_check_miles = $carModel->miles_last_check;
                    $model->save();

                    $carModel = Car::findOne($model->car_id);
                    $carModel->miles_last_check = $carModel->miles + 10000;
                    $carModel->save();
                // edit record from เอาไปซ่อมศูนย์ to other
                } elseif ($oldStatus == $carPeriodCheck && $model->car_fixed_type_id != $carPeriodCheck) {
                    $carModel = Car::findOne($model->car_id);
                    $carModel->miles_last_check = $carFixedLastTime['last_check_miles'];
//                        $carModel->miles_last_check -= 10000;
//                        if ($carModel->miles_last_check < $carModel->miles) {
//                            $carModel->miles_last_check += 10000;
//                        }
                    $carModel->save();
                // edit record from other to เอาไปซ่อมศูนย์
                } elseif ($oldStatus != $carPeriodCheck && $model->car_fixed_type_id == $carPeriodCheck && $oldStatus != null) {
                    $carModel = Car::findOne($model->car_id);
                    $model->last_check_miles = $carModel->miles_last_check;
                    $model->save();

                    $carModel = Car::findOne($model->car_id);
                    $carModel->miles_last_check = $carModel->miles + 10000;
                    $carModel->save();
                } else {
                    $carModel = Car::findOne($model->car_id);
                    $model->last_check_miles = $carModel->miles_last_check;
                    $model->save();
                }

                if ($model->detail) {
                    if ($create) {
                        $this->saveToCarFixedDetail($model->detail, $model->id);
                    } else {
                        foreach ($model->carFixedDetail as $item => $value) {
                            $carFixedDetailModel = CarFixedDetail::findOne($value->id);
                            $carFixedDetailModel->delete();
                        }
                        $this->saveToCarFixedDetail($model->detail, $model->id);
                    }
                }
                return $this->redirect(['/car-booking/car/view', 'id' => $model->car_id]);
            }
        }
    }

    private function saveToCarFixedDetail($details, $carFixedId) {
        foreach ($details as $detail => $value) {
            $carFixedDetail = new CarFixedDetail();
            $carFixedDetail->load($value);
            $carFixedDetail->car_fixed_id = $carFixedId;
            $carFixedDetail->save();
        }
    }

    /**
     * Finds the CarFixed model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CarFixed the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CarFixed::findOne($id)) !== null) {
//            $model->detail = $model;
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private  function findCarFixedTypeStatus(){
        return CarFixedType::find()->where(['name' => 'ตรวจสภาพตามระยะทาง'])->scalar();
    }

    public function actionCarFixedDetail()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $post = Yii::$app->request->post();
        $model = CarFixedDetail::find()->where(['car_fixed_id' => $post['id']])->all();
        return $model;

    }
}
