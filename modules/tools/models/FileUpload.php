<?php

namespace app\modules\tools\models;

use yii\base\Model;
use yii\web\UploadedFile;

class FileUpload extends Model
{
    /**
     * @var UploadedFile
     */
    public $file;

    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => false],
        ];
    }

    public function upload($path)
    {
        if ($this->validate()) {
            $this->file->saveAs($path);
            return true;
        } else {
            return false;
        }
    }
}
