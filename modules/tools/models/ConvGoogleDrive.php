<?php

namespace app\modules\tools\models;

use Yii;

class ConvGoogleDrive extends \yii\db\ActiveRecord
{
    public $link;
    public $result;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['link','validateLink', 'on' => 'default'],
            [['link', 'result'], 'safe']
        ];
    }

    public function validateLink() {
        $compare_link = strrpos($this->link,"https://drive.google.com/open?id=");
        if ($compare_link !== 0) {
            $this->addError('link','กรุณาใส่ลิ้งตามตัวอย่างนี้ https://drive.google.com/open?id=xxxx');
        }
    }
}
