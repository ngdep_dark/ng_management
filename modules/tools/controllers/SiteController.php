<?php

namespace app\modules\tools\controllers;

use app\modules\tools\models\ConvGoogleDrive;
use app\modules\tools\models\FileUpload;
//use app\modules\tools\models\SpareCard;
use Yii;
use yii\helpers\BaseFileHelper;
use yii\web\UploadedFile;

class SiteController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionConvGoogledriveImg()
    {
        $model = new ConvGoogleDrive();
        $result = null;
        $post = Yii::$app->request->post();

        if ($post && $model->load($post)) {
            $model->validateLink();
            $compare_link = strrpos($model->link, "https://drive.google.com/open?id=");

            if ($compare_link === 0) {
                $data = explode('/', $model->link);
                $id = substr($data[3], 8);
                $model->result = "https://drive.google.com/thumbnail?id=" . $id . "&sz=w800-h640";
            }
        }

        return $this->render('conv-googledrive-img', [
            'model' => $model,
        ]);
    }

    public function actionSpareCard()
    {
        ini_set('pcre.backtrack_limit', "3000000");
        ini_set('max_execution_time', 300);

        $model = new FileUpload();

        $post = Yii::$app->request->post();

        if ($post && $model->load($post)) {
            $model->file = UploadedFile::getInstance($model, 'file');

            $mainPath = Yii::getAlias('@app') . DIRECTORY_SEPARATOR . 'web';
            $listFile = scandir($mainPath);
            $path = Yii::getAlias('@app') . DIRECTORY_SEPARATOR . 'web'
                . DIRECTORY_SEPARATOR . 'spare_card' . DIRECTORY_SEPARATOR;

            if (!in_array('spare_card', $listFile)) {
                BaseFileHelper::createDirectory($path, 0777);
            }

            $file = $model->file;

            $fileName = $file->name;
            $filePath = $path . $file->name;

            if ($model->upload($filePath)) {

                $file = fopen($filePath, "r");

                $i = 0;
                $data = [];
                $title = $fileName;
                while (($line = fgets($file, 9999)) !== false) {
                    $data[] = $line;
                    $i++;
                }
                fclose($file);
                if (unlink($filePath)) {
                    $html = $this->renderPartial('../report/spare-card', [
                        'data' => $data,
                    ]);
                    return Yii::$app->mpdfManage->newPdf($html, $title, $top = 14, $left = 25, $right = 25, $bottom = 0, $tag_hr = false);
                }
            }
        }

        return $this->render('spare-card', [
            'model' => $model,
        ]);

    }
}