<?php
/* @var $this yii\web\View */
use yii\widgets\ActiveForm;
use yii\helpers\Html;
$this->title = 'ตัวแปลง link google drive';
$this->params['breadcrumbs'][] = ['label' => 'เครื่องมือ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php $form = ActiveForm::begin(); ?>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?= $form->field($model, 'link')->textInput(['maxlength' => true, 'placeholder' => 'ใส่ url https://drive.google.com/open?id=xxx']) ?>
    </div>
    <?php if ($model->result) : ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?= $form->field($model, 'result')->textInput(['maxlength' => true, 'readonly' => true]) ?>
    </div>
    <?php endif; ?>
 </div>

<div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>
