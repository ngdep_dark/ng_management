<?php
$this->title = 'เครื่องมือ';
$this->params['breadcrumbs'][] = $this->title;
/* @var $this yii\web\View */

$this->registerCss("
    .padding-border {
        padding: 10px 20px;
    }
")
?>
<div class="car-booking-report">
    <div class="clearfix">
        <div class="row">
            <div class="col-md-6">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="padding-border box-title text-olive"><i class="fa fa-file-pdf-o"></i> <small></small> เครื่องมือ </h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="box-group">
                            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                            <div class="row fontawesome-icon-list">
                                <div class="padding-border fa-hover col-md-4 col-sm-4 col-xs-12" style="font-size: 16px"><a href="/tools/site/conv-googledrive-img"><i class="fa fa-file-pdf-o"></i> ตัวแปลง link google drive</a></div>
                                <div class="padding-border fa-hover col-md-4 col-sm-4 col-xs-12" style="font-size: 16px"><a href="/tools/site/spare-card"><i class="fa fa-file-pdf-o"></i> บัตรสำรอง </a></div>

                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="padding-border box-title text-olive"><i class="fa fa-file-photo-o"></i> <small></small> รูปภาพ </h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="box-group">
                            <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                            <div class="row fontawesome-icon-list">
                                <div class="padding-border fa-hover col-md-4 col-sm-4 col-xs-12" style="font-size: 16px"><a href="/tools/image/index"><i class="fa fa-hand-scissors-o"></i> เครื่องมือตัดภาพ </a></div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>