<?php
use kartik\widgets\FileInput;
use kartik\form\ActiveForm;
?>
<?php $form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data']
]);
?>
<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <?= $form->field($model, 'file')->widget(FileInput::classname(), [
            'pluginOptions' => [
                'allowedFileExtensions' => ["txt", "csv"]
            ]
        ]);
        ?>
    </div>
</div>
<?php ActiveForm::end(); ?>

