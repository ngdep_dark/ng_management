<?php

use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
$product = \yii\helpers\ArrayHelper::map(\app\modules\store\models\Product::find()->all(), 'id', 'name');
?>
<div class="row">
    <div class="box box-warning box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">รายงานการเบิก</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?php $form = ActiveForm::begin([
                    'method'=>'get'
            ]); ?>
            <div class="row">
                <div class="col-md-2">
                    <?= $form->field($model, 'status')->dropDownList([0 => 'เบิกสินค้า', 1 => 'เพิ่มสินค้า']) ?>
                </div>
                <div class="col-md-4">
                    <?php
                    echo '<label class="control-label">เลือกสินค้า</label>';
                    echo Select2::widget([
                        'model' => $model,
                        'attribute' => 'product',
                        'data' => $product,
                        'options' => ['placeholder' => 'กรุณาเลือกสินค้า ...'],
                        'pluginOptions' => [
                            'multiple' => true
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-md-4">
                    <?php
                    echo '<label class="control-label">เลือกวันที่</label>';
                    echo \kartik\daterange\DateRangePicker::widget([
                        'model' => $model,
                        'attribute' => 'date',
                        'useWithAddon' => true,
                        'language' => 'th',             // from demo config
                        'hideInput' => true,           // from demo config
                        'presetDropdown' => true, // from demo config
                        'pluginOptions' => [
                            'locale' => ['format' => 'DD-MM-YYYY'], // from demo config
                            'separator' => '-',       // from demo config
                            'opens' => 'left'
                        ]
                    ]);
                    ?>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <br>
                        <?= Html::submitButton('<i class="fa fa-search"></i> ค้นหา', ['class' => 'btn btn-success ']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
            <?php if (!empty($dataProvider)): ?>
                <?php
                $dataStart = substr($model->date, 0, 10);
                $dataEnd = substr($model->date, 13, 10);
                ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'hover' => true,
                    'responsiveWrap' => false,
                    'showPageSummary' => true,
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY,
                        'heading' => 'รายงานการเบิก ' . $model->date,
                    ],
                    'columns' => [
                        ['class' => 'kartik\grid\SerialColumn'],
                        [
                            'label' => 'รายการสินค้า',
                            'attribute' => 'name'
                        ],
                        [
                            'label' => 'จำนวน',
                            'attribute' => 'amount',
                            'pageSummary' => true
                        ],
                        [
                            'label' => 'รายละเอียด',
                            'attribute' => 'details'
                        ],
                        [
                            'label' => 'วันที่บันทึก',
                            'attribute' => 'created_at'
                        ],

                    ],
                ]); ?>
            <?php endif ?>
        </div>
        <!-- /.box-body -->
    </div>
</div>
