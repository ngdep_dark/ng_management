<?php
use kartik\grid\GridView;
$this->title ="รายงานการคืน"
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'hover' => true,
    'responsiveWrap' => false,
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
        'heading' =>$this->title
    ],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'รายการสินค้า',
            'attribute' => 'name'
        ],
        [
            'label' => 'จำนวนเบิก',
            'contentOptions' => ['class'=>'text-right'],
            'attribute' => 'amount'
        ],
        [
            'label' => 'รายละเอียด',
            'attribute' => 'details'
        ],
        [
            'label' => 'จำนวนคงเหลือ',
            'contentOptions' => ['class'=>'text-right'],
            'headerOptions' => ['class'=>'text-right'],
            'value' => function($model){
        $total =$model['use_amount']+$model['take_amount'];
                return $total*-1;
            }
        ],
        [
            'label' => 'จำนวนที่ใช้ได้',
            'contentOptions' => ['class'=>'text-right'],
            'headerOptions' => ['class'=>'text-right'],
            'attribute' => 'take_amount'
        ],
        [
            'label' => 'วันที่คืน',
            'attribute' => 'updated_at'
        ],

    ],


    ]); ?>
