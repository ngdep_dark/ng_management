<?php
/* @var $this yii\web\View */
?>
<div class="row">
    <div class="col-md-4 col-sm-6 col-xs-12">
        <a href="<?=\yii\helpers\Url::to(['report/report-take'])?>">
        <div class="info-box bg-blue">
            <span class="info-box-icon"><i class="fa fa-calendar"></i></span>
            <div class="info-box-content">
                <span class="info-box-text"><br></span>
                <span class="info-box-number"><h5>รายงานการเบิก</h5></span>
                <div class="progress">
                    <div class="progress-bar" style="width: 70%"></div>
                </div>
            </div>
            <!-- /.info-box-content -->

        </div>
        </a>
        <!-- /.info-box -->
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <a href="<?=\yii\helpers\Url::to(['report/calculator'])?>">
            <div class="info-box bg-blue">
                <span class="info-box-icon"><i class="fa fa-calculator"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text"><br></span>
                    <span class="info-box-number"><h5>รายงานการสั่งซื้อ</h5></span>
                    <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                    </div>
                </div>
                <!-- /.info-box-content -->
            </div>
        </a>
        <!-- /.info-box -->
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">

        <a href="<?=\yii\helpers\Url::to(['report/order-take'])?>">
            <div class="info-box bg-blue">
                <span class="info-box-icon"><i class="fa fa-diamond"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text"><br></span>
                    <span class="info-box-number"><h5>รายงานการคืน</h5></span>
                    <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                    </div>
                </div>
                <!-- /.info-box-content -->
            </div>
        </a>
        <!-- /.info-box -->
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <a href="<?=\yii\helpers\Url::to(['report/use-take'])?>">
            <div class="info-box bg-blue">
                <span class="info-box-icon"><i class="fa fa-thumbs-down"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text"><br></span>
                    <span class="info-box-number"><h5>รายงานบันทึกการใช้งานบัตรเสีย</h5></span>
                    <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                    </div>
                </div>
                <!-- /.info-box-content -->
            </div>
        </a>
        <!-- /.info-box -->
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <a href="<?=\yii\helpers\Url::to(['report/good-take'])?>">
            <div class="info-box bg-blue">
                <span class="info-box-icon"><i class="fa fa-thumbs-up"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text"><br></span>
                    <span class="info-box-number"><h5>รายงานบันทึกการใช้งานบัตรดี และ ออเดอร์</h5></span>
                    <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                    </div>
                </div>
                <!-- /.info-box-content -->
            </div>
        </a>
        <!-- /.info-box -->
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <a href="<?=\yii\helpers\Url::to(['/store/code-details'])?>">
            <div class="info-box bg-blue">
                <span class="info-box-icon"><i class="fa fa-file"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text"><br></span>
                    <span class="info-box-number"><h5>รายงานอธิบาย Code สินค้า</h5></span>
                    <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                    </div>
                </div>
                <!-- /.info-box-content -->
            </div>
        </a>
        <!-- /.info-box -->
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <a href="<?=\yii\helpers\Url::to(['/store/report/report-card-all'])?>">
            <div class="info-box bg-blue">
                <span class="info-box-icon"><i class="fa fa-list-ol"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text"><br></span>
                    <span class="info-box-number"><h5>รายงาน บัตรดี บัตรเสีย บัตร order</h5></span>
                    <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                    </div>
                </div>
                <!-- /.info-box-content -->
            </div>
        </a>
        <!-- /.info-box -->
    </div>
    <div class="col-md-4 col-sm-6 col-xs-12">
        <a href="<?=\yii\helpers\Url::to(['/store/report/report-list-all'])?>">
            <div class="info-box bg-green">
                <span class="info-box-icon"><i class="fa fa-product-hunt"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text"><br></span>
                    <span class="info-box-number"><h5>รายงานสินค้าคงเหลือทั้งหมด</h5></span>
                    <div class="progress">
                        <div class="progress-bar" style="width: 70%"></div>
                    </div>
                </div>
                <!-- /.info-box-content -->
            </div>
        </a>
        <!-- /.info-box -->
    </div>
</div>
