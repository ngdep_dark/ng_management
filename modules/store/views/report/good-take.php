<?php
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;
use kartik\daterange\DateRangePicker;
use yii\helpers\ArrayHelper;
use app\modules\store\models\Product;
use kartik\grid\GridView;
$product =$product = ArrayHelper::map(Product::find()->all(), 'id', 'name');
$this->title='รายงานบันทึกบัตรดีและออรเดอร์';
?>
<div class="row">
    <div class="box box-success box-solid">
        <div class="box-header with-border">
            <h3 class="box-title"><?=$this->title?></h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>
            <div class="row">

                <div class="col-md-4">
                    <?php
                    echo '<label class="control-label">เลือกสินค้า</label>';
                    echo Select2::widget([
                        'model' => $model,
                        'attribute' => 'product',
                        'data' => $product,
                        'options' => ['placeholder' => 'กรุณาเลือกสินค้า ...'],
                        'pluginOptions' => [
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-md-4">
                    <?php
                    echo '<label class="control-label">เลือกวันที่</label>';
                    echo \kartik\daterange\DateRangePicker::widget([
                        'model' => $model,
                        'attribute' => 'date',
                        'useWithAddon' => true,
                        'language' => 'th',             // from demo config
                        'hideInput' => true,           // from demo config
                        'presetDropdown' => true, // from demo config
                        'pluginOptions' => [
                            'locale' => ['format' => 'DD-MM-YYYY'], // from demo config
                            'separator' => '-',       // from demo config
                            'opens' => 'left'
                        ]
                    ]);
                    ?>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <br>
                        <?= Html::submitButton('<i class="fa fa-search"></i> ค้นหา', ['class' => 'btn btn-success ']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
            <?php if (!empty($dataProvider)): ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'hover' => true,
                    'showPageSummary' => true,
                    'responsiveWrap' => false,
                    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
                    'panel' => [
                        'type' => GridView::TYPE_SUCCESS,
                        'heading' => 'รายงานบันทึกบัตรดีและออรเดอร์ ' . $model->date,
                    ],
                    'columns' => [
                        [
                            'class' => 'kartik\grid\SerialColumn',
                            'contentOptions' => ['class' => 'kartik-sheet-style'],
                            'width' => '36px',
                            'header' => '',
                            'headerOptions' => ['class' => 'kartik-sheet-style']
                        ],
                        [
                            'label' => 'โรงเรียน',
                            'attribute' => 'name'
                        ],
                        [
                            'label' => 'จำนวน',
                            'attribute' => 'amount',
                            'contentOptions' => ['class' => 'kartik-sheet-style '],
                            'format' => ['decimal', 0],
                            'pageSummary' => true
                        ],
                    ],
                ]); ?>
            <?php endif ?>
        </div>
        <!-- /.box-body -->
    </div>
</div>
