<?php
$this->title = 'รายการคำนวนการสั่งซื้อ';
use yii\helpers\Html;
?>

<?= \kartik\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'kartik\grid\SerialColumn'],
        [
            'label' => 'วันที่',
            'value' => function ($model) {
                return Yii::$app->formatter->asDate($model['created_at'],'dd/M/Y');
            }
        ],

         [
            'format' => ['decimal',0],
            'attribute' => 'amount'
        ],
        'percent',
        [
            'format' => ['decimal',0],
            'attribute' => 'balance'
        ],
        ['class' => 'kartik\grid\ActionColumn',
            'template' => '{view} {delete-cal}',
            'buttons' => [
                'view' => function ($url,$model, $key) {
                $url= \yii\helpers\Url::to(['report/report','id'=>$model['id']]);
                    return Html::a('<i class="glyphicon glyphicon-print"></i>',$url);
                },
                 'delete-cal' => function ($url,$model, $key) {
                //$url= \yii\helpers\Url::to(['report/report','id'=>$model['id']]);
                    return Html::a('<i class="glyphicon glyphicon-trash"></i>',$url,['data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],]);
                }
            ]
        ],


    ],
]); ?>

