<?php
$this->title="รายงานสินค้าคงเหลือทั้งหมด";
use kartik\grid\GridView;
?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'hover' => true,
    'showPageSummary' => true,
    'responsiveWrap' => false,
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
    'panel' => [
        'type' => GridView::TYPE_SUCCESS,
    ],
    'columns' => [
        [
            'class' => 'kartik\grid\SerialColumn',
            'contentOptions' => ['class' => 'kartik-sheet-style'],
            'width' => '36px',
            'header' => '',
            'headerOptions' => ['class' => 'kartik-sheet-style']
        ],
        [
            'attribute' => 'name',
            'label' => ' รายการสินค้า'
        ],
        [
            'attribute' => 'amount',
            'format' => ['decimal',0],
            'headerOptions' => ['class'=>'text-center'],
            'contentOptions' => ['class'=>'text-right'],
            'label' => ' จำนวนคงเหลือ'
        ]
    ],
]); ?>
