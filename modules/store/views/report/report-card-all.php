<?php

use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

$this->title = 'รายงาน บัตรดี บัตรเสีย บัตร order';
$School = ArrayHelper::map(\app\modules\store\models\School::find()->all(), 'id', 'name');
$product = ArrayHelper::map(\app\modules\store\models\Product::find()->all(), 'id', 'name');
$getData = Yii::$app->request->get();
if (!empty($getData['school_id']) && !empty($getData['product_id']) && !empty($getData['date'])) {
    $SchoolName = \app\modules\store\models\School::findOne(['id' => $getData['school_id']]);
    $product_value=$getData['product_id'];
    $school_value=$getData['school_id'];
    $date_value=$getData['date'];
}

?>
<div class="row">
    <div class="box box-info box-solid">
        <div class="box-header with-border">
            <h3 class="box-title">รายงาน บัตรดี บัตรเสีย บัตร
                order <?= !empty($SchoolName) ? '(โรงเรียน' . $SchoolName->name . ')' : '' ?></h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
            <!-- /.box-tools -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?php $form = ActiveForm::begin(['method' => 'get']); ?>
            <div class="row">
                <div class="col-md-3">
                    <?php
                    echo '<label class="control-label">เลือกสินค้า</label>';
                    echo Select2::widget([
                        'name' => 'product_id',
                        'data' => $product,
                        'value' => !empty($product_value)?$product_value:'',
                        'options' => ['placeholder' => 'กรุณาเลือกสินค้า ...'],
                        'pluginOptions' => [
//                            'multiple' => true
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-md-3">
                    <?php
                    echo '<label class="control-label">เลือกโรงเรียน</label>';
                    echo Select2::widget([
                        'name' => 'school_id',
                        'data' => $School,
                        'value' => !empty($school_value)?$school_value:'',
                        'options' => ['placeholder' => 'กรุณาเลือกโรงเรียน ...'],
                        'pluginOptions' => [
                            //'multiple' => true
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-md-4">
                    <?php
                    echo '<label class="control-label">เลือกวันที่</label>';
                    echo \kartik\daterange\DateRangePicker::widget([
                        'name' => 'date',
                        'value' => !empty($date_value)?$date_value:'',
                        'useWithAddon' => true,
                        'language' => 'th',             // from demo config
                        'hideInput' => true,           // from demo config
                        'presetDropdown' => true, // from demo config
                        'pluginOptions' => [
                            'locale' => ['format' => 'DD-MM-YYYY'], // from demo config
                            'separator' => '-',       // from demo config
                            'opens' => 'left'
                        ]
                    ]);
                    ?>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <br>
                        <?= Html::submitButton('<i class="fa fa-search"></i> ค้นหา', ['class' => 'btn btn-success ']) ?>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
            <?php if (!empty($dataProvider)): ?>

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                    'hover' => true,
                    'responsiveWrap' => false,
                    'showPageSummary' => true,
                    'pageSummaryRowOptions' => ['class' => 'text-right kv-page-summary success'],
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY,
                        'heading' => $this->title,
                    ],
                    'columns' => [
                        ['class' => 'kartik\grid\SerialColumn'],
                        [
                            'label' => 'วันที่',
                            'attribute' => 'date'

                        ],
                        [
                            'label' => 'บัตรดี',
                            'attribute' => 'cardgood',
                            'contentOptions' => ['class' => 'text-right'],
                            'pageSummary' => true
                        ],
                        [
                            'label' => 'บัตรOrder',
                            'attribute' => 'cardorder',
                            'contentOptions' => ['class' => 'text-right'],
                            'pageSummary' => true
                        ],
                        [
                            'label' => 'บัตรเสีย',
                            'contentOptions' => ['class' => 'text-right'],
                            'attribute' => 'cardbad',
                            'pageSummary' => true
                        ],
                        [
                            'label' => 'สาเหตุ',
                            'attribute' => 'causes',
                        ],
                        [
                            'contentOptions' => ['class' => 'text-right'],
                            'label' => 'จำนวนที่ใช้ทั้งหมด',
                            'value' => function ($model) {
                                return $model['cardgood'] + $model['cardbad'] + $model['cardorder'];
                            },
                            'pageSummary' => true
                        ],

                    ],
                ]); ?>
            <?php endif ?>
        </div>
        <!-- /.box-body -->
    </div>
</div>
