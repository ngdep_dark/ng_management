<?php

use app\modules\store\models\Product;

$dataProduct = Product::find()->where(['id' => [1, 2, 62, 7, 9, 6, 8]])->all();
$CARD = [1, 2];
$INK = [7, 9, 6, 62];
$FILM = [8];

function CheckBalance($item)
{
    if ($item > 0) {
        echo 'bg-aqua';
    } else {
        echo 'bg-red';
    }
}

?>
<style>


</style>
<div class="container">
    <h1 class="text-right" style="font-size: 50px">Monitor Stock บัตร</h1>
    <div class="row">
        <h1 class="header-group"> <i class="fa fa-address-card-o" aria-hidden="true"></i> บัตร</h1>
        <div class="progress xs">
            <div class="progress-bar progress-bar-yellow" style="width: 90%;"></div>
        </div>
        <?php foreach (Product::find()->where(['id' => $CARD])->all() as $model): ?>
            <div class="info-box <?php CheckBalance($model->balance)?>">
                <span class="info-box-icons"></span>
                <div class="info-box-contents">
                    <span class="info-box-text">card</span>
                    <span class="info-box-number" style="font-size: 48px;">
                        <div class="row">
                            <div class="col-md-8"> <?= @$model->name ?></div>
                             <div class="col-md-3"> <?= @number_format($model->balance, 0) ?></div>
                        </div>
                    </span>
                    <div class="progress">
                        <div class="progress-bar" style="width: 20%"></div>
                    </div>
                    <span class="progress-description">
                  </span>
                </div>
                <!-- /.info-box-content -->
            </div>
        <?php endforeach; ?>
    </div>
    <div class="row">
        <h1 class="header-group"> <i class="fa fa-adjust" aria-hidden="true"></i> หมึก</h1>
        <div class="progress xs">
            <div class="progress-bar progress-bar-yellow" style="width: 90%;"></div>
        </div>
        <?php foreach (Product::find()->where(['id' => $INK])->orderBy('id')->all() as $model): ?>
            <div class="info-box <?php CheckBalance($model->balance)?>">
                <span class="info-box-icons"></span>
                <div class="info-box-contents">
                    <span class="info-box-text">INK</span>
                    <span class="info-box-number" style="font-size: 48px;">
                        <div class="row">
                            <div class="col-md-8"> <?= @$model->name ?></div>
                             <div class="col-md-3"> <?= @number_format($model->balance * $model->use, 0) ?></div>
                        </div>
                    </span>
                    <div class="progress">
                        <div class="progress-bar" style="width: 20%"></div>
                    </div>
                    <span class="progress-description">
                  </span>
                </div>
                <!-- /.info-box-content -->
            </div>
        <?php endforeach; ?>
    </div>
    <div class="row">
        <h1 class="header-group"> <i class="fa fa-briefcase" aria-hidden="true"></i> ฟิล์ม</h1>
        <div class="progress xs">
            <div class="progress-bar progress-bar-yellow" style="width: 90%;"></div>
        </div>
        <?php foreach (Product::find()->where(['id' => $FILM])->all() as $model): ?>
            <div class="info-box <?php CheckBalance($model->balance)?>">
                <span class="info-box-icons"></span>
                <div class="info-box-contents">
                    <span class="info-box-text">FILM</span>
                    <span class="info-box-number" style="font-size: 48px;">
                        <div class="row">
                            <div class="col-md-8"> <?= @$model->name ?></div>
                             <div class="col-md-3"> <?= @number_format($model->balance * $model->use, 0) ?></div>
                        </div>
                    </span>
                    <div class="progress">
                        <div class="progress-bar" style="width: 20%"></div>
                    </div>
                    <span class="progress-description">
                  </span>
                </div>
                <!-- /.info-box-content -->
            </div>
        <?php endforeach; ?>
    </div>
</div>

<?php
$this->registerCss("
.header-group{
    font-weight:bold;
    font-size:60px;
}
.info-box-contents {
        padding: 5px 10px;
        /* margin-left: 90px; */
    }
    .info-box-icons {
        border-top-left-radius: 2px;
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 2px;
        display: block;
        float: left;
//        height: 90px;
//        width: 90px;
//        text-align: center;
//        font-size: 45px;
//        line-height: 90px;
        /*background: rgba(0,0,0,0.2);*/
    }
", ['depends' => [\dmstr\web\AdminLteAsset::className()], \yii\web\View::POS_HEAD]);
$this->registerJs('
setTimeout(function(){ 
    location.reload();
}, 7200000);
'
);
?>
