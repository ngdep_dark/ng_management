<?php
Yii::$app->formatter->locale = 'th_TH';
use \app\modules\store\models\Product;
?>
<h2>คำนวนการสั่งซื้อ  <?=  Yii::$app->formatter->asDate($data['created_at'],'d/M/Y')?></h2>
<div class="row">
    <div class="col-xs-3">
        <h3>จำนวนที่ต้องการ : <?= number_format( $data['amount'])?></h3>
    </div>
    <div class="col-xs-4 text-center">
        <h3><strong> คำนวนเผื่อ % : </strong><?=$data['percent']?></h3>
    </div>
    <div class="col-xs-3 text-right">
        <h3>ยอดที่ได้ : <?php
            $cal =$data['percent']  * $data['amount'] / 100 ;

           echo number_format($cal + $data['amount'])?></h3>
    </div>
</div>
<table class="table table-hover">
    <thead>
    <tr>
        <th>#</th>
        <th  align="center">รายการสินค้า</th>
        <th  align="right">จำนวนคงเหลือ</th>
        <th  align="right">จำนวนที่พิมพ์ได้</th>
        <th  align="right">จำนวนที่ขาด</th>
        <th  align="right">จำนวนที่ต้องสั่งเพิ่ม</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach (\app\modules\store\models\CalculatorOrder::find()->where(['cal_id'=>$data['id']])->all() as$k=> $model):?>
    <tr>
        <td align="center"><?=$k+1?></td>
        <td><?php $pro =Product::findOne(['id'=>$model['product_id']]); echo $pro->name ?></td>
        <td align="right"><?=number_format($model['balance'])?></td>
        <td align="right"><?=number_format($model['balance'] * $model->amount_use)?></td>
        <td align="right"><?= number_format($model->amount_take)?></td>
        <td align="right"><?=number_format($model->amount_order) ?> <?=@$pro->group_per?></td>
    </tr>
    <?php endforeach;?>
    </tbody>
</table>