<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายชื่อโรงเรียน';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-index">

    <?php Pjax::begin(); ?>
    <div class="text-right">
        <?= Html::a('เพิ่มข้อมูล', ['create'], ['class' => 'btn btn-success']) ?>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'name',

            ['class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'delete' => function ($key, $model) {

                        $itemDetails = \app\modules\store\models\ItemDetails::findOne(['school_id' => $model->id]);
                        if (!isset($itemDetails)) {
                        return   Html::a('<i class="fa fa-trash"></i>', ['school/delete', 'id' => $model->id], [
                                'class' => '',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                            ]);
                     }
                        // var_dump($model);
                    }
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
