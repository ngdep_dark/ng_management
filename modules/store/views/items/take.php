<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\store\models\Items */

$this->title = 'เบิกสินค้า';
$this->params['breadcrumbs'][] = ['label' => 'Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="items-create">
    <div class="items-form">
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
                    </div>
                    <div class="panel-body">
                        <?php $form = ActiveForm::begin(); ?>
                        <?php
                        echo $form->field($model, 'product_id')->widget(\kartik\select2\Select2::classname(), [
                            'language' => 'th',
                            'data' => \yii\helpers\ArrayHelper::map(\app\modules\store\models\Product::find()->all(),'id','name'),
                            'options' => ['placeholder' => '',],
                            'pluginOptions' => [
//            'tags' => true,
                            ],
                        ]);
                        ?>
                        <?= $form->field($model, 'amount')->textInput() ?>
                        <?= $form->field($model, 'details')->textarea(['maxlength' => true,'rows'=>3]) ?>
                        <?= $form->field($model, 'use_check')->radioList([
                            1=>'บันทึกการใช้งาน', 0=>'ไม่บันทึกการใช้งาน'
                        ]) ?>
                        <div class="form-group">
                            <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-block']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="info-box bg-green">
                    <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Likes</span>
                        <span class="info-box-number">41,410</span>

                        <div class="progress">
                            <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                    70% Increase in 30 Days
                  </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <div class="col-md-5">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">iCheck - Checkbox &amp; Radio Inputs</h3>
                    </div>
                    <div class="box-body">
                    </div>
                </div>
            </div>
        </div>


    </div>

</div>
