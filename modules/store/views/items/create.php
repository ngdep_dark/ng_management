<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\store\models\Items */
$product='';
$take =Yii::$app->request->get('take');
if(!empty($take)){
    $product = \app\modules\store\models\Product::findOne($take);
    $this->title = ''.!empty($product->name)?'เพิ่มสินค้าเข้า & เบิกสินค้า ('. $product->name.' ) ':'';
    $model->product_id = $product->id;
}else{
    $this->title = 'เพิ่มสินค้าเข้า & เบิกสินค้า';
}

$this->params['breadcrumbs'][] = ['label' => 'Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title ;
$this->registerJsFile('@web/js/angular.min.js', ['depends' => [\yii\web\JqueryAsset::className(), \dmstr\web\AdminLteAsset::className()]]);
$this->registerJsFile('@web/js/app.js', ['depends' => [\yii\web\JqueryAsset::className(), \dmstr\web\AdminLteAsset::className()]]);


$provider = new \yii\data\ArrayDataProvider([
    'allModels' => Yii::$app->db->createCommand("SELECT ite.product_id, ite.status,ite.use_check ,ite.id,ite.details,p.name,ite.amount,p.locations, ite.created_at FROM items ite
LEFT join product p on ite.product_id = p.id
WHERE ite.status in (0,1)  
ORDER BY ite.id desc
 ")->queryAll(),
    'sort' => [
        'attributes' => ['name', 'locations', 'details'],
    ],
    'pagination' => [
        'pageSize' => 10,
    ],
]);


?>
<style>
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak {
        display: none !important;
    }
</style>
<div class="items-create" ng-app="appApp" ng-controller="appCtrl" ng-cloak>
    <div class="items-form">
        <div class="row">
            <div class="col-md-4">
                <div class="panel" ng-class="{'panel-success': add == true , 'panel-danger': take == true}">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?= Html::encode($this->title);  ?> </h3>
                    </div>
                    <div class="panel-body">
                        <div class="text-center">
                            <a class="btn btn-app " ng-class="{'bg-yellow': add == true}" ng-click="addProduct()">
                                <i class="fa fa-plus-square  text-success"></i> เพิ่มสินค้า
                            </a>
                            <a class="btn btn-app" ng-class="{'bg-yellow': take == true}" ng-click="takeProduct()">
                                <i class="fa  fa-minus-square text-danger"></i> เบิกสินค้า
                            </a>
                        </div>
                        <?php $form = ActiveForm::begin(); ?>
                        <?php
                        echo $form->field($model, 'product_id')->widget(\kartik\select2\Select2::classname(), [
                            'language' => 'th',
                            'data' => \yii\helpers\ArrayHelper::map(\app\modules\store\models\Product::find()->where(['status'=>1])->all(), 'id', 'name'),
                            'options' => ['placeholder' => '', 'ng-change'=>'balanceProduct()','ng-model' => 'product_id'],
                            'pluginOptions' => [

                            ],
                        ]);
                        ?>
                        <?= $form->field($model, 'amount')->textInput(['ng-model' => 'amount', 'type' => 'number', 'min' => 0, 'ng-model' => 'amount']) ?>
                        <?= $form->field($model, 'details')->textarea(['maxlength' => true, 'rows' => 3]) ?>
                        <div class="" ng-if="add">
                            <?= $form->field($model, 'status')->textInput()->hiddenInput(['value' => 1])->label(false) ?>
                            <?= $form->field($model, 'use_check')->radioList([
                                1 => 'บันทึกการใช้งาน', 0 => 'ไม่บันทึกการใช้งาน'
                            ])->hiddenInput(['value' => 0])->label(false) ?>
                            <div class="form-group">
                                <?= Html::submitButton('บันทึกการเพิ่มสินค้า', ['class' => 'btn btn-success btn-block']) ?>
                            </div>
                        </div>
                        <div class="" ng-if="take">
                            <?= $form->field($model, 'status')->textInput()->hiddenInput(['value' => 0])->label(false) ?>
                            <?= $form->field($model, 'use_check')->radioList([
                                1 => 'บันทึกการใช้งาน', 0 => 'ไม่บันทึกการใช้งาน'
                            ]) ?>
                            <div class="form-group">
                                <div class="alert alert-danger alert-dismissible"  style="display: none;">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <h4><i class="icon fa fa-ban"></i> ไม่สามารถเบิกได้ !</h4>
                                <p>เนื่องจากสินค้านี้ยังไม่บันทึกการใช้งาน...</p>
                                </div>
                                <button type="submit" class="btn btn-danger btn-block" ng-disabled="(Checkbtn || BalanceTake)">
                                       <span >บันทึกการเบิกสินค้า </span>
                                </button>
                            </div>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
            <div ng-class="{'col-md-3': product_id != null }" ng-repeat="model in Check(product_id)">
                <div class="info-box bg-green">
                    <span class="info-box-icon"><i class="glyphicon glyphicon-piggy-bank"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">ยอดคคงเหลือ <span
                                    class="text-green">{{CheckTake(model.amount)}}</span></span>
                        <span class="info-box-number" style="font-size: 24px;" ng-if="take">{{model.amount - amount| number }}</span>
                        <span class="info-box-number" style="font-size: 24px;" ng-if="add">{{model.amount + amount| number }}</span>
                        <div class="progress">
                            <div class="progress-bar" style="width: 90%"></div>
                        </div>
                        <span class="progress-description">
                  </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <div class="info-box bg-yellow">
                    <span class="info-box-icon"><i class="fa fa-location-arrow"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">สถานที่จัดเก็บ</span>
                        <span class="info-box-number" style="font-size: 24px;">{{model.locations}}</span>
                        <div class="progress">
                            <div class="progress-bar" style="width: 70%"></div>
                        </div>
                        <span class="progress-description">
                  </span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <div ng-class="{'col-md-5': product_id != null , 'col-md-8':product_id ==null}">
                <div class="box box-success">
                    <div class="box-header">
                        <h3 class="box-title">ประวัติ</h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                        <?= \yii\grid\GridView::widget([
                            'dataProvider' => $provider,
                            'rowOptions' => function ($model) {
                                if ($model['use_check'] == 1) {
                                    return ['class' => 'success'];
                                }
                            },
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                [
                                    'label' => 'ทำรายการ',
                                    'format' => 'raw',
                                    'attribute' => 'status',
                                    'value' => function ($model) {
                                        return $model['status'] == 1 ? '<span class="text-green">' . 'เพิ่มสินค้า' . '</span>' : '<span class="text-red">' . 'เบิกสินค้า' . '</span>';
                                    }
                                ],
                                [
                                    'attribute' => 'name',
                                    'label' => 'ข้อมูลสินค้า',
                                    'format' => 'raw',
                                    'value' => function($model){
                                        return Html::a($model['name'],['product/view','id'=>$model['product_id']]);
                                    }
                                ],
                                'name:text:รายการสินค้า',
                                'details:text:รายละเอียด',
                                [
                                    'label' => 'จำนวน',
                                    'format' => 'raw',
                                    'value' => function ($model) {
                                        return $model['status'] == 1 ? '<span class="text-green">' . $model['amount'] . '</span>' : '<span class="text-red">' . $model['amount'] . '</span>';
                                    }
                                ],
                                //'status',
                                //'status_order',
                                //'created_by',
                                //'updated_by',
                                'created_at:text:วันที่',
                                //'updated_at',
                                ['class' => 'yii\grid\ActionColumn', 'template' => '{delete}', 'buttons' => [
                                    'delete' => function ($model, $data) {
                                        $date = Yii::$app->formatter->asDate(time(), 'php:Y-m-d');
                                        $dateCreate = Yii::$app->formatter->asDate($data['created_at'], 'php:Y-m-d');
                                        if($date ==$dateCreate) {
                                            return Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $data['id']], [
                                                'data' => [
                                                    'confirm' => 'Are you sure you want to delete this item?',
                                                    'method' => 'post',
                                                ],
                                            ]);
                                        }
                                    }
                                ]],
                            ],
                        ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

</div>

