<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\store\models\Items */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="items-form">
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Panel title</h3>
                </div>
                <div class="panel-body">

            <?php $form = ActiveForm::begin(); ?>
            <?php
            echo $form->field($model, 'product_id')->widget(\kartik\select2\Select2::classname(), [
                'language' => 'th',
                'data' => \yii\helpers\ArrayHelper::map(\app\modules\store\models\Product::find()->all(),'id','name'),
                'options' => ['placeholder' => '',],
                'pluginOptions' => [
                    // 'allowClear' => true,
//            'tags' => true,
                ],
            ]);
            ?>
            <?= $form->field($model, 'amount')->textInput() ?>
            <?= $form->field($model, 'details')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'use_check')->textInput() ?>
            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-block']) ?>
            </div>
            <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
        <div class="col-md-8"></div>
    </div>


</div>
