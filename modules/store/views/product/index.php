<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\store\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ข้อมูลสินค้า';
$this->params['breadcrumbs'][] = $this->title;

?>


            <?php Pjax::begin(); ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <div class="text-right">
                <?= Html::a('<i class="fa fa-plus"></i> เพิ่มข้อมูลสินค้า', ['create'], ['class' => 'btn btn-success']) ?>
            </div>
            <div class="table-responsive">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'hover' => true,
                'responsiveWrap' => false,
                'panel' => [
                    'type' => GridView::TYPE_SUCCESS,
                    'heading' => $this->title,
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'options' => ['style' => 'width:150px;'],
                        'format' => 'raw',
                        'attribute' => 'photos',
                        'value' => function ($model) {
                            return Html::tag('div', '', [
                                'style' => 'width:120px;height:100px;
                          background-image:url(' . $model->photoViewer . ');
                          background-size: cover;
                          background-position:center center;
                          background-repeat:no-repeat;
                          ']);
                        }
                    ],
                    'code',
                    [
                        'attribute' => 'name',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a($model['name'], ['product/view', 'id' => $model['id']]);
                        }
                    ],

                    [
                        'attribute'=>'types_id',
                        'width'=>'150px',
                        'value'=>function ($model, $key, $index, $widget) {
                            return $model->nameType;
                        },
                        'filterType'=>GridView::FILTER_SELECT2,
                        'filter'=>ArrayHelper::map(\app\modules\store\models\Types::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['allowClear'=>true],
                        ],
                        'filterInputOptions'=>['placeholder'=>'เลือกประเภทสินค้า..']
                    ],
                    'detail',
                    //'group_type',
                    //'status',
                    //'price',
                    'locations',
                    [
                         'format' => ['decimal',0],
                         'width' => '100px',
                        'contentOptions' => ['class'=>'text-center text-success'],
                        'attribute' => 'balance'
                    ],
                    [
                        'format' => 'raw',
                        'label' => 'ใช้งานได้',
                        'width' => '90px',
                        'contentOptions' => ['class'=>'text-center '],
                        'value' => function($model){
                        $use = strval($model->use);
                        $type =!empty($model->group_per)?$model->group_per:'ไม่ได้กำหนด !';
                            return $model->use .' / '.$type;
                        }
                    ],
                    ['class' => 'yii\grid\ActionColumn',
                        'template' => '{view} {update}',
                        'buttons' => [
                            'take' => function ($url,$model, $key) {
                                return Html::a('<i class="glyphicon glyphicon-briefcase"></i>',$url);
                            }
                        ]
                    ],
                ],
            ]); ?>
            </div>

    <?php Pjax::end(); ?>

