<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\data\ActiveDataProvider;
$dataProvider = new ActiveDataProvider([
    'query' => \app\modules\store\models\Items::find()->where(['product_id'=>$product->id,'status'=>0]),
]);
?>
<div class="row">
    <div class="col-md-5">
        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'product_id')->textInput([ 'value' => $product->name,'disabled' => true]) ?>
        <?= $form->field($model, 'amount')->textInput([ 'type' => 'number', 'min' => 0]) ?>
        <?= $form->field($model, 'details')->textarea(['maxlength' => true, 'rows' => 3]) ?>
        <?= $form->field($model, 'use_check')->radioList([
            1 => 'บันทึกการใช้งาน', 0 => 'ไม่บันทึกการใช้งาน'
        ]) ?>

        <?= Html::submitButton('บันทึกการเบิกสินค้า', ['class' => 'btn btn-success btn-block']) ?>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="col-md-6">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'hover' => true,
            'responsiveWrap' => false,
            'panel' => [
                'type' => GridView::TYPE_SUCCESS,
                'heading' => $this->title,
            ],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'amount',
                'details',
                //'group_type',
                //'status',
                //'price',
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{delete-item} ',
                    'buttons' => [
                        'delete-item' => function ($url,$model, $key) use ($product) {
                         return   Html::a('<i class="glyphicon glyphicon-trash"></i>', ['product/delete-item','id'=>$model->id,'page'=>$model->product_id], [
                                'class' => '',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                            ]);
                        }
                    ]
                ],
            ],
        ]); ?>
    </div>
</div>
