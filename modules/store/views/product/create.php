<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\store\models\Product */

$this->title = 'สร้างสินค้า';
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
