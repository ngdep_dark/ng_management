<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;
/* @var $this yii\web\View */
/* @var $model app\modules\store\models\Product */
/* @var $form yii\widgets\ActiveForm */

?>
<?php
?>


<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-5">
            <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            <?php
            echo $form->field($model, 'types_id')->widget(\kartik\select2\Select2::classname(), [
                'language' => 'th',
                'data' => \yii\helpers\ArrayHelper::map(\app\modules\store\models\Types::find()->all(), 'id', 'name'),
                'options' => ['placeholder' => '',],
                'pluginOptions' => [
                    // 'allowClear' => true,
                    'tags' => true,
                ],
            ])->hint('<code>สามารถเพิ่มใหม่ได้ </code>');
            ?>
            <?php
            echo $form->field($model, 'group_type')->widget(\kartik\select2\Select2::classname(), [
                'language' => 'th',
                'data' => \yii\helpers\ArrayHelper::map(\app\modules\store\models\Product::find()->distinct('group_type')->all(), 'group_type', 'group_type'),
                'options' => ['placeholder' => '',],
                'pluginOptions' => [
                    // 'allowClear' => true,
                    'tags' => true,
                ],
            ])->hint('<code>สามารถเพิ่มใหม่ได้ </code>');
            ?>
            <?php
            echo $form->field($model, 'photos')->widget(FileInput::classname(), [
                'options' => ['accept' => 'image/*'],
            ]);
            ?>

        </div>
        <div class="col-md-7">
            <?= $form->field($model, 'locations')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'price')->textInput(['type'=>'number']) ?>
            <?= $form->field($model, 'use')->textInput(['maxlength' => true,'type'=>'number']) ?>
            <?= $form->field($model, 'group_per')->textInput() ?>
            <?= $form->field($model, 'detail')->textarea(['maxlength' => true, 'rows' => 3]) ?>
            <?= $form->field($model, 'lock_cal_products')->radioList([1 => 'เปิด Lock สินค้า', 0 => 'ไม่ใช้งาน']) ?>
            <?= $form->field($model, 'lock_card_bad')->radioList([1 => 'เปิด Lock สินค้าบัตร test', 0 => 'ไม่ใช้งาน']) ?>
            <?= $form->field($model, 'status')->radioList([1 => 'เปิดใช้งาน', 0 => 'ไม่ใช้งาน']) ?>
        </div>
    </div>

    <div class="form-group text-right">
        <?= Html::submitButton('บันทึกข้อมูล', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
