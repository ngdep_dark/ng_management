<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$username = ['lnwdark'];

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">
    <div class="text-right">
        <?= Html::a('เพิ่มสินค้าเข้า & เบิกสินค้าออก', ['/store/items/create', 'take' => $model->id], ['class' => 'btn btn-success']) ?>
        <?= Html::a('แก้ไขข้อมูลสินค้า', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">รายละเอียดสินค้า</h3>
                </div>
                <div class="box-body">
                    <div class="info-box bg-green">
                        <span class="info-box-icon"><i class="glyphicon glyphicon-piggy-bank"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">ยอดคคงเหลือ</span>
                            <span class="info-box-number" style="font-size: 26px;"><?= $model->getBalance() ?></span>
                            <div class="progress">
                                <div class="progress-bar" style="width: 70%"></div>
                            </div>
                            <span class="progress-description">
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <div class="info-box bg-yellow">
                        <span class="info-box-icon"><i class="fa fa-map"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">ที่จัดเก็บ</span>
                            <span class="info-box-number" style="font-size: 20px;"><?= $model->locations ?></span>
                            <div class="progress">
                                <div class="progress-bar" style="width: 50%"></div>
                            </div>
                            <span class="progress-description">
                            </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            //'id',
                            'code',
                            'nameType',
                            'name',
                            'detail',
                            'group_type',
                            'price',
                            [
                                'attribute' => 'balance'
                            ],
                            'nameuser:text:สร้างโดย',
                            // 'updated_by',
                            'created_at',
                            // 'updated_at',
                        ],
                    ]) ?>
                </div>
                <!-- /.box-body -->
            </div>

        </div>
        <div class="col-md-8">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Transactions</h3>
                </div>
                <div class="box-body">
                    <?= \yii\grid\GridView::widget([
                        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => ''],
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            [
                                'label' => 'ผู้สร้าง',
                                'attribute' => 'username',
                                'contentOptions' => ['class' => 'text-center text-info'],
                                'headerOptions' => ['class' => 'text-center']
                            ],
                            'created_at:date:วันที่',
                            [
                                'attribute' => 'details',
                                'format' => 'raw',
//                            'contentOptions' => ['style' => 'width:50px; '],
                            ],
                            [
                                'label' => 'เพิ่มสินค้า',
                                'format' => ['decimal', 0],
                                'attribute' => 'addamount',
                                'contentOptions' => ['class' => 'text-center text-yellow'],
                                'headerOptions' => ['class' => 'text-center']
                            ],
                            [
                                'label' => 'เบิกสินค้า',
                                'format' => ['decimal', 0],
                                'attribute' => 'takeamount',
                                'contentOptions' => ['class' => 'text-center text-red'],
                                'headerOptions' => ['class' => 'text-center']
                            ],
                            [
                                'label' => 'ยอดคงเหลือ',
                                'attribute' => 'balance',
                                'format' => ['decimal', 0],
                                'contentOptions' => ['class' => 'text-center text-green'],
                                'headerOptions' => ['class' => 'text-center']
                            ],
                            [
                                'format' => 'raw',
                                'value' => function ($model) {
                                    $date = Yii::$app->formatter->asDate(time(), 'php:Y-m-d');
                                    $dateCreate = Yii::$app->formatter->asDate($model['created_at'], 'php:Y-m-d');
                                    if ($date === $dateCreate) {
                                        return Html::a('<i class="glyphicon glyphicon-remove-circle"></i>', ['product/delete-lock', 'id' => $model['items_id'], 'page' => $model['product_id']]);
                                    }
                                }
                            ]

//                'amount:text:ยอดคงเหลือ',
                        ],
                    ]); ?>
                </div>
                <!-- /.box-body -->
            </div>
        </div>

    </div>
</div>