<?php

use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
function Db($sql)
{
    return Yii::$app->db->createCommand($sql);
}

$this->title = 'บันทึการใช้งาน';
\app\assets\StoreAsset::register($this);

$searchModel = new \app\modules\store\models\StoreSearch();
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

?>
<style>
    [v-cloak] {
        display: none
    }

    .multiselect__input, .multiselect__single {
        min-height: 33px;
    }
</style>
<div class="row" id="app-take" v-cloak>
    <div class="col-md-5">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">บันทึกรายการ</h3>
            </div>
            <div class="box-body text-center">
                <div class="btn-group ">
                    <button type="button" :class="!cardReplace ? 'btn btn-warning' : 'btn btn-success'"
                            @click="CardReplacesEvent"> replace
                    </button>
                    <button type="button" :class="!allSelected ? 'btn btn-warning' : 'btn btn-success'"
                            @click="selectAll"> พิมพ์จาก order
                    </button>
                    <button type="button" :class="!CradGoods ? 'btn btn-warning' : 'btn btn-success'" @click="cardGood">
                        บันทึกบัตรดี
                    </button>
                    <button type="button" :class="!cardBad ? 'btn btn-warning' : 'btn btn-success'" @click="cardBadBtn">
                        บันทึกบัตรเสีย
                    </button>
                </div>
                <hr>
                <input type="text" class="form-control" v-model="searchReplace" @input="getDataReplace"
                       placeholder="พิมพ์หมายเลข Replace " v-if="cardReplace">
                <input type="text" class="form-control" v-model="searchOrder" @input="getOrder"
                       placeholder="พิมพ์หมายเลข order " v-if="allSelected">
            </div>
        </div>
        <div class="box box-solid " v-if="replace_details.length > 0">
            <div class="box-header with-border">
                <h3 class="box-title">บันทึกรายการจาก Replace</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                </div>
            </div>

            <div class="box-body" v-if="replace_details.length > 0">
                <div class="row">
                    <div class="alert alert-danger alert-dismissible" v-if="CheckSelect">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> ไม่สามารถบันทึกการใช้งานได้</h4>
                        <li>เนื่องจากสินค้าที่เบิกไม่เพียงพอ !...</li>
                    </div>
                    <div class="col-md-6">
                        <h4> {{SchoolName}}</h4>
                        <h4> จำนวนบัตร {{replace_details.length}} ใบ</h4>
                    </div>
                    <div class="col-md-4 pull-right">
                        <button type="button" class="btn btn-success  btn-block" :disabled="CheckSelect"
                                @click="saveRepalce">บันทึกข้อมูล
                        </button>
                    </div>
                </div>
                <hr>

                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>รายชื่อ</th>
                        <th>ห้อง</th>
                        <th>จำนวน</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="(model,index) in replace_details">
                        <td>{{index+1}}</td>
                        <td>{{model.student_name}}</td>
                        <td>{{model.class}}</td>
                        <td>{{1}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="overlay" v-else>
                <i class="fa fa-circle-o-notch fa-spin"></i>
            </div>
        </div>
        <div class="box box-success " v-if="dataOrder.length > 0">
            <div class="box-header with-border">
                <h3 class="box-title">บันทึกรายการจาก Order</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                </div>
            </div>

            <div class="box-body" v-if="dataOrder.length > 0">
                <div class="row">
                    <div class="alert alert-danger alert-dismissible" v-if="CheckSelect">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> ไม่สามารถบันทึกการใช้งานได้</h4>
                        <li>เนื่องจากสินค้าที่เบิกไม่เพียงพอ !...</li>
                    </div>
                    <div class="col-md-6">
                        <h4> {{SchoolName}}</h4>
                        <h4> จำนวนบัตร {{dataOrder.length}} ใบ</h4>
                    </div>
                    <div class="col-md-4 pull-right">
                        <button type="button" class="btn btn-success  btn-block" :disabled="CheckSelect"
                                @click="saveOrder">บันทึกข้อมูล
                        </button>
                    </div>
                </div>
                <hr>
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>รายชื่อ</th>
                        <th>ห้อง</th>
                        <th>จำนวน</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="(model,index) in dataOrder">
                        <td>{{index+1}}</td>
                        <td>{{model.name}}</td>
                        <td>{{model.class}}</td>
                        <td>{{model.amount}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="overlay" v-else>
                <i class="fa fa-circle-o-notch fa-spin"></i>
            </div>
        </div>
        <div class="box box-success box-solid" v-if="CradGoods">
            <div class="box-header with-border">
                <h3 class="box-title">บันทึกรายการบัตรดี</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-8">
                        <label>เลือกโรงเรียน</label>
                        <v-select :options="options" v-model="SelectSchool"></v-select>
                    </div>
                    <div class="col-xs-4">
                        <label>จำนวน</label>
                        <input type="number" class="form-control" placeholder="" min="0" v-model="amount"
                               @input="selectCheckAmount()">
                    </div>
                </div>

                <hr>
                <div class="row">
                    <div class="col-md-3">
                        <input type="radio" id="std" v-model="checked_card" value="5" checked>
                        <label for="std"> บัตรนักเรียน</label>
                    </div>
                    <div class="col-md-3">
                        <input type="radio" id="std" v-model="checked_card" value="6">
                        <label for="std"> บัตรครู</label>
                    </div>
                </div>
                <div class="alert alert-danger alert-dismissible" v-if="CheckSelect">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> ไม่สามารถบันทึกการใช้งานได้</h4>
                    <li>เนื่องจากสินค้าที่เบิกไม่เพียงพอ !...</li>
                </div>
                <button type="submit" class="btn btn-success pull-right" :disabled="!btnGood || CheckSelect"
                        @click="saveCardGood">
                    บันทึกข้อมูล
                </button>
            </div>
        </div>
        <div class="box box-danger box-solid" v-if="cardBad">
            <div class="box-header with-border">
                <h3 class="box-title">บันทึกรายการบัตรเสีย</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">

                <div class="row">
                    <div class="col-xs-8">
                        <label>เลือกโรงเรียน</label>
                        <v-select :options="options" v-model="SelectSchool"></v-select>
                    </div>
                    <div class="col-xs-4">
                        <label>จำนวน</label>
                        <input type="number" class="form-control" placeholder="" min="0" v-model="amount"
                               @input="selectCheckAmount()">
                    </div>
                </div>
                <div class="alert alert-danger alert-dismissible" v-if="CheckSelect">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-ban"></i> ไม่สามารถบันทึกการใช้งานได้</h4>
                    <li>เนื่องจากสินค้าที่เบิกไม่เพียงพอ !...</li>
                </div>
                <hr>
                <textarea rows="2" placeholder="สาเหตุ" class="form-control" v-model="causes"></textarea>
                <div class="row">
                    <div class="col-md-3">
                        <input type="radio" id="std" v-model="checked_card" value="5" checked>
                        <label for="std"> บัตรนักเรียน</label>
                    </div>
                    <div class="col-md-3">
                        <input type="radio" id="std" v-model="checked_card" value="6">
                        <label for="std"> บัตรครู</label>
                    </div>
                </div>

                <label for="">อาการเสีย</label>
                <input type="radio" id="cardOne" value="0" v-model="cardBadselect">
                <label for="cardOne">เสีย 1 หน้า </label>
                <input type="radio" id="cardTwo" value="1" v-model="cardBadselect">
                <label for="cardTwo">เสีย 2 หน้า </label>
                <br>
                <div v-if="cardBadselect === '0'">
                    <span class="text-danger"> กรณีเสีย 1 หน้า จะบันทึกเข้าไปให้สินค้าบัตรเทส อัตโนมัติ (เฉพาะสินค้าที่เลือก lockไว้)</span>
                </div>
                <button type="submit" class="btn btn-success pull-right"
                        :disabled="cardBadselect === null ? true : false || CheckSelect || !btnGood"
                        @click="saveCardBad">บันทึกข้อมูล
                </button>
            </div>
        </div>

    </div>
    <div class="col-md-7">
        <div class="box box-success">
            <div class="box-header">
                <h3 class="box-title">รายการเบิก</h3>
            </div>
            <div class="overlay" v-if="!isLoading">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
            <div class="box-body">
                <table class="table table-hover" v-if="isLoading">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>รายการ</th>
                        <th>เบิก</th>
                        <th>พิมพ์ได้ / ใช้ไป</th>
                        <th>คงเหลือ</th>
                        <th>จัดการ</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="(model,index,key) in filterTake" :class="checkSelect(model.id),">
                        <td><input type="checkbox" :id="model.id" :value="model.id" v-model="select"
                                   @click="selectId()"> {{key+1}}
                        </td>
                        <td><label :for="model.id">{{model.name}}</label></td>
                        <td>{{model.amount }}</td>
                        <td v-if="model.lock_cal_products === 1">{{model.amount }} / <span class="text-red">{{model.take_amount }}</span>
                        <td v-else>{{model.amount * model.use }} / <span class="text-red">{{model.take_amount }}</span>
                        </td>
                        <td v-if="model.lock_cal_products === 1">{{model.amount - model.take_amount }}</td>
                        <td v-else>{{model.amount * model.use - model.take_amount }}</td>

                        <td><a href="#" class="btn  btn-xs btn-danger" @click="removeTake(model)">ยกเลิกสินค้า</a>
                            <a href="#" class="btn  btn-xs btn-success" @click="cancelTake(model)"
                               v-if="model.take_amount < 0">คืนสินค้า</a></td>

                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="box box-warning box-solid">
    <div class="box-header with-border">
        <h3 class="box-title">ประวัติ</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
        <!-- /.box-tools -->
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <div class="text-right">
            <button class="btn btn-danger" id="btn-delete">Delete Select</button>
        </div>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'rowOptions' => function ($model) {
                if ($model['status'] == 1) {
                    return ['class' => 'success'];
                } elseif ($model['status'] == 2) {
                    return ['class' => 'danger'];
                } elseif ($model['status'] == 4) {
                    return ['class' => 'warning'];

                } else {
                    return ['class' => 'info'];
                }
            },
            'columns' => [
                ['class' => 'kartik\grid\SerialColumn'],
                ['class' => 'kartik\grid\CheckboxColumn'],
                [
                    'label' => 'สินค้า',
                    'attribute' => 'product_name',
                    'value' => function ($model) {
                        return $model['product_name'] . ' (เบิก #' . $model['item_id'] . ')';
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => ArrayHelper::map(\app\modules\store\models\Product::find()->orderBy('name')->asArray()->all(), 'name', 'name'),
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'filterInputOptions' => ['placeholder' => 'ค้นหาสินค้า'],
                    'format' => 'raw'
                ],
                [
                    'label' => 'การบันทึก',
                    'attribute' => 'school_name',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        $status = \app\modules\store\models\ItemDetails::Status($model['status']);
                        $order = $model['status'] == 3 || 4 ? '( ' . $model['order_id'] . ' )' : '';
                        if ($model['card_use'] == 5) {
                            $card_use = '( บัตรนักเรียน )';
                        } elseif ($model['card_use'] === 6) {
                            $card_use = '( บัตรครู )';
                        } else {
                            $card_use = '';
                        }
                        return $status . ' ' . $order . ' # โรงเรียน' . $model['school_name'] . ' ' . $card_use;
                    },
                    'filterType' => GridView::FILTER_SELECT2,
                    'filter' => ArrayHelper::map(\app\modules\store\models\School::find()->orderBy('name')->asArray()->all(), 'name', 'name'),
                    'filterWidgetOptions' => [
                        'pluginOptions' => ['allowClear' => true],
                    ],
                    'filterInputOptions' => ['placeholder' => 'ค้นหาโรงเรียน'],
                    'format' => 'raw'
                ],
                [
                    'label' => 'จำนวน',
                    'contentOptions' => ['class' => 'text-right'],
                    'attribute' => 'amount'
                ],
                [
                    'label' => 'วันที่ทำรายการ',
                    'attribute' => 'created_at'
                ],
                [
                    'label' => 'ผู้บันทึก',
                    'attribute' => 'username'
                ],
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
                    'buttons' => [
                        'update' => function ($url, $model, $key) {
                            $url = \yii\helpers\Url::to(['default/update-details', 'id' => $model['id']]);
                            return \yii\helpers\Html::a('<i class="glyphicon glyphicon-pencil"></i>', $url);
                        },
                        'delete' => function ($url, $model, $key) {
                            $url = \yii\helpers\Url::to(['default/delete-details', 'id' => $model['id']]);
                            return \yii\helpers\Html::a('<i class="glyphicon glyphicon-trash"></i>', $url);
                        }
                    ]
                ],
            ],
            'responsive' => true,
            'hover' => true
        ]); ?>
    </div>
    <!-- /.box-body -->
</div>
<div class="control-sidebar-bg"></div>

<?php
$this->registerJs('
  jQuery("#btn-delete").click(function(){
    var keys = $("#w0").yiiGridView("getSelectedRows");
    let count = keys.length;
     console.log(count);
     if(count > 0){
       let con = confirm("ยืนการลบทั้งหมด "+count +" รายการ")
       if(con){
            jQuery.post("'.Url::to(['default/delete-all-default']).'",{ids:keys.join()},function(){
                
            });
       }
     }else{
        
     }
     
  });
');
?>