<?php

use yii\widgets\ActiveForm;
use app\modules\store\models\Product;
use app\modules\store\models\School;
use yii\helpers\Html;
?>
<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-3">
        <?= $form->field($model, 'product_id')->textInput(['value'=>Product::findOne(['id'=>$model->product_id])->name,'disabled'=>true])->label('ข้อมูลสินค้า') ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'school_id')->textInput(['value'=>School::findOne(['id'=>$model->school_id])->name,'disabled'=>true])->label('ข้อมูลโรงเรียน') ?>
    </div>
    <div class="col-md-3">
        <?= $form->field($model, 'amount')->textInput(['type'=>'number'])->label('จำนวน') ?>
    </div>
    <div class="col-md-3">
        <br>
        <?= Html::submitButton('Update', ['class' => 'btn btn-info ']) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>