<?php
$this->title = "คำนวนการสั่งซื้อ";

use yii\helpers\Html;

\app\assets\StoreAsset::register($this);
?>
<style>
    /*.multiselect__input, .multiselect__single {*/
        /*min-height: 33px;*/
    /*}*/
    [v-cloak] {display: none}
    /*.v-select .dropdown-toggle .clear {*/
        /*position: absolute;*/
        /*bottom: 9px;*/
        /*right: 30px;*/
        /*font-size: 23px;*/
        /*font-weight: 700;*/
        /*line-height: 1;*/
        /*color: rgba(60, 60, 60, .5);*/
        /*padding: 0;*/
        /*border: 0;*/
        /*background-color: transparent;*/
        /*cursor: pointer;*/
        /*display: none;*/
    /*}*/
</style>
<div class="box box-success box-solid" id="cal-stock" v-cloak>
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo Html::decode($this->title) ?></h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="text-right" v-if="inputProduct.length > 1">
        <button class="btn btn-warning btn-sm" @click="Export()"><i class="fa fa-print"></i> บันทึกและออกรายงาน</button>
        </div>
        <div class="row">

            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1">จำนวน</label>
                    <input type="number" class="form-control text-right" id="exampleInputEmail1" v-model="amount"
                           placeholder="จำนวนการผลิต">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1">คำนวนเผื่อ %</label>
                    <input type="text" class="form-control text-right" maxlength="3" min='0' placeholder="%"
                           v-model="percent">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="exampleInputEmail1">ยอด</label>
                    <input type="text" class="form-control text-right"  disabled :value="SumAmount">
                </div>
            </div>
        </div>
        <table class="table table-bordered table-striped " v-if="amount!=null">
            <thead>
            <tr>
                <th>#</th>
                <th>รายการสินค้า</th>
                <th class="text-right">จำนวนคงเหลือ</th>
                <th class="text-right">จำนวนที่พิมพ์ได้</th>
                <th class="text-right">จำนวนที่ขาด</th>
                <th class="text-right">จำนวนที่ต้องสั่งเพิ่ม</th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="(model,index) in inputProduct">
                <td>{{index+1}}</td>
                <td>
                    <v-select label="label" :options="DataProduct" v-model="model.product" @input="Product"
                              :searchable="false" :show-labels="false"></v-select>
                </td>
                <td class="text-right">
                    <h5> {{model.amount}} </h5>
                </td>
                <td class="text-right">
                    <h5> {{ model.balance |formatNumber}} </h5>
                </td>
                <td class="text-right">
                    <h5 :class="model.takeAmount > 0 ? 'text-green' : 'text-red' "> {{ model.takeAmount=SumBalance(model.balance ,SumAmount) |formatNumber}}</h5>
                </td>
                <td class="text-right">
                    <h5> {{ model.takeOrder =TakeOut(model.balance,model.use) |formatNumber}} </h5>
                </td>
                <td>
                    <button class="btn btn-danger" @click="Remove(model)"><i class="fa fa-trash-o"></i></button>
                </td>
            </tr>
            </tbody>
        </table>

        <button class="btn btn-success" @click="addForm"> เพิ่มรายการ</button>
    </div>
</div>
