<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\store\models\CodeDetails */

$this->title = 'Create Code Details';
$this->params['breadcrumbs'][] = ['label' => 'Code Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="code-details-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
