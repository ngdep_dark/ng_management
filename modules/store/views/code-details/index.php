<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\store\models\CodeDetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'อธิบาย Code สินค้า';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="code-details-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="text-right">
        <?= Html::a('Create Code Details', ['create'], ['class' => 'btn btn-success']) ?>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'code',
            'detail',
          //  'status',
//            'created_by',
            //'updated_by',
            'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn','template' => '{update} {delete}'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
