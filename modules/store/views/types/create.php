<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\store\models\Types */

$this->title = 'สร้างประเภทสินค้า';
$this->params['breadcrumbs'][] = ['label' => 'ประเภทสินค้า', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="types-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
