<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\store\models\Types */

$this->title = 'แก้ไขประเภทสินค้า: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'ประเภทสินค้า', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="types-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
