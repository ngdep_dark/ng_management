<?php

namespace app\modules\store;

use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * store module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\store\controllers';

    /**
     * {@inheritdoc}
     */

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
