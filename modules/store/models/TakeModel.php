<?php

namespace app\modules\store\models;

use yii\base\Model;
use app\models\User;

/**
 * Signup form
 */
class TakeModel extends Model
{
    public $product;
    public $date;
     public $status;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date', 'product','status'], 'required'],
            [['product', 'date','status'], 'safe']
        ];
    }


}
