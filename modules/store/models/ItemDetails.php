<?php

namespace app\modules\store\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "item_details".
 *
 * @property int $id
 * @property int $item_id
 * @property int $amount
 * @property int $school_id
 * @property int $product_id
 * @property int $status
 * @property string $card_lose
 * @property string $card_lose_2
 * @property int $created_by
 * @property string $order_id
 * @property string $card_use
 * @property int $updated_by
 * @property string $created_at
 * @property string $updated_at
 * @property string $causes
 */
class ItemDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'item_details';
    }
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_id', 'amount', 'school_id', 'status', 'created_by', 'updated_by','causes'], 'default', 'value' => null],
            [['item_id', 'amount', 'school_id', 'status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at','card_lose_2','card_lose','product_id','order_id','card_use','causes'], 'safe'],
            //[['card_lose', 'card_lose_2'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'item_id' => 'Item ID',
            'amount' => 'Amount',
            'school_id' => 'School ID',
            'status' => 'Status',
            'card_lose' => 'Card Lose',
            'card_lose_2' => 'Card Lose 2',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    static function Status($key){
        $data = [
            1=>'บัตรดี',
            2=>'บัตรเสีย',
            3=>'ออเดอร์',
            4=>'Replace'
        ];
        return $data[$key];
    }
}
