<?php

namespace app\modules\store\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "calculator_order".
 *
 * @property int $id
 * @property int $amount
 * @property int $percent
 * @property int $cal_id
 * @property int $product_id
 * @property int $balance
 * @property int $amount_use
 * @property int $amount_take
 * @property int $amount_order
 * @property string $created_at
 * @property string $updated_at
 */
class CalculatorOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'calculator_order';
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['amount', 'percent', 'cal_id', 'product_id', 'balance', 'amount_use', 'amount_take', 'amount_order'], 'default', 'value' => null],
            [['amount', 'percent', 'cal_id', 'product_id', 'balance', 'amount_use', 'amount_take', 'amount_order'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'amount' => 'จำนวนที่ต้องการ',
            'percent' => 'คิดเปอร์เซ็นต์',
            'cal_id' => 'Cal ID',
            'product_id' => 'Product ID',
            'balance' => 'ยอดที่ได้',
            'amount_use' => 'Amount Use',
            'amount_take' => 'Amount Take',
            'amount_order' => 'Amount Order',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
