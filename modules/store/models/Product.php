<?php

namespace app\modules\store\models;

use app\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $code
 * @property int $types_id
 * @property string $name
 * @property string $detail
 * @property string $group_type
 * @property int $status
 * @property double $price
 * @property string $locations
 * @property int $created_by
 * @property int $updated_by
 * @property int $lock_cal_products
 * @property string $created_at
 * @property string $updated_at
 * @property string $group_per
 * @property int $use
 * @property  int $lock_card_bad
 * @property string $photos
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['code', 'filter', 'filter' => 'trim'],
            ['code', 'required'],
            ['code', 'unique', 'targetClass' => 'app\modules\store\models\Product', 'message' => 'รหัสสินค้ามีในระบบแล้ว !'],
            [['status','name','types_id'],'required'],
           // [['types_id', 'status', 'created_by', 'updated_by'], 'default', 'value' => null],
           // [['types_id', 'status', 'created_by', 'updated_by'], 'integer'],
            [['price'], 'number'],
            [['created_at', 'updated_at','use','group_per','lock_cal_products','photos','lock_card_bad'], 'safe'],
            [['code', 'name', 'detail', 'group_type', 'locations'], 'string', 'max' => 255],
            [['photos'], 'file',
                'skipOnEmpty' => true,
                'extensions' => 'png,jpg'
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'group_per'=>'หน่วย / ชิ้น',
            'lock_card_bad'=>'บันทึกเข้าบัตร test',
            'lock_cal_products'=>'เปิด functions คำนวนตัวต่อ 1',
            'code' => 'Code รหัสสินค้า',
            'types_id' => 'ประเภทสินค้า',
            'name' => 'ขื่อสินค้า',
            'detail' => 'รายละเอียดสินค้า',
            'group_type' => 'Group Type',
            'status' => 'สถานะ',
            'nameType'=>'ประเภทสินค้า',
            'price' => 'ราคา',
            'locations' => 'ที่จัดเก็บสินค้า',
            'created_by' => 'สร้างโดย',
            'updated_by' => 'Updated By',
            'created_at' => 'วันที่สร้าง',
            'balance'=>'ยอดคงเหลือ',
            'use'=>'จำนวนที่ใช้งาน',
            'updated_at' => 'Updated At',
        ];
    }
    public function getBalance(){
         return Items::find()->where(['product_id'=>$this->id])->sum('amount');
    }
    public function getType(){
        return $this->hasOne(Types::className(),['id'=>'types_id']);
    }
    public function getNameType(){
        return $this->type->name;
    }
    public function getUser(){
        return $this->hasOne(User::className(),['id'=>'created_by']);
    }
    public function getNameUser(){
        return $this->user->username;
    }
    public function getPhotoViewer(){
        return empty($this->photos) ? Yii::getAlias('@web').'/photos_products/none-img/none.png' : Yii::getAlias('@web').'/'.'photos_products/'.$this->photos;
    }
}
