<?php

namespace app\modules\store\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "items".
 *
 * @property int $id
 * @property int $product_id
 * @property string $details
 * @property int $use_check
 * @property int $amount
 * @property int $status
 * @property int $status_order
 * @property int $created_by
 * @property int $updated_by
 * @property string $created_at
 * @property string $updated_at
 */
class Items extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'items';
    }
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
           [['product_id', 'use_check', 'amount'],'required'],
            [['product_id', 'use_check', 'amount', 'status', 'status_order', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['product_id', 'use_check', 'amount', 'status', 'status_order', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['details'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'สินค้า',
            'details' => 'รายละเอียด',
            'use_check' => 'การใช้งาน',
            'amount' => 'จำนวน',
            'status' => 'Status',
            'status_order' => 'Status Order',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
