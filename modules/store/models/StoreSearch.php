<?php

namespace app\modules\store\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\store\models\Product;

/**
 * ProductSearch represents the model behind the search form of `app\modules\store\models\Product`.
 */
class StoreSearch extends Model
{
    public $product_name;
    public $school_name;
    public function rules()
    {
        return [
            [['school_name', 'product_name'], 'safe'],
        ];
    }


    public function search($params)
    {
        $query = Yii::$app->db->createCommand("SELECT
	itd.ID,
	P.NAME AS product_name,
	itd.item_id,
	itd.amount,
	itd.status,
	itd.created_at,
	sh.NAME as school_name,
	itd.order_id,
	itd.card_use,
	u.username
FROM
	item_details itd
	LEFT JOIN product P ON itd.product_id = P.
	ID LEFT JOIN school sh ON itd.school_id = sh.ID 
	LEFT join \"user\" u on  u.id =itd.created_by 
ORDER BY
	itd.ID DESC")->queryAll();


        if ($this->load($params)) {
            $product_name = strtolower(trim($this->product_name));
            $query = array_filter($query, function ($role) use ($product_name) {
                return (empty($product_name) || strpos((strtolower(is_object($role) ? $role->product_name : $role['product_name'])), $product_name) !== false);
            });

            $school_name = strtolower(trim($this->school_name));
            $query = array_filter($query, function ($role) use ($school_name) {
                return (empty($school_name) || strpos((strtolower(is_object($role) ? $role->school_name : $role['school_name'])), $school_name) !== false);
            });
        }

        $dataProvider = new \yii\data\ArrayDataProvider([
            'key' => 'id',
            'allModels' => $query,
            'pagination' => [
                'pageSize' => 10
            ]
        ]);
        return $dataProvider;
    }
}
