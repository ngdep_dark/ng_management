<?php

namespace app\modules\store\controllers;

use app\models\User;
use app\modules\store\models\CardBad;
use app\modules\store\models\ItemDetails;
use app\modules\store\models\Items;
use app\modules\store\models\Product;
use app\modules\store\models\School;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\rbac\Item;
use yii\rest\Controller;
use yii\web\Response;
use Yii;

class  AppController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return ArrayHelper::merge([
            [
                'class' => Cors::className(),
                'cors' => [
                    'Access-Control-Request-Method' => ['GET', 'HEAD', 'OPTIONS'],
                ],
            ],
        ], $behaviors);
        // return $behaviors;
    }

    public function actions()
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: authorization, content-type');
        return parent::actions();
    }

    public function actionCancelTake($id)
    {
        $Items = Items::findOne($id);
        $Items->delete();
        return [
            'status' => 'ok',
        ];
    }

    public function actionSaveReplace()
    {
        $post = Yii::$app->request->rawBody;
        $dataPost = Json::decode($post, true);
        if (!empty($dataPost)) {
            $order_id = $dataPost['order_id'];
            $dataSelect = $dataPost['select'];
            $srt = str_replace("โรงเรียน", " ", $dataPost['SchoolName']);
            $school = trim($srt);
            $school_id = null;
            $Amount = !empty($dataPost['dataOrder']) ? count($dataPost['dataOrder']) : 0;
            if (!is_numeric($school)) {
                $check = School::findOne(['name' => $school]);
                if (empty($check)) {
                    $sc = new School();
                    $sc->name = $school;
                    $sc->save();
                    $school_id = $sc->id;
                } else {
                    $school_id = $check->id;
                }
            } else {
                $ScCheck = School::findOne(['id' => $school]);
                $school_id = $ScCheck->id;
            }
            foreach ($dataSelect as $k => $product_id) {
                $dataProduct = $this->selectProduct($product_id);
                $totalAmount = $Amount;
                foreach ($dataProduct as $model) {
                    $takeAmount = abs($model['amount']);
                    if ($takeAmount <= $totalAmount) {
//                        echo  'น้อย'.'<br />';
                        $totalAmount -= $takeAmount;
                        $it = Items::findOne(['id' => $model['id']]);
                        $it->use_check = 3;
                        if ($it->save()) {
                            $this->SaveItemReplace($it->id, $it->product_id, $takeAmount, $school_id, $order_id);
                        }
                        if ($totalAmount <= 0) break;
                    } else {
//                          echo  'มาก'.'<br />';
                        $this->SaveItemReplace($model['id'], $model['product_id'], intval($totalAmount), $school_id, $order_id);
                        $totalAmount -= $takeAmount;
                        if ($totalAmount <= 0) break;
//                        break;
                    }
                }
            }
            return [
                'status' => 'ok',
            ];
//
        } else {
            return [
                'status' => 'not',
            ];
        }
    }

    public function actionSaveOrder()
    {
        $post = Yii::$app->request->rawBody;
        $dataPost = Json::decode($post, true);
        if (!empty($dataPost)) {
            $order_id = $dataPost['order_id'];
            $dataSelect = $dataPost['select'];
            $srt = str_replace("โรงเรียน", " ", $dataPost['SchoolName']);
            $school = trim($srt);
            $school_id = null;
            $Amount = !empty($dataPost['dataOrder']) ? count($dataPost['dataOrder']) : 0;
            if (!is_numeric($school)) {
                $check = School::findOne(['name' => $school]);
                if (empty($check)) {
                    $sc = new School();
                    $sc->name = $school;
                    $sc->save();
                    $school_id = $sc->id;
                } else {
                    $school_id = $check->id;
                }
            } else {
                $ScCheck = School::findOne(['id' => $school]);
                $school_id = $ScCheck->id;
            }
            foreach ($dataSelect as $k => $product_id) {
                $dataProduct = $this->selectProduct($product_id);
                $totalAmount = $Amount;
                foreach ($dataProduct as $model) {
                    $takeAmount = abs($model['amount']);
                    if ($takeAmount <= $totalAmount) {
//                        echo  'น้อย'.'<br />';
                        $totalAmount -= $takeAmount;
                        $it = Items::findOne(['id' => $model['id']]);
                        $it->use_check = 3;
                        if ($it->save()) {
                            $this->SaveItemOrder($it->id, $it->product_id, $takeAmount, $school_id, $order_id);
                        }
                        if ($totalAmount <= 0) break;
                    } else {
//                          echo  'มาก'.'<br />';
                        $this->SaveItemOrder($model['id'], $model['product_id'], intval($totalAmount), $school_id, $order_id);
                        $totalAmount -= $takeAmount;
                        if ($totalAmount <= 0) break;
                        break;
                    }
                }
            }
            return [
                'status' => 'ok',
            ];

        } else {
            return [
                'status' => 'not',
            ];
        }
    }

    public function actionTake()
    {
        $data = [-100, -100, -100, -300];
        $dataSelect = [11, 4];
        $amount = 200;
        $totalReturn = $amount;
        // $totalBorrow = array_sum($data) * -1;
        foreach ($data as $k => $v) {
            $currentAmount = $v < 0 ? $v * -1 : $v;
            //echo abs($v);
//            $currentReturn = $currentAmount <= $totalReturn ? $currentAmount : $totalReturn;
//            $totalReturn -= $currentReturn;
//////            echo $totalReturn;
//////            exit();
//            echo ($k+1).' return '. $currentReturn.'/'.$currentAmount.'<br />';
//            echo "insert into log (id, return_value) values ({$k},{$currentAmount})<br />";
//            if ($totalReturn <= 0)   echo  $totalReturn; break;

            if ($currentAmount <= $totalReturn) {
                $totalReturn -= $currentAmount;
                echo ($k + 1) . ' return ' . $currentAmount . '/' . $currentAmount . '<br />';
                echo "insert into log (id, return_value) values ({$k},{$currentAmount})<br />";
            } else {
                echo ($k + 1) . ' return ' . $totalReturn . '/' . $currentAmount . '<br />';
                echo "insert into log (id, return_value) values ({$k},{$totalReturn})<br />";
                $totalReturn -= $currentAmount;
                break;
            }
        }

        echo "Remaining Todo: " . $totalReturn;

//        $sum =0;
//        $total_take =0 ;
////        foreach ($data as $value){
////            $total_take += $value *-1;
////        }
//        foreach ($data as$k=> $m){
//            $total_take =$m + $amount;
//            echo 'คนที่ ';
//            echo  $k+1;
//            echo '=  ';
//            echo $total_take;
////            $sum =$amount +$amount;
//
//        }
    }

    public function actionSaveGood()
    {
        $post = Yii::$app->request->rawBody;
        $dataPost = Json::decode($post, true);
        $school_id = $dataPost['data']['school']['id'];
        $dataSelect = $dataPost['data']['select'];
        $card_use = $dataPost['data']['checked_card'];
        $Amount = intval($dataPost['data']['amount']);
//        $school_id = 2;
//        $toTolSum = 0;
//        $Amount = 50;
//        $dataSelect = [11];
        if (!empty($dataPost)) {
            foreach ($dataSelect as $k => $product_id) {
                $dataProduct = $this->selectProduct($product_id);
                $totalAmount = $Amount;
                foreach ($dataProduct as $model) {
                    $takeAmount = abs($model['amount']);
                    if ($takeAmount <= $totalAmount) {
                        //echo  'น้อย'.'<br />';
                        $totalAmount -= $takeAmount;
                        $it = Items::findOne(['id' => $model['id']]);
                        $it->use_check = 3;
                        if ($it->save()) {
                            $this->SaveItemDetail($it->id, $it->product_id, $takeAmount, $school_id, $card_use);
                        }
                        if ($totalAmount <= 0) break;
                    } else {
                        //  echo  'มาก'.'<br />';
                        $this->SaveItemDetail($model['id'], $model['product_id'], $totalAmount, $school_id, $card_use);
                        $totalAmount -= $takeAmount;
                        if ($totalAmount <= 0) break;
                        break;
                    }
                }
            }
            return [
                'status' => 'ok',
            ];
        } else {
            return [
                'status' => 'not',
            ];
        }


    }

    public function actionCardBadOne()
    {
        $post = Yii::$app->request->rawBody;
        $dataPost = Json::decode($post, true);
        $school_id = $dataPost['data']['school']['id'];
        $Amount = $dataPost['data']['amount'];
        $causes = $dataPost['data']['causes'];
        var_dump($causes);exit();
    }

    public function actionSaveBad()
    {
        $post = Yii::$app->request->rawBody;
        $dataPost = Json::decode($post, true);
        $school_id = $dataPost['data']['school']['id'];
        $card_use = $dataPost['data']['checked_card'];
        $Amount = $dataPost['data']['amount'];
        $causes = $dataPost['data']['causes'];
        $dataSelect = $dataPost['data']['select'];
        $cardBadselect = $dataPost['data']['cardBadselect'];
        if (!empty($dataPost['data'])) {
            foreach ($dataSelect as $k => $product_id) {
                $dataProduct = $this->selectProduct($product_id);
                $totalAmount = $Amount;
                foreach ($dataProduct as $model) {
                    $takeAmount = abs($model['amount']);
                    if ($takeAmount <= $totalAmount) {
                        //echo  'น้อย'.'<br />';
                        $totalAmount -= $takeAmount;
                        $it = Items::findOne(['id' => $model['id']]);
                        $it->use_check = 3;
                        if ($it->save()) {
                            $this->actionSaveItemBad($it->id, $it->product_id, $takeAmount, $school_id, $cardBadselect, $card_use, $causes);
                        }
                        if ($totalAmount <= 0) break;
                    } else {
                        //  echo  'มาก'.'<br />';
                        $this->actionSaveItemBad($model['id'], $model['product_id'], $totalAmount, $school_id, $cardBadselect, $card_use, $causes);
                        $totalAmount -= $takeAmount;
                        if ($totalAmount <= 0) break;
                        break;
                    }
                }
            }
            return [
                'status' => 'ok',
            ];
        } else {
            return [
                'status' => 'not',
            ];
        }
    }

    protected function CheckItem($product_id)
    {
//    public function actionCheckItem($product_id){
        $data = Yii::$app->db->createCommand("select ite.id,ite.details,ite.product_id,sum(ite.amount * p.use) as amount ,sum(itd.amount) as amount_take ,ite.created_at  FROM items ite 
            left join product p on p.id = ite.product_id
            left join item_details itd  on ite.id = itd.item_id
            WHERE ite.use_check = 1  and ite.status = 0 and ite.product_id = :product_id 
            GROUP BY ite.id 
            ORDER BY ite.created_at ")->bindValues(['product_id' => $product_id])->queryOne();
        $data['amount'] = $data['amount'] + $data['amount_take'];
        return $data;
    }

    protected function selectProduct($product_id)
//    public function actionSelectProduct($product_id)
    {
        $data = Yii::$app->db->createCommand("select ite.id,ite.details,ite.product_id,sum(ite.amount * p.use) as amount  ,p.use,ite.created_at  ,p.lock_cal_products FROM items ite 
            left join product p on p.id = ite.product_id
            WHERE ite.use_check = 1  and ite.status = 0 and ite.product_id = :product_id
            GROUP BY ite.id ,p.lock_cal_products,p.use
            ORDER BY ite.created_at  ")->bindValues(['product_id' => $product_id])->queryAll();
        foreach ($data as &$m) {
            $dataItem = ItemDetails::find()->where(['item_id' => $m['id']])->sum('amount');
            if ($m['lock_cal_products'] === 1) {
                $m['amount'] = $m['amount'] / $m['use'] + $dataItem;
                $m['take_amount'] = $dataItem;
            } else {
                $m['amount'] = $m['amount'] + $dataItem;
                $m['take_amount'] = $dataItem;
            }
        }
        return $data;
    }

    public function actionRemoveTake($id)
    {
        $it = Items::findOne(['id' => $id]);
        $it->use_check = 2;
        $it->save();
        return [
            'status' => 'ok'
        ];
    }

    protected function SaveItemReplace($item, $product_id, $amount, $school, $order_id)
    {
        $itemDetails = new ItemDetails();
        $itemDetails->item_id = $item;
        $itemDetails->order_id = $order_id;
        $itemDetails->product_id = $product_id;
        $itemDetails->status = 4; //replace
        $itemDetails->amount = $amount;
        $itemDetails->school_id = $school;
        $itemDetails->save();

    }

    protected function SaveItemOrder($item, $product_id, $amount, $school, $order_id)
    {
        $itemDetails = new ItemDetails();
        $itemDetails->item_id = $item;
        $itemDetails->order_id = $order_id;
        $itemDetails->product_id = $product_id;
        $itemDetails->status = 3; //order
        $itemDetails->amount = $amount;
        $itemDetails->school_id = $school;
        $itemDetails->save();

    }

    protected function SaveItemDetail($item, $product_id, $amount, $school, $card_use)
    {
        $itemDetails = new ItemDetails();
        $itemDetails->item_id = $item;
        $itemDetails->product_id = $product_id;
        $itemDetails->status = 1;
        $itemDetails->card_use = $card_use;
        $itemDetails->amount = $amount;
        $itemDetails->school_id = $school;
        $itemDetails->save();

    }

    public function actionSaveItemBad($item, $product_id, $amount, $school, $selectBad, $card_use, $causes)
    {
        // status   1 =  ดี 2 = เสีย
        $selectBads = intval($selectBad);
        $amount = intval($amount);
        $itemDetails = new ItemDetails();
        $itemDetails->item_id = $item;
        $itemDetails->status = 2;
        $itemDetails->product_id = $product_id;
        $itemDetails->amount = $amount;
        $itemDetails->causes = $causes;
        $itemDetails->card_use = $card_use;
        $itemDetails->school_id = $school;
        if ($selectBads == 0) {
            //cardBad 1
            $itemDetails->card_lose = $selectBads;
        } else {
            $itemDetails->card_lose_2 = $selectBads;
        }
        $saveLff = $itemDetails->save();
        if ($saveLff && $selectBads == 0) {
           $product = Product::findOne(['id'=>$product_id,'lock_cal_products'=>1]);
            if(!empty($product)){
               $lock_card_bad= Product::findOne(['lock_card_bad'=>1]);
               if(!empty($lock_card_bad)){
                   $Items = new Items();
                   $Items->status = 1;
                   $Items->use_check = 4; //card_test
                   $Items->amount = $amount;
                   $Items->product_id = $lock_card_bad->id;
                   $Items->details = $causes.'#'.$item.'/'.$product_id;
                   $Items->status_order = $school;
                   $Items->save(false);
               }

            }

        }

    }

    public function actionSchool()
    {
        $data = School::find()->all();
        $array = [];
        foreach ($data as $k => $model) {
            $array[$k]['id'] = $model->id;
            $array[$k]['label'] = $model->name;
        }
        return $array;
    }

    public function actionCheck($item_id, $amount)
    {
        $item_id = intval($item_id);
        $amount = intval($amount);
        $item = Items::findOne(['id' => $item_id]);
        $product = Product::findOne(['id' => $item->product_id]);
        $total = $item->amount * -1 * $product->use;
        $item_details = ItemDetails::find()->where(['item_id' => $item_id])->sum('amount');
        if ($item_details + intval($amount) >= $total) {
            return true;
        } else {
            return false;
        }
    }

    public function actionItemUse($id)
    {
        $item = Items::findOne(['product_id' => $id, 'use_check' => 1]);
        if (!empty($item)) {
            return true;
        } else {
            return false;
        }
    }

    public function actionGetProduct()
    {
        $product = Product::find()->all();
        $data = [];
        foreach ($product as $k => &$model) {
            $data[$k]['id'] = $model->id;
            $data[$k]['label'] = $model->name;
            $data[$k]['status'] = true;
            if ($model['lock_cal_products'] == 1) {
                $data[$k]['use'] = 1;
            } else {
                $data[$k]['use'] = !empty($model->use) ? $model->use : 1;
            }
            $data[$k]['balance'] = $model->balance;
        }
        return $data;
    }
    public function  actionGenApp(){
        $users = new User();
        $users->id = 31;
        $users->username = 'panot';
        $users->full_name ='panot';
        $users->password = '123456789';
        $users->email =  'panot'. "@gmail.com";
        $users->status = 10;
        $users->role_shift = 1;
        $users->setPassword($users->password);
        $users->generateAuthKey();
        $users->save(false);
    }
}