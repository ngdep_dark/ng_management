<?php

namespace app\modules\store\controllers;

use app\modules\store\models\TakeModel;
use app\modules\store\models\CalculatorOrder;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\helpers\Url;
use Mpdf\Mpdf;
use yii\web\Controller;
use Yii;

class ReportController extends \yii\web\Controller
{

    public function behaviors()
    {
        return [
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'delete' => ['post'],
//                ],
//            ],
//            'access'=>[
//                'class'=>AccessControl::className(),
//                'rules'=>[
//                    [
//                        'allow'=>true,
//                        'actions'=>['index','calculator','delete-cal','report-take',
//                            'order-take','use-take','good-take','report-card-all','report-list-all','monitor'],
//                        'roles'=>['Admin','manage']
//                    ],
//
//                ]
//            ]
        ];
    }
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCalculator()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => CalculatorOrder::find()->where('cal_id IS NULL')
        ]);
        return $this->render('calculator', ['dataProvider' => $dataProvider]);
    }

    public function actionReport($id)
    {
        if (isset($id)) {
            $params = Yii::$app->params;
            $fontDirs = $params['defaultConfig']['fontDir'];
            $fontData = $params['defaultFontConfig']['fontdata'];
            $mpdf = new Mpdf([
                'fontDir' => array_merge($fontDirs, [
                    Url::base() . 'fonts/THSarabun',
                ]),
                'fontdata' => $fontData + ['thsarabun' => $params['SetTHSarabun']],
                'default_font_size' => 10,
                'default_font' => 'thsarabun',
                'mode' => 'utf-8',
                'format' => 'A4',
                'margin_top' => 10,
                'margin_left' => 15,
                'margin_right' => 10,
                'margin_bottom' => 50,
            ]);
            $stylesheet = file_get_contents(Url::base() . 'css/store.css');
            $mpdf->WriteHTML($stylesheet, 1);
            $mpdf->SetTitle('คำนวนการสั่งซื้อ');
            $mpdf->WriteHTML($this->renderPartial('report', ['data' => CalculatorOrder::findOne(['id' => $id])]), 2);
            $mpdf->Output();
        }
    }

    public function actionDeleteCal($id)
    {
        CalculatorOrder::findOne(['id' => $id])->delete();
        CalculatorOrder::deleteAll(['cal_id' => $id]);
        return $this->redirect(['report/calculator']);
    }

    public function actionReportTake()
    {
        $model = new TakeModel();
        $dataProvider = [];
        if ($model->load(Yii::$app->request->get())) {
            if ($model->validate()) {
                // all inputs are valid
                $dataStart = substr($model->date, 0, 10);
                $dataEnd = substr($model->date, 13, 10);

                $product_id = null;
                $delimiter = '';
                foreach ($model->product as $v) {
                    $product_id .= $delimiter . (int)($v);
                    $delimiter = ',';
                }
//                var_dump($product_id);exit();
                if (!empty($product_id) && !empty($model->date)) {
                    $dataProvider = new \yii\data\ArrayDataProvider([
                        'allModels' => Yii::$app->db->createCommand("SELECT it.*,p.name from items it 
                            LEFT join product p on it.product_id = p.id
                            WHERE DATE(it.created_at) BETWEEN :start and :end
                            and it.status = :status and product_id in( $product_id) ")->bindValues(['status' => $model->status, 'start' => date_format(date_create($dataStart), 'Y-m-d'), 'end' => date_format(date_create($dataEnd), 'Y-m-d')])->queryAll(),
                        'pagination' => [
                            'pageSize' => false
                        ]
                    ]);
                }
            } else {
                $errors = $model->errors;

                Yii::$app->session->setFlash('danger', 'กรุณาเลือกรายการ');
                // validation failed: $errors is an array containing error messages
            }
        }
        return $this->render('report-take', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionOrderTake()
    {
        $model = new TakeModel();
        $dataProvider = new ArrayDataProvider([
            'allModels' => Yii::$app->db->createCommand("SELECT it.*,p.name ,(p.use * it.amount) as use_amount ,(SELECT sum(amount) FROM item_details itd WHERE itd.item_id = it.id) as take_amount  from items it 
left join product p on it.product_id = p.id
WHERE it.use_check = 2 ORDER BY updated_at desc")->queryAll(),
            'sort' => ['attributes' => ['updated_at' => SORT_DESC]
            ]
        ]);
        return $this->render('order-take', [
                'dataProvider' => $dataProvider
            ]
        );
    }

    public function actionUseTake()
    {
        $model = new TakeModel();
        $dataProvider = [];
        if ($model->load(Yii::$app->request->get())) {
            if (!empty($model->product)) {
                if (!$model->validate()) {
                    // all inputs are valid
                    $dataStart = substr($model->date, 0, 10);
                    $dataEnd = substr($model->date, 13, 10);
                    $dataProvider = new ArrayDataProvider([
                        'allModels' => Yii::$app->db->createCommand("SELECT  sh.name,itm.causes,
                        case itm.card_lose WHEN '0' THEN sum(itm.amount)  END AS card_1,
                        case itm.card_lose_2 WHEN '1' THEN sum(itm.amount)  END AS card_2
                        FROM item_details itm 
                        LEFT join school sh on itm.school_id = sh.id
                        WHERE   DATE(itm.created_at) BETWEEN :start and :end and itm.product_id =:product  and itm.status = 2
                         GROUP BY sh.name ,itm.card_lose_2,itm.card_lose,itm.causes")->bindValues(['product' => $model->product, 'start' => date_format(date_create($dataStart), 'Y-m-d'), 'end' => date_format(date_create($dataEnd), 'Y-m-d')])->queryAll(),
                        'pagination' => [
                            'pageSize' => false
                        ]
                    ]);
                }
            }
        }

        return $this->render('use-take', [
            'model' => $model,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionGoodTake()
    {
        $model = new TakeModel();
        $dataProvider = [];
        if ($model->load(Yii::$app->request->post())) {
            if (!$model->validate()) {
                // all inputs are valid
                $dataStart = substr($model->date, 0, 10);
                $dataEnd = substr($model->date, 13, 10);
                if (!empty($model->product) && !empty($dataStart) && !empty($dataEnd)) {

                    $dataProvider = new ArrayDataProvider([
                        'allModels' => Yii::$app->db->createCommand("SELECT  
sh.name,
sum(itm.amount) as amount
FROM item_details itm 
LEFT join school sh on itm.school_id = sh.id
WHERE  DATE(itm.created_at) BETWEEN :start and :end and itm.product_id =:product  and itm.status in (1,3)
GROUP BY sh.name ")->bindValues(['product' => $model->product, 'start' => date_format(date_create($dataStart), 'Y-m-d'), 'end' => date_format(date_create($dataEnd), 'Y-m-d')])->queryAll(),
                        'pagination' => [
                            'pageSize' => false
                        ]
                    ]);
                }

            }
        }

        return $this->render('good-take', [
            'model' => $model,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionReportCardAll()
    {
        $post = Yii::$app->request->get();
        $dataProvider = [];
        if (!empty($post)) {
            $dataStart = substr($post['date'], 0, 10);
            $dataEnd = substr($post['date'], 13, 10);
            if (!empty($post['product_id']) && !empty($post['school_id']) && !empty($dataStart) && !empty($dataEnd)) {
                $sql = "SELECT case ite.status WHEN 1 THEN sum(ite.amount)  END AS cardGood,
CASE ite.status = 2 WHEN TRUE THEN sum(ite.amount) END AS cardBad,
CASE ite.status = 3 WHEN TRUE THEN sum(ite.amount) END AS cardOrder ,

ite.causes,DATE(ite.created_at),ite.product_id,sch.name,ite.status
FROM item_details ite 
left join school sch on ite.school_id = sch.id
        WHERE school_id = :school_id and ite.product_id=:product_id and DATE(ite.created_at) BETWEEN :start and :end
GROUP BY ite.product_id,ite.status,ite.causes,DATE(ite.created_at),sch.name,ite.status 
ORDER BY DATE(ite.created_at) asc";
                $rawData = Yii::$app->db->createCommand($sql)->bindValues(['product_id' => intval($post['product_id']), 'school_id' => $post['school_id'], 'start' => date_format(date_create($dataStart), 'Y-m-d'), 'end' => date_format(date_create($dataEnd), 'Y-m-d')])->queryAll();
                $dataProvider = new ArrayDataProvider([
                    'allModels' => $rawData,
                    'pagination' => [
                        'pageSize' => false
                    ]
                ]);
            }
        }

        return $this->render('report-card-all', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionReportListAll()
    {
        $rawData = Yii::$app->db->createCommand("SELECT p.name ,sum(it.amount) as amount from product p 
left join items it on p.id = it.product_id
GROUP BY p.id ORDER BY p.types_id")->queryAll();
        $dataProvider = new ArrayDataProvider([
            'allModels' => $rawData,
            'pagination' => [
                'pageSize' => false
            ]
        ]);
        return $this->render('report-list-all', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionMonitor()
    {
        $this->layout = 'main';
        return $this->render('monitor');
    }
}