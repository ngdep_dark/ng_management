<?php

namespace app\modules\store\controllers;

use app\modules\store\models\Items;
use app\modules\store\models\Types;
use Imagine\Image\Box;
use Yii;
use app\modules\store\models\Product;
use app\modules\store\models\ProductSearch;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\imagine\Image;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'delete' => ['post'],
//                ],
//            ],
            'access'=>[
                'class'=>AccessControl::className(),
                'rules'=>[
                    [
                        'allow'=>true,
                        'actions'=>['index','view','create','update','delete','delete-lock'],
                        'roles'=>['Admin','manage', 'store']
                    ],

                ]
            ]
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDeleteLock($id,$page)
    {
        $Items= Items::findOne(['id'=>$id])->delete();
        return $this->redirect(['product/view','id'=>$page]);
    }

    public function actionView($id)
    {
        $data = Yii::$app->db->createCommand("SELECT ite.product_id, ite.id as items_id, u.username, ite.created_at,ite.status,ite.details, case ite.status WHEN 1 THEN ite.amount  END AS addAmount,
CASE ite.status = 0 WHEN TRUE THEN ite.amount END AS takeAmount 
FROM items ite 
LEFT join \"user\" u on u.id = ite.created_by
WHERE ite.product_id = :id ORDER BY ite.created_at ASC ")->bindValues(['id' => $id])->queryAll();
        $sum = 0;
        foreach ($data as &$model) {
            $sum += $model['addamount'] + $model['takeamount'];
            $model['balance'] = $sum;

        }
        $dataProvider = new ArrayDataProvider([
            'allModels' => $data,
            'pagination' => FALSE,

            'sort' => [
                'attributes' => [
                    'created_at' => [
                        'desc' => ['created_at' => SORT_DESC],
                    ],
                ],
                'defaultOrder' => ['created_at' => SORT_DESC],
            ]
        ]);

        return $this->render('view', [
            'dataProvider' => $dataProvider,
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function upload($model,$attribute)
    {
        $photos  = UploadedFile::getInstance($model, $attribute);
        $path = Yii::getAlias('@webroot').'/'.'photos_products'.'/';
        if(!empty($photos)) {
            $fileName = time(). '.' . $photos->extension;
            if($photos->saveAs($path . $fileName)){
                return $fileName;
            };
            $imagine = Image::getImagine();
            //เรียกรูปที่ save มา resize
            $image = $imagine->open($path . $photos);
            $image->resize(new Box(360, 360))->save($path.$fileName, ['quality' => 60]);

        }
        return $model->isNewRecord ? false : $model->getOldAttribute($attribute);
    }

    public function actionCreate()
    {
        $model = new Product();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if (!is_numeric($model->types_id)) {
                $ty = new  Types();
                $ty->name = $model->types_id;
                $ty->save();
                $model->types_id = $ty->id;
            }
            $model->use = !empty($model->use) ?$model->use :1;
            $model->photos = $this->upload($model,'photos');
            $model->save();
            return $this->redirect(['index', 'id' => $model->id]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionTake($id)
    {
        $product= Product::findOne($id);
        $model = new  Items();
        if($model->load(Yii::$app->request->post())){
            $model->product_id = $id;
            $model->amount =  -$model->amount;
            $model->status=0;
            $model->save();

            return $this->redirect(['product/index']);
        }
        return $this->render('take', [
            'model' => $model,
            'product'=>$product
        ]);
    }
    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDeleteItem($id,$page)
    {
       Items::findOne($id)->delete();
        return $this->redirect(['product/take','id'=>$page]);
    }
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (!is_numeric($model->types_id)) {
                $ty = new  Types();
                $ty->name = $model->types_id;
                $ty->save();
                $model->types_id = $ty->id;
            }
            $model->use = !empty($model->use) ?$model->use :1;
            $model->photos = $this->upload($model,'photos');
            $model->save();
            return $this->redirect(['index', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        $photos=Product::findOne($id)->photos;
        $path = Yii::getAlias('@webroot').'/'.'photos_products'.'/'.$photos;
        if(!empty($photos))echo 'ok'.$path; unlink($path);
        $check = Items::findOne(['product_id' => $id]);
        if (!empty($check)) {
            Items::findOne(['product_id' => $id])->delete();
        }
       // return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */


    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}