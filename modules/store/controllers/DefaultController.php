<?php

namespace app\modules\store\controllers;

use app\modules\store\models\CalculatorOrder;
use app\modules\store\models\ItemDetails;
use app\modules\store\models\Items;
use yii\helpers\Url;
use Mpdf\Mpdf;
use yii\web\Controller;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;


/**
 * Default controller for the `store` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'get-take', 'delete-details', 'calculator', 'export', 'report', 'update-details', 'delete-all-default'],
                        'roles' => ['Admin', 'manage', 'store']
                    ],

                ]
            ],
        ];
    }

    public function beforeAction($action)
    {
        if (in_array($action->id, ['export'])) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionGetTake()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = \Yii::$app->db->createCommand("
        SELECT P
            .use,
            ite.ID,
            P.NAME,
            p.id as product_id,
            p.lock_cal_products,
            ite.amount,
            ite.status,
            ite.use_check,
            SUM ( itd.amount )  as take_amount
        FROM
            items ite
            LEFT JOIN product P ON ite.product_id = P.
            ID LEFT JOIN item_details itd ON ite.ID = itd.item_id 
        WHERE
            use_check = 1 
        GROUP BY
            P.use,
             p.id,
            ite.ID,
            P.NAME,
            ite.amount,
            ite.status,
            ite.use_check ")->queryAll();
        $dataArray = [];
        foreach ($data as $k => &$m) {
            if ($m['product_id'] === $m['product_id']) {
                //  $amount =intval($m['amount']) *-1;
                error_reporting(~E_NOTICE);
                $take_amount = !empty($m['take_amount']) ? intval($m['take_amount']) : 0;
                $dataArray[$m['product_id']]['id'] = !empty($m['product_id']) ? $m['product_id'] : 0;
                $dataArray[$m['product_id']]['item_id'] = !empty($m['id']) ? $m['id'] : 0;
                $dataArray[$m['product_id']]['use'] = !empty($m['use']) ? $m['use'] : 1;
                $dataArray[$m['product_id']]['name'] = !empty($m['name']) ? $m['name'] : 0;
                $dataArray[$m['product_id']]['lock_cal_products'] = !empty($m['lock_cal_products']) ? $m['lock_cal_products'] : '';
                $dataArray[$m['product_id']]['amount'] += isset($m['amount']) ? intval($m['amount']) * -1 : 0;
                $dataArray[$m['product_id']]['take_amount'] += isset($m['take_amount']) ? intval($m['take_amount']) : 0;

            }

//            $data['id']= !empty($m['product_id'])?$m['product_id']:0;
//            $data['item_id']= !empty($m['id'])?$m['id']:0;
//            $data['use']= !empty($m['use'])?$m['use'] *-1:1;
//            $data['name']= !empty($m['name'])?$m['name']:0;
//            $data['amount'] = !empty($m['amount'])?$m['amount'] :0;
//            $data['take_amount']= !empty($m['take_amount'])?$m['take_amount']:0;
            // $dataArray[$k]['product_id'] =$m['product_id'];
        }

        return $dataArray;
    }

    public function actionDeleteDetails($id)
    {
        $check = ItemDetails::findOne(['id' => $id]);
        $Items = Items::findOne(['id' => $check->item_id]);
        $Items->use_check = 1;
        $Items->save();
        ItemDetails::findOne(['id' => $id])->delete();
        return $this->redirect(['default/index']);
    }

    public function actionCalculator()
    {
        return $this->render('calculator');
    }

    public function actionExport()
    {
        $post = \Yii::$app->request->rawBody;
        $dataPost = Json::decode($post, true);
        $cal = new  CalculatorOrder();
        $cal->amount = intval($dataPost['data']['amount']);
        $cal->percent = intval($dataPost['data']['percent']);
        $cal->balance = intval($dataPost['data']['sumALl']);
        if ($cal->save()) {
            foreach ($dataPost['data']['dataCal'] as $model) {
                $cal_details = new  CalculatorOrder();
                $cal_details->cal_id = $cal->id;
                $cal_details->product_id = $model['product']['id'];
                $cal_details->balance = intval($model['product']['balance']);
                $cal_details->amount_use = intval($model['product']['use']);
                $cal_details->amount_take = intval($model['takeAmount']);
                $cal_details->amount_order = intval(!empty($model['takeOrder']) ? $model['takeOrder'] : 0);
                $cal_details->save();
            }
            return $cal->id;
        }
        // return $this->redirect(['default/export',['dataPost'=>$dataPost]]);
    }

    public function actionUpdateDetails($id)
    {
        $model = ItemDetails::findOne(['id' => $id]);
        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            return $this->redirect(['default/index']);
        }
        return $this->render('update-details', ['model' => $model]);
    }

    public function actionDeleteAllDefault()
    {
        $delete_ids = explode(',', Yii::$app->request->post('ids'));
        foreach ($delete_ids as $model) {
            $check = ItemDetails::findOne(['id' => $model]);
            $Items = Items::findOne(['id' => $check->item_id]);
            $Items->use_check = 1;
            $Items->save();
        }
        ItemDetails::deleteAll(['in','id',$delete_ids]);
        return  $this->redirect(['default/index']);
    }
}