<?php

namespace app\modules\attd\models;

use Yii;
Use app\models\User;

/**
 * This is the model class for table "attendance".
 *
 * @property int $id
 * @property int $user_id
 * @property string $att_date
 * @property string $att_check
 * @property string $att_checkout
 * @property string $overtime
 * @property string $att_status
 * @property string $updated_at
 * @property int $updated_by
 * @property string $created_at
 * @property int $created_by
 * @property int $leave_id
 * @property int $overtime_id
 * @property int $outside_id
 */
class Attendance extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $date_start;
    public $date_end;
    public static function tableName()
    {
        return 'attendance';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'updated_by', 'created_by', 'leave_id', 'overtime_id', 'outside_id'], 'default', 'value' => null],
            [['user_id', 'updated_by', 'created_by', 'leave_id', 'overtime_id', 'outside_id'], 'integer'],
            [['att_date', 'att_check', 'att_checkout', 'updated_at', 'created_at', 'log_id'], 'safe'],
            [['overtime', 'att_status'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'att_date' => 'Att Date',
            'att_check' => 'Att Check',
            'att_checkout' => 'Att Checkout',
            'overtime' => 'Overtime',
            'att_status' => 'Att Status',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'date_start' => 'วันที่เริ่มค้นหา',
            'date_end' => 'วันที่สิ้นสุดการค้นหา',
            'leave_id' => 'เลขที่การลา',
            'overtime_id' => 'เลขที่ OT',
            'outside_id' => 'เลขที่ทำงานนอกสถานที่',
            'log_id' => 'log_attendance_id'
        ];
    }

    public function getLeave(){
        return $this->hasOne(Leave::className(), ['id' => 'leave_id']);
    }
    public function getOutside(){
        return $this->hasOne(Leave::className(), ['id' => 'outside_id']);
    }

    public function getName(){
        return $this->hasOne(User::className(),['id'=>'user_id']);
    }

    public function getLog(){
        return $this->hasOne(AttendanceCronLog::className(),['id'=>'log_id']);
    }

    public function getUserName(){
        return $this->name['full_name'];
    }
}