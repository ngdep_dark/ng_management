<?php

namespace app\modules\attd\models;

use Yii;

/**
 * This is the model class for table "attendance_group".
 *
 * @property int $id
 * @property string $name
 *
 * @property AttendanceGroupDescription[] $attendanceGroupDescriptions
 */
class AttendanceGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attendance_group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttendanceGroupDescriptions()
    {
        return $this->hasMany(AttendanceGroupDescription::className(), ['group_id' => 'id']);
    }
}
