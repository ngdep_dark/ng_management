<?php

namespace app\modules\attd\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\User;

/**
 * This is the model class for table "leave".
 *
 * @property int $id
 * @property int $user_id
 * @property string $username
 * @property int $leave_type
 * @property string $leave_start
 * @property string $description
 * @property int $approve_status
 * @property string $created_at
 * @property string $create_by
 * @property string $approve_by
 * @property int $start_time
 * @property int $end_time
 * @property int $half_time
 * @property int $leave_hours
 */
class Leave extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public static function tableName()
    {
        return 'leave';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

            [['user_id', 'leave_type', 'approve_status'], 'default', 'value' => null],
            [['user_id', 'leave_type', 'approve_status', 'half_time', 'leave_hours'], 'integer'],
            [['leave_start', 'leave_end', 'created_at', 'create_by', 'start_time', 'end_time'], 'safe'],
            [['description'], 'required'],
            [['username', 'description', 'approve_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'username' => 'Username',
            'leave_type' => Yii::$app->messageManage->leave_type,
            'leave_start' => Yii::$app->messageManage->leave_start,
            'leave_end' => Yii::$app->messageManage->leave_end,
            'description' => Yii::$app->messageManage->leave_description,
            'approve_status' => Yii::$app->messageManage->leave_approve_status,
            'created_at' => Yii::$app->messageManage->create_at,
            'create_by' => Yii::$app->messageManage->create_by,
            'approve_by' => Yii::$app->messageManage->leave_approve_by,
            'start_time' => 'เวลาเริ่มต้น',
            'end_time' => 'เวลาสิ้นสุด',
            'half_time' => 'ลาครึ่งวัน?',
            'leave_hours' => 'ชั่วโมงที่ลา'
        ];
    }

//    public function validateLeaveCount($user, $type){
//        $leave = Leave::find()->where(['user_id' => $user])->andWhere(['leave_type'=> $type])->andWhere(['approve_status' => 2])->all();
//        $count = count($leave);
////        var_dump($count);
////        exit();
//        if ($type == 1 && $count == 7){
//            return $this->message = 'จำนวนวันลาของคุณครบการใช้งานแล้ว.';
//        }
//        elseif ($type == 2 && $count == 1){
//            return $this->message = 'จำนวนวันลาของคุณครบการใช้งานแล้ว.';
//        }
//        elseif ($type == 3 && $count == 6){
//            return $this->message = 'จำนวนวันลาของคุณครบการใช้งานแล้ว.';
//        }
//    }

    public function getStatusName()
    {
        return ArrayHelper::getValue($this->getStatusData(), $this->status);
    }

    public function getLeaveType()
    {
        return $this->hasOne(LeaveType::className(), ['id' => 'leave_type']);
    }

    public function getTypeName(){
        return $this->leaveType->leave_type_name;
    }

    public function getName(){
        return $this->hasOne(User::className(),['id'=>'user_id']);
    }

    public function getUserName(){
        return $this->name->full_name;
    }

    public function HalftimeStatus($data){
        $message = null;
        if ($data == 1 ){
            return $message = "ลาแบบครึ่งวัน";
        }
        elseif ($data == 0){
            return $message = "ลาเต็มวัน";
        }
        return $message;
    }


}
