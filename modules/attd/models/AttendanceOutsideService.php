<?php

namespace app\modules\attd\models;

use Yii;
use app\models\User;

/**
 * This is the model class for table "attendance_outside_service".
 *
 * @property int $id
 * @property int $user_id
 * @property string $outside_date_start
 * @property string $outside_date_end
 * @property int $outside_type
 * @property string $outside_province
 * @property string $detail
 * @property int $status
 */
class AttendanceOutsideService extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attendance_outside_service';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'outside_type', 'status'], 'default', 'value' => null],
            [['user_id', 'outside_type', 'status'], 'integer'],
            [['outside_date_start', 'outside_date_end'], 'safe'],
            [['outside_province', 'detail'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'outside_date_start' => 'วันที่ไป',
            'outside_date_end' => 'วันที่กลับ',
            'outside_type' => 'ประเภทงาน',
            'outside_province' => 'สถานที่',
            'detail' => 'รายละเอียด',
            'status' => 'Status',
        ];
    }

    public function getServiceType()
    {
        return $this->hasOne(AttendanceServiceType::className(), ['id' => 'outside_type']);
    }

    public function getTypeName(){
        return $this->serviceType->type_name;
    }

    public function getName(){
        return $this->hasOne(User::className(),['id'=>'user_id']);
    }

    public function getUserName(){
        return $this->name->full_name;
    }

    public function getApproveName(){
        return $this->hasOne(User::className(),['id' =>'status']);
    }

    public function getApprove(){
        return $this->approveName->full_name;
    }


}
