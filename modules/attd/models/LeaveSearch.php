<?php

namespace app\modules\attd\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\attd\models\Leave;

/**
 * LeaveSearch represents the model behind the search form of `app\modules\attd\models\Leave`.
 */
class LeaveSearch extends Leave
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'leave_type', 'approve_status'], 'integer'],
            [['username', 'leave_start', 'description', 'created_at', 'create_by', 'approve_by', 'start_time', 'end_time'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Leave::find()
            ->where(['approve_status' => 2])
            ->andWhere(['between', 'leave_start', date('Y').'-01-01', date('Y').'-12-31'])
            ->orderBy(['leave_start' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'leave_type' => $this->leave_type,
            'leave_start' => $this->leave_start,
            'approve_status' => $this->approve_status,
            'created_at' => $this->created_at,
            'create_by' => $this->create_by,
            'start_time' => $this->start_time,
            'end_time' => $this->end_time,
        ]);

        $query->andFilterWhere(['ilike', 'username', $this->username])
            ->andFilterWhere(['ilike', 'description', $this->description])
            ->andFilterWhere(['ilike', 'approve_by', $this->approve_by]);

        return $dataProvider;
    }
}
