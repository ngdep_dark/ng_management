<?php

namespace app\modules\attd\models;

use Yii;

/**
 * This is the model class for table "holiday".
 *
 * @property int $id
 * @property string $holiday_name
 * @property string $description
 * @property int $holiday_date
 * @property string $username
 */
class Holiday extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $username;
    public $att_date;
    public $att_check;
    public $att_checkout;
    public $status_check;
    public static function tableName()
    {
        return 'holiday';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['holiday_date'], 'safe'],
            [['holiday_name', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'holiday_name' => 'Holiday Name',
            'description' => 'Description',
            'holiday_date' => 'Holiday Date',
        ];
    }
}
