<?php

namespace app\modules\attd\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\attd\models\Workoutattendance;

/**
 * WorkoutattendanceSearch represents the model behind the search form of `app\modules\attd\models\Workoutattendance`.
 */
class WorkoutattendanceSearch extends Workoutattendance
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['clockin_Date', 'clockin', 'clockout', 'description', 'note', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Workoutattendance::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'clockin_Date' => $this->clockin_Date,
            'clockin' => $this->clockin,
            'clockout' => $this->clockout,
        ]);

        $query->andFilterWhere(['ilike', 'description', $this->description])
            ->andFilterWhere(['ilike', 'note', $this->note])
            ->andFilterWhere(['ilike', 'status', $this->status]);

        return $dataProvider;
    }
}
