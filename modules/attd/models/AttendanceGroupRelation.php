<?php

namespace app\modules\attd\models;

use app\models\User;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "attendance_group_relation".
 *
 * @property int $id
 * @property int $parent
 * @property int $child
 *
 * @property AttendanceGroupDescription $parent0
 * @property AttendanceGroupDescription $child0
 */
class AttendanceGroupRelation extends \yii\db\ActiveRecord
{
    public $groupChild;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attendance_group_relation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent', 'child'], 'required'],
            [['parent', 'child'], 'default', 'value' => null],
            [['parent', 'child'], 'integer'],
            [['groupChild'], 'safe'],
//            [['parent'], 'exist', 'skipOnError' => true, 'targetClass' => AttendanceGroupDescription::className(), 'targetAttribute' => ['user_id' => 'parent']],
//            [['child'], 'exist', 'skipOnError' => true, 'targetClass' => AttendanceGroupDescriptionSearch::className(), 'targetAttribute' => ['user_id' => 'child']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent' => 'กลุ่มหลัก',
            'child' => 'กลุ่มรอง',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(AttendanceGroupDescription::className(), ['id' => 'parent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChild()
    {
        return $this->hasOne(AttendanceGroupDescription::className(), ['id' => 'child']);
    }

    public function getGroup()
    {
        return $this->hasOne(AttendanceGroup::className(), ['id' => 'parent']);
    }

    public function  getGroupNames(){
        return $this->group->name;
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'parent']);
    }

    public function getUserName(){
        return $this->user->username;
    }

    public function getUserData()
    {
        return $query = ArrayHelper::map(User::find()->orderBy(['id' => SORT_ASC])->all(), 'id', 'username');
    }

    public function getGroupName()
    {
        return $query = ArrayHelper::map(AttendanceGroup::find()->orderBy(['id' => SORT_ASC])->all(), 'id', 'name');
    }
}
