<?php

namespace app\modules\attd\models;

use app\models\User;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "attendance_group_description".
 *
 * @property int $id
 * @property int $user_id
 * @property int $group_id
 * @property bool $group_leader
 *
 * @property AttendanceGroup $group
 * @property User $user
 * @property AttendanceGroupRelation[] $attendanceGroupRelations
 * @property AttendanceGroupRelation[] $attendanceGroupRelations0
 */
class AttendanceGroupDescription extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attendance_group_description';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'group_id'], 'default', 'value' => null],
            [['user_id', 'group_id'], 'integer'],
            [['group_leader'], 'boolean'],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => AttendanceGroup::className(), 'targetAttribute' => ['group_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'group_id' => 'Group ID',
            'group_leader' => 'Group Leader',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(AttendanceGroup::className(), ['id' => 'group_id']);
    }

    public function  getGroupNames(){
        return $this->group->name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUserName(){
        return $this->user->full_name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttendanceGroupRelations()
    {
        return $this->hasMany(AttendanceGroupRelation::className(), ['parent' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttendanceGroupRelations0()
    {
        return $this->hasMany(AttendanceGroupRelation::className(), ['child' => 'id']);
    }

    public function getUserData()
    {
        return $query = ArrayHelper::map(User::find()->orderBy(['id' => SORT_ASC])->all(), 'id', 'full_name');
    }

    public function getGroupName()
    {
        return $query = ArrayHelper::map(AttendanceGroup::find()->orderBy(['id' => SORT_ASC])->all(), 'id', 'name');
    }



}
