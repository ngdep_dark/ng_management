<?php

namespace app\modules\attd\models;

use Yii;

/**
 * This is the model class for table "attendance_service_type".
 *
 * @property int $id
 * @property string $type_name
 * @property string $created_at
 * @property string $created_by
 * @property string $updated_at
 * @property int $updated_by
 */
class AttendanceServiceType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attendance_service_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['updated_by'], 'default', 'value' => null],
            [['created_by', 'updated_by'], 'integer'],
            [['type_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_name' => 'Type Name',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
}
