<?php

namespace app\modules\attd\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\attd\models\attendanceCronLog;

/**
 * AttendanceCronLogSearch represents the model behind the search form of `app\modules\attd\models\attendanceCronLog`.
 */
class AttendanceCronLogSearch extends attendanceCronLog
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'record'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = attendanceCronLog::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'record' => $this->record,
        ]);

        $query->andFilterWhere(['ilike', 'created_at', $this->created_at]);

        return $dataProvider;
    }
}