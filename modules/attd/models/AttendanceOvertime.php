<?php

namespace app\modules\attd\models;

use Yii;
use app\models\User;

/**
 * This is the model class for table "attendance_overtime".
 *
 * @property int $id
 * @property int $user_id
 * @property int $ot_duration
 * @property int $status
 * @property string $approve_by
 * @property string $created_time
 * @property string $description
 * @property string $start_time
 * @property string $end_time
 * @property string $overtime_date
 */
class AttendanceOvertime extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attendance_overtime';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ot_duration', 'status'], 'default', 'value' => null],
            [['ot_duration', 'status', 'user_id'], 'integer'],
            [['overtime_date', 'start_time', 'end_time', 'created_time'], 'safe'],
            [['approve_by', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'ผู้ใช้งาน',
            'ot_duration' => 'ระยะเวลาทำงาน',
            'status' => 'Status',
            'approve_by' => 'Approve By',
            'created_time' => 'Created At',
            'description' => 'หมายเหตุ',
            'overtime_date' => 'OT_date',
            'start_time' => 'เวลาเริ่มต้น',
            'end_time' => 'เวลาที่สิ้นสุด',
        ];
    }

    public function validatePermission($permit){
        if ($permit == true){
            return $this->status = 2;
        }
        else{
            return $this->status = 1;
        }
    }

    public function getName()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUserName()
    {
        return $this->name->username;
    }
}
