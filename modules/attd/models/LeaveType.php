<?php

namespace app\modules\attd\models;

use Yii;

/**
 * This is the model class for table "leave_type".
 *
 * @property int $id
 * @property string $leave_type_name
 * @property string $created_at
 * @property string $created_by
 */
class LeaveType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'leave_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'created_by'], 'safe'],
            [['leave_type_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leave_type_name' => 'Leave Type Name',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
        ];
    }
}
