<?php

namespace app\modules\attd\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\attd\models\Holiday;

/**
 * HolidaySearch represents the model behind the search form of `app\modules\attd\models\Holiday`.
 */
class HolidaySearch extends Holiday
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'holiday_date'], 'integer'],
            [['holiday_name', 'description'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Holiday::find()->orderBy(['holiday_date' => SORT_ASC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'holiday_date' => $this->holiday_date,
        ]);

        $query->andFilterWhere(['ilike', 'holiday_name', $this->holiday_name])
            ->andFilterWhere(['ilike', 'description', $this->description]);

        return $dataProvider;
    }
}
