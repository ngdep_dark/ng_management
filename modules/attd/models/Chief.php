<?php

namespace app\modules\attd\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\User;

/**
 * This is the model class for table "chief".
 *
 * @property int $id
 * @property int $user_id
 * @property int $group_id
 * @property int $status
 */
class Chief extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'chief';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'group_id', 'status'], 'default', 'value' => null],
            [['user_id', 'group_id', 'status'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'group_id' => 'Group ID',
            'status' => 'Status',
        ];
    }

    public function getGroupData()
    {
        return $this->hasOne(AttdGroup::className(), ['group_id_' => 'id']);
    }

    public function getGroupName()
    {
        return $query = ArrayHelper::map(AttdGroup::find()->orderBy(['group_name' => SORT_ASC])->all(), 'id', 'group_name');
    }

    public function getUserName()
    {
        return $this->name->username;
    }

    public function getUserData()
    {
        return $query = ArrayHelper::map(User::find()->orderBy(['id' => SORT_ASC])->all(), 'id', 'username');
    }

    public function getStatusName()
    {
        return ArrayHelper::getValue($this->getStatusData(),$this->status);
    }

    public function getStatusData()
    {
        return [
            1 => 'หัวหน้า',
            2 => 'ลูกน้อง'
        ];
    }


}
