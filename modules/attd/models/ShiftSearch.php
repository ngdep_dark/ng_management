<?php

namespace app\modules\attd\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\attd\models\Shift;

/**
 * ShiftSearch represents the model behind the search form of `app\modules\attd\models\Shift`.
 */
class ShiftSearch extends Shift
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name', 'come_late_time', 'back_early_time', 'no_attend_time', 'day_time_details', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Shift::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'come_late_time' => $this->come_late_time,
            'back_early_time' => $this->back_early_time,
            'no_attend_time' => $this->no_attend_time,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'day_time_details', $this->day_time_details]);

        return $dataProvider;
    }
}
