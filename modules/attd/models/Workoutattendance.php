<?php

namespace app\modules\attd\models;

use Yii;

/**
 * This is the model class for table "workout_attendance".
 *
 * @property int $id
 * @property string $user_id
 * @property string $clock_date
 * @property string $clock_in
 * @property string $clock_out
 * @property string $description
 * @property string $note
 * @property string $status
 */
class Workoutattendance extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'workout_attendance';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['clock_Date', 'clock_in', 'clock_out'], 'safe'],
            [['description'], 'string', 'max' => 500],
            [['note', 'status'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id => User ID',
            'clock_date' => 'วันที่',
            'clock_in' => 'เวลาเข้างาน',
            'clock_out' => 'เวลาออกงาน',
            'description' => 'รายละเอียด',
            'note' => 'หมายเหตุ',
            'status' => 'Status',
        ];
    }
}
