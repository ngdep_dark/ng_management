<?php

namespace app\modules\attd\models;

use app\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "shift".
 *
 * @property int $id
 * @property string $name
 * @property string $come_late_time
 * @property string $back_early_time
 * @property string $no_attend_time
 * @property string $day_time_details
 * @property string $created_at
 * @property string $updated_at
 */
class Shift extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shift';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['come_late_time', 'back_early_time', 'no_attend_time', 'created_at', 'updated_at'], 'safe'],
            [['day_time_details'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ชื่อโปรไฟล์',
            'come_late_time' => 'เวลาเข้างาน',
            'back_early_time' => 'เวลาเลิกงาน',
            'no_attend_time' => 'เวลาขาดงาน',
            'day_time_details' => 'Day Time Details',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getUserRole(){
        return $this->hasOne(User::className(),['id'=>'role_user']);
    }

}
