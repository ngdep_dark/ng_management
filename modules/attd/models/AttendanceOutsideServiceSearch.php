<?php

namespace app\modules\attd\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\attd\models\AttendanceOutsideService;

/**
 * AttendanceOutsideServiceSearch represents the model behind the search form of `app\modules\attd\models\AttendanceOutsideService`.
 */
class AttendanceOutsideServiceSearch extends AttendanceOutsideService
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'outside_type', 'user_id', 'status'], 'integer'],
            [['outside_date_start', 'outside_date_end','passengers', 'outside_province', 'detail'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AttendanceOutsideService::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'outside_date_start' => $this->outside_date_start,
            'outside_date_end' => $this->outside_date_end,
            'outside_type' => $this->outside_type,
            'status' => $this->status
        ]);

        $query->andFilterWhere(['ilike', 'outside_province', $this->outside_province])
            ->andFilterWhere(['ilike', 'detail', $this->detail]);

        return $dataProvider;
    }
}
