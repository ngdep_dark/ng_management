<?php

namespace app\modules\attd\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "attendance_cron_log".
 *
 * @property int $id
 * @property int $type
 * @property string $created_at
 */
class AttendanceCronLog extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'attendance_cron_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['record'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'record' => 'Record',
            'created_at' => 'Created At',
        ];
    }

    public function getAttendances(){
        return $this->hasMany(Attendance::className(),['log_id'=>'id']);
    }
}