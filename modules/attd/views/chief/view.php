<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\attd\models\Chief */

$this->title = $model->chief_name;
$this->params['breadcrumbs'][] = ['label' => 'Chiefs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$data = \app\modules\attd\models\Chief::find()->where(['chief_id' => $model->id])->all();
//var_dump($data)
$i = 1;
?>


<div class="chief-view">
    <div class="col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">ข้อมูลแผนก</h3>
            </div>
            <div class="panel-body">
                <table class="row">
                    <table class="col-md-6">
                        <thead>
                        <tr>
                            <th style="font-size: 18px; line-height: 1.5cm;">ชื่อแผนก</th>
                        </tr>
                        <tr>
                            <th style="font-size: 18px; line-height: 1.5cm;">หัวหน้าแผนก</th>
                        </tr>
                        <tr>
                            <th style="font-size: 18px; line-height: 1.5cm;">ลูกน้องในแผนก</th>
                        </tr>
                        </thead>
                    </table>
                </table>
                <table class="row">
                    <table class="col-lg-6 col-md-6">
                        <tbody>
                        <tr>
                            <td style="font-size: 16px; line-height: 1.5cm;"><?= $model->chief_name ?></td>
                        </tr>
                        <?php $query = \app\models\User::find()->select('username')->where(['id' => $model->user_id])->scalar(); ?>
                        <tr>
                            <td style="font-size: 16px; line-height: 1.5cm;"><?= $query ?></td>
                        </tr>

                        <?php foreach ($data as $value) : ?>
                            <?php $query = \app\models\User::find()->where(['id' => $value['user_id']])->all(); ?>
                            <tr>
                            <?php foreach ($query as $values) : ?>
                                <td style="font-size: 16px; line-height: 1cm;"><?= $i++ . "." . $values['username'] ?></td>
                                </tr>
                            <?php endforeach;
                            ?>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </table>
            </div>
        </div>
    </div>

</div>
