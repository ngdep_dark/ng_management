<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\DataColumn;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\attd\models\ChiefSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Chiefs';
$this->params['breadcrumbs'][] = $this->title;

//var_dump($dataProvider);
//exit();
?>
<div class="chief-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('สร้างกลุ่ม', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'Group_name',
                'label' => 'ชื่อกลุ่ม'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'accept' => function ($url, $model, $key) {
                        return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', ['schedule/accept', 'id' => $model->id]);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
