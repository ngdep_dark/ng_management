<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\ActiveField;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\modules\attd\models\Chief */
/* @var $form yii\widgets\ActiveForm */

$admin = Yii::$app->user->can('Admin');

?>

<div class="chief-form">


    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-6">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
            'data' => $model->userData,
            'options' => ['placeholder' => 'กรุณาระบุพนักงาน', 'multiple' => true],
        ]);
        ?>

        <?= $form->field($model, 'group_id')->widget(Select2::className(), [
            'data' => $model->groupName,
            'options' => ['placeholder' => 'กรุณาระบุหัวหน้างาน'],
            'pluginOptions' => [
                'allowClear' => true,
//                'disabled' => true,
            ],
        ]) ?>


        <?= $form->field($model, 'status')->radioList($model->statusData); ?>

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
