<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use app\modules\attd\models\LeaveType;


/* @var $this yii\web\View */
/* @var $model app\modules\attd\models\Leave */

$leave_type = LeaveType::find()->where(['id' => $model->leave_type])->one();


$this->title = "ข้อมูลการ" . $leave_type->leave_type_name . "ของ" . "   " . $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Leaves', 'url' => ['index']];

?>
<div class="leave-view">

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'user_id',
            'username',
            [
                  'attribute' => 'leave_type',
                'label' => 'ประเภทการลา',
                'value' => function($model){
                    return $model->typeName;
                }
            ],
            'leave_start',
            'description',
            'approve_status',
            'created_at',
            'create_by',
            'approve_by',
            'start_time',
            'end_time',
        ],
    ]) ?>

</div>
