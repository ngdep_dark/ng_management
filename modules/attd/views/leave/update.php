<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\attd\models\Leave */

$this->title = 'แก้ไขการลา';
$this->params['breadcrumbs'][] = ['label' => 'Leaves', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="leave-update">

    <?=
    $this->render('_form', [
        'model' => $model,
        'user_id' => $user_id,
        'leave_type' => $leave_type,
        'leave_date' => $leave_date,
        'time' => $time,
        'check' => $check,
    ]) ?>

</div>
