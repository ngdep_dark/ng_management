<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\modules\attd\models\LeaveType;
use app\models\User;
//use kartik\checkbox\CheckboxX;
use kartik\widgets\DateTimePicker;
use kartik\widgets\TimePicker;


//var_dump($leave_date);
//exit();
/* @var $this yii\web\View */
/* @var $model app\modules\attd\models\Leave */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="col-md-12 col-lg-12">
        <div class="leave-form">
            <?php $form = ActiveForm::begin(); ?>
            <div class="form-group">
                <?php echo '<label class="control-label">ผู้ลา</label>';
                echo Select2::widget([
                    'name' => 'user_id',
                    'data' => ArrayHelper::map(User::find()->orderBy(['id' => SORT_ASC])->all(), 'id', 'full_name'),
                    'value' => $user_id,
                    'options' => [
                        'placeholder' => '',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]);
                ?>
            </div>
            <div class="form-group">
                <?php echo '<label class="control-label">ประเภทการลา</label>';
                echo Select2::widget([
                    'name' => 'leaveType',
                    'data' => ArrayHelper::map(LeaveType::find()->orderBy(['id' => SORT_ASC])->all(), 'id', 'leave_type_name'),
                    'value' => $leave_type,
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ]);
                ?>
            </div>


            <?php if ($model->isNewRecord): ?>

                <div class="form-group">
                    <?php
                    echo kartik\checkbox\CheckboxX::widget([
                        'name' => 'check',
                        'value' => 0,
                        'options' => ['id' => 's_1'],
                        'pluginOptions' => ['threeState' => false]
                    ]);
                    echo '<label class="cbx-label" for="s_1"><strong>ลางานแบบครึ่งวัน</strong></label>';
                    ?>
                </div>

                <div id="date" class="form-group">
                    <?php echo '<label class="control-label">วันลา</label>';
                    echo DateRangePicker::widget([
                        'name' => 'date_range',
                        'hideInput' => true
                    ]);
                    ?>
                </div>

                <div id="dateTime" class="form-group ">
                    <div class="form-group">
                        <?php
                        echo '<label class="control-label">วันและเวลาที่ลา</label>';
                        echo DateTimePicker::widget([
                            'name' => 'date_range1',
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd hh:ii:ss'
                            ]
                        ]);
                        ?>
                    </div>
                    <div class="form-group">
                        <?php
                        echo $form->field($model, 'leave_hours')->textInput(['maxlength' => true])
                        ?>
                    </div>




                </div>

            <?php else: ?>
                <?php if ($check == 0) : ?>
                    <div id="date" class="form-group">
                        <?php echo '<label class="control-label">วันลา</label>';
                        echo DateRangePicker::widget([
                            'name' => 'date_range',
                            'value' => $leave_date,
                            'hideInput' => true
                        ]);
                        ?>

                    </div>
                <?php else: ?>
                    <div id="dateTime" class="form-group">
                        <?php
                        echo '<label class="control-label">วันและเวลาที่ลา</label>';
                        echo DateTimePicker::widget([
                            'name' => 'date_range1',
                            'value' => $leave_date . ' ' . $time,
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd hh:ii:ss'
                            ]
                        ]);
                        ?>
                        <?php
                        echo '<label class="control-label"></label>';
                        echo $form->field($model, 'leave_hours')->textInput(['maxlength' => true])
                        ?>

                    </div>

                <?php endif; ?>
            <?php endif; ?>
            <div class="form-group">
                <?= $form->field($model, 'description')->textInput(['maxlength' => true])->textarea(['row' => '10']) ?>
                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    <?= Html::a('back', ['index'], ['class' => 'btn btn-danger']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
    <!---->
<?php
$this->registerJs(<<<JS
$("#date").show();
$("#dateTime").hide();
$("#s_1").change(function(){
    var check = $("#s_1").val();
    if (check == 0){
        $("#date").show();
        $("#dateTime").hide();
    } else{
        $("#date").hide();
        $("#dateTime").show();
    }
    console.log(check);
});

var varcheck =$check;
if (varcheck == 0){
     $("#date").show();
        $("#dateTime").hide();
} else{
    $("#date").hide();
        $("#dateTime").show();
}
console.log(varcheck);
// // var aa = $("#w2").val();
// console.log(aa)
JS
);
