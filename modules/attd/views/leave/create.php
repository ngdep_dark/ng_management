<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\attd\models\Leave */

$this->title = 'สร้างการลา';
$this->params['breadcrumbs'][] = ['label' => 'Leaves', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="leave-create">

    <?= $this->render('_form', [
        'model' => $model,
        'user_id' => Yii::$app->user->id,
        'leave_type' => $leave_type,
        'check' => $check,
    ]) ?>

</div>
