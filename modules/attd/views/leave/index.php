<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use app\models\User;
use app\modules\attd\models\Leave;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\attd\models\LeaveSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'บันทึกการลา';
$this->params['breadcrumbs'][] = $this->title;

function Username($id)
{
    return User::findOne($id);
}

function leaveDate($id)
{
    return Leave::findOne($id);
}


?>
<div class="leave-index">

    <?php Pjax::begin(); ?>

    <p>
        <?= Html::a('เพิ่มการลางาน', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsive' => true,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'contentOptions' => ['style' => 'width: 50px;', 'class' => 'text-center'],
            ],
            [
                'attribute' => 'username',
                'label' => 'ชื่อพนักงาน',
                'headerOptions' => ['style' => 'width: 200px;'],

            ],
            [
                'attribute' => 'leave_start',
                'label' => 'วันที่ลา',
                'headerOptions' => ['style' => 'width: 200px;'],

            ],
            [
                'attribute' => 'leave_type',
                'label' => 'ประเภทการลา',
                'headerOptions' => ['style' => 'width: 170px;'],
                'value' => function ($model) {
                    return $model->half_time == 0 ? $model->typeName . ' (' . $model->halftimeStatus($model->half_time) . ') ' : $model->typeName . ' (' . $model->halftimeStatus($model->half_time) . ')';
                }
            ],
            [
                'attribute' => 'description',
                'label' => 'หมายเหตุ',
                'headerOptions' => ['style' => 'width: 380px;'],

            ],
            [
                'attribute' => 'approve_by',
                'label' => 'ผู้บันทึก',
                'headerOptions' => ['style' => 'width: 200px;'],
            ],
            [
                'attribute' => 'created_at',
                'label' => 'เวลาที่บันทึก',
                'headerOptions' => ['style' => 'width: 200px;'],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Actions',
                'headerOptions' => ['style' => 'width: 80px;'],
                'template' => '{update} {delete}',
                'visibleButtons' => [
                    'update' => function ($url, $data, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', \yii\helpers\Url::to(['/attd/leave/update', 'id' => $data['id']]));
                    },
                    'delete' => function ($url, $model, $key) {
                        $username = Username(Yii::$app->user->id)->username;
                        $approver = leaveDate($model)->approve_by;
                        if ($username == $approver) {
                            return Html::a('<i class="glyphicon glyphicon-trash"></i>', ['/attd/leave/delete', 'id' => $model['id']], [
                                'data' => [
                                    'confirm' => 'คุณต้องการลบรายการนี้หรือไม่',
                                    'method' => 'post',
                                ],
                            ]);
                        } else {
                            return null;
                        }
                    }
                ]
            ]
        ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<h2 class="panel-title">การลา</h2>',
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>