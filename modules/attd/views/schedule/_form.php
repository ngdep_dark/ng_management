<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use kartik\widgets\TimePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\modules\attd\models\LeaveType;

/* @var $this yii\web\View */
/* @var $model app\modules\attd\models\Workoutattendance */
/* @var $form yii\widgets\ActiveForm */


?>
<div class="col-md-12 col-lg-12">
    <div class="workoutattendance-form">
        <?php $form = ActiveForm::begin(); ?>
        <div class="form-group">
            <?php echo '<label class="control-label">ผู้ลา </label>';
            echo Select2::widget([
                'name' => 'user_id',
                'data' => ArrayHelper::map(\app\models\User::find()->orderBy(['id' => SORT_ASC])->all(), 'id', 'username'),
                'options' => [
                    'placeholder' => ' ',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]);
            ?>
        </div>
        <div class="form-group">
            <?php echo '<label class="control-label">ประเภทการลา</label>';
            echo Select2::widget([
                'name' => 'leaveType',
                'data' => ArrayHelper::map(LeaveType::find()->orderBy(['id' => SORT_ASC])->all(), 'id', 'leave_type_name'),
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]);
            ?>
        </div>
        <div class="form-group">
            <?php echo '<label class="control-label">วันลา</label>';
            echo DateRangePicker::widget([
                'name' => 'date_range',
                'hideInput' => true
            ]);
            ?>
        </div>
        <div class="row">
            <div class="col-md-6">
                <?php echo '<label class="control-label">Start Time</label>';
                echo TimePicker::widget([
                    'name' => 't1',
                    'pluginOptions' => [
                        'showSeconds' => false,
                        'showMeridian' => false,
                        'minuteStep' => 30,
                    ]
                ]);
                ?>
            </div>
            <div class="col-md-6">
                <?php echo '<label class="control-label">End Time</label>';
                echo TimePicker::widget([
                    'name' => 't1',
                    'pluginOptions' => [
                        'showSeconds' => false,
                        'showMeridian' => false,
                        'minuteStep' => 30,
                    ]
                ]);
                ?>
            </div>
        </div>
        <br>
        <div class="form-group">
            <?= $form->field($model, 'description')->textInput(['maxlength' => true])->textarea(['row' => '10']) ?>
            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                <?= Html::a('back', ['index'], ['class' => 'btn btn-danger']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>


