<?php

use app\modules\attd\models\Leave;
//use app\modules\attd\models\Attendance;
use yii\helpers\Url;

$late = $shiftdata['come_late_time'];
$retire = $shiftdata['back_early_time'];
$start_time = '';
$end_time = '';


$this->title = "รายการลงเวลางาน";
$this->params['breadcrumbs'][] = ['label' => 'Schedule', 'url' => ['pre-index']];
$this->params['breadcrumbs'][] = $this->title;

function leave($id)
{
    return Leave::findOne($id);
}

function outsideType($id){
    return \app\modules\attd\models\AttendanceOutsideService::findOne($id);

}

?>

<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="panel panel-info">
            <div class="panel-heading" style="font-size: 20px"><strong><?= $username->full_name ?></strong></div>
            <div class="panel-body">
                <div class="col-md-3 col-lg-3">
                    <div class="panel panel-primary">
                        <div class="panel-heading" style="font-size: 18px"><strong>ข้อมูลส่วนตัวพนักงาน</strong></div>
                        <div class="panel-body">
                            <table class="table table-hover">
                                <tr>
                                    <td><strong>ชื่อพนักงาน: <?= $username->full_name ?></strong></td>
                                </tr>
                                <tr>
                                    <td><strong>เวลาเข้างาน: <?= $late ?></strong></td>
                                </tr>
                                <tr>
                                    <td><strong>เวลาเลิกงาน: <?= $retire ?></strong></td>
                                </tr>

                            </table>
                        </div>
                        <div class="panel-heading" style="font-size: 18px"><strong>ข้อมูลสรุปวันลา</strong>
                        </div>
                        <div class="panel-body">
                            <table class="table table-hover">
                                <tr>
                                    <td><strong>ลากิจ: <?= $errandLeavecount ?> วัน </strong></td>
                                </tr>
                                <tr>
                                    <td><strong>ลาป่วย: <?= $sickLeavecount ?> วัน</strong></td>
                                </tr>
                                <tr>
                                    <td><strong>ลาพักร้อน: <?= $holidayLeavecount ?> วัน </strong></td>
                                </tr>
                                <tr>
                                    <td><strong>ลากิจ (หักเงิน): <?= $ELDCount ?> วัน </strong></td>
                                </tr>
                                <tr>
                                    <td><strong>ลาป่วย (ไม่มีใบรับรองแพทย์ หักเงิน): <?= $SLMCcount ?> วัน </strong></td>
                                </tr>
                                <tr>
                                    <td><strong>จำนวนขาดงาน: <?= $absencework ?> วัน </strong></td>
                                </tr>
                                <tr>
                                    <td><strong>ลาอื่น ๆ : <?= $otherLeavecount ?> วัน </strong></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-lg-9">
                    <div class="panel panel-primary">
                        <div class="panel-heading" style="font-size: 18px"><strong>การลงเวลา</strong></div>
                        <div class="panel-body">
                            <div class="col-md-4 col-lg-4">
                                <div class="panel panel-default">
                                    <div class="panel panel-heading text-right"><strong>จำนวนการลงเวลา</strong></div>
                                    <div class="panel-body text-right" style="line-height: 1px">
                                        <?php echo $clockincount . " " . "วัน" ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <div class="panel panel-default">
                                    <div class="panel panel-heading text-right"><strong>จำนวนวันลา</strong></div>
                                    <div class="panel-body text-right" style="line-height: 1px">
                                        <?php echo $leavecount . " " . "วัน" ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <div class="panel panel-default">
                                    <div class="panel panel-heading text-right"><strong>จำนวนวันที่ขาด</strong></div>
                                    <div class="panel-body text-right" style="line-height: 1px">
                                        <?php echo $absencework . " " . "วัน" ?>
                                    </div>
                                </div>
                            </div>
                            <!-- ข้อมูลการลงเวลาเข้า-ออก -->
                            <div class="col-md-12 col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel panel-heading"><strong>บันทึกการลงเวลาเข้า-ออก (ย้อนหลัง 30
                                            วัน)</strong></div>
                                    <div class="panel-body" style="line-height: 1px">
                                        <table class="table table-hover">
                                            <thead style="line-height: 1.5px">
                                            <tr>
                                                <th>เวลาบันทึก</th>
                                                <th>เวลาเข้า</th>
                                                <th>เวลาออก</th>
                                                <th>ระยะเวลาทำงาน</th>
                                                <th>หมายเหตุ</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($clockin as $clockTime => $clockvalue): ?>
                                                <tr style="line-height: 1.5px">
                                                    <td><?= Yii::$app->datetimeManage->dtFormat($clockvalue['att_date']) ?></td>
                                                    <td><?= substr($clockvalue['att_check'], 11) ?></td>
                                                    <td><?php $nulldate = substr($clockvalue['att_checkout'], 11);
                                                        if ($clockvalue['att_date'] == date('Y-m-d') && $clockvalue['att_checkout'] == null) {
                                                            echo '';
                                                        } elseif ($clockvalue['att_date'] < date('Y-m-d')) {
                                                            echo $nulldate;
                                                        }
                                                        ?></td>
                                                    <td><?php
                                                        $time1 = substr($clockvalue['att_check'], 11, 5);
                                                        $time2 = substr($clockvalue['att_checkout'], 11, 5);
                                                        $diffTime = differenceInHours($time1, $time2);
                                                        if ($clockvalue['att_date'] == date('Y-m-d')) {
                                                            echo '';
                                                        }
                                                        elseif (empty($time2)){
                                                            echo '';
                                                        }
                                                        elseif ($clockvalue['att_date'] < date('Y-m-d')) {
                                                            echo round($diffTime) - 1 . " ชั่วโมง ";
                                                        }
                                                        ?></td>

                                                    <?php if (substr($clockvalue['att_check'], 11) > $late) : ?>
                                                        <td><span class="label label-warning">สาย</span></td>
                                                    <?php else: ?>
                                                        <td><span class="label label-success">ปกติ</span></td>
                                                    <?php endif; ?>

                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>

                                        </table>
                                    </div>
                                </div>
                            </div>
<!--                           -->
                            <!-- ข้อมูลการลางาน -->
                            <div class="col-md-12 col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel panel-heading"><strong>การลางาน</strong></div>
                                    <div class="panel-body" style="line-height: 1px">
                                        <table class="table table-hover">
                                            <thead style="line-height: 1.5px">
                                            <tr>
                                                <th>วันที่ลา</th>
                                                <th>ประเภทการลา</th>
                                                <th>หมายเหตุ</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($leave as $time): ?>
                                                <tr style="line-height: 1.5px">
                                                        <td><?= Yii::$app->datetimeManage->dtFormat($time->leave_start) ?></td>
                                                        <td><?php $leavetype = Leave($time->id);
                                                            echo $leavetype->typeName ?></td>
                                                        <td><?= $time->description ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- ข้อมูลการลางาน -->
                            <div class="col-md-12 col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel panel-heading"><strong>การทำงานนอกสถานที่</strong></div>
                                    <div class="panel-body" style="line-height: 1px">
                                        <table class="table table-hover">
                                            <thead style="line-height: 1.5px">
                                            <tr>
                                                <th>วันที่</th>
                                                <th>ประเภทการทำงาน</th>
                                                <th>รายละเอียด</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($outside_work as $outside): ?>
                                                <tr style="line-height: 1.5px">
                                                    <td><?= Yii::$app->datetimeManage->dtFormat($outside->outside_date_start) ?></td>
                                                    <td><?php $outsidetype = outsideType($outside->id);
                                                        echo $outsidetype->typeName ?></td>
                                                    <td><?= $outside->detail ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
function differenceInHours($startdate, $enddate)
{
    $starttimestamp = strtotime($startdate);
    $endtimestamp = strtotime($enddate);
    $difference = abs($endtimestamp - $starttimestamp) / 3600;
    return $difference;
}

if (!empty($_POST["submit"])) {
    $hours_difference = differenceInHours($_POST["startdate"], $_POST["enddate"]);
    $message = "The Difference is " . $hours_difference . " hours";
}
?>
