<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\attd\models\Workoutattendance */
//var_dump($errandLeavecount);
//var_dump($sickLeavecount);
//var_dump($holidayLeavecount);
//exit();


$this->title = '';
$this->params['breadcrumbs'][] = ['label' => 'Workoutattendances', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'แบบฟอร์มการลางาน';


?>
<div class="workoutattendance-create">

    <h1><?= Html::encode('แบบฟอร์มการลางาน') ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
