<?php

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ข้อมูลการลงเวลาของพนักงาน';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-sm-12">
        <div class="user-index">
            <?php Pjax::begin(); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'responsive' => true,
                'columns' => [
                    [
                        'attribute' => 'id',
                        'label' => 'รหัสพนักงาน',
                        'headerOptions' => ['style' => 'width: 500px;'],
                    ],
                    [
                        'attribute' => 'full_name',
                        'label' => 'รายชื่อพนักงาน'
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Actions',
                        'headerOptions' => ['style' => 'width: 80px;'],
                        'template' => '{view}',
                        'buttons' => [
                            'view' => function ($url, $data, $model) {
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', Url::to(['/attd/schedule/index', 'id' => $data['id']]));
                            },
                        ]
                    ]
                ],
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => '<h2 class="panel-title">ดูการลงเวลาย้อนหลังรายบุคคล</h2>',
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>


