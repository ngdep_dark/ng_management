<?php

use app\models\User;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use app\modules\attd\models\Leave;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;

$user = User::find()->where(['id' => Yii::$app->user->id])->one();
$this->title = "รายงานสรุปการลาของ" . " " . $user['username'];

$holidayLeave = 7;
$sickLeave = 30;
$errandLeave = 7;

$holidayCount = $holidayLeave - $holidayLeaveCount;
$sickCount = $sickLeave - $sickLeaveCount;
$errandCount = $errandLeave - $errandLeaveCount;

$this->registerCss("
    .style-td{
        font-size: 18px; 
        line-height: 1cm;
    }
");

function leave($id){
   return Leave::findOne($id);
}

$dummy = 2;
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">ข้อมูลสรุปวันลา</h3>
            </div>
            <div class="panel-body">
                <table class="row">
                    <table class="col-lg-12 col-md-12">
                        <tbody>
                        <tr class="style-td">
                            <td></td>
                            <td class="style-td; col-md-3">ลากิจ</td>
                            <td class="style-td; col-md-3">ลาป่วย</td>
                            <td class="style-td; col-md-3">ลาพักร้อน</td>
                        </tr>
                        <tr class="style-td">
                            <td class="style-td; col-md-3">สรุปวันลา</td>
                            <td class="style-td; col-md-3"><?= $holidayLeaveCount ?> วัน</td>
                            <td class="style-td; col-md-3"><?= $sickLeaveCount ?> วัน</td>
                            <td class="style-td; col-md-3"><?= $errandLeaveCount ?> วัน</td>
                        </tr>
                        <tr class="style-td">
                            <td class="style-td; col-md-3">ยอดคงเหลือ</td>
                            <td class="style-td; col-md-3"><?= $holidayCount ?> วัน</td>
                            <td class="style-td; col-md-3"><?= $sickCount ?> วัน</td>
                            <td class="style-td; col-md-3"><?= $errandCount ?> วัน</td>
                        </tr>
                        </tbody>
                    </table>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <?php $form = ActiveForm::begin(['method' => 'post']); ?>
    <div class="col-md-12">
        <div class="col-md-3">
           <?php echo '<label class="control-label">Date Range</label>';
            echo '<div class="drp-container">';
                echo DateRangePicker::widget([
                'name'=>'date_range',
                'presetDropdown'=>true,
                'hideInput'=>true
                ]);
                ?>
        </div>
    </div>
    <div>
        <br>
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php $form = ActiveForm::end(); ?>
</div>

<?php
$post = Yii::$app->request->post();
if ($post) : ?>
    <div class="col-md-12">
        <?php echo
        GridView::widget([
            'dataProvider' => $dataProvider,
            'options' => ['style' => 'width:100%'],
            'columns' => [
                [
                    'attribute' => 'user_id',
                    'label' => 'User ID',
                    'headerOptions' => ['style' => 'width:80px'],
                ],
                [
                    'attribute' => 'username',
                    'label' => 'ชื่อผู้ใช้งาน',
                    'headerOptions' => ['style' => 'width: 180px'],
                ],
                [
                    'attribute' => 'leave_type',
                    'label' => 'ประเภทการลา',
                    'headerOptions' => ['style' => 'width: 80px'],
                    'value' => function ($model) {
                       $levObj = \leave($model['id']) ;
                       return $levObj->typeName;
                    }
                ],
                [
                    'attribute' => 'leave_start',
                    'label' => 'วันที่เริ่มลา',
                    'headerOptions' => ['style' => 'width: 80px'],
                ],
                [
                    'attribute' => 'leave_end',
                    'label' => 'วันที่สิ้นสุดการลา',
                    'headerOptions' => ['style' => 'width: 80px'],
                ]
            ]
        ])

        ?>
    </div>

<?php endif; ?>
