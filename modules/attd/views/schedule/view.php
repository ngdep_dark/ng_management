<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\attd\models\Workoutattendance */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'Workoutattendances', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="workoutattendance-view">
    <div class="panel-heading">
        <div class="col-md-4 col-xs-4 col-lg-4">
            <p>
                <?= Html::a('Archive', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-primary',
                    'data' => [
                        'confirm' => 'Are you sure you want to archive this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'username',
                    'leave_start',
                    'leave_end',
                    'description',
                    'approve_by'
                ],
            ]) ?>
        </div>
    </div>
</div>
