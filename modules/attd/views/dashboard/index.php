<?php

use yii\widgets\Pjax;
use yii\web\JsExpression;

/* @var $this yii\web\View */
$this->title = Yii::$app->messageManage->dash_board;
$this->registerCss("
    th { background-color: #4cffad; }
");
$i = 1;
$a = 1;
//$normal = '09:00:00';
$late = '09:00:01';
?>
    <div class="dash-board-calendar">

        <?php Pjax::begin(); ?>
        <?= \yii\helpers\Html::a("Refresh", [
            '/attd/dashboard'],
            [
                'id' => 'refreshButton',
                'style' => 'display: none-img'
            ])
        ?>

        <div class="row">
            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">ปฏิทินการลางาน และ ปฏิทินวันหยุด</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form">
                        <div class="box-body">
                            <?= yii2fullcalendar\yii2fullcalendar::widget([
                                'id' => 'calendar',
                                'options' => [
                                    'lang' => 'th',
                                ],
                                'events' => \yii\helpers\Url::to(['/attd/dashboard/json-leave-calendar']),
                            ]);
                            ?>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="font-size: 20px">
                        บันทึกการลงเวลาในวันนี้
                    </div>
                    <table class="table table-bordered" style="font-size: 18px">
                        <tr >
                            <th style="width: 100px; text-align: center">Num</th>
                            <th style="width: 450px; text-align: center">Name</th>
                            <th style="text-align: center; width: 152px">ทดเวลา</th>
                            <th style="text-align: center">Clock_in</th>
                            <th style="text-align: center">Clock_out</th>
                        </tr>
                        <?php foreach ($timescan as $value): ?>
                            <tr>
                                <td><?= $i++ ?></td>
                                <td><?= $value['full_name'] . ' (' . $value['username'] . ')' ?></td>
                                <td style="text-align: center"><?= $value['reduce_time'] != null ? substr($value['reduce_time'], 1, 1).':' . substr($value['reduce_time'], 3, 2) .' ชั่วโมง' : '-' ?></td>
                                <!-- เช็คเวลามาปกติ -->
                                <?php if (substr($value['att_check'], 11) < $value['come_late_time']): ?>
                                    <td class="label-success"><?= substr($value['att_check'], 11) ?></td>

                                    <!-- เช็คเวลาสายแต่ไม่เกิน 15 นาที -->
                                <?php elseif(substr($value['att_check'], 11) >= $value['come_late_time'] && substr($value['att_check'], 11) <= date('H:i:s', strtotime($value['come_late_time']) +15*60)) : ?>
                                    <td class="label-warning"><?= substr($value['att_check'], 11) ?></td>

                                    <!-- เช็คเวลามาสายเกิน 15 นาที -->
                                <?php elseif(substr($value['att_check'], 11) > date('H:i:s', strtotime($value['come_late_time']) +15*60)): ?>
                                    <td style="background-color: lightgrey"><?= substr($value['att_check'], 11) ?></td>
                                <?php endif; ?>

                                <?php if (substr($value['att_checkout'], '11') < $value['back_early_time'] && $value['att_checkout'] != null): ?>
                                    <td class="label-warning"><?= substr($value['att_checkout'], '11') == null ? '--:--:--' : substr($value['att_checkout'], '11') ?></td>
                                <?php elseif (substr($value['att_checkout'], '11') > $value['back_early_time'] && $value['att_checkout'] != null): ?>
                                    <td class="label-primary"><?= substr($value['att_checkout'], '11') == null ? '--:--:--' : substr($value['att_checkout'], '11') ?></td>
                                <?php else: ?>
                                    <td><?= substr($value['att_checkout'], '11') == null ? '--:--:--' : substr($value['att_checkout'], '11') ?></td>
                                <?php endif; ?>

                            </tr>
                        <?php endforeach; ?>
                        <?php foreach ($no_scan_user as $value): ?>
                            <tr>
                                <td><?= $i++ ?></td>
                                <td><?= $value['full_name'] . ' (' . $value['username'] . ')' ?></td>
                                <td style="text-align: center"><?= '-' ?></td>
                                <td class="label-danger"><?= '--:--:--' ?></td>
                                <td><?= '--:--:--' ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>

                <h4>หมายเหตุ: ทดเวลาเข้างานจะแสดงเฉพาะผู้ที่สแกนนิ้วออกหลังเที่ยงคืนเท่านั้น</h4>

                <div class="panel panel-primary">
                    <div class="panel-heading" style="font-size: 26px">
                        รายชื่อคนที่ลาในวันนี้
                    </div>
                    <table class="table table-bordered" style="font-size: 26px">
                        <tr>
                            <th style="width: 100px">Num</th>
                            <th style="width: 320px">Name</th>
                            <th style="width: 200px; text-align: center">ประเภทการลา</th>
                            <th>รายละเอียด</th>
                        </tr>
                        <?php foreach ($user_leave as $value): ?>
                            <tr class="label-info">
                                <td><?= $a++ ?></td>
                                <td><?= $value['full_name'] . ' (' . $value['username'] . ')' ?></td>
                                <td style="text-align: center"><?= $value['leave_type_name'] ?></td>
                                <td><?= $value['description'] ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>

            </div>
        </div>
    </div>

<?php Pjax::end(); ?>

<?php
$this->registerJs(<<<JS
$(document).ready(function() {
    setInterval(function(){
        $("#refreshButton").click(); 
    }, 300000);
});

$('#calendar').fullCalendar({
    eventClick: function(calEvent) {
    alert('Event: ' + calEvent.title);
   
    console.log(calEvent.title);
  }
});



JS
);