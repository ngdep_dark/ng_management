<?php
/**
 * Created by PhpStorm.
 * User: shiranko
 * Date: 7/1/2562
 * Time: 10:01 น.
 */

use yii\widgets\Pjax;

$i = 1;
$a = 1;
$late = '09:00:01';
$very_late = '09:15:01';
$timeout = '18:00:00'

?>
    <h1 class="pull-left">TimeScan Monitor</h1>
    <h1 class="pull-right">วันที่ <?= Yii::$app->datetimeManage->dtFormat(date('Y-m-d')) ?></h1>
    <div class="col-md-12 ">

        <?php Pjax::begin(); ?>
        <?= \yii\helpers\Html::a("Refresh", [
            '/attd/dashboard/monitor-scan'],
            [
                'id' => 'refreshButton',
                'style' => 'display: none-img'
            ])
        ?>

        <div class="panel panel-primary">
            <div class="panel-heading" style="font-size: 26px">
                บันทึกการลงเวลาในวันนี้
            </div>
            <table class="table table-bordered" style="font-size: 26px">
                <tr >
                    <th style="width: 100px; text-align: center">Num</th>
                    <th style="width: 450px; text-align: center">Name</th>
                    <th style="text-align: center; width: 152px">ทดเวลา</th>
                    <th style="text-align: center">Clock_in</th>
                    <th style="text-align: center">Clock_out</th>
                </tr>
                <?php foreach ($timescan as $value): ?>
                    <tr>
                        <td><?= $i++ ?></td>
                        <td><?= $value['full_name'] . ' (' . $value['username'] . ')' ?></td>
                        <td style="text-align: center"><?= $value['reduce_time'] != null ? substr($value['reduce_time'], 1, 1).':' . substr($value['reduce_time'], 3, 2) .' ชั่วโมง' : '-' ?></td>
                        <!-- เช็คเวลามาปกติ -->
                        <?php if (substr($value['att_check'], 11) < $value['come_late_time']): ?>
                            <td class="label-success"><?= substr($value['att_check'], 11) ?></td>

                        <!-- เช็คเวลาสายแต่ไม่เกิน 15 นาที -->
                        <?php elseif(substr($value['att_check'], 11) >= $value['come_late_time'] && substr($value['att_check'], 11) <= date('H:i:s', strtotime($value['come_late_time']) +15*60)) : ?>
                            <td class="label-warning"><?= substr($value['att_check'], 11) ?></td>

                        <!-- เช็คเวลามาสายเกิน 15 นาที -->
                        <?php elseif(substr($value['att_check'], 11) > date('H:i:s', strtotime($value['come_late_time']) +15*60)): ?>
                            <td style="background-color: lightgrey"><?= substr($value['att_check'], 11) ?></td>
                        <?php endif; ?>

                        <?php if (substr($value['att_checkout'], '11') < $value['back_early_time'] && $value['att_checkout'] != null): ?>
                            <td class="label-warning"><?= substr($value['att_checkout'], '11') == null ? '--:--:--' : substr($value['att_checkout'], '11') ?></td>
                        <?php elseif (substr($value['att_checkout'], '11') > $value['back_early_time'] && $value['att_checkout'] != null): ?>
                            <td class="label-primary"><?= substr($value['att_checkout'], '11') == null ? '--:--:--' : substr($value['att_checkout'], '11') ?></td>
                        <?php else: ?>
                            <td><?= substr($value['att_checkout'], '11') == null ? '--:--:--' : substr($value['att_checkout'], '11') ?></td>
                        <?php endif; ?>

                    </tr>
                <?php endforeach; ?>
                <?php foreach ($no_scan_user as $value): ?>
                    <tr>
                        <td><?= $i++ ?></td>
                        <td><?= $value['full_name'] . ' (' . $value['username'] . ')' ?></td>
                        <td style="text-align: center"><?= '-' ?></td>
                        <td class="label-danger"><?= '--:--:--' ?></td>
                        <td><?= '--:--:--' ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>

        <h3>หมายเหตุ: ทดเวลาเข้างานจะแสดงเฉพาะผู้ที่สแกนนิ้วออกหลังเที่ยงคืนเท่านั้น</h3>

        <div class="panel panel-primary">
            <div class="panel-heading" style="font-size: 26px">
                รายชื่อคนที่ลาในวันนี้
            </div>
            <table class="table table-bordered" style="font-size: 26px">
                <tr>
                    <th style="width: 100px">Num</th>
                    <th style="width: 320px">Name</th>
                    <th style="width: 200px; text-align: center">ประเภทการลา</th>
                    <th>รายละเอียด</th>
                </tr>
                <?php foreach ($user_leave as $value): ?>
                    <tr class="label-info">
                        <td><?= $a++ ?></td>
                        <td><?= $value['full_name'] . ' (' . $value['username'] . ')' ?></td>
                        <td style="text-align: center"><?= $value['leave_type_name'] ?></td>
                        <td><?= $value['description'] ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>


        <?php Pjax::end(); ?>
    </div>

<?php
$this->registerJs(<<<JS
$(document).ready(function() {
    setInterval(function(){
        $("#refreshButton").click(); 
    }, 600000);
});

$('#calendar').fullCalendar({
    eventClick: function(calEvent) {
    alert('Event: ' + calEvent.title);
   
    console.log(calEvent.title);
  }
});



JS
);