<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\attd\models\HolidaySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ตั้งค่าวันหยุด';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="holiday-index">

    <p>
        <?= Html::a('สร้างวันหยุด', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'holiday_name',
            'description',
            'holiday_date',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
