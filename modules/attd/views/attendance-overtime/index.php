<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\attd\models\AttendanceOvertimeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'บันทึกการทำงานล่วงเวลา';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attendance-overtime-index">

    <?php Pjax::begin(); ?>
    <p>
        <?= Html::a('เพิ่มการทำงานล่วงเวลา', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'id',
            [
                'attribute' => 'user_id',
                'label' => 'ชื่อพนักงาน',
                'headerOptions' => ['style' => 'width: 120px'],
                'value' => function($model){
                    return $model->userName;
                }

            ],
            [
                'attribute' => 'overtime_date',
                'label' => 'วันที่ทำงานล่วงเวลา',
                'headerOptions' => ['style' => 'width: 150px'],
                'value' => function ($model) {
                    return Yii::$app->datetimeManage->dtFormat($model['overtime_date']);
                }
            ],
            [
                'attribute' => 'start_time',
                'headerOptions' => ['style' => 'width: 150px'],
                'label' => 'เวลาเริ่ม',

            ],
            [
                'attribute' => 'end_time',
                'headerOptions' => ['style' => 'width: 150px'],
                'label' => 'เวลาเลิก'
            ],
            [
                'attribute' => 'description',
                'label' => 'หมายเหตุ'
            ],
            [
                'attribute' => 'ot_duration',
                'label' => 'ระยะเวลาทำงาน',
                'value' => function ($model) {
                    return floatval($model->ot_duration / 10) .  " ชั่วโมง ";
                }
            ],
            [
                'attribute' => 'approve_by',
                'label' => 'บันทึกโดย'
            ],
            [
                'attribute' => 'created_time',
                'label' => 'เวลาที่บันทึก'
            ],

//            ['class' => 'yii\grid\ActionColumn'],
        ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<h2 class="panel-title">รายการบันทึกการทำงานล่วงเวลา</h2>',
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
