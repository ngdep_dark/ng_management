<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\widgets\TimePicker;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\attd\models\AttendanceOvertime */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-md-6">
    <div class="attendance-overtime-form">
        <?php $form = ActiveForm::begin(); ?>
        <div class="form-group">
            <?php echo '<label class="control-label">ชื่อผู้ใช้งาน</label>';
            echo Select2::widget([
                'name' => 'user_id',
                'data' => ArrayHelper::map(\app\models\User::find()->orderBy(['id' => SORT_ASC])->all(), 'id', 'full_name'),
                'value' => $user_id,
                'options' => [
                    'placeholder' => ' ',
//                    'multiple' => true
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]);
            ?>
        </div>
        <div class="row">
            <div class="col-md-12">
                <?php
                echo '<label class="control-label">วันที่ทำงานล่วงเวลา</label>';
                echo DatePicker::widget([
                    'name' => 'overtime_date',
                    'value' => date('Y-m-d'),
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd'
                    ]
                ]);
                ?>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-6">
                <?php echo '<label class="control-label">เวลาเริ่ม</label>';
                echo TimePicker::widget([
                    'name' => 'start_time',
                    'pluginOptions' => [
                        'showSeconds' => false,
                        'showMeridian' => false,
                        'defaultTime' => '18:00',
                        'minuteStep' => 5,
                    ]
                ]);
                ?>
            </div>
            <div class="col-md-6">
                <?php echo '<label class="control-label">เวลาสิ้นสุด</label>';
                echo TimePicker::widget([
                    'name' => 'end_time',
                    'pluginOptions' => [
                        'showSeconds' => false,
                        'showMeridian' => false,
                        'defaultTime' => '20:00',
                        'minuteStep' => 5,
                    ]
                ]);
                ?>
            </div>
        </div>
        <br>
        <div class='form-group'>
            <?= $form->field($model, 'description')->textarea(['rows' => '6']) ?>
        </div>
        <br>
        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
        <?php ActiveForm::end(); ?>

    </div>

</div>
