<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\attd\models\AttendanceOvertime */

$this->title = 'สร้างบันทึกการทำงานล่วงเวลา';
$this->params['breadcrumbs'][] = ['label' => 'Attendance Overtimes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attendance-overtime-create">


    <?= $this->render('_form', [
        'model' => $model,
        'user_id' => Yii::$app->user->id
    ]) ?>

</div>
