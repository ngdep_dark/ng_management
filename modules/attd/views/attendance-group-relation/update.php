<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\attd\models\AttendanceGroupRelation */

$this->title = 'Update Attendance Group Relation: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Attendance Group Relations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="attendance-group-relation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
