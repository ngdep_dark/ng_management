<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\modules\attd\models\AttendanceGroupRelation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="attendance-group-relation-form">

    <div class="col-md-4">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'parent')->widget(Select2::classname(), [
            'data' => $model->groupName,
            'options' => ['placeholder' => 'Group', 'multiple' => false],
        ]);
        ?>


        <?= $form->field($model, 'groupChild')->widget(Select2::className(), [
            'data' => $model->groupName,
            'options' => ['placeholder' => 'Relation Group', 'multiple' => true],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]) ?>


        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>


</div>
