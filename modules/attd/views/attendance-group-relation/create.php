<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\attd\models\AttendanceGroupRelation */

$this->title = 'Create Attendance Group Relation';
$this->params['breadcrumbs'][] = ['label' => 'Attendance Group Relations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attendance-group-relation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
