<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\attd\models\AttendanceCronLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Attendance Cron Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attendance-cron-log-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Attendance Cron Log', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'record',
            'created_at:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>