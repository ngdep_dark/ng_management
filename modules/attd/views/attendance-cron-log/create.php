<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\attd\models\attendanceCronLog */

$this->title = 'Create Attendance Cron Log';
$this->params['breadcrumbs'][] = ['label' => 'Attendance Cron Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attendance-cron-log-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
