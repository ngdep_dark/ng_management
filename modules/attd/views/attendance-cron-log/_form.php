<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\attd\models\attendanceCronLog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="attendance-cron-log-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'record')->textInput() ?>

    <?= $form->field($model, 'created_at')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>