<div class="col-lg-4">
    <div class="panel panel-color panel-dark">
        <div class="panel-heading">
            <h3 class="panel-title"><?=$model->name?></h3>

        </div>
        <div class="panel-body">
            <div class="text-right">
                <span>
                     <a href=""><h2 ><i class="mdi mdi-playlist-remove"></i></h2></a>
                </span>
            </div>
            <h5>กำหนด</h5>
            <?= \yii\widgets\DetailView::widget([
            'model' => $model,
            'attributes' => [
            'come_late_time',
            'back_early_time',
            'no_attend_time',
            ],
            ]) ?>
        </div>
    </div>
</div>
