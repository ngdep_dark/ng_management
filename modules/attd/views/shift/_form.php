<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\TimePicker;

$this->title = 'ตั้งค่าโปรไฟล์การทำงาน'
/* @var $this yii\web\View */
/* @var $model app\modules\attd\models\Shift */
/* @var $form yii\widgets\ActiveForm */
?>

        <div class="shift-form">

            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                    <div class="row">
                        <div class="col-md-4">
                            <?=
                            $form->field($model, 'come_late_time')->widget(TimePicker::classname(), [
                                'pluginOptions' => [
                                    'minuteStep' => 1,
                                    'defaultTime' => '09:00',
                                    'showMeridian' => false
                                ],
                            ]);
                            ?>
                        </div>
                        <div class="col-md-4">
                            <?=
                            $form->field($model, 'back_early_time')->widget(TimePicker::classname(), [
                                'pluginOptions' => [
                                    'minuteStep' => 1,
                                    'defaultTime' => '18:00',
                                    'showMeridian' => false
                                ],
                            ]);
                            ?>
                        </div>
                        <div class="col-md-4">
                            <?=
                            $form->field($model, 'no_attend_time')->widget(TimePicker::classname(), [
                                'pluginOptions' => [
                                    'minuteStep' => 1,
                                    'defaultTime' => '23:50',
                                    'showMeridian' => false
                                ],
                            ]);
                            ?>
                        </div>
                    </div>
                </div>

            </div>
            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
