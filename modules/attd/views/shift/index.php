<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\attd\models\ShiftSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'กำหนดเวลาเข้างาน';
$this->params['breadcrumbs'][] = $this->title;
?>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="text-right">
        <?= Html::a('สร้างโปรไฟล', ['create'], ['class' => 'btn btn-success']) ?>

    </div>
<br>

    <?php
    $model =\app\modules\attd\models\Shift::find()->all();
    ?>
        <div class="row">

            <?php
            foreach ($model as $m):
            ?>
            <div class="col-md-4">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 class="panel-title"><?=$m->name?></h3>
                            </div>
                            <div class="col-md-6 text-right">
                                <span>
                                    <?= Html::a('<i class="glyphicon glyphicon-pencil"></i>', ['update', 'id' => $m->id], [
                                        'class' => 'text-white',
                                    ]) ?>
                                    <?php if($m->id != 1):?>
                                    <?= Html::a('<i class="glyphicon glyphicon-trash"></i>', ['delete', 'id' => $m->id], [
                                        'class' => 'text-white',
                                        'data' => [
                                            'confirm' => 'Are you sure you want to delete this item?',
                                            'method' => 'post',
                                        ],
                                    ]) ?>
                                     <?php endif;?>
                                    </span>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <th class="text-warning">เวลามาสาย</th>
                                <td><?=$m->come_late_time?></td>
                            </tr>
                            <tr>
                                <th class="text-info">เวลาออกก่อน</th>
                                <td><?=$m->back_early_time?></td>
                            </tr>
                            <tr>
                                <th class="text-danger">เวลาขาดงาน</th>
                                <td><?=$m->no_attend_time?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <?php endforeach;?>
        </div>

    <?php   GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            'come_late_time',
            'back_early_time',
            'no_attend_time',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
