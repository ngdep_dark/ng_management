<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\modules\attd\models\AttendanceServiceType;
use kartik\daterange\DateRangePicker;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\attd\models\AttendanceOutsideService */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="attendance-outside-service-form">
    <?php $form = ActiveForm::begin(); ?>
    <?php if ($model->isNewRecord) : ?>
        <div class="form-group">
            <?php echo '<label class="control-label">เลือกพนักงาน</label>';
            echo Select2::widget([
                'name' => 'user_id',
                'data' => ArrayHelper::map(User::find()->where(['status' => 10])->orderBy(['id' => SORT_ASC])->all(), 'id', 'full_name'),
                'value' => Yii::$app->user->id,
                'options' => [
                    'placeholder' => '',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]);
            ?>
        </div>

    <?php else: ?>
        <div class="form-group">
            <?php echo '<label class="control-label">เลือกพนักงาน</label>';
            echo Select2::widget([
                'name' => 'user_id',
                'data' => ArrayHelper::map(User::find()->where(['status' => 10])->orderBy(['id' => SORT_ASC])->all(), 'id', 'full_name'),
                'value' => $model->user_id,
                'options' => [
                    'placeholder' => '',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]);
            ?>
        </div>

    <?php endif; ?>

    <div class="form-group">
        <?php echo '<label class="control-label">ประเภทการทำงาน</label>';
        echo Select2::widget([
            'name' => 'serviceType',
            'data' => ArrayHelper::map(AttendanceServiceType::find()->orderBy(['id' => SORT_ASC])->all(), 'id', 'type_name'),
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]);
        ?>
    </div>

    <div class="form-group">
        <?= $form->field($model, 'outside_province')->textInput() ?>

    </div>

    <?php if ($model->isNewRecord): ?>
        <div class="form-group">
            <?php echo '<label class="control-label">วันที่ไป / วันที่กลับ</label>';
            echo DateRangePicker::widget([
                'name' => 'date_range',
                'hideInput' => false
            ]);
            ?>
        </div>
    <?php else: ?>
        <div class="form-group">
            <?php echo '<label class="control-label">วันที่ไป / วันที่กลับ</label>';
            echo DatePicker::widget([
                'name' => 'date_range',
                'value' => $model->outside_date_start,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]);
            ?>
        </div>
    <?php endif; ?>
    <div class="form-group">
        <?= $form->field($model, 'detail')->textInput(['maxlength' => true])->textarea(['row' => '2']) ?>

    </div>

    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>


    <?php ActiveForm::end(); ?>


</div>

