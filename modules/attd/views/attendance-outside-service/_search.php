<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\attd\models\AttendanceOutsideServiceSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="attendance-outside-service-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'outside_date_start') ?>

    <?= $form->field($model, 'outside_date_end') ?>

    <?= $form->field($model, 'outside_type') ?>

    <?= $form->field($model, 'outside_province') ?>

    <?php // echo $form->field($model, 'detail') ?>

    <?php // echo $form->field($model, 'passenger') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
