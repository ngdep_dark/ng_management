<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\attd\models\AttendanceOutsideService */

$this->title = $model->typeName;
$this->params['breadcrumbs'][] = ['label' => 'Attendance Outside Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attendance-outside-service-view">

    <h3><?= Html::encode( 'งาน' . $model->typeName . ' ที่' . $model->outside_province . ' ระหว่างวันที่ ' . $model->outside_date_start) ?></h3>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title text-left">รายระเอียด</h3>
        </div>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'outside_date_start',
                    'value' => function ($model) {
                        return Yii::$app->datetimeManage->dtFormat($model->outside_date_start);
                    }
                ],

                [
                    'attribute' => 'outside_type',
                    'value' => function ($model) {
                        return $model->typeName;
                    }
                ],

                'outside_province',
                'detail',
            ],
        ]) ?>
    </div>

</div>
