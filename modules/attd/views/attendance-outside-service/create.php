<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\attd\models\AttendanceOutsideService */

$this->title = 'สร้างการทำงานนอกสถานที่';
$this->params['breadcrumbs'][] = ['label' => 'Attendance Outside Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attendance-outside-service-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
