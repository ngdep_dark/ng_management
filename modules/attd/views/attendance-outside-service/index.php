<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\attd\models\AttendanceOutsideServiceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ทำงานนอกสถานที่';
$this->params['breadcrumbs'][] = $this->title;

function approve($id)
{
    return \app\modules\attd\models\AttendanceOutsideService::findOne($id);
}
?>
<div class="attendance-outside-service-index">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('เพิ่มรายการ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'user_id',
                'label' => 'ชื่อพนักงาน',
                'value' => function ($model) {
                    return $model->userName;
                }
            ],
            'outside_date_start',
            [
                'attribute' => 'outside_type',
                'label' => 'ประเภทงาน',
                'value' => function($model){
                    return $model->typeName;
                }
            ],
            'outside_province',
            'detail',
            [
                'attribute' => 'status',
                'label' => 'ผู้บันทึก',
                'value' => function ($model) {
                    return $model->approve;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
