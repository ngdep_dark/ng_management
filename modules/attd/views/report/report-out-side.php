<?php

//use yii\grid\GridView;
use yii\helpers\Html;
use app\modules\attd\models\AttendanceOutsideService;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use kartik\grid\GridView;

/**
 * Created by PhpStorm.
 * User: magimari
 * Date: 30/8/2561
 * Time: 11:25 น.
 */

$this->title = "รายงานสรุปการลา";
$this->params['breadcrumbs'][] = $this->title;

$post = Yii::$app->request->post();

$holidayLeave = 7;
$sickLeave = 30;
$errandLeave = 7;

$this->registerCss("
    .style-td{
        font-size: 16px; 
        line-height: 1cm;
    }
");

$post = Yii::$app->request->post();
$user = ArrayHelper::map(\app\models\User::find()->orderBy(['id' => SORT_ASC])->all(), 'id', 'full_name');
$leave_tpye = ArrayHelper::map(\app\modules\attd\models\LeaveType::find()->orderBy(['id' => SORT_ASC]) ->all(), 'id', 'leave_type_name');
if ($post) {
    $date_data = $post['date_range'];
}

function type($id)
{
    return AttendanceOutsideService::findOne($id);
}


?>
<?php Pjax::begin(); ?>
<?php $form = ActiveForm::begin(); ?>

<div class="row">
    <div class="col-md-3">
        <?php echo '<label class="control-label">ชื่อพนักงาน</label>';
        echo Select2::widget([
            'name' => 'user_id',
            'data' => $user,
            'value' => $post ? $user : null ,
            'options' => ['placeholder' => 'Select a Username',],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]);
        ?>
    </div>

    <div class="col-md-3">
        <?php echo '<label class="control-label">วันที่ต้องการค้นหา</label>';
        echo DateRangePicker::widget([
            'name' => 'date_range',
            'data' => date('Y-m-d') . " - " . date('Y-m-d'),
            'presetDropdown' => true,
            'hideInput' => true
        ]);
        ?>
    </div>

    <div class="col-md-1">
        <br>
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    </div>

    <div class="form-group">
        <?php
        $post = Yii::$app->request->post();
        if (!empty($post)) :
            $date_range = explode(' - ', $post['date_range']);
            ?>
            <div style="float: right; padding-top: 20px; padding-right: 30px">
                <?= Html::a('Generate PDF', ['report/reportpdf-out-side', 'id' => $post['user_id'], 'start' => $date_range[0], 'end' => $date_range[1]], ['target' => '_blank', 'data-pjax' => "0", 'class' => 'btn btn-success']) ?>
            </div>
        <?php endif; ?>
    </div>


</div>

<?php $form = ActiveForm::end(); ?>

<br>

<?php
$post = Yii::$app->request->post();
if ($post) : ?>
    <div class="row">
        <div class="col-md-12">
            <?php echo
            GridView::widget([
                'dataProvider' => $dataProvider,
                'responsive' => true,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'user_id',
                        'label' => 'ชื่อผู้ใช้งาน',
                        'value' =>  function($model){
                            $typeObj = \type($model['id']);
                            return $typeObj->userName;
                        }
                    ],
                    [
                        'attribute' => 'outside_date_start',
                        'label' => 'วันที่',
                    ],
                    [
                        'attribute' => 'outside_type',
                        'label' => 'ประเภท',
                        'value' => function ($model) {
                            $typeObj = \type($model['id']);
                            return $typeObj->typeName;
                        }
                    ],
                    [
                        'attribute' => 'outside_province',
                        'label' => 'จังหวัดที่ไป',
                    ],
                    [
                        'attribute' => 'detail',
                        'label' => 'รายละเอียด',
                    ],
                    [
                        'attribute' => 'status',
                        'label' => 'ผู้บันทึก',
                        'value' => function ($model) {
                            $typeObj = \type($model['id']);
                            return $typeObj->approve;
                        }
                    ],

                ],
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => 'รายการทำงานต่างจังหวัด'
                ],
            ])
            ?>
        </div>
    </div>



<?php endif; ?>


<?php Pjax::end(); ?>


