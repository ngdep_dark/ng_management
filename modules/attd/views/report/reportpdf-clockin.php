<?php

use yii\helpers\Html;
use yii\db\Query;

$this->title = 'รายงานการลงเวลาเข้า-ออกรายบุคคล';

$end_time ='';
$i = 0;
?>

<div class="container">
    <h1 style="font-weight: bold"><?= Html::encode($this->title) ?></h1>
</div>
<!---->


<div>
    <p style="font-size: 28px; font-weight: bold">การลงเวลาเข้า - ออก ตามช่วงเวลาแบบรายบุคคล</p>
    <table class="table table-bordered">
        <tr>
            <th width="70">Day</th>
            <th width="85">Date</th>
            <th style="width: 80px">User Name</th>
            <th style="width: 90px">IN</th>
            <th style="width: 90px">OUT</th>
            <th width="70">Total work</th>
            <th style="width: 80px">Status</th>
            <th>Note</th>
            <!--            <th style="width: 50px;">Result time</th>-->
        </tr>
        <?php foreach ($selectday as $value) : ?>
        <?php
            $query = (new Query())
                ->select('"user".id, "user".username, "user".role_shift, "user".full_name,
                  "attendance".att_date, "attendance".att_check, "attendance".att_checkout,"attendance".overtime, "attendance".att_status, "attendance".leave_id,
                   "shift".come_late_time, "shift".back_early_time, "shift".no_attend_time , "holiday".holiday_name, "holiday".holiday_date')
                ->from('user')
                ->leftJoin('attendance', '"user".id="attendance".user_id')
                ->leftJoin('shift', '"user".role_shift="shift".id')
                ->leftJoin('holiday', '"holiday".holiday_date = "attendance".att_date')
                ->where(['user_id' => $id ])
                ->andWhere(['att_date' => $value])
                ->orderBy(['att_date' => SORT_ASC])
                ->one();

            $day = date("l", strtotime($value));
            $holiday = \app\modules\attd\models\Holiday::find()->where(['holiday_date' => $value])->one();
            ?>
        <?php if (!empty($holiday)): ?>
        <tr>
            <td><?= Yii::$app->datetimeManage->dateName($day); ?></td>
            <td><?= Yii::$app->datetimeManage->dtFormat($holiday['holiday_date'],true); ?></td>
            <td style="font-weight: bold"><?= $holiday['holiday_name'] ?></td>
            <td style="font-weight: bold"><?= $holiday['description'] ?></td>
            <td></td>
            <td><span class="label label-default" style="font-size: 13px">วันหยุด</span></td>
        </tr>
        <?php else: ?>
        <tr>
            <td><?= Yii::$app->datetimeManage->dateName($day) ?></td>
            <td><?= Yii::$app->datetimeManage->dtFormat($value,true); ?></td>
            <td><?= $query['full_name']?></td>
            <td><?= substr($query['att_check'], 11) ?></td>
            <td><?= substr($query['att_checkout'], 11) ?></td>
            <td>
                <?php
                        $time1 = substr($query['att_check'], 11, 5);
                        $time2 = substr($query['att_checkout'], 11, 5);
                        $diffTime = differenceInHours($time1, $time2);
                        if ($query['att_date'] == date('Y-m-d')) {
                            echo '';
                        }
                        elseif (empty($time2)){
                            echo '';
                        }
                        elseif ($query['att_date'] < date('Y-m-d')) {
                            echo round($diffTime) - 1 . " ชั่วโมง ";
                        }
                        ?>
            </td>
            <?php if ($day == 'Saturday' || $day == 'Sunday'): ?>
            <td><span class="label label-primary" style="font-size: 13px"><?= '' ?></span></td>
            <?php elseif ($query['att_status'] != null): ?>
            <td><span class="label label-info" style="font-size: 13px"><?= $query['att_status'] ?></span></td>
            <?php elseif ($query['att_check'] != null && substr($query['att_check'], 11) > $query['come_late_time']): ?>
            <td><span class="label-warning" style="font-size: 13px">สาย</span></td>
            <?php elseif ($query['att_check'] != null && substr($query['att_check'], 11) < $query['come_late_time']): ?>
            <td><span class="label label-success" style="font-size: 13px">ปกติ</span></td>
            <?php elseif ($query['att_check'] == null): ?>
            <td><span class="label label-danger" style="font-size: 13px">ไม่ได้สแกนนิ้ว/ขาดงาน</span></td>
            <?php endif; ?>
        </tr>
        <?php endif; ?>
        <?php endforeach; ?>
    </table>
</div>


<?php
function differenceInHours($startdate, $enddate)
{
    $starttimestamp = strtotime($startdate);
    $endtimestamp = strtotime($enddate);
    $difference = abs($endtimestamp - $starttimestamp) / 3600;
    return $difference;
}

if (!empty($_POST["submit"])) {
    $hours_difference = differenceInHours($_POST["startdate"], $_POST["enddate"]);
    $message = "The Difference is " . $hours_difference . " hours";
}
?>