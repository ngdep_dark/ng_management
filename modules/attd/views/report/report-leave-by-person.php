<?php

use yii\grid\GridView;
use yii\helpers\Html;
use app\modules\attd\models\Leave;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;

/**
 * Created by PhpStorm.
 * User: magimari
 * Date: 30/8/2561
 * Time: 11:25 น.
 */

$this->title = "รายงานการลาตามช่วงวันที่";
$this->params['breadcrumbs'][] = $this->title;

$post = Yii::$app->request->post();

$holidayLeave = 7;
$sickLeave = 30;
$errandLeave = 7;


$this->registerCss("
    .style-td{
        font-size: 16px; 
        line-height: 1cm;
    }
");

$user = ArrayHelper::map(\app\models\User::find()->orderBy(['id' => SORT_ASC])->all(), 'id', 'username');
if ($post = Yii::$app->request->post()) {
    $date_data = $post['date_range'];
}

function leave($id)
{
    return Leave::findOne($id);
}

function name($id)
{
    return \app\models\User::findOne($id);
}


?>
<?php Pjax::begin(); ?>
<?php $form = ActiveForm::begin(); ?>

<div class="row">

    <div class="col-md-4">
        <?php echo '<label class="control-label">Date Range</label>';
        echo DateRangePicker::widget([
            'name' => 'date_range',
            'data' => date('Y-m-d') . " - " . date('Y-m-d'),
            'presetDropdown' => true,
            'hideInput' => true
        ]);
        ?>
    </div>
    <div class="col-md-3">
        <br>
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    </div>


</div>

<?php $form = ActiveForm::end(); ?>

<br>

<?php
$post = Yii::$app->request->post();
if (!empty($post)) :
    $date_range = explode(' - ', $post['date_range']);
    ?>
    <div style="float: right; margin-right: 40px">
        <?= Html::a('Generate PDF', ['report/report-leave', 'start' => $date_range[0], 'end' => $date_range[1]], ['target' => '_blank', 'data-pjax' => "0", 'class' => 'btn btn-success']) ?>
    </div>
<?php endif; ?>
<div class="col-md-12">
    <?php echo
    GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'headerOptions' => ['style' => 'width: 50px;'],

            ],

            [
                'attribute' => 'user_id',
                'label' => 'User ID',
                'headerOptions' => ['style' => 'width: 70px;'],

            ],
            [
                'attribute' => 'username',
                'label' => 'ชื่อผู้ใช้งาน',
                'headerOptions' => ['style' => 'width: 170px;'],
                'value' => function ($model){
                    $name = \leave($model['id']);
                    return $name->userName;

                }

            ],
            [
                'attribute' => 'leave_type',
                'label' => 'ประเภทการลา',
                'headerOptions' => ['style' => 'width: 170px;'],
                'value' => function ($model) {
                    $levObj = \leave($model['id']);
                    return $model['half_time'] == 1 ? $levObj->typeName . " (ลาครึ่งวัน) จำนวน " . $model['leave_hours'] . " ชั่วโมง" : $levObj->typeName;
                }
            ],
            [
                'attribute' => 'leave_start',
                'label' => 'วันที่ลา',
                'headerOptions' => ['style' => 'width: 170px;'],
                'value' => function($model) {
                   return  Yii::$app->datetimeManage->dtFormat($model['leave_start']);
                }

            ],
            [
                'attribute' => 'description',
                'label' => 'สาเหตุการลา',
                'headerOptions' => ['style' => 'width: 170px;'],

            ],
            [
                'attribute' => 'approve_by',
                'label' => 'บันทึกโดย',
                'headerOptions' => ['style' => 'width: 170px;'],

            ]
        ]
    ])
    ?>
</div>

<?php //endif; ?>
<?php Pjax::end(); ?>
