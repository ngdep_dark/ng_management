<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'รายงานการทำงานล่วงเวลา ณ ช่วงวันที่'. Yii::$app->datetimeManage->dtFormat($date_start)  . " ถึงวันที่ " . Yii::$app->datetimeManage->dtFormat($date_end);
//$this->params['breadcrumbs'][] = ['label' => 'Attendances', 'url' => ['showclockin']];
//$this->params['breadcrumbs'][] = $this->title;
//$username = ArrayHelper::map(\app\models\User::find()->orderBy(['id' => SORT_ASC])->all(), 'id', 'username');

?>

<div class="container">
    <h3 style="font-weight: bold"><?= Html::encode($this->title) ?></h3>
</div>
<!---->


<div>
    <table class="table table-bordered">
        <tr>
            <th>Date</th>
            <th>เวลาเริ่ม</th>
            <th>เวลาสิ้นสุด</th>
            <th>รายละเอียด</th>
            <th>รวมระยะเวลาทั้งสิ้น</th>
            <th>บันทึกโดย</th>
        </tr>
        <?php foreach ($overtime_model as $value) : ?>
            <tr>
                <td style="width: 100px"><?= Yii::$app->datetimeManage->dtFormat($value['overtime_date']) ?></td>
                <td style="width: 100px"><?= $value['start_time'] ?></td>
                <td style="width: 100px"><?= $value['end_time'] ?></td>
                <td style="width: 200px"><?= $value['description'] ?></td>
                <td><?= floatval($value->ot_duration / 10) ?> ชั่วโมง</td>
                <td style="width: 80px"><?= $value['approve_by'] ?></td>
            </tr>
        <?php endforeach; ?>
    </table>


</div>


