<?php

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\helpers\Html;
use kartik\daterange\DateRangePicker;
use yii\widgets\Pjax;
use app\modules\attd\models\Attendance;
use yii\db\Query;


/**
 * Created by PhpStorm.
 * User: magimari
 * Date: 30/8/2561
 * Time: 11:24 น.
 */
$this->title = "รายงานการลงเวลารายบุคคลตามช่วงวันที่";
$this->params['breadcrumbs'][] = $this->title;

$this->registerCss("
    .style-inline{
        float: right;
    }
");

$user = ArrayHelper::map(\app\models\User::find()->where(['status' => 10])->orderBy(['id' => SORT_ASC])->all(), 'id', 'full_name');
if ($post = Yii::$app->request->post()) {
    $user_value = $post['user_id'];
    $date_data = $post['date_range'];
}

function userName($id)
{
    return Attendance::findOne($id);
}

$late = '09:00:01';

?>

<?php Pjax::begin() ?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-12">
        <div class="col-md-3">
            <?php echo '<label class="control-label">ชื่อพนักงาน</label>';
            echo Select2::widget([
                'name' => 'user_id',
                'data' => $user,
                'value' => !empty($user_value) ? $user_value : '',
                'options' => ['placeholder' => 'Select a Username',],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]);
            ?>
        </div>



        <div class="col-md-3">
            <?php echo '<label class="control-label">ช่วงเวลาที่ต้องการค้นหา</label>';
            echo DateRangePicker::widget([
                'name' => 'date_range',
                'data' => date('Y-m-d'),
                'presetDropdown' => true,
                'hideInput' => true
            ]);
            ?>
        </div>

        <div class="col-md-3">
            <br>
            <p style="padding-top: 5px;">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>

            </p>
        </div>

        <div class="col-md-3">
            <?php
            $post = Yii::$app->request->post();
            if (!empty($post)) :
                $date_range = explode(' - ', $post['date_range']);
                ?>
                <div style="float: right; padding-top: 20px">
                    <?= Html::a('Generate PDF', ['report/report-clockin', 'id' => $post['user_id'], 'start' => $date_range[0], 'end' => $date_range[1]], ['target' => '_blank', 'data-pjax' => "0", 'class' => 'btn btn-success']) ?>
                </div>
            <?php endif; ?>
        </div>

    </div>
</div>


<?php $form = ActiveForm::end(); ?>
<br/>
<?php
$post = Yii::$app->request->post();
if ($post) : ?>
    <table class="table table-bordered">
        <tr>
            <th>วัน</th>
            <th>วันที่</th>
            <th>ชื่อผู้สแกน</th>
            <th>เวลาสแกนเข้า</th>
            <th>เวลาสแกนออก</th>
            <th>รวม</th>
            <th>หมายเหตุ</th>
        </tr>
        <?php foreach ($SelectDay as $item):
            $query = (new Query())
                ->select('"user".id, "user".username, "user".role_shift, "user".full_name, "user".status,
                  "attendance".att_date, "attendance".att_check, "attendance".att_checkout,"attendance".overtime, "attendance".att_status, "attendance".leave_id,
                   "shift".come_late_time, "shift".back_early_time, "shift".no_attend_time , "holiday".holiday_name, "holiday".holiday_date')
                ->from('user')
                ->leftJoin('attendance', '"user".id="attendance".user_id')
                ->leftJoin('shift', '"user".role_shift="shift".id')
                ->leftJoin('holiday', '"holiday".holiday_date = "attendance".att_date')
                ->where(['user_id' => $post['user_id']])
                ->andWhere(['status' => 10])
                ->andWhere(['att_date' => $item])
                ->orderBy(['att_date' => SORT_ASC])
                ->one();

            $day = date("l", strtotime($item));
            $holiday = \app\modules\attd\models\Holiday::find()->where(['holiday_date' => $item])->one();
            ?>
            <?php if (!empty($holiday)): ?>
            <tr>
                <td><?= Yii::$app->datetimeManage->dateName($day) ?> </td>
                <td><?= Yii::$app->datetimeManage->dtFormat($holiday['holiday_date']); ?></td>
                <td style="font-weight: bold"><?= $holiday['holiday_name'] ?></td>
                <td style="font-weight: bold"><?= $holiday['description'] ?></td>
                <td></td>
                <td><span class="label label-default" style="font-size: 13px">วันหยุด</span></td>
            </tr>
        <?php else: ?>
            <tr>
                <td><?= Yii::$app->datetimeManage->dateName($day) ?> </td>
                <td><?= Yii::$app->datetimeManage->dtFormat($item); ?></td>
                <td><?= $query['full_name'] ?></td>
                <td><?= substr($query['att_check'], 11) ?></td>
                <td><?= substr($query['att_checkout'], 11) ?></td>
                <td>
                    <?php
                    $time1 = substr($query['att_check'], 11, 5);
                    $time2 = substr($query['att_checkout'], 11, 5);
                    $diffTime = differenceInHours($time1, $time2);
                    if ($query['att_date'] == date('Y-m-d')) {
                        echo '';
                    }
                    elseif (empty($time2)){
                        echo '';
                    }
                    elseif ($query['att_date'] < date('Y-m-d')) {
                        echo round($diffTime) - 1 . " ชั่วโมง ";
                    }
                    ?>
                </td>
                <?php if ($day == 'Saturday' || $day == 'Sunday'): ?>
                    <td><span class="label label-primary"
                              style="font-size: 13px"><?= '' ?></span></td>
                <?php elseif ($query['att_status'] != null): ?>
                    <td><span class="label label-info" style="font-size: 13px"><?= $query['att_status'] ?></span></td>
                <?php elseif ($query['att_check'] != null && substr($query['att_check'], 11) > $late): ?>
                    <td><span class="label label-warning" style="font-size: 13px">สาย</span></td>
                <?php elseif ($query['att_check'] != null && substr($query['att_check'], 11) < $late): ?>
                    <td><span class="label label-success" style="font-size: 13px">ปกติ</span></td>
                <?php elseif ($query['att_check'] == null): ?>
                    <td><span class="label label-danger" style="font-size: 13px">ไม่ได้สแกนนิ้ว/ขาดงาน</span></td>
                <?php endif; ?>
            </tr>
        <?php endif; ?>
        <?php endforeach;
        ?>
    </table>
<?php endif; ?>

<?php Pjax::end() ?>

<?php
function differenceInHours($startdate, $enddate)
{
    $starttimestamp = strtotime($startdate);
    $endtimestamp = strtotime($enddate);
    $difference = abs($endtimestamp - $starttimestamp) / 3600;
    return $difference;
}

if (!empty($_POST["submit"])) {
    $hours_difference = differenceInHours($_POST["startdate"], $_POST["enddate"]);
    $message = "The Difference is " . $hours_difference . " hours";
}
?>
