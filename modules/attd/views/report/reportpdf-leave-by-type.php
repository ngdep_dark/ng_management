<?php
use app\modules\attd\models\Leave;
/**
 * Created by PhpStorm.
 * User: akinochan
 * Date: 25/3/2562
 * Time: 16:34 น.
 */


$this->title = 'รายงานสรุปการลา';

function leaveType($id)
{
    return Leave::findOne($id);
}

?>

<h1>รายงานสรุปการลาตามประเภทและวันที่</h1>

<div>
    <table class="table table-bordered">
        <tr>
            <th>User Name</th>
            <th>ประเภทการลา</th>
            <th>วันที่ลา</th>
            <th>รายละเอียดการลา</th>
            <th>ผู้บันทึก</th>
        </tr>
        <?php foreach ($data as $value) : ?>
            <tr>
                <td style="width: 90px"><?= $value['username'] ?></td>
                <td style="width: 100px"><?= leaveType($value['id'])->typeName ?></td>
                <td style="width: 90px"><?= Yii::$app->datetimeManage->dtFormat($value['leave_start']) ?></td>
                <td style="width: 180px"><?= $value['description'] ?></td>
                <td style="width: 80px"><?= $value['approve_by'] ?></td>
            </tr>
        <?php endforeach;?>
    </table>

    <table class="table table-bordered">
        <tr>
            <td style="width: 100px; font-weight: bold">ประเภทการลา</td>
            <td><?= $type['leave_type_name'] ?></td>
            <td style="width: 100px; font-weight: bold">จำนวนวันที่ลาแล้ว</td>
            <td><?= $leave_count . ' วัน'?></td>
        </tr>
    </table>
</div>