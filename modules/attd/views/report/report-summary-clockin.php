<?php
/**
 * Created by PhpStorm.
 * User: shiranko
 * Date: 17/12/2561
 * Time: 15:18 น.
 */

use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use yii\helpers\Html;
use app\modules\attd\models\Holiday;
use \app\modules\attd\models\Shift;
use \app\modules\attd\models\Attendance;

$post = Yii::$app->request->post();

$this->registerCss("
table {
    width: 100%;
}

");

?>


<?php Pjax::begin() ?>

<?php $form = ActiveForm::begin(); ?>
<div class="row">
    <div class="col-md-4">
        <?php echo '<label class="control-label">ช่วงเวลาที่ต้องการค้นหา</label>';
        echo DateRangePicker::widget([
            'name' => 'date_range',
            'data' => date('Y-m-d'),
            'presetDropdown' => true,
            'hideInput' => true
        ]);
        ?>
    </div>
    <div class="col-md-4" style="padding-top: 5px">
        <br>
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    </div>

    <div class="col-md-4">
        <?php
        if (!empty($post)) :
            $date_range = explode(' - ', $post['date_range']);
            ?>
            <div style="float: right; padding-top: 20px">
                <?= Html::a('Generate PDF', ['report/pdf-summary-report', 'start' => $date_range[0], 'end' => $date_range[1]], ['target' => '_blank', 'data-pjax' => "0", 'class' => 'btn btn-success']) ?>
            </div>
        <?php endif; ?>
    </div>
</div>
<?php $form = ActiveForm::end(); ?>

<?php Pjax::end() ?>
<br/>

<?php if ($post):
    $date_range = explode(' - ', $post['date_range']);
    ?>
    <div class="row" style="overflow-x: auto">
        <div class="col-md-12">
            <table class="table table-striped">
                <tr>
                    <th rowspan="2" style="width: 100px">ชื่อ\วันที่</th>
                    <th colspan="5" style="width: 40px; text-align: center">สถิติการลงเวลา</th>
                    <?php foreach ($date as $item):
                        $day = date("l", strtotime($item));
                        $holiday = \app\modules\attd\models\Holiday::find()->where(['holiday_date' => $item])->one();
                        $scalar = \app\modules\attd\models\Attendance::find()->select('att_date')->where(['att_date' => $item])
                            ->andWhere(['not', ['att_check' => null]])->scalar();
                        $fullDay = date('l', strtotime($scalar));
                        ?>
                        <?php if ($day == 'Saturday' || $day == 'Sunday'): ?>
                        <?php if ($fullDay == 'Saturday' || $fullDay == 'Sunday') : ?>
                            <th style="width: 40px"><?= date("d/m", strtotime($item)); ?></th>
                        <?php endif; ?>
                    <?php elseif (!empty($holiday)) : ?>
                    <?php else: ?>
                        <th style="width: 40px"><?= date("d/m", strtotime($item)); ?></th>
                    <?php endif; ?>
                    <?php endforeach; ?>
                </tr>
                <tr>
                    <th style="width: 40px">ปกติ</th>
                    <th style="width: 40px">สาย</th>
                    <th style="width: 40px">ขาด</th>
                    <th style="width: 40px">ลา</th>
                    <th style="width: 40px">รวม</th>
                    <?php foreach ($date as $item):
                        $day = date("l", strtotime($item));
                        $holiday = Holiday::find()->where(['holiday_date' => $item])->one();
                        $scalar = Attendance::find()->select('att_date')->where(['att_date' => $item])
                            ->andWhere(['not', ['att_check' => null]])->scalar();
                        $fullDay = date('l', strtotime($scalar));
                        ?>
                        <?php if ($day == 'Saturday' || $day == 'Sunday'): ?>
                        <?php if ($fullDay == 'Saturday' || $fullDay == 'Sunday') : ?>
                            <th style="width: 40px"><?= Yii::$app->datetimeManage->dateName($fullDay); ?></th>
                        <?php endif; ?>
                    <?php elseif (!empty($holiday)) : ?>
                    <?php else: ?>
                        <th style="width: 40px"><?= Yii::$app->datetimeManage->dateName($day); ?></th>
                    <?php endif; ?>
                    <?php endforeach;
                    ?>
                </tr>
                <?php
                foreach ($get_user as $value):
                    $a = 0;
                    $l = 0;
                    $n = 0;
                    $leave = \app\modules\attd\models\Leave::find()
                        ->where(['user_id' => $value['id']])->andWhere(['approve_status' => 2])
                        ->andWhere(['between', 'leave_start', $date_range[0], $date_range[1]])->all();
                    $leave_count = count($leave);

                    $check = \app\modules\attd\models\Attendance::find()
                        ->where(['user_id' => $value['id']])
                        ->andWhere(['between', 'att_date', $date_range[0], $date_range[1]])
                        ->andWhere(['not', ['att_check' => null]])
                        ->andWhere(['leave_id' => null])
                        ->all();
                    $check_count = count($check);

                    foreach ($date as $dates) {
                        $day = date("l", strtotime($dates));
                        $holiday = \app\modules\attd\models\Holiday::find()->where(['holiday_date' => $dates])->one();
                        if ($day == 'Saturday' || $day == 'Sunday') {
                        } elseif (!empty($holiday)) {

                        } else {
                            $shift = \app\modules\attd\models\Shift::find()->where(['id' => $value['role_shift']])->one();

                            $no_check = \app\modules\attd\models\Attendance::find()
                                ->where(['user_id' => $value['id']])
                                ->andWhere(['att_date' => $dates])
                                ->all();
                            $no_check_count = count($no_check);

                            $att_check = \app\modules\attd\models\Attendance::find()
                                ->where(['user_id' => $value['id']])
                                ->andWhere(['att_date' => $dates])
                                ->one();

                            if ($no_check_count == 0) {
                                if ($dates < date('Y-m-d')) {
                                    $a = $a + 1;
                                } elseif ($dates == date('Y-m-d') && $att_check['att_check'] == null) {
                                    $a = $a + 0;
                                }
                            }

                            if (substr($att_check['att_check'], '11') > $shift['come_late_time']
                                && substr($att_check['att_check'], '11') != null && $att_check['att_status'] == null && substr($att_check['att_check'], '11' ) != date('Y-m-d')) {
                                $l = $l + 1;
                            }elseif (substr($att_check['att_check'], '11') > $shift['come_late_time']
                                && substr($att_check['att_check'], '11') != null && $att_check['att_status'] == null && substr($att_check['att_check'], '11' ) == date('Y-m-d')) {
                                $l = $l + 0;
                            }if (substr($att_check['att_check'], '11') < $shift['come_late_time']
                                && substr($att_check['att_check'], '11') != null && $att_check['att_status'] == null) {
                                $n = $n + 1;
                            }
                        }
                    }
                    ?>
                    <tr>
                        <td style="font-weight: bold"><?= $value['username'] ?></td>
                        <td style="font-weight: bold"><?= $n ?></td>
                        <td style="font-weight: bold"><?= $l ?></td>
                        <td style="font-weight: bold"><?= $a ?></td>
                        <td style="font-weight: bold"><?= $leave_count ?></td>
                        <td style="font-weight: bold"><?= $n + $l + $a + $leave_count ?></td>
                        <?php foreach ($date as $val): ?>
                            <?php
                            $attendance = \app\modules\attd\models\Attendance::find()
                                ->where(['user_id' => $value['id']])
                                ->andWhere(['att_date' => $val])
                                ->orderBy(['att_date' => 4])
                                ->one();
                            $day = date("l", strtotime($val));
                            $holiday = Holiday::find()->where(['holiday_date' => $val])->one();
                            $shift = Shift::find()->where(['id' => $value['role_shift']])->one();
                            $scalar = \app\modules\attd\models\Attendance::find()->select('att_date')
                                ->where(['att_date' => $val])
                                ->andWhere(['not', ['att_check' => null]])
                                ->scalar();
                            $fullDay = date('l', strtotime($scalar));

                            ?>
                            <?php if ($day == 'Saturday' || $day == 'Sunday'): ?>
                                <?php if ($fullDay == 'Saturday' || $fullDay == 'Sunday') : ?>
                                    <td style="width: 40px">
                                        <?php if ($attendance['att_check'] != null && $attendance['att_checkout'] != null): ?>
                                            <span class="label label-info"><?= 'เข้า: ' . substr($attendance['att_check'], 11, 5) ?>  </span>
                                            <br>
                                            <span class="label label-info"><?= 'ออก: ' . substr($attendance['att_checkout'], 11, 5) ?>  </span>
                                            <br>
                                            <?php $difftime = differenceInHours($attendance['att_check'], $attendance['att_checkout']) ?>
                                            <span class="label label-default"
                                                  style="text-align: center">รวม: <?= substr(round($difftime), 0, 3) - 1 . ' ชั่วโมง' ?></span>
                                        <?php elseif ($attendance['att_check'] != null && $attendance['att_checkout'] == null): ?>
                                            <span class="label label-info"><?= 'เข้า: ' . substr($attendance['att_check'], 11, 5) ?>  </span>
                                        <?php else: ?>
                                            <span></span>
                                        <?php endif; ?>
                                    </td>
                                <?php elseif ($fullDay == 'Saturday' || $fullDay == 'Sunday' && $attendance['att_check'] == null): ?>
                                    <td></td>
                                <?php endif; ?>
                            <?php elseif (!empty($holiday)) : ?>

                            <?php elseif ($value['id'] == $attendance['user_id'] && $val == substr($attendance['att_date'], '0', '10')): ?>
                                <?php if (substr($attendance['att_check'], '11') >= $shift['come_late_time']): ?>
                                    <?php if (!empty($attendance['att_status'])): ?>
                                        <td>
                                            <span class="label label-info"><?= wordwrap($attendance['att_status'], 10, "<br />\n")  ?> </span>
                                        </td>
                                    <?php else: ?>
                                        <td>
                                        <span class="label label-warning">เข้า: <?= substr($attendance['att_check'], 11, 5) ?>  </span>
                                        <br/>
                                        <?php if (empty($attendance['att_checkout']) && $attendance['att_date'] == date('Y-m-d')): ?>
                                        <?php elseif (empty($attendance['att_checkout']) && $attendance['att_date'] < date('Y-m-d')): ?>
                                            <span class="label label-warning">ออก: <?= substr($shift['back_early_time'], 0, 5) ?> </span>
                                            <br/>
                                            <?php $difftime = differenceInHours($attendance['att_check'], substr($attendance['att_check'], 0, 10) . ' ' . $shift['back_early_time']) ?>
                                            <span class="label label-default"
                                                  style="text-align: center">รวม: <?= substr(round($difftime), 0, 3) - 1 . ' ชั่วโมง' ?></span>

                                        <?php else: ?>
                                            <span class="label label-warning">ออก: <?= substr($attendance['att_checkout'], 11, 5) ?> </span>
                                            <br/>
                                            <?php $difftime = differenceInHours($attendance['att_check'], $attendance['att_checkout']) ?>
                                            <span class="label label-default"
                                                  style="text-align: center">รวม: <?= substr(round($difftime), 0, 3) - 1 . ' ชั่วโมง' ?></span>
                                            </td>
                                        <?php endif; ?>
                                    <?php endif; ?>

                                <?php elseif (substr($attendance['att_check'], '11') <= $shift['come_late_time']): ?>
                                    <?php if (!empty($attendance['att_status'])): ?>
                                        <td>
                                            <span class="label label-info"><?= wordwrap($attendance['att_status'], 10, "<br />\n") ?></span>
                                        </td>
                                    <?php else: ?>
                                        <td>
                                            <span class="label label-success">เข้า: <?= substr($attendance['att_check'], 11, 5) ?></span><br/>
                                            <?php if (empty($attendance['att_checkout']) && $attendance['att_date'] == date('Y-m-d')): ?>
                                            <?php elseif (empty($attendance['att_checkout']) && $attendance['att_date'] < date('Y-m-d')): ?>
                                                <span class="label label-success">ออก: <?= substr($shift['back_early_time'], 0, 5) ?> </span>
                                                <br/>
                                                <?php $difftime = differenceInHours($attendance['att_check'], substr($attendance['att_check'], 0, 10) . ' ' . $shift['back_early_time']) ?>
                                                <span class="label label-default"
                                                      style="text-align: center">รวม: <?= substr(round($difftime), 0, 3) - 1 . ' ชั่วโมง' ?></span>
                                            <?php else: ?>
                                                <span class="label label-success">ออก: <?= substr($attendance['att_checkout'], 11, 5) ?> </span>
                                                <br/>
                                                <?php $difftime = differenceInHours($attendance['att_check'], $attendance['att_checkout']) ?>
                                                <span class="label label-default"
                                                      style="text-align: center">รวม: <?= substr(round($difftime), 0, 3) - 1 . ' ชั่วโมง' ?></span>
                                            <?php endif; ?>

                                        </td>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php else: ?>
                                <?php if ($val < date('Y-m-d')): ?>
                                    <td><span class="label label-danger" style="font-size: 12px">ขาดงาน</span></td>
                                <?php elseif ($val == date('Y-m-d') && $attendance['att_check'] == null): ?>
                                    <td><span class="label label-default" style="font-size: 12px">Not Scan</span></td>
                                <?php elseif ($val > date('Y-m-d')): ?>
                                    <td><span style="font-size: 12px"> </span></td>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endforeach;
                        ?>
                    </tr>
                <?php endforeach;
                ?>
            </table>
        </div>
    </div>
<?php endif; ?>
<?php

function differenceInHours($startdate, $enddate)
{
    $starttimestamp = strtotime($startdate);
    $endtimestamp = strtotime($enddate);
    $difference = abs($endtimestamp - $starttimestamp) / 3600;
    return $difference;
}

if (!empty($_POST["submit"])) {
    $hours_difference = differenceInHours($_POST["startdate"], $_POST["enddate"]);
    $message = "The Difference is " . $hours_difference . " hours";
}
?>

