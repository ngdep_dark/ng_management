<?php

use yii\helpers\Html;
use app\modules\attd\models\Leave;

//var_dump($start_date);
//exit();
//$this->title = 'รายงานการลาตามช่วงวันที่'. Yii::$app->datetimeManage->dtFormat($start_date). "ถึง" . Yii::$app->datetimeManage->dtFormat($end_date);

function leaveType($id)
{
    return Leave::findOne($id);
}

?>

<h2>
    <strong><?php echo 'รายงานการลาตามช่วงวันที่'. Yii::$app->datetimeManage->dtFormat($start_date). "ถึง" . Yii::$app->datetimeManage->dtFormat($end_date); ?></strong>
</h2>

<div class="container">
    <h1 style="font-weight: bold"><?= Html::encode($this->title) ?></h1>
</div>

<div>
    <table class="table table-bordered">
        <tr>
            <th>User ID</th>
            <th>User Name</th>
            <th>ประเภทการลา</th>
            <th>วันที่ลา</th>
            <th>รายละเอียดการลา</th>
            <th>อนุมัติโดย</th>
        </tr>
        <?php foreach ($model as $value) :
            $data = leaveType($value->id) ?>
            <tr>
                <td style="width: 60px"><?= $value['user_id'] ?></td>
                <td style="width: 90px"><?= $data->userName ?></td>
                <td style="width: 170px"><?= $value['half_time'] == 1 ? $data->typeName . " (" . $data->HalftimeStatus($value->half_time) . ")" . " จำนวน" . $value->leave_hours . " ชั่วโมง": leaveType($value->id)->typeName ?></td>
                <td style="width: 90px"><?= Yii::$app->datetimeManage->dtFormat($value['leave_start']) ?></td>
                <td style="width: 80px"><?= $value['description'] ?></td>
                <td style="width: 80px"><?= $value['approve_by'] ?></td>
            </tr>
        <?php endforeach;?>
    </table>
</div>


