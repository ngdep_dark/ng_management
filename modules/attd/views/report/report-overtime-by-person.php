<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;

$this->title = "รายงานการทำงานล่วงเวลา";
$this->params['breadcrumbs'][] = $this->title;

$post = Yii::$app->request->post();

$this->registerCss("
    .style-td{
        font-size: 16px; 
        line-height: 1cm;
    }
");

$user = ArrayHelper::map(\app\models\User::find()->orderBy(['id' => SORT_ASC])->all(), 'id', 'full_name');
$username = ArrayHelper::map(\app\models\User::find()->orderBy(['id' => SORT_ASC])->all(), 'id', 'username');
if ($post = Yii::$app->request->post()) {
    $user_value = $post['user_id'];
    $date_data = $post['date_range'];
}
//
//foreach ($overtime_person as $item) {
//    var_dump($item['overtime_date']);
//}
//exit();

?>
<?php Pjax::begin(); ?>

<?php ActiveForm::begin(['options' => []]); ?>
    <div class="row">
        <div class="col-md-3">
            <?php echo '<label class="control-label">Username</label>';
            echo Select2::widget([
                'name' => 'user_id',
                'data' => $user,
                'value' => !empty($user_value) ? $user_value : '',
                'options' => ['placeholder' => 'Select a Username',],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]);
            ?>
        </div>
        <div class="col-md-3">
            <?php echo '<label class="control-label">Date Range</label>';
            echo DateRangePicker::widget([
                'name' => 'date_range',
                'value' => !empty($date_data) ? $date_data : date('Y-m-d') . " - " . date('Y-m-d'),
                'presetDropdown' => true,
                'hideInput' => true
            ]);
            ?>
        </div>
        <div class="col-md-3">
            <br>
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        </div>
        <div class="col-md-3">
            <?php
            $post = Yii::$app->request->post();
            if (!empty($post)) :
                $date_range = explode(' - ', $post['date_range']);
                ?>
                <div style="float: right">
                    <?= Html::a('Generate PDF', ['report/report-overtime', 'id' => $post['user_id'], 'start' => $date_range[0], 'end' => $date_range[1]], ['target' => '_blank', 'data-pjax'=>"0" ,'class' => 'btn btn-success']) ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
<?php ActiveForm::end(); ?>

    <br>
<?php if ($post): ?>
    <?php if (!empty($overtime_person)): ?>
        <div class="panel-group" id="accordion">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion">

                            <strong><?= "การทำงานล่วงเวลา" ?></strong>
                        </a>
                    </h4>
                </div>
                <div id="collapse1" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <table class="table table-hover">
                            <tr>
                                <th>#</th>
                                <th>วันที่ทำ</th>
                                <th>เวลาเริ่ม</th>
                                <th>เวลาสิ้นสุด</th>
                                <th>หมายเหตุ</th>
                                <th>ชั่วโมงรวม</th>
                                <th>ผู้บันทึก</th>
                            </tr>
                            <?php foreach ($overtime_person as $value): ?>
                                <tr>
                                    <td></td>
                                    <td><?= Yii::$app->datetimeManage->dtFormat($value->overtime_date) ?></td>
                                    <td><?= $value->start_time ?></td>
                                    <td><?= $value->end_time ?></td>
                                    <td><?= $value->description ?></td>
                                    <td><?= floatval($value->ot_duration / 10) ?></td>
                                    <td><?= $value->approve_by ?></td>
                                </tr>
                            <?php endforeach; ?>

                        </table>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="panel-group" id="accordion">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion">
                            <strong>สรุปการทำงานล่วงเวลา</strong>
                        </a>
                    </h4>
                </div>
                <div id="collapse1" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <table class="table table-hover">
                            <tr>
                                <th>#</th>
                                <th>รวมชั่วโมงการทำงานทั้งหมด</th>
                            </tr>
                            <tr>
                                <td></td>
                                <td style="text-align: left"><?php
                                    $sum = 0;
                                    foreach ($overtime_person as $val => $item) {
                                        $sum += $item->ot_duration;
                                    }
                                    echo floatval($sum / 10) . " ชั่วโมง "
                                    ?></td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    <?php endif; ?>
<?php endif; ?>

<?php Pjax::end(); ?>