<?php
/**
 * Created by PhpStorm.
 * User: akinochan
 * Date: 26/3/2562
 * Time: 11:36 น.
 */

//$this->title = 'รายงานสรุปการลา';

function username($id)
{
    return \app\modules\attd\models\AttendanceOutsideService::findOne($id);
}
?>
<h1><?= $title ?></h1>

<div>
    <table class="table table-bordered">
        <tr>
            <th>User Name</th>
            <th>วันที่</th>
            <th>ประเภท</th>
            <th>สถานที่</th>
            <th>รายละเอียด</th>
            <th>ผู้บันทึก</th>
        </tr>
        <?php foreach ($data as $value) : ?>
            <tr>
                <td style="width: 80px"><?= username($value['id'])->userName ?></td>
                <td style="width: 90px"><?= Yii::$app->datetimeManage->dtFormat($value['outside_date_start']) ?></td>
                <td style="width: 80px"><?= username($value['id'])->typeName ?></td>
                <td style="width: 120px"><?= $value['outside_province'] ?></td>
                <td style="width: 180px"><?= $value['detail'] ?></td>
                <td style="width: 60px"><?= username($value['id'])->approve ?></td>
            </tr>
        <?php endforeach;?>
    </table>
</div>