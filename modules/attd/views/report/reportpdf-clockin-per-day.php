<?php

/**
 * Created by PhpStorm.
 * User: shiranko
 * Date: 14/12/2561
 * Time: 18:34 น.
 */

use yii\helpers\Html;
use yii\db\Query;

$this->title = 'รายงานการลงเวลาเข้า-ออกรายวัน';

$end_time ='';
$i = 0;

//var_dump($timescan);
//exit();
?>

<div class="container">
    <h1 style="font-weight: bold"><?= Html::encode($this->title) ?></h1>
</div>
<!---->

<div>
    <h2>รายงานการสแกนนิ้วรายวัน</h2>
    <table class="table table-bordered">
        <tr>
            <th>วันที่</th>
            <th>ชื่อผู้ใช้งาน</th>
            <th>เวลาเข้า</th>
            <th>เวลาออก</th>
        </tr>
        <?php foreach ($timescan as $item): ?>
        <tr>
            <td><?= $item['att_date'] ?></td>
            <td><?= $item['full_name'] ?></td>
            <td><?= $item['att_check'] ?></td>
            <td><?= $item['att_checkout'] ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
</div>
