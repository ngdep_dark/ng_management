<?php
/**
 * Created by PhpStorm.
 * User: shiranko
 * Date: 14/12/2561
 * Time: 14:39 น.
 */

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\daterange\DateRangePicker;
use yii\widgets\Pjax;
use kartik\widgets\DatePicker;
use yii\db\Query;


$this->title = "รายงานการลงเวลารายวัน";
$this->params['breadcrumbs'][] = $this->title;

$post = Yii::$app->request->post();
//var_dump($time);
//exit();
$i = 1;
$late = '09:00:01';
?>

<div class="row">
    <div class="col-md-3">
        <?php $form = ActiveForm::begin(); ?>
        <?php echo '<label class="control-label">เลือกวันที่</label>';
        echo DatePicker::widget([
            'name' => 'datetime',
            'value' => date('Y-m-d'),
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd'
            ]
        ]);
        ?>
    </div>
    <div class="col-md-3">
        <p style="padding-top: 23px"><?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?></p>
    </div>
    <div class="col-md-6">
        <?php if ($post): ?>
            <div style="float: right; padding-top: 22px">
                <?= Html::a('Generate PDF', ['report/reportpdf-clockin-per-day', 'date' => $post['datetime']], ['target' => '_blank', 'data-pjax' => "0", 'class' => 'btn btn-success']) ?>

            </div>
        <?php endif; ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<br/>
<div class="row">
    <?php if ($post): ?>
        <table class="table table-bordered">
            <tr>
                <th>ลำดับที่</th>
                <th>วันที่</th>
                <th>ชื่อผู้ใช้งาน</th>
                <th>เวลาเข้างาน</th>
                <th>เวลาเลิกงาน</th>
                <th>สถานะ</th>
            </tr>
            <?php foreach ($time as $item): ?>
                <?php
                $user_shift = \app\models\User::find()->select('role_shift')->where(['id' => $item['id']])->one();
                $role_shift = \app\modules\attd\models\Shift::find()->select('come_late_time')->where(['id' => $user_shift->role_shift])->one();
                ?>
                <tr>
                    <td><?= $i++ ?></td>
                    <td><?= $item['att_date'] ?></td>
                    <td><?= $item['full_name'] ?></td>
                    <td><?= $item['att_check'] ?> </td>
                    <td><?= $item['att_checkout'] ?> </td>
                    <?php if (substr($item['att_check'], '11') < $role_shift->come_late_time): ?>
                        <td><span class="label label-success">ปกติ</span></td>
                    <?php else: ?>
                        <td><span class="label label-warning">สาย</span></td>
                    <?php endif; ?>
                </tr>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
</div>
