<?php
/* @var $this yii\web\View */
$this->registerCss("
    .padding-border {
        padding: 10px 20px;
    }
");
$this->title = "Report";
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="report">
    <div class="clearfix">
        <div class="row">
            <div class="col-md-6">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="padding-border box-title text-olive"><i class="fa fa-file-pdf-o"></i> <small></small>รายงานการลงเวลา</h3>
                    </div>
                    <div class="box-body">
                        <div class="box-group">
                            <div class="row fontawesome-icon-list">
                                <div class="padding-border fa-hover col-md-6 col-sm-6 col-xs-12"><a href="/attd/report/report-clockin-by-person"><i class="fa fa-file-pdf-o"></i>รายงานการลงเวลารายบุคคลตามช่วงเวลา</a></div>
<!--                                <div class="padding-border fa-hover col-md-4 col-sm-4 col-xs-12"><a href="/attd/report/report-clockin-per-day"><i class="fa fa-file-pdf-o"></i> รายงานการลงเวลารายวัน</a></div>-->
                                <div class="padding-border fa-hover col-md-6 col-sm-6 col-xs-12"><a href="/attd/report/report-summary-clockin"><i class="fa fa-file-pdf-o"></i>รายงานสรุปผลการลงเวลาตามช่วงเวลา</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="padding-border box-title text-olive"><i class="fa fa-file-pdf-o"></i> <small></small>รายงานการลา</h3>
                    </div>
                    <div class="box-body">
                        <div class="box-group">
                            <div class="row fontawesome-icon-list">
                                <div class="padding-border fa-hover col-md-4 col-sm-4 col-xs-12"><a href="/attd/report/report-leave-by-person"><i class="fa fa-file-pdf-o"></i> รายงานการลาตามช่วงวันที่</a></div>
                                <div class="padding-border fa-hover col-md-4 col-sm-4 col-xs-12"><a href="/attd/report/report-leave-by-type"><i class="fa fa-file-pdf-o"></i> รายงานสรุปการลาตามประเภท</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <h3 class="padding-border box-title text-olive"><i class="fa fa-file-pdf-o"></i> <small></small>การทำงานนอกสถานที่ของพนักงาน</h3>
                    </div>
                    <div class="box-body">
                        <div class="box-group">
                            <div class="row fontawesome-icon-list">
                                <div class="padding-border fa-hover col-md-4 col-sm-4 col-xs-12"><a href="/attd/report/report-out-side"><i class="fa fa-file-pdf-o"></i> รายงานการทำงานนอกสถานที่</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>