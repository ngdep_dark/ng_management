<?php

//use yii\grid\GridView;
use yii\helpers\Html;
use app\modules\attd\models\Leave;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use kartik\grid\GridView;

/**
 * Created by PhpStorm.
 * User: magimari
 * Date: 30/8/2561
 * Time: 11:25 น.
 */

$this->title = "รายงานสรุปการลา";
$this->params['breadcrumbs'][] = $this->title;

$post = Yii::$app->request->post();

$holidayLeave = 7;
$sickLeave = 30;
$errandLeave = 7;


$this->registerCss("
    .style-td{
        font-size: 16px; 
        line-height: 1cm;
    }
");

$post = Yii::$app->request->post();
$user = ArrayHelper::map(\app\models\User::find()->orderBy(['id' => SORT_ASC])->all(), 'id', 'full_name');
$leave_tpye = ArrayHelper::map(\app\modules\attd\models\LeaveType::find()->orderBy(['id' => SORT_ASC]) ->all(), 'id', 'leave_type_name');
if ($post) {
    $date_data = $post['date_range'];
}

function leave($id)
{
    return Leave::findOne($id);
}

?>
<?php Pjax::begin(); ?>
<?php $form = ActiveForm::begin(); ?>

<div class="row">
    <div class="col-md-3">
        <?php echo '<label class="control-label">ชื่อพนักงาน</label>';
        echo Select2::widget([
            'name' => 'user_id',
            'data' => $user,
            'value' => $post ? $user : null ,
            'options' => ['placeholder' => 'Select a Username',],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]);
        ?>
    </div>

    <div class="col-md-3">
        <?php echo '<label class="control-label">ประเภทการลา</label>';
        echo Select2::widget([
            'name' => 'leave_type',
            'data' => $leave_tpye,
            'value' => $post ?  $leave_tpye : null,
            'options' => ['placeholder' => 'Select a Username',],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ]);
        ?>
    </div>

    <div class="col-md-3">
        <?php echo '<label class="control-label">วันที่ต้องการค้นหา</label>';
        echo DateRangePicker::widget([
            'name' => 'date_range',
            'data' => date('Y-m-d') . " - " . date('Y-m-d'),
            'presetDropdown' => true,
            'hideInput' => true
        ]);
        ?>
    </div>

    <div class="col-md-1">
        <br>
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    </div>

    <div class="form-group">
        <?php
        $post = Yii::$app->request->post();
        if (!empty($post)) :
            $date_range = explode(' - ', $post['date_range']);
            ?>
            <div style="float: right; padding-top: 20px; padding-right: 30px">
                <?= Html::a('Generate PDF', ['report/reportpdf-leave-by-type', 'id' => $post['user_id'], 'start' => $date_range[0], 'end' => $date_range[1], 'type' => $post['leave_type']], ['target' => '_blank', 'data-pjax' => "0", 'class' => 'btn btn-success']) ?>
            </div>
        <?php endif; ?>
    </div>


</div>

<?php $form = ActiveForm::end(); ?>

<br>

<?php
$post = Yii::$app->request->post();
if ($post) : ?>
<div class="row">
    <div class="col-md-12">
        <?php echo
        GridView::widget([
            'dataProvider' => $dataProvider,
            'responsive' => true,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'username',
                    'label' => 'ชื่อผู้ใช้งาน',
                ],
                [
                    'attribute' => 'leave_type',
                    'label' => 'ประเภทการลา',
                    'value' => function ($model) {
                        $levObj = \leave($model['id']);
                        return $levObj->typeName;
                    }
                ],
                [
                    'attribute' => 'leave_start',
                    'label' => 'วันที่ลา',
                ],
                [
                    'attribute' => 'description',
                    'label' => 'สาเหตุการลา',
                ],
                [
                    'attribute' => 'approve_by',
                    'label' => 'บันทึกโดย',
                ]
            ],
            'panel' => [
                'type' => GridView::TYPE_PRIMARY,
                'heading' => 'รายการลางานตามประเภท'
            ],
        ])
        ?>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="panel panel-primary">
            <div class="panel-heading" style="font-size: 18px"><strong>สรุปวันลา</strong></div>
            <div class="panel-body">
                <table class="table table-hover">
                    <tr>
                        <td><strong>ประเภทการลา:  <?= $type['leave_type_name'] ?> &nbsp; จำนวนวันที่ลาแล้ว: &nbsp; <?= $leaveCount?> &nbsp; วัน</strong></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>


<?php endif; ?>


<?php Pjax::end(); ?>


