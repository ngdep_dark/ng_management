<?php
/**
 * Created by PhpStorm.
 * User: magimari
 * Date: 17/8/2561
 * Time: 13:48 น.
 */

use kartik\grid\GridView;
use yii\helpers\Html;
Use kartik\alert\Alert;

$this->title = "รายการที่ต้องอนุมัติ";

?>

<?= \yii\helpers\Html::a("Refresh", [
    '/attd/approve/approve'],
    [
        'id' => 'refreshButton',
        'style' => 'display: none-img'
    ])
?>

    <div class="panel-group">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">
                <a>
                    <strong>อนุมัติการลา</strong></a>
            </h3>
        </div>
        <div>
            <div class="panel-body">
                <div class="container">
                    <div class="panel-heading">
                        <div class="col-md-12 col-lg-12 col-xs-12">
                            <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                'responsive' => true,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
                                    [
                                        'attribute' => 'user_id',
                                        'label' => 'รหัสพนักงาน'
                                    ],
                                    [
                                        'attribute' => 'username',
                                        'label' => Yii::$app->messageManage->user_name,
                                        'value' => function ($model) {
                                            return $model->userName;
                                        }
                                    ],
                                    [
                                        'attribute' => 'leave_type',
                                        'label' => 'ประเภทการลา',
                                        'value' => function($model){
                                            return $model->typeName;
                                        }
                                    ],
                                    [
                                        'attribute' => 'leave_start',
                                        'label' => Yii::$app->messageManage->leave_start
                                    ],
                                    [
                                        'attribute' => 'description',
                                        'label' => Yii::$app->messageManage->leave_description
                                    ],
                                    [
                                        'attribute' => 'created_at',
                                        'label' => Yii::$app->messageManage->create_at
                                    ],
                                    [
                                        'class' => 'yii\grid\ActionColumn',
                                        'template' => '{accept} {reject}',
                                        'header' => 'Actions',
                                        'buttons' => [
                                            'accept' => function ($url, $model, $key) {
                                                return Html::a('<i class="glyphicon glyphicon-ok"></i>', ['approve/accept', 'id' => $model['id']], [
                                                        'data' => [
                                                            'method' => 'post',
                                                        ]
                                                    ]
                                                );
                                            },

                                            'reject' => function ($url, $model, $key) {
                                                return Html::a('<i class="glyphicon glyphicon-remove"></i>', ['approve/reject', 'id' => $model['id']], [
                                                    'data' => [
                                                        'confirm' => 'คุณต้องการปฏิเสธคำขอนี้ใช่หรือไม่?',
                                                        'method' => 'post',
                                                    ],
                                                ]);
                                            }
                                        ]
                                    ],

                                ],
                                'panel' => [
                                    'type' => GridView::TYPE_PRIMARY,
                                    'heading' => '<h2 class="panel-title">การลาของพนักงานในความดูแล</h2>',
                                ],
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel-group">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">
                <a>
                    <strong>อนุมัติการทำงานนอกสถานที่</strong>
                </a>
            </h3>
        </div>
        <div>
            <div class="panel-body">
                <div class="container">
                    <div class="panel-heading">
                        <div class="col-md-12 col-lg-12 col-xs-12">
                            <?= GridView::widget([
                                'dataProvider' => $outsideProvider,
                                'responsive' => true,
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],
                                    [
                                        'attribute' => 'user_id',
                                        'label' => 'รหัสพนักงาน'
                                    ],
                                    [
                                        'attribute' => 'username',
                                        'label' => Yii::$app->messageManage->user_name,
                                        'value' => function ($model) {
                                            return $model->userName;
                                        }
                                    ],
                                    [
                                            'attribute' => 'outside_type',
                                        'label' => 'ประเภท',
                                        'value' => function($model){
                                            return $model->typeName;
                                        }
                                    ],
                                    [
                                        'attribute' => 'outside_date_start',
                                        'label' => Yii::$app->messageManage->outside_start
                                    ],
                                    [
                                        'attribute' => 'outside_date_end',
                                        'label' => Yii::$app->messageManage->outside_end
                                    ],
                                    [
                                        'attribute' => 'outside_province',
                                        'label' => 'จังหวัด'
                                    ],
                                    [
                                        'attribute' => 'detail',
                                        'label' => 'รายละเอียด'
                                    ],
                                    [
                                        'class' => 'yii\grid\ActionColumn',
                                        'template' => '{accept} {reject}',
                                        'header'=> 'Actions',
                                        'buttons' => [
                                            'accept' => function ($url, $model, $key) {
                                                return Html::a('<i class="glyphicon glyphicon-ok"></i>', ['approve/accept-outside', 'id' => $model['id']], [
                                                        'data' => [
                                                            'method' => 'post',
                                                        ]
                                                    ]
                                                );
                                            },
                                            'reject' => function ($url, $model, $key) {
                                                return Html::a('<i class="glyphicon glyphicon-remove"></i>', ['approve/reject-outside', 'id' => $model['id']], [
                                                    'data' => [
                                                        'confirm' => 'คุณต้องการปฏิเสธคำขอนี้ใช่หรือไม่?',
                                                        'method' => 'post',
                                                    ],
                                                ]);
                                            }

                                        ]
                                    ],

                                ],
                                'panel' => [
                                    'type' => GridView::TYPE_PRIMARY,
                                    'heading' => '<h2 class="panel-title">รายการทำงานนอกสถานที่</h2>',
                                ],
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php
$this->registerJs(<<<JS
$(document).ready(function() {
    setInterval(function(){
        $("#refreshButton").click(); 
    }, 300000);
});
JS
);
