<?php
/**
 * Created by PhpStorm.
 * User: magimari
 * Date: 20/8/2561
 * Time: 16:39 น.
 */

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;


$this->title = "รายการอนุมัติ";

?>
<div class="panel-group" id="accordion">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a>
                    รายการลาที่ได้รับอนุมัติแล้ว</a>
            </h4>
        </div>
        <div>
            <div class="panel-body">
                <div class="panel-heading">
                    <div class="col-md-12 col-lg-12 col-xs-12">
                        <?php Pjax::begin(); ?>
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'responsive' => true,
                            'rowOptions' => function ($model, $key, $index, $column) {
                                if ($model['approve_status'] == 1) return ['class' => 'info'];
                                elseif ($model['approve_status'] == 2) return ['class' => 'success'];
                                else return ['class' => 'danger'];
                            },
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                [
                                    'attribute' => 'user_id',
                                    'label' => 'รหัสพนักงาน'
                                ],
                                [
                                    'attribute' => 'username',
                                    'label' => Yii::$app->messageManage->user_name
                                ],
                                [
                                    'attribute' => 'leave_start',
                                    'label' => Yii::$app->messageManage->leave_start
                                ],
                                [
                                    'attribute' => 'description',
                                    'label' => Yii::$app->messageManage->leave_description
                                ],
                                [
                                    'attribute' => 'approve_by',
                                    'label' => 'ผู้อนุมัติ'
                                ],
                                [
                                    'attribute' => 'created_at',
                                    'label' => Yii::$app->messageManage->create_at
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{accept} {reject}',
                                    'buttons' => [
                                        'accept' => function ($url, $model, $key) {
                                            return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', ['leave/view', 'id' => $model->id], [
                                                ]
                                            );
                                        },
                                    ]
                                ],
                            ],
                            'panel' => [
                                'type' => GridView::TYPE_PRIMARY,
                                'heading' => '<h2 class="panel-title"> รายการลาที่ได้รับอนุมัตแล้ว</h2>',
                            ],
                        ]); ?>
                        <?php Pjax::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--    -->
</div>
