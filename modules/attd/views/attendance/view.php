<?php

use yii\helpers\Html;
use \yii\grid\GridView;
use yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\attd\models\Attendance */
/* @var $models app\models\user */

$this->title = "การลงเวลาของ". $models->full_name;
$this->params['breadcrumbs'][] = ['label' => 'Attendances', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$model = new \app\modules\attd\models\Attendance();

?>
<div class="attendance-view">
    <div>
        <?php $form = ActiveForm::begin(['method' => 'post']); ?>
        <div class="col-md-12">
            <div class="col-md-3">
                <?php echo '<label class="control-label">ค้นหาตามช่วงเวลา</label>';
                echo DateRangePicker::widget([
                    'name' => 'date_range',
                    'presetDropdown' => true,
                    'hideInput' => true
                ]); ?>
            </div>
            <div class="col-md-3">
                <br>
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>

        <?php $form = ActiveForm::end(); ?>

        <?php
        $post = Yii::$app->request->post();
        if (!empty($post)) : ?>
        <div style="float: right">
            <?= Html::a('Generate PDF', ['attendance/report', 'id' => $models['id'], 'start' => $date_range[0], 'end' => $date_range[1]], ['class' => 'btn btn-success']) ?>
        </div>
        <?php endif; ?>

    </div>

    <?php echo
    GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => ['style' => 'width:100%'],
        'columns' => [
            [
                'attribute' => 'user_id',
                'label' => 'User ID',
                'headerOptions' => ['style' => 'width:80px'],
            ],
            [
                'attribute' => 'full_name',
                'label' => 'Name',
                'headerOptions' => ['style' => 'width:200px'],
            ],

            [
                'attribute' => 'att_date',
                'label' => 'Date',
                'headerOptions' => ['style' => 'width:150px'],
//                'value' => function ($data) {
//                    return substr($data['att_check'], '0', '11');
//                }
                'value' => function ($data) {
                    return \Yii::$app->datetimeManage->dtFormat($data['att_date'],true);
                }

            ],
            [
                'attribute' => 'att_check',
                'label' => 'IN',
                'headerOptions' => ['style' => 'width:170px'],
                'value' => function($model,$key,$index) {
//                    return \Yii::$app->datetimeManage->dtFormat($model['att_check'],true);
                    return substr($model['att_check'], '10');
                }
            ],
            [
                'attribute' => 'att_checkout',
                'label' => 'OUT',
                'headerOptions' => ['style' => 'width:170px'],
                'value' => function($model,$key,$index) {
                    return substr($model['att_checkout'], '10');
                }
            ],
            [
                'attribute' => 'status_check',
                'label' => 'Status',
                'format' => 'html',
                'headerOptions' => ['style' => 'width:130px'],
                'value' => function ($data) {
                    if (empty($data['att_status'])) {
                        return
                            $data['status_check'] == "ปกติ" ? "<span style= \"color: green;\">ปกติ</span>" :
                                "<span style= \"color: orange;\">สาย</span>";
                    } else {
                        return $data['att_status'];
                    }
                }
            ]
//            ['class' => 'yii\grid\ActionColumn'],
        ],

    ])

    ?>


</div>