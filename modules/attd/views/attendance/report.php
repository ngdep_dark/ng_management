<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\CheckboxColumn;
use yii\grid\DataColumn;
use kartik\daterange\DateRangePicker;
use yii\widgets\ActiveForm;
use kartik\form\ActiveField;

/* @var $this yii\web\View */
/* @var $model app\models\Attendance */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายงานการลงเวลาเข้า-ออก';
//$this->params['breadcrumbs'][] = ['label' => 'Attendances', 'url' => ['showclockin']];
//$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container">
    <h1 style="font-weight: bold"><?= Html::encode($this->title) ?></h1>
</div>
<!---->


<div>
    <table class="table table-bordered">
        <tr>
            <th>User ID</th>
            <th>User Name</th>
            <th>Date</th>
            <th>IN</th>
            <th>OUT</th>
            <th>Status</th>
        </tr>
        <?php foreach ($model as $value) : ?>
            <tr>
                <td style="width: 60px"><?= $value['user_id'] ?></td>
                <td style="width: 200px"><?= $value['full_name'] ?></td>
                <td style="width: 150px"><?= Yii::$app->datetimeManage->dtFormat($value['att_date'], true)?></td>
                <td style="width: 150px"><?php $leaveCheck = substr($value['att_check'],'11'); echo  $leaveCheck == '00:00:00' ? '-' : $leaveCheck ?></td>
                <td style="width: 150px"><?php  $leaveOut = substr($value['att_checkout'], '11'); echo $leaveOut == '00:00:00' ? '-' : $leaveOut ?></td>
                <?php if (empty($value['att_status'])) : ?>
                    <td style="width: 80px"><?= $value['status_check'] ?></td>

                <?php else: ?>
                    <td style="width: 80px"><?= $value['att_status'] ?></td>
                <?php endif; ?>
            </tr>
        <?php endforeach; ?>
    </table>
</div>


