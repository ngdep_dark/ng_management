<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\DataColumn;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\Attendance */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Attendance';
$this->params['breadcrumbs'][] = ['label' => 'Attendances', 'url' => ['showclockin']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['style' => 'width:100%'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        'full_name',
        'att_date',
        'att_check',
        'att_checkout',
        [
            'attribute' => 'status_check',
            'format' => 'html',
            'value' => function ($model, $key, $index, $column) {
                return $model['status_check'] == "ปกติ" ? "<span style=\"color:green;\">ปกติ</span>" : "<span style=\"color:red;\">สาย</span>";
            }

        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Actions',
            'template' => '{report}',
            'buttons' => [
                'report' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', \yii\helpers\Url::to(['/attd/attendance/report', 'id' => $model['user_id']]));
                }
            ]
        ]
    ],
])

?>
