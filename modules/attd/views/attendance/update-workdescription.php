<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

$this->title = "แก้ไขรายละเอียดการทำงาน";

?>

<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'att_status')->textInput(['maxlength' => true])->label("รายละเอียดการทำงาน") ?>

<div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    <?= Html::a('back', ['index'], ['class' => 'btn btn-danger']) ?>
</div>

<?php ActiveForm::end(); ?>
