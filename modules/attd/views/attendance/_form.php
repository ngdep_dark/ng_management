<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DateTimePicker;
use yii\helpers\Url;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\modules\attd\models\Shift;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $model app\modules\attd\models\Attendance */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="col-md-12">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="form-group">
            <?php echo '<label class="control-label">ชื่อพนักงาน</label>';
            echo Select2::widget([
                'name' => 'user_id',
                'data' => ArrayHelper::map(\app\models\User::find()->where(['status' => 10])->orderBy(['id' => SORT_ASC])->all(), 'id', 'full_name'),
                'options' => [
                    'placeholder' => 'เลือกชื่อพนักงาน',
                ],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]);
            ?>
        </div>

        <div class="form-group">
            <?php echo '<label class="control-label">เวลาบันทึก</label>';
            echo DateTimePicker::widget([
                'name' => 'attend_time',
                'value' => date('Y-m-d H:i:s'),
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd hh:ii:ss'
                ]
            ]);
            ?>
        </div>

        <div class="form-group">
            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                <?= Html::a('back', ['index'], ['class' => 'btn btn-danger']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>

