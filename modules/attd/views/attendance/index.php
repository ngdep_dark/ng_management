<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\DataColumn;
use kartik\daterange\DateRangePicker;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\db\Query;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\attd\models\AttendanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'บันทึกการลงเวลา';
$this->params['breadcrumbs'][] = $this->title;

$permission = Yii::$app->user->can('Admin');
//var_dump($data); exit();
?>
<div class="attendance-index">
    <p>
        <?= Html::a('บันทึกการลงเวลาย้อนหลัง', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="col-md-12">
        <div class="row">
            <div class="panel panel-primary">
                <div class="panel panel-heading">
                    <h2 class="panel-title">บันทึกการลงเวลา</h2>
                </div>
                <div class="panel panel-body">
                    <div class="row">
                        <?php
                        echo GridView::widget([
                            'dataProvider' => $dataProvider,
                            'rowOptions' => ['style' => 'height: 50px'],
                            'columns' => [
                                [
                                    'attribute' => 'att_date',
                                    'label' => 'วันที่บันทึก',
                                    'headerOptions' => ['style' => 'width: 150px'],
                                    'value' => function ($model) {
                                        $datetime = (datetimeManage())->dtFormat($model->att_date);
                                        return $datetime;
                                    }
                                ],
                                [
                                    'attribute' => 'att_check',
                                    'label' => 'เวลาที่บันทึก',
                                    'headerOptions' => ['style' => 'width: 150px'],
                                    'value' => function ($model) {
                                        return substr($model->att_check, '10');
                                    }
                                ],
                                [
                                    'attribute' => 'user_id',
                                    'label' => "User ID",
                                    'headerOptions' => ['style' => 'width: 400px;'],
                                    'value' => function ($model) {
                                        return $model->userName;
                                    }
                                ],
                                [
                                    'attribute' => 'log_id',
                                    'label' => "ดึงเวลาเมื่อ",
                                    'headerOptions' => ['style' => 'width: 400px;'],
                                    'value' => function ($model) {
                                        return is_null($model->log) ? null : (datetimeManage())->dtFormat($model->log->created_at);
                                    }
                                ],
                            ],
                        ])
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>