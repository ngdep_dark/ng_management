<?php

use yii\helpers\Html;
use yii\db\Query;


/* @var $this yii\web\View */
/* @var $model app\models\Attendance */

$this->title = 'เพิ่มบันทึกการลงเวลา Manual';
$this->params['breadcrumbs'][] = ['label' => 'Attendances', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attendance-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
