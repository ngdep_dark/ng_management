<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\attd\models\AttendanceGroupDescription */

$this->title = 'Create Attendance Group Description';
$this->params['breadcrumbs'][] = ['label' => 'Attendance Group Descriptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attendance-group-description-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
