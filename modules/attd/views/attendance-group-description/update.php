<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\attd\models\AttendanceGroupDescription */

$this->title = 'Update Attendance Group Description: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Attendance Group Descriptions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="attendance-group-description-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
