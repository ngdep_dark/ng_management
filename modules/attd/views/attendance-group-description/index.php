<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use app\modules\attd\models\AttendanceGroup;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\attd\models\AttendanceGroupDescriptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Attendance Group Descriptions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="attendance-group-description-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('สร้างรายละเอียดกลุ่มผู้ใช้งาน', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'user_id',
                'label' => 'ชื่อผู้ใช้งาน',
                'value' => function ($model) {
                    return $model->userName;
                }
            ],
            [
                'attribute' => 'group_id',
                'label' => 'ชื่อกลุ่ม',
                'value' => function ($model) {
                    return $model->groupNames;
                }
            ],

//            'group_leader:boolean',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
