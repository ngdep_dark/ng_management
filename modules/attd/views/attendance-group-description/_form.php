<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
Use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\modules\attd\models\AttendanceGroupDescription */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="attendance-group-description-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
        'data' => $model->userData,
        'options' => ['placeholder' => 'กรุณาระบุชื่อพนักงาน', 'multiple' => false],
    ]);
    ?>


    <?= $form->field($model, 'group_id')->widget(Select2::className(), [
        'data' => $model->groupName,
        'options' => ['placeholder' => 'ระบุกลุ่มผู้ใช้งาน'],
        'pluginOptions' => [
            'allowClear' => true,
//                'disabled' => true,
        ],
    ]) ?>

    <?= $form->field($model, 'group_leader')->checkbox() ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>



