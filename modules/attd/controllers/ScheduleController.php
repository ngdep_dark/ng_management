<?php

namespace app\modules\attd\controllers;

use app\models\UserSearch;
use app\modules\attd\models\AttendanceOutsideService;
use Yii;
use app\modules\attd\models\Attendance;
use app\models\User;
use app\modules\attd\models\AttendanceGroupDescription;
use app\modules\attd\models\AttendanceOvertime;
use app\modules\attd\models\Leave;
use kartik\alert\AlertBlock;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use DateTime;
use yii\db\Query;

class ScheduleController extends \yii\web\Controller
{
    public function actionIndex($id)
    {
        //สูตรการหาวันที่ในช่วง 30วันย้อนหลังจากวันที่ปัจจุบัน
        $start_date = date('Y-m-d H:i:s');
        $start_time = strtotime($start_date);
        $end_time = strtotime("today - 30 days", $start_time);

        for ($i = $start_time; $i > $end_time; $i -= 86400) {
            $list[] = date('Y-m-d', $i);
        }
        //______________________________________________________________________________________________________________

        //สูตรหาจำนวนวันที่ลงเวลา และ วันที่ขาดงาน
        $clockin = Attendance::find()->where(['user_id' => $id])->andWhere(['not', ['att_check' => null]])->all();
        $clockinCount = count($clockin);

        //______________________________________________________________________________________________________________

        //สูตรหาวันลาของพนักงานที่ได้รับการอนุมัติ
        $leave = Leave::find()
            ->where(['user_id' => $id])
            ->andWhere(['between', 'leave_start', date('Y').'-01-01', date('Y').'-12-31'])
            ->orderBy(['leave_start' => SORT_DESC])
            ->all();

        $leavefull = Leave::find()
            ->where(['user_id' => $id])
            ->andWhere(['between', 'leave_start', date('Y').'-01-01', date('Y').'-12-31'])
            ->andWhere(['half_time' => 0])
            ->orderBy(['leave_start' => SORT_DESC])
            ->all();
        $leavefullCount = count($leavefull);

        $leavehalf = Leave::find()
            ->where(['user_id' => $id])
            ->andWhere(['between', 'leave_start', date('Y').'-01-01', date('Y').'-12-31'])
            ->andWhere(['half_time' => 1])
            ->orderBy(['leave_start' => SORT_DESC])
            ->all();
        $leavehalfCount = count($leavehalf);
        $avg_leavehalf = $leavehalfCount / 2;

        $summary_leave = $leavefullCount + $avg_leavehalf;

        //______________________________________________________________________________________________________________

        //สูตรหาจำนวนวันที่ลาหยุดตามประเภท
        $errandLeaveQuery = Leave::find()->where(['user_id' => $id])
            ->andWhere(['between', 'leave_start', date('Y').'-01-01', date('Y').'-12-31'])
            ->andWhere(['leave_type' => 1])
            ->andWhere(['half_time' => 0])
            ->all();
        $errandLeaveCount = count($errandLeaveQuery);

        $errandHalfLeave = Leave::find()->where(['user_id' => $id])
            ->andWhere(['between', 'leave_start', date('Y').'-01-01', date('Y').'-12-31'])
            ->andWhere(['leave_type' => 1])
            ->andWhere(['half_time' => 1])
            ->all();
        $errandHalfLeaveCount = count($errandHalfLeave);
        $avg_errandHalf = $errandHalfLeaveCount / 2;

        $summary_errandLeave = $errandLeaveCount + $avg_errandHalf;
        //______________________________________________________________________________________________________________

        //ลาป่วย
        $sickLeaveQuery = Leave::find()->where(['user_id' => $id])
            ->andWhere(['between', 'leave_start', date('Y').'-01-01', date('Y').'-12-31'])
            ->andWhere(['leave_type' => 2])
            ->andWhere(['half_time' => 0])
            ->all();
        $sickLeaveCount = count($sickLeaveQuery);

        $sickHalfLeave = Leave::find()->where(['user_id' => $id])
            ->andWhere(['between', 'leave_start', date('Y').'-01-01', date('Y').'-12-31'])
            ->andWhere(['leave_type' => 2])
            ->andWhere(['half_time' => 1])
            ->all();
        $sickHalfLeaveCount = count($sickHalfLeave);
        $avg_sickHalf = $sickHalfLeaveCount / 2;

        $summary_sickleave = $sickLeaveCount + $avg_sickHalf;
        //______________________________________________________________________________________________________________

        //ลาพักร้อน
        $holidayLeaveQuery = Leave::find()->where(['user_id' => $id])
            ->andWhere(['between', 'leave_start', date('Y').'-01-01', date('Y').'-12-31'])
            ->andWhere(['leave_type' => 3])
            ->andWhere(['half_time' => 0])
            ->all();
        $holidayLeaveCount = count($holidayLeaveQuery);

        $holidayHalfLeave = Leave::find()->where(['user_id' => $id])
            ->andWhere(['between', 'leave_start', date('Y').'-01-01', date('Y').'-12-31'])
            ->andWhere(['leave_type' => 3])
            ->andWhere(['half_time' => 1])
            ->all();
        $holidayHalfLeaveCount = count($holidayHalfLeave);
        $avg_holidayhalf = $holidayHalfLeaveCount / 2;

        $summary_holidayleave = $holidayLeaveCount + $avg_holidayhalf;
        //______________________________________________________________________________________________________________

        //ลาอื่น ๆ
        $otherLeaveQuery = Leave::find()->where(['user_id' => $id])
            ->andWhere(['leave_type' => 9])
            ->andWhere(['half_time' => 0])
            ->all();
        $otherLeaveCount = count($otherLeaveQuery);

        $otherHalfLeave = Leave::find()->where(['user_id' => $id])
            ->andWhere(['leave_type' => 9])
            ->andWhere(['half_time' => 1])
            ->all();
        $otherHalfLeaveCount = count($otherHalfLeave);

        $avg_otherLeave = $otherHalfLeaveCount / 2;

        $summary_otherLeave = $otherLeaveCount + $avg_otherLeave;
        //______________________________________________________________________________________________________________

        $errandLeaveWithDeductionQuery = Leave::find()
            ->where(['user_id' => $id])
            ->andWhere(['leave_type' => 6])
            ->andWhere(['half_time' => 0])
            ->all();
        $ELDCount = count($errandLeaveWithDeductionQuery);

        $halfErrandLeaveWithDeductionQuery = Leave::find()
            ->where(['user_id' => $id])
            ->andWhere(['leave_type' => 6])
            ->andWhere(['half_time' => 1])
            ->all();
        $halfErrandLeaveWithDeductionCount = count($halfErrandLeaveWithDeductionQuery);

        $avg_halfELD = $halfErrandLeaveWithDeductionCount / 2 ;

        $summary_halfELD = $ELDCount + $avg_halfELD;

        //______________________________________________________________________________________________________________

        $sickLeaveWithoutMedicalCertificateQuery = Leave::find()
            ->where(['user_id' => $id])
            ->andWhere(['leave_type' => 7])
            ->andWhere(['half_time' => 0])
            ->all();
        $SLMCcount = count($sickLeaveWithoutMedicalCertificateQuery);

        $halfSickLeaveWithoutMedicalCertificateQuery = Leave::find()
            ->where(['user_id' => $id])
            ->andWhere(['leave_type' => 7])
            ->andWhere(['half_time' => 1])
            ->all();
        $halfSLMCcount = count($halfSickLeaveWithoutMedicalCertificateQuery);

        $avg_halfSLMC = $halfSLMCcount / 2;

        $summary_halfSLMC = $SLMCcount + $avg_halfSLMC;

        //______________________________________________________________________________________________________________

        $absencework = Leave::find()->where(['user_id' => $id])->andWhere(['leave_type' => 8])->all();
        $absenceCount = count($absencework);

        $outside_work = AttendanceOutsideService::find()
            ->where(['user_id' => $id])
            ->andWhere(['between', 'outside_date_start', date('Y').'-01-01', date('Y').'-12-31'])
            ->orderBy(['outside_date_start' => SORT_DESC])
            ->limit(10)
            ->all();

        //หาชื่อพนักงานเฉย ๆ
        $username = User::find()->where(['id' => $id])->andWhere(['status' => 10])->one();
        //----------------
        $attendanceQuery = Attendance::find()
            ->where(['user_id' => $id])
            ->andWhere(['not', ['att_check' => null]])
            ->andWhere(['leave_id' => null])
            ->orderBy(['att_date' => SORT_DESC])
            ->limit(30)
            ->all();

        $shiftQuery = (new Query())
            ->select('"user".id, "user".username, "user".role_shift,
                                     "shift".come_late_time, "shift".back_early_time, "shift".no_attend_time')
            ->from('user')
            ->leftJoin('shift', '"user".role_shift = "shift".id')
            ->where(['"user".id' => $id])
            ->one();

        return $this->render('index', [
            'errandLeavecount' => $summary_errandLeave,
            'sickLeavecount' => $summary_sickleave,
            'holidayLeavecount' => $summary_holidayleave,
            'otherLeavecount' => $summary_otherLeave,
            'ELDCount' => $summary_halfELD,
            'SLMCcount' => $summary_halfSLMC,
            'clockin' => $attendanceQuery,
            'leave' => $leave,
             'outside_work' => $outside_work,
            'leavecount' => $summary_leave,
            'clockincount' => $clockinCount,
            'absencework' => $absenceCount,
            'shiftdata' => $shiftQuery,
            'username' => $username
        ]);
    }

    public function actionOverallEmployee(){
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('overall-employee', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Workoutattendance model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionReject($id)
    {
        $model = $this->findModel($id);

        if (!empty($model->id)) {
            $model->approve_status = 3;
            $model->save(false);
        }

        Yii::$app->session->setFlash('success', 'ปฏิเสธคำขอเรียบร้อย');
        return $this->redirect(['approve']);
    }

    public function actionOverallApprove()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Leave::find()->where(['approve_status' => 2])
        ]);

        $overtimeProvider = new ActiveDataProvider([
            'query' => AttendanceOvertime::find()->where(['status' => 2])
        ]);

        return $this->render('approve-admin',
            [
                'overtimeProvider' => $overtimeProvider,
                'dataProvider' => $dataProvider,
            ]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);
        $dataProvider = new ActiveDataProvider([
            'query' => Leave::find()->where(['id' => $id])
        ]);
        return $this->render('view',
            [
                'dataProvider' => $dataProvider,
                'model' => $model
            ]);
    }



    public function actionReportLeave()
    {
        $query = [];
        $errandLeaveQuery = Leave::find()->where(['user_id' => Yii::$app->user->id])->andWhere(['approve_status' => 2])->andWhere(['leave_type' => 1])->all();
        $errandLeaveCount = count($errandLeaveQuery);
        $sickLeaveQuery = Leave::find()->where(['user_id' => Yii::$app->user->id])->andWhere(['approve_status' => 2])->andWhere(['leave_type' => 2])->all();
        $sickLeaveCount = count($sickLeaveQuery);
        $holidayLeaveQuery = Leave::find()->where(['user_id' => Yii::$app->user->id])->andWhere(['approve_status' => 2])->andWhere(['leave_type' => 3])->all();
        $holidayLeaveCount = count($holidayLeaveQuery);

        $post = Yii::$app->request->post();
        if ($post) {
            $date_range = explode(' - ', $post['date_range']);
            $query = Leave::find()
                ->where(['approve_status' => 2])
                ->andWhere(['between', 'leave_start', $date_range[0], $date_range[1]])
                ->andWhere(['user_id' => Yii::$app->user->id])
                ->asArray()
                ->all();
        }
        $dataProvider = new ArrayDataProvider([
            'allModels' => $query
        ]);

        return $this->render('report-leave',
            [
                'dataProvider' => $dataProvider,
                'errandLeaveCount' => $errandLeaveCount,
                'sickLeaveCount' => $sickLeaveCount,
                'holidayLeaveCount' => $holidayLeaveCount
            ]);
    }

    public function actionOverTime($id)
    {
        if (!empty($id)) {
            $attModel = $this->findAttendanceModel($id);
            $roleUser = User::findOne(Yii::$app->user->id);
            $retireTime = strtotime($roleUser->roleShift['back_early_time']);
            $start_ot = (date("H:i:s", strtotime('+30 minutes', $retireTime)));
            $Start = new DateTime(substr($attModel->att_checkout, '0', '11') . $start_ot);
            $End = new DateTime($attModel->att_checkout);
            $dateDiff = $Start->diff($End);

            $model = new AttendanceOvertime();
            $model->user_id = Yii::$app->user->id;
            $model->clock_in = $attModel->att_check;
            $model->clock_out = $attModel->att_checkout;
            $model->retire_time = substr($attModel->att_checkout, '0', '11') . $roleUser->roleShift['back_early_time'];
            $model->ot_duration = intval($dateDiff->format("%H"));
            $model->status = 1;
            $model->attendance_overtime_id = $id;
            $model->created_at = date('Y-m-d H:i:s');
            if ($model->save(false)) {
                $attModel->overtime_id = $model->id;
                $attModel->save(false);
            }
        }

        $this->redirect(array('schedule/index'));
    }



    /**
     * Finds the Attendance model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Leave the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Leave::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Finds the Attendance model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AttendanceOvertime the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findOvertimeModel($id)
    {
        if (($model = AttendanceOvertime::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Finds the Attendance model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Attendance the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findAttendanceModel($id)
    {
        if (($model = Attendance::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
