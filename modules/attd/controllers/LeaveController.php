<?php

namespace app\modules\attd\controllers;

use Yii;
use app\modules\attd\models\LeaveType;
use app\modules\attd\models\Leave;
use app\modules\attd\models\LeaveSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\User;
use app\modules\attd\models\Attendance;
use yii\helpers\ArrayHelper;
use app\modules\attd\models\AttendanceGroupDescription;
use yii\helpers\Html;

/**
 * LeaveController implements the CRUD actions for Leave model.
 */
class LeaveController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Leave models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LeaveSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Leave model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Leave model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Leave();
        $check = 0;

        $leave_name = $this->findLeaveName();
        if (!$leave_name) {
            $leave_data = [
                ['id' => 1, 'leave_type_name' => 'ลากิจ', 'create_at' => date('Y-m-d H:i:s')],
                ['id' => 2, 'leave_type_name' => 'ลาป่วย', 'create_at' => date('Y-m-d H:i:s')],
                ['id' => 3, 'leave_type_name' => 'ลาพักร้อน', 'create_at' => date('Y-m-d H:i:s')],
            ];
            foreach ($leave_data as $items) {
                $add_leavetype = new LeaveType();
                $add_leavetype->id = $items['id'];
                $add_leavetype->leave_type_name = $items['leave_type_name'];
                $add_leavetype->created_at = $items['create_at'];
                $add_leavetype->save();
            }
        }

        $post = Yii::$app->request->post();
        if ($post) {
            $user = User::find()->where(['id' => $post['user_id']])->one();
            $leave = Leave::find()->where(['user_id' => $post['user_id']])->andWhere(['leave_type' => $post['leaveType']])->andWhere(['approve_status' => 2])->all();
            $count = count($leave);
            $name = User::find()->select('username')->where(['id' => Yii::$app->user->id])->one();
            $findcount = $this->findCountLeave($post['leaveType'], $count);
            if ($findcount == true) {
                if ($post['check'] == 1) {
                    $leavemodel = new Leave();
                    $leavemodel->user_id = $post['user_id'];
                    $leavemodel->approve_status = 2;
                    $leavemodel->username = $user['username'];
                    $leavemodel->leave_type = $post['leaveType'];
                    $leavemodel->leave_start = substr($post['date_range1'], 0, 10);
                    $leavemodel->created_at = date('Y-m-d');
                    $leavemodel->approve_by = $name->username;
                    $leavemodel->start_time = substr($post['date_range1'], 11);
                    $leavemodel->half_time = $post['check'];
                    $leavemodel->load($post);
                    if ($leavemodel->save()) {
                        $this->actionSaveAttendance($post['user_id'], substr($post['date_range1'], 0, 10), substr($post['date_range1'], 11), $leavemodel->id, $post['leaveType'], date('Y-m-d'));
                    }
                    $this->redirect(array('leave/index'));
                } elseif ($post['check'] == 0)
                    if ($post['date_range'] != null) {
                        $date_range = explode(' - ', $post['date_range']);
                        $start_date = $date_range[0];
                        $end_date = $date_range[1];
                        $start_time = strtotime($start_date);
                        $end_time = strtotime($end_date);
                        for ($i = $start_time; $i <= $end_time; $i += 86400) {
                            $list[] = date('Y-m-d', $i);
                        }
                        foreach ($list as $item) {
                            $day = date("D", strtotime($item));
                            if ($day == 'Sat' || $day == 'Sun') {
                            } else {
                                $leaveModel = new Leave();
                                $leaveModel->load($post);
                                $leaveModel->created_at = date('Y-m-d');
                                $leaveModel->user_id = $post['user_id'];
                                $leaveModel->approve_status = 2;
                                $leaveModel->username = $user['username'];
                                $leaveModel->leave_type = $post['leaveType'];
                                $leaveModel->leave_start = $item;
                                $leaveModel->approve_by = $name->username;
                                $leaveModel->half_time = $post['check'];
                                if ($leaveModel->save()) {
                                    $this->actionSaveAttendance($post['user_id'], $item, null, $leaveModel->id, $post['leaveType'], date('Y-m-d'));
                                }
                                $this->redirect(array('leave/index'));
                            }
                        }
                    }

            } else {
                $this->actionAlert('danger', 'แจ้งเตือน', 'คุณได้ใช้สิทธ์การลาครบแล้ว');
            }
        }
        return $this->render('create', [
            'model' => $model,
            'user_id' => Yii::$app->user->id,
            'leave_type' => ArrayHelper::map(LeaveType::find()->orderBy(['id' => SORT_ASC])->all(), 'id', 'leave_type_name'),
            'check' => $check
        ]);
    }

    public function actionSaveAttendance($user_id, $date, $check, $leave_id, $leave_type, $date_create)
    {
        if ($check == null) {
            $att_model = new Attendance();
            $att_model->user_id = $user_id;
            $att_model->att_date = $date;
            $att_model->leave_id = $leave_id;
            $att_model->created_at = $date_create;
            if ($leave_type == 1) {
                $att_model->att_status = 'ลากิจ';
            } elseif ($leave_type == 2) {
                $att_model->att_status = 'ลาป่วย';
            } elseif ($leave_type == 3) {
                $att_model->att_status = 'ลาพักร้อน';
            } elseif ($leave_type == 6) {
                $att_model->att_status = 'ลากิจ (หักเงิน)';
            } elseif ($leave_type == 7) {
                $att_model->att_status = 'ลาป่วย (ไม่มีใบรับรองแพทย์ หักเงิน)';
            } elseif ($leave_type == 8) {
                $att_model->att_status = 'ขาดงาน';
            } elseif ($leave_type == 9) {
                $att_model->att_status = 'ลาอื่น ๆ';
            }
            $att_save = $att_model->save(false);
            if ($att_save) {
                $this->actionAlert('success', 'แจ้งเตือน', 'บันทึกรายการเรียบร้อยแล้ว');
            }
        } elseif ($check != null) {
            $updateatt = Attendance::find()->where(['user_id' => $user_id])->andWhere(['att_date' => $date])->one();
            if (!empty($updateatt)) {
                $updateatt->att_status = 'ลาอื่น ๆ';
                $updateatt->leave_id = $leave_id;
                $updateatt->updated_at = date('Y-m-d H:i:s');
                $updateatt->updated_by = Yii::$app->user->id;
                if ($updateatt->save(false)) {
                    $this->actionAlert('success', 'แจ้งเตือน', 'บันทึกรายการเรียบร้อยแล้ว');
                }
            }
        }

    }

    /**
     * Updates an existing Leave model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {

            $post = Yii::$app->request->post();
            if($model->half_time == 1){
                $model->user_id = intval($post['user_id']);
                $model->leave_type = intval($post['leaveType']);
                $model->leave_start = substr($post['date_range1'], 0, 10);
                $model->start_time = substr($post['date_range1'], 11);
                $model->save();
            }else{
                if($post['date_range'] != null){
                    $dateRange = explode(' - ', $post['date_range']);
                    $model->user_id = intval($post['user_id']);
                    $model->leave_type = intval($post['leaveType']);
                    $model->leave_start = $dateRange[0];
                    $model->save();
                }
            }
            return $this->redirect(['index']);
        }
        return $this->render('update', [
            'model' => $model,
            'user_id' => $model->user_id,
            'leave_type' => $model->leave_type,
            'leave_date' => $model->leave_start,
            'check' => $model->half_time,
            'time' => $model->start_time
        ]);
    }

    /**
     * Deletes an existing Leave model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public
    function actionDelete($id)
    {
        $delattend = Attendance::findOne(['leave_id' => $id]);
        if ($this->findModel($id)->delete()) {
            if (empty($delattend)) {
                return $this->redirect(['index']);
            } else {
                if ($delattend->att_status == "ลาครึ่งวัน") {
                    $delattend->att_status = null;
                    $delattend->att_checkout = null;
                    $delattend->leave_id = null;
                    $delattend->save(false);
                } else {
                    $delattend->delete();
                }
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Leave model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Leave the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected
    function findModel($id)
    {
        if (($model = Leave::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionAlert($type, $title, $message)
    {
        return Yii::$app->getSession()->setFlash('alert', [
            'type' => $type,
            'duration' => 3000,
            'icon' => ' glyphicon glyphicon-th-large',
            'title' => Yii::t('app', Html::encode($title)),
            'message' => Yii::t('app', Html::encode($message)),
            'positonY' => 'top',
            'positonX' => 'right'
        ]);

//        return $this->render('/attd/create');
    }

    public function findCountLeave($type, $count)
    {
        if ($type == 1 && $count == 7) {
            return false;
        } elseif ($type == 2 && $count == 30) {
            return false;
        } elseif ($type == 3 && $count == 6) {
            return false;
        } else {
            return true;
        }
    }

    private function findLeaveName()
    {
        return LeaveType::find()->where(['leave_type_name' => 'ลากิจ'])->scalar();
    }



}
