<?php

namespace app\modules\attd\controllers;

use Yii;
use app\models\User;
use app\modules\attd\models\AttendanceGroupDescription;
use yii\helpers\ArrayHelper;
use yii\data\ArrayDataProvider;
use app\modules\attd\models\AttendanceOvertime;
use app\modules\attd\models\Leave;
use app\modules\attd\models\Attendance;
use yii\web\NotFoundHttpException;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use app\modules\attd\models\LeaveType;
use app\modules\attd\models\AttendanceOutsideService;
use yii\helpers\Html;

class ApproveController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['approve', 'accept', 'reject', 'accept-outside', 'reject-outside', 'view'],
                        'roles' => ['manage']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['overall-approve'],
                        'roles' => ['Admin']
                    ],
                ]
            ]
        ];
    }

    public
    function actionOverallApprove()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Leave::find()->where(['approve_status' => 2])
        ]);

        return $this->render('approve-admin',
            [
                'dataProvider' => $dataProvider,
            ]);
    }

    public function actionAcceptOutside($id)
    {
        $model = AttendanceOutsideService::find()->where(['id' => $id])->one();
        $model->status = 2;
        $this->actionAddOutsideClockin($id);
        $model->save();
        $this->actionAlert('success', 'แจ้งเตือน', 'ทำรายการเรียบร้อยแล้ว');
        return $this->redirect(['approve']);
    }

    public function actionRejectOutside($id)
    {
        $model = $this->findOutsideModel($id);
        if (!empty($model->id)) {
            $model->status = 3;
            $model->save();
            $this->actionAlert('success', 'แจ้งเตือน', 'ทำรายการเรียบร้อยแล้ว');
        }
        return $this->redirect(['approve']);
    }

    public function actionAddOutsideClockin($id)
    {
        $model = $this->findOutsideModel($id);
        $start_date = $model->outside_date_start;
        $end_date = $model->outside_date_end;
        $start_time = strtotime($start_date);
        $end_time = strtotime($end_date);
        for ($i = $start_time; $i <= $end_time; $i += 86400) {
            $list[] = date('Y-m-d', $i);
        }
        foreach ($list as $time) {
            $clockin = new Attendance();
            $clockin->user_id = $model->user_id;
            $clockin->att_date = $time;
            $clockin->att_status = $model->detail;
            $clockin->outside_id = $model->id;
            $clockin->save();
        }
    }

    public function actionAlert($type, $title, $message)
    {
        return Yii::$app->getSession()->setFlash('alert', [
            'type' => $type,
            'duration' => 3000,
            'icon' => ' glyphicon glyphicon-th-large',
            'title' => Yii::t('app', Html::encode($title)),
            'message' => Yii::t('app', Html::encode($message)),
            'positonY' => 'top',
            'positonX' => 'right'
        ]);
    }

    /**
     * Finds the Attendance model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Leave the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected
    function findLeaveModel($id)
    {
        if (($model = Leave::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Finds the Attendance model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AttendanceOvertime the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected
    function findOvertimeModel($id)
    {
        if (($model = AttendanceOvertime::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Finds the Attendance model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Attendance the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected
    function findAttendanceModel($id)
    {
        if (($model = Attendance::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Finds the Attendance model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AttendanceOutsideService the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected
    function findOutsideModel($id)
    {
        if (($model = AttendanceOutsideService::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
