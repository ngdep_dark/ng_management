<?php

namespace app\modules\attd\controllers;

use app\models\User;
use app\modules\attd\models\AttendanceOutsideService;
use app\modules\attd\models\AttendanceOvertime;
use app\modules\attd\models\Leave;
use app\modules\attd\models\LeaveType;
use yii\data\ArrayDataProvider;
use app\modules\attd\models\Attendance;
use yii\db\Query;
use Mpdf;
use yii\helpers\Url;
use Yii;


class ReportController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    //สารบัญการค้นหา
    //Report Clock_in PDF = ReportClockin
    //Report Leave PDF = ReportLeave
    //Report Overtime PDF = ReportOvertime

    public function actionReportClockinByPerson()
    {
        $data = [];
        $post = Yii::$app->request->post();

        if ($post) {
            $date_range = explode(' - ', $post['date_range']);
            $start_date = $date_range[0];
            $end_date = $date_range[1];
            $start_time = strtotime($start_date);
            $end_time = strtotime($end_date);
            for ($i = $start_time; $i <= $end_time; $i += 86400) {
                $list[] = date('Y-m-d', $i);
            }
            foreach ($list as $item) {
                $data[] = $item;
            }
        }

        return $this->render('report-clockin-by-person', [
            'SelectDay' => $data,
            'pagination' => ['pageSize' => 50],

        ]);
    }

//รายงานการลารายบุคคล
    public
    function actionReportLeaveByPerson()
    {
        $query = [];
        $post = Yii::$app->request->post();
        if ($post) {
            $date_range = explode(' - ', $post['date_range']);
            $query = Leave::find()
                ->where(['approve_status' => 2])
                ->andWhere(['between', 'leave_start', $date_range[0], $date_range[1]])
                ->orderBy(['leave_start' => SORT_ASC])
                ->asArray()
                ->all();
            $LeaveCount = count($query);
        }
        $dataProvider = new ArrayDataProvider([
            'allModels' => $query,
            'pagination' => false,
        ]);

        return $this->render('report-leave-by-person', [
            'dataProvider' => $dataProvider,
            'LeaveCount' => $post ? $LeaveCount : null,
            'pagination' => 50,
        ]);
    }

    public function actionReportLeaveByType()
    {
        $post = Yii::$app->request->post();
        $current_leave = 0;
        if ($post) {
            $date_range = explode(' - ', $post['date_range']);
            $query = Leave::find()
                ->where(['between', 'leave_start', $date_range[0], $date_range[1]])
                ->andWhere(['leave_type' => $post['leave_type']])
                ->andWhere(['user_id' => $post['user_id']])
                ->orderBy(['leave_start' => SORT_ASC])
                ->asArray()
                ->all();
            $LeaveCount = count($query);

            $type = LeaveType::find()
                ->where(['id' => $post['leave_type']])
                ->one();

            $dataProvider = new ArrayDataProvider([
                'allModels' => $query,
                'pagination' => false,
            ]);

        }
        return $this->render('report-leave-by-type', [
            'dataProvider' => $post ? $dataProvider : null,
            'leaveCount' => $post ? $LeaveCount : null,
            'current_leave' => $current_leave,
            'type' => $post ? $type : null,
            'pagination' => 50,
        ]);
    }

//รายงานทำงานล่วงเวลา
    public
    function actionReportOvertimeByPerson()
    {
        $post = Yii::$app->request->post();
        if ($post) {
            $date_range = explode(' - ', $post['date_range']);
            $overtime_query = AttendanceOvertime::find()
                ->Where(['user_id' => $post['user_id']])
                ->andWhere(['between', 'overtime_date', $date_range[0], $date_range[1]])
                ->andWhere(['status' => 2])
                ->all();
        }
        return $this->render('report-overtime-by-person', [
            'overtime_person' => $post ? $overtime_query : null,
        ]);
    }

//รายงานการลงเวลารายวัน
    public
    function actionReportClockinPerDay()
    {
        $post = Yii::$app->request->post();
        if ($post) {
            $timeScan = (new Query())
                ->select('"user".id, "user".username, "user".full_name, 
                "attendance".user_id, "attendance".att_date, "attendance".att_check , "attendance".att_checkout')
                ->from('user')
                ->leftJoin('attendance', '"user".id = "attendance".user_id')
                ->where(['att_date' => $post['datetime']])
                ->orderBy(['att_check' => SORT_ASC])
                ->all();
        }
        return $this->render('report-clockin-per-day', [
            'time' => $post ? $timeScan : null,
        ]);
    }

    public
    function actionReportSummaryClockin()
    {
        $post = Yii::$app->request->post();
        if ($post) {
            $user = User::find()->where(['status' => 10])
                ->orderBy(['id' => SORT_ASC])
                ->all();
            $date_range = explode(' - ', $post['date_range']);
            $start_date = $date_range[0];
            $end_date = $date_range[1];
            $start_time = strtotime($start_date);
            $end_time = strtotime($end_date);
            for ($i = $start_time; $i <= $end_time; $i += 86400) {
                $list[] = date('Y-m-d', $i);
            }
            foreach ($list as $item) {
                $data[] = $item;
            }

            $start_check = $date_range[0];
            $end_check = date('Y-m-d');
            $start_time1 = strtotime($start_check);
            $end_time1 = strtotime($end_check);
            for ($i = $start_time1; $i <= $end_time1; $i += 86400) {
                $list1[] = date('Y-m-d', $i);
            }
            foreach ($list1 as $items) {
                $data2[] = $items;
            }
        }
        return $this->render('report-summary-clockin', [
            'date' => $post ? $data : null,
            'get_user' => $post ? $user : null,
            'check_date' => $post ? $data2 : null
        ]);
    }


//PDF การลงเวลา
    public
    function actionReportClockin($id, $start, $end)
    {
        $data = [];
        $start_date = $start;
        $end_date = $end;
        $start_time = strtotime($start_date);
        $end_time = strtotime($end_date);
        for ($i = $start_time; $i <= $end_time; $i += 86400) {
            $list[] = date('Y-m-d', $i);
        }
        foreach ($list as $item) {
            $data[] = $item;
        }

        $pdf_content = $this->renderPartial('reportpdf-clockin', [
            'selectday' => $data,
            'id' => $id

        ]);

        $params = Yii::$app->params;
        $fontDirs = $params['defaultConfig']['fontDir'];
        $fontData = $params['defaultFontConfig']['fontdata'];

        $mpdf = new Mpdf\Mpdf([
            'fontDir' => array_merge($fontDirs, [
                Url::base() . 'fonts/THSarabun',
            ]),
            'fontdata' => $fontData + ['thsarabun' => $params['SetTHSarabun']],
            'default_font' => 'thsarabun',
            'format' => 'A4',
            'mode' => 'utf-8',
            'default_font_size' => 18,
            'margin_top' => 10,
            'margin_left' => 10,
            'margin_right' => 10,
            'margin_bottom' => 10,
        ]);
        $stylesheet = file_get_contents(Url::base() . 'css/bootstrap.css');
        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->WriteHTML($pdf_content, 2);
        $mpdf->Output();
    }

    public function actionReportpdfLeaveByType($id, $start, $end, $type)
    {
        $query = Leave::find()
            ->where(['between', 'leave_start', $start, $end])
            ->andWhere(['leave_type' => $type])
            ->orderBy(['leave_start' => SORT_ASC])
            ->asArray()
            ->all();

        $LeaveCount = count($query);

        $typemode = LeaveType::find()
            ->where(['id' => $type])
            ->one();

        $content = $this->renderPartial('reportpdf-leave-by-type', [
            'data' => $query,
            'leave_count' => $LeaveCount,
            'type' => $typemode
        ]);

        $footer = $this->renderPartial('_footer');

        $format = [
            "papersize" => 'A4',
            "content" => $content,
            "orientation" => 'P',
            "footer" => $footer
        ];

        $title = 'รายงานสรุปการลา';
        $this->actionGeneratePDF($format, 10, 10, 10, 10, $title, true);
    }

//PDF การลางาน
    public
    function actionReportLeave($start, $end)
    {
        $queryLeave = Leave::find()
            ->where(['approve_status' => 2])
            ->andWhere(['between', 'leave_start', $start, $end])
            ->orderBy(['leave_start' => SORT_ASC])
            ->all();

        $pdf_content = $this->renderPartial('reportpdf-leave', [
            'model' => $queryLeave,
            'start_date' => $start,
            'end_date' => $end,
        ]);

        $params = \Yii::$app->params;
        $fontDirs = $params['defaultConfig']['fontDir'];
        $fontData = $params['defaultFontConfig']['fontdata'];
        $mpdf = new Mpdf\Mpdf([
            'fontDir' => array_merge($fontDirs, [
                Url::base() . 'fonts/THSarabun',
            ]),
            'fontdata' => $fontData + ['thsarabun' => $params['SetTHSarabun']],
            'default_font' => 'thsarabun',
            'format' => 'A4',
            'mode' => 'utf-8',
            'default_font_size' => 16,
            'margin_top' => 10,
            'margin_left' => 10,
            'margin_right' => 10,
            'margin_bottom' => 10,
        ]);
        $stylesheet = file_get_contents(Url::base() . 'css/bootstrap.css');
        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->WriteHTML($pdf_content, 2);
        $mpdf->Output();
    }

//PDF การทำงานล่วงเวลา
    public
    function actionReportOvertime($start, $end)
    {
        $data = [];
        $overtime_data = [];
        $query = Attendance::find()->Where(['not', ['overtime_id' => null]])
            ->andWhere(['between', 'att_date', $start, $end])
            ->all();
        foreach ($query as $value) {
            $overtime_query = AttendanceOvertime::find()
                ->where(['id' => $value->overtime_id])
                ->one();
            $data[] = $value;
            $overtime_data[] = $overtime_query;
        }

        $params = Yii::$app->params;
        $fontDirs = $params['defaultConfig']['fontDir'];
        $fontData = $params['defaultFontConfig']['fontdata'];

        $mpdf = new Mpdf\Mpdf([
            'fontDir' => array_merge($fontDirs, [
                Url::base() . 'fonts/THSarabun',
            ]),
            'fontdata' => $fontData + ['thsarabun' => $params['SetTHSarabun']],
            'default_font' => 'thsarabun',
            'format' => 'A4',
            'mode' => 'utf-8',
            'default_font_size' => 16,
            'margin_top' => 10,
            'margin_left' => 10,
            'margin_right' => 10,
            'margin_bottom' => 10,
        ]);
        $stylesheet = file_get_contents(Url::base() . 'css/bootstrap.css');

        $pdf_content = $this->renderPartial('reportpdf-overtime', [
            'overtime_model' => $overtime_data,
            'overtime_person' => $data,
            'date_start' => $start,
            'date_end' => $end
        ]);

        $footer = $this->renderPartial('_footer', [
            'overtime_model' => $overtime_data,
        ]);

        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->WriteHTML($pdf_content, 2);
        $mpdf->SetHTMLFooter($footer);
        $mpdf->Output();
    }

    public
    function actionReportpdfClockinPerDay($date)
    {
        $timeScan = (new Query())
            ->select('"user".id, "user".username, "user".full_name, 
                "attendance".user_id, "attendance".att_date, "attendance".att_check , "attendance".att_checkout')
            ->from('user')
            ->leftJoin('attendance', '"user".id = "attendance".user_id')
            ->where(['att_date' => $date])
            ->orderBy(['att_check' => SORT_ASC])
            ->all();

        $pdf_content = $this->renderPartial('reportpdf-clockin-per-day', [
            'timescan' => $timeScan,
        ]);

        $params = Yii::$app->params;
        $fontDirs = $params['defaultConfig']['fontDir'];
        $fontData = $params['defaultFontConfig']['fontdata'];

        $mpdf = new Mpdf\Mpdf([
            'fontDir' => array_merge($fontDirs, [
                Url::base() . 'fonts/THSarabun',
            ]),
            'fontdata' => $fontData + ['thsarabun' => $params['SetTHSarabun']],
            'default_font' => 'thsarabun',
            'format' => 'A4',
            'mode' => 'utf-8',
            'default_font_size' => 18,
            'margin_top' => 10,
            'margin_left' => 10,
            'margin_right' => 10,
            'margin_bottom' => 10,
        ]);
        $stylesheet = file_get_contents(Url::base() . 'css/bootstrap.css');
        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->WriteHTML($pdf_content, 2);
        $mpdf->Output();
    }


    public
    function actionPdfSummaryReport($start, $end)
    {
        $setusr = User::find()->where(['status' => 10])->orderBy(['id' => SORT_ASC])->all();
        $count_user = count($setusr);

        $data = [];
        $data2 = [];
        $count_date = 0;
        $start_date = $start;
        $end_date = $end;
        $start_time = strtotime($start_date);
        $end_time = strtotime($end_date);
        for ($i = $start_time; $i <= $end_time; $i += 86400) {
            $list[] = date('Y-m-d', $i);
        }
        foreach ($list as $item) {
            $data[] = $item;
            $day = date("l", strtotime($item));
            $holiday = \app\modules\attd\models\Holiday::find()->where(['holiday_date' => $item])->one();
            if ($day == 'Saturday' || $day == 'Sunday') {

            } elseif (!empty($holiday)) {

            } else {

                $count_date = $count_date + 1;
            }
        }

        $start_check = $start;
        $end_check = date('Y-m-d');
        $start_time1 = strtotime($start_check);
        $end_time1 = strtotime($end_check);
        for ($i = $start_time1; $i <= $end_time1; $i += 86400) {
            $list1[] = date('Y-m-d', $i);
        }
        foreach ($list1 as $items) {
            $data2[] = $items;
        }

        $pdf_content = $this->renderPartial('pdf-summary-report', [
            'user' => $setusr,
            'date' => $data,
            'no_check' => $data2,
            'start' => $start,
            'end' => $end,
            'count_user' => $count_user,
            'work_date' => $count_date
        ]);

        $params = Yii::$app->params;
        $fontDirs = $params['defaultConfig']['fontDir'];
        $fontData = $params['defaultFontConfig']['fontdata'];

        $mpdf = new Mpdf\Mpdf([
            'fontDir' => array_merge($fontDirs, [
                Url::base() . 'fonts/THSarabun',
            ]),
            'fontdata' => $fontData + ['thsarabun' => $params['SetTHSarabun']],
            'default_font' => 'thsarabun',
            'format' => 'A4-L',
            'mode' => 'utf-8',
            'default_font_size' => 18,
            'margin_top' => 20,
            'margin_left' => 10,
            'margin_right' => 10,
            'margin_bottom' => 15,
        ]);

        $header = $this->renderPartial('_summary_header', [
            'date' => $data
        ]);

        $footer = $this->renderPartial('_summary_footer', [

        ]);


        $stylesheet = file_get_contents(Url::base() . 'css/bootstrap.css');
        $mpdf->SetHeader($header);
        $mpdf->SetFooter($footer);
        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->WriteHTML($pdf_content, 2);
        $mpdf->Output();
    }

    public function actionReportOutSide()
    {
        $post = Yii::$app->request->post();
        if ($post) {
            $date_range = explode(' - ', $post['date_range']);
            $query = AttendanceOutsideService::find()
                ->where(['between', 'outside_date_start', $date_range[0], $date_range[1]])
                ->andWhere(['user_id' => $post['user_id']])
                ->orderBy(['outside_date_start' => SORT_ASC])
                ->asArray()
                ->all();
//            var_dump($query);
//            exit();
            $dataProvider = new ArrayDataProvider([
                'allModels' => $query,
                'pagination' => false,
            ]);

        }
        return $this->render('report-out-side', [
            'dataProvider' => $post ? $dataProvider : null,
            'pagination' => 50,
        ]);
    }

    public function actionReportpdfOutSide($id, $start, $end)
    {

        $query = AttendanceOutsideService::find()
            ->where(['between', 'outside_date_start', $start, $end])
            ->andWhere(['user_id' => $id])
            ->orderBy(['outside_date_start' => SORT_ASC])
            ->all();

        $title = 'รายงานสรุปการทำงานนอกสถานที่';
        $content = $this->renderPartial('reportpdf-out-side', [
            'data' => $query,
            'title' => $title
        ]);

        $footer = $this->renderPartial('_footer');

        $format = [
            "papersize" => 'A4',
            "content" => $content,
            "orientation" => 'P',
            "footer" => $footer
        ];

        $this->actionGeneratePDF($format, 10, 10, 10, 10, $title, true);

    }

    public function actionGeneratePDF($format, $mar_top = 0, $mar_left = 0, $mar_right = 0, $mar_bot = 0, $title = 'PDF title', $footer = true)
    {
        ["papersize" => $papersize, "content" => $content, "orientation" => $orientation, "footer" => $footers] = $format;

        $params = Yii::$app->params;
        $fontDirs = $params['defaultConfig']['fontDir'];
        $fontData = $params['defaultFontConfig']['fontdata'];
        $mpdf = new Mpdf\Mpdf([
            'fontDir' => array_merge($fontDirs, [
                Url::base() . 'fonts/THSarabun',
            ]),
            'fontdata' => $fontData + ['thsarabun' => $params['SetTHSarabun']],
            'default_font' => 'thsarabun',
            'mode' => 'utf-8',
            'format' => $papersize,
            'language' => 'th',
            'margin_top' => $mar_top,
            'margin_left' => $mar_left,
            'margin_right' => $mar_right,
            'margin_bottom' => $mar_bot,
            'orientation' => $orientation,
        ]);
        $stylesheet = file_get_contents(Url::base() . 'css/bootstrap.css');
        $mpdf->SetTitle($title);
        $mpdf->WriteHTML($stylesheet, 1);
        $mpdf->WriteHTML($content, 2);
        if ($footer == true){
            $mpdf->SetHTMLFooter($footers);
        }
        $mpdf->Output();
    }

}


