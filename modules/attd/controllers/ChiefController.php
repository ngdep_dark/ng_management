<?php

namespace app\modules\attd\controllers;

use app\modules\attd\models\AttdGroup;
use Yii;
use app\modules\attd\models\Chief;
use app\modules\attd\models\ChiefSearch;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\attd\models\AttdGroupSearch;

/**
 * ChiefController implements the CRUD actions for Chief model.
 */
class ChiefController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Chief models.
     * @return mixed
     */
    public function actionIndex()
    {
//        $model = new Chief();
        $searchModel = new ChiefSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
//            'model' => $model
        ]);
    }

    /**
     * Displays a single Chief model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $data = \app\modules\attd\models\Chief::find()->where(['chief_id' => $id])->all();
        return $this->render('view', [
            'model' => $this->findModel($id),
            'data' => $data
        ]);
    }

    /**
     * Creates a new Chief model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Chief();
        $post = Yii::$app->request->post();
        if (!empty($post)) {
            $model->load($post);
            $model->user_id = $model->chief_id;
            $model->status = 1;
            $model->chief_id = null;
            if ($model->save()) {
                foreach ($post['Chief']['user_id'] as $value){
                    $chiefModel = new Chief();
                    $chiefModel->user_id = $value;
                    $chiefModel->chief_name = $model->chief_name;
                    $chiefModel->status = 2;
                    $chiefModel->chief_id = $model->id;
                    $chiefModel->save();
                }
            }
            return $this->redirect(['index']);

        }
        return $this->render('create', ['model' => $model,]);
    }

    /**
     * Updates an existing Chief model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Chief model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Chief model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Chief the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Chief::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
