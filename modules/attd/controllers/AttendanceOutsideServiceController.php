<?php

namespace app\modules\attd\controllers;

use app\modules\attd\models\Attendance;
use app\modules\attd\models\AttendanceServicePassenger;
use app\modules\attd\models\AttendanceServiceType;
use Yii;
use app\modules\attd\models\AttendanceOutsideService;
use app\modules\attd\models\AttendanceOutsideServiceSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AttendanceOutsideServiceController implements the CRUD actions for AttendanceOutsideService model.
 */
class AttendanceOutsideServiceController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AttendanceOutsideService models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AttendanceOutsideServiceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AttendanceOutsideService model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AttendanceOutsideService model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AttendanceOutsideService();
        $post = Yii::$app->request->post();
        $type = AttendanceServiceType::find()->scalar();
        if ($type == false) {
            $service_type = [
                ['type_name' => 'Service'],
                ['type_name' => 'PhotoHunt'],
                ['type_name' => 'อบรมครู'],
                ['type_name' => 'อบรมผู้ปกครอง']
            ];
            foreach ($service_type as $item) {
                $add_service = new AttendanceServiceType();
                $add_service->type_name = $item['type_name'];
                $add_service->created_at = date("Y-m-d");
                $add_service->save();
            }
        }
        if ($post) {
            $dateRange = Explode(' - ', $post['date_range']);
            $start_date = $dateRange[0];
            $end_date = $dateRange[1];
            $start_time = strtotime($start_date);
            $end_time = strtotime($end_date);
            for ($i = $start_time; $i <= $end_time; $i += 86400) {
                $list[] = date('Y-m-d', $i);
            }
            foreach ($list as $time) {
                $outside_model = new AttendanceOutsideService();
                $outside_model->load(Yii::$app->request->post());
                $outside_model->user_id = $post['user_id'];
                $outside_model->outside_type = $post['serviceType'];
                $outside_model->outside_date_start = $time;
                $outside_model->status = Yii::$app->user->id;
                if ($outside_model->save()) {
                    $clockin = new Attendance();
                    $clockin->user_id = $outside_model->user_id;
                    $clockin->att_date = $time;
                    $clockin->att_status = $outside_model->detail;
                    $clockin->created_at = date('Y-m-d');
                    $clockin->outside_id = $outside_model->id;
                    $clockin->save(false);
                }
            }

            return $this->redirect('index');
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AttendanceOutsideService model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $clockin = Attendance::find()->where(['outside_id' => $id])->one();
        $post = Yii::$app->request->post();
        if ($post) {
            $model->load(Yii::$app->request->post());
            $model->outside_date_start = $post['date_range'];
            $model->outside_type = $post['serviceType'];
            if ($model->save()){
                if (!empty($clockin)){
                    $clockin->att_date = $model->outside_date_start;
                    $clockin->att_status = $model->detail;
                    $clockin->created_at = date('Y-m-d');
                    $clockin->save(false);
                }
            }
            return $this->redirect(['index']);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionUpdatePassengers($passengers, $model)
    {

    }

    /**
     * Deletes an existing AttendanceOutsideService model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $delattend = Attendance::findOne(['outside_id' => $id]);
        if ($this->findModel($id)->delete()) {
            if (!empty($delattend)) {
                $delattend->delete();
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the AttendanceOutsideService model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AttendanceOutsideService the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = AttendanceOutsideService::findOne($id);
        return $model;


        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findServiceType($id)
    {
        $type = AttendanceServiceType::find()->select('type_name')->where(['id' => $id])->one();
        return $type->type_name;
    }
}
