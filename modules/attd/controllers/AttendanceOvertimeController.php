<?php

namespace app\modules\attd\controllers;

use Yii;
use app\models\User;
use app\modules\attd\models\AttendanceOvertime;
use app\modules\attd\models\AttendanceOvertimeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\attd\models\AttendanceGroupDescription;
use yii\helpers\ArrayHelper;

/**
 * AttendanceOvertimeController implements the CRUD actions for AttendanceOvertime model.
 */
class AttendanceOvertimeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AttendanceOvertime models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AttendanceOvertimeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AttendanceOvertime model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AttendanceOvertime model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AttendanceOvertime();
        $post = Yii::$app->request->post();
        if ($post) {
            $model->load($post);
            $createusr = User::findOne(['id' => Yii::$app->user->id]);
            $model->overtime_date = $post['overtime_date'];
            $model->start_time = $post['start_time'];
            $model->end_time = $post['end_time'];
            $model->user_id = $post['user_id'];
            $model->status = 1;

            $starttime = strtotime($model->start_time);
            $endtime = strtotime($model->end_time);
            $difftime = abs($starttime - $endtime) / 3600;

            $model->ot_duration = intval($difftime * 10);
            $model->approve_by = $createusr->username;
            $model->created_time = date('Y-m-d H:i:s');
            if ($model->save()) {
                $this->actionSend($model->id);

                return $this->redirect('index');
            }

        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionSend($id)
    {
        $item = AttendanceOvertime::findOne(['id' => $id]);
        $username = User::findOne(['id' => $item->user_id]);

        $manyLeader = [];
        $groupLeader = AttendanceGroupDescription::find()
            ->where(['user_id' => $item->user_id])
            ->andWhere(['group_leader' => false])
            ->one();

        $groupDescription = AttendanceGroupDescription::find()
            ->select('user_id')
            ->where(['group_leader' => true])
            ->andWhere(['group_id' => $groupLeader->group_id])
            ->groupBy('user_id')
            ->asArray()
            ->all();

        $leaderId = ArrayHelper::map($groupDescription, 'user_id', 'user_id');
        $leader = User::find()->where(['id' => $leaderId])->all();
        if (count($leader) > 1) {
            foreach ($leader as $value) {
                $manyLeader[] = $value->full_name;
            }
            $message = 'รายการ OT ของพนักงาน' . $username->full_name . ' ' . 'ณ วันที่ ' . Yii::$app->datetimeManage->dtFormat($item['overtime_date']) . ' ' . ' กำลังรอคุณ ' . $manyLeader[0] . ' ' . 'หรือคุณ' . ' ' . $manyLeader[1] . ' อนุมัติ';
        } else {
            foreach ($leader as $value) {
                $message = 'รายการ OT ของพนักงาน' . $username->full_name . ' ' . 'ณ วันที่ ' . Yii::$app->datetimeManage->dtFormat($item['overtime_date']) . ' ' . ' กำลังรอคุณ ' . $value->full_name . ' อนุมัติ';
            }
        }
        $line_api = 'https://notify-api.line.me/api/notify';
        $access_token = '7RZgffg0uKYk8Wy2Yuz1tIgEkmM5TqWTOVNmi5UFCjD'; //OT notification: ส่งเข้ากลุ่มเฒ่าหัวงู
//        $access_token = 'Vc2nA8zw96rhkqNWEqEi7T3mvrHoG26iT0prXJk5auh'; //แจ้งเตือน: ส่งเข้ากลุ่ม NG_Company
        $str = $message;    //text max 1000 ตัวอักษร
        $sticker_package_id = 2;  // Package ID ของสติกเกอร์
        $sticker_id = 161;    // ID ของสติกเกอร์
        $message_data = array(
            'message' => $str,
            'stickerPackageId' => $sticker_package_id,
            'stickerId' => $sticker_id
        );

        $result = $this->send_notify_message($line_api, $access_token, $message_data);
        //print_r($result);
    }

    private function send_notify_message($line_api, $access_token, $message_data)
    {
        $headers = array('Method: POST', 'Content-type: multipart/form-data', 'Authorization: Bearer ' . $access_token);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $line_api);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $message_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        // Check Error
        if (curl_error($ch)) {
            $return_array = array('status' => '000: send fail', 'message' => curl_error($ch));
        } else {
            $return_array = json_decode($result, true);
        }
        curl_close($ch);
        return $return_array;
    }

    /**
     * Updates an existing AttendanceOvertime model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AttendanceOvertime model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AttendanceOvertime model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AttendanceOvertime the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AttendanceOvertime::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
