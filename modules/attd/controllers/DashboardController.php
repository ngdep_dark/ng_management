<?php

namespace app\modules\attd\controllers;

use Yii;
use yii\db\Query;
use app\models\User;
use DateTime;
use yii\web\Response;
use app\modules\attd\models\Shift;
use app\modules\attd\models\Leave;
use yii2fullcalendar\models\Event;
use app\modules\attd\models\Holiday;
use app\modules\attd\models\Attendance;

class DashboardController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $user_data = User::find()->where(['status' => 10])->orderBy(['id' => 4])->all();
        $user_name = [];
        $shift_work = [];

        $timeScan = (new Query())
            ->select('"user".id, "user".username,  "user".full_name, "user".status , "attendance".user_id, "attendance".att_date, "attendance".att_check, "attendance".att_checkout,
            "shift".id, "shift".come_late_time, "shift".back_early_time')
            ->from('user')
            ->leftJoin('attendance', '"user".id = "attendance".user_id')
            ->leftJoin('shift', '"user".role_shift = "shift".id')
            ->where(['att_date' => date('Y-m-d')])
            ->andWhere(['status' => 10])
            ->andWhere(['leave_id' => null])
            ->andWhere(['outside_id' => null])
            ->orderBy(['att_check' => SORT_ASC])
            ->all();

        foreach ($timeScan as $value) {
            $date = date_create($value['att_date']);
            date_modify($date, '-1 day');
            $trueDate = date_format($date, 'Y-m-d');

            $previous_btw_clockInOut_time = Attendance::find()->where(['att_date' => $trueDate])->andWhere(['user_id' => $value['user_id']])->one();
            $value['reduce_time'] = 0;
            if (substr($previous_btw_clockInOut_time['att_checkout'], 11) > '00:00:01' && substr($previous_btw_clockInOut_time['att_checkout'], 11) < '05:00:00') {
                $time1 = '00:00';
                $time2 = substr($previous_btw_clockInOut_time['att_checkout'], 11, 5);

                $diffTime = differenceInHours($time1, $time2);
                $timestamp = strtotime($value['come_late_time']) + 60 * 60 * $diffTime;

                $time = date('H:i:s', $timestamp);

                $firstTime = strtotime($time1);
                $secondTime = strtotime($time2);
                $thirdTime = $secondTime - $firstTime;

                $find_time_stamp_to_Min = strtotime($time1) + 60 * 60 * $thirdTime / 60 / 60;
                $timeMin = date('H:i', $find_time_stamp_to_Min);

                $value['come_late_time'] = $time;
                $value['reduce_time'] = $timeMin;
            }
            $shift_work[] = $value;
        }

        foreach ($user_data as $value) {
            $empty_clock = Attendance::find()->where(['user_id' => $value['id']])->andWhere(['att_date' => date('Y-m-d')])->one();
            if (empty($empty_clock)) {
                $user_name[] = $value;
            }
        }


        $user_leave = (new Query())
            ->select('"user".id, "user".full_name, "leave".user_id, "leave".username, "leave".leave_start, "leave".description, "leave".leave_type, "leave_type".id, "leave_type".leave_type_name')
            ->from('user')
            ->leftJoin('leave', '"user".id = "leave".user_id')
            ->leftJoin('leave_type', '"leave".leave_type = "leave_type".id')
            ->where(['leave_start' => date('Y-m-d')])
            ->andWhere(['approve_status' => 2])
            ->all();

        return $this->render('index', [
            'timescan' => $shift_work,
            'user_leave' => $user_leave,
            'no_scan_user' => $user_name,
        ]);
    }

    public function actionJsonLeaveCalendar()
    {

        Yii::$app->response->format = Response::FORMAT_JSON;

        $leave_model = Leave::find()
            ->where(['approve_status' => 2])
            ->all();

        $holiday_model = Holiday::find()->all();

        $events = [];

        if (!is_null($leave_model)) {
            foreach ($leave_model AS $item) {
                $name = User::findOne(['id' => $item->user_id]);
                $first_name = substr($name->full_name, 0, strpos($name->full_name, ' '));
                $Event = new Event();
                $Event->id = $item->id;
                $Event->title = $first_name . ' ' . $item->description;
                $Event->start = $item->leave_start;
                $Event->end = $item->leave_start;
                $Event->color = '#0485EA';
                $events[] = $Event;
            }
        }

        if (!is_null($holiday_model)) {
            foreach ($holiday_model as $item) {
                $Event = new Event();
                $Event->id = $item->id;
                $Event->title = $item->holiday_name;
                $Event->start = $item->holiday_date;
                $Event->end = $item->holiday_date;
                $Event->color = '#FC0383';
                $events[] = $Event;
            }
        }
        return $events;
    }

    public function actionJsonLeaveHolidayCalendar()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = Holiday::find()->all();

        $events = [];

        if (!is_null($model)) {
            foreach ($model AS $item) {
                $name = User::findOne(['id' => $item->user_id]);
                $first_name = substr($name->full_name, 0, strpos($name->full_name, ' '));
                $Event = new Event();
                $Event->id = $item->id;
                $Event->title = $first_name . ' ' . $item->description;
                $Event->start = $item->leave_start;
                $timeScan = (new Query())
                    ->select('"user".id, "user".username,  "user".full_name, "attendance".user_id, "attendance".att_date, "attendance".att_check')
                    ->from('user')
                    ->leftJoin('attendance', '"user".id = "attendance".user_id')
                    ->where(['att_date' => date('Y-m-d')])
                    ->orderBy(['att_check' => SORT_ASC])
                    ->all();

                return $this->render('index', [
                    'timeScan' => $timeScan
                ]);
                $Event->end = $item->leave_start;

                $events[] = $Event;
            }
        }
        return $events;
    }

    public function actionMonitorScan()
    {
        $this->layout = 'mylayout.php';
        $user_data = User::find()->where(['status' => 10])->orderBy(['id' => 4])->all();
        $user_name = [];
        $shift_work = [];

        $timeScan = (new Query())
            ->select('"user".id, "user".username,  "user".full_name, "attendance".user_id, "attendance".att_date, "attendance".att_check, "attendance".att_checkout,
            "shift".id, "shift".come_late_time, "shift".back_early_time')
            ->from('user')
            ->leftJoin('attendance', '"user".id = "attendance".user_id')
            ->leftJoin('shift', '"user".role_shift = "shift".id')
            ->where(['att_date' => date('Y-m-d')])
            ->andWhere(['leave_id' => null])
            ->andWhere(['outside_id' => null])
            ->orderBy(['att_check' => SORT_ASC])
            ->all();

        foreach ($timeScan as $value) {
            $date = date_create($value['att_date']);
            date_modify($date, '-1 day');
            $trueDate = date_format($date, 'Y-m-d');

            $previous_btw_clockInOut_time = Attendance::find()->where(['att_date' => $trueDate])->andWhere(['user_id' => $value['user_id']])->one();
            $value['reduce_time'] = 0;
            if (substr($previous_btw_clockInOut_time['att_checkout'], 11) > '00:00:01' && substr($previous_btw_clockInOut_time['att_checkout'], 11) < '05:00:00') {
                $time1 = '00:00';
                $time2 = substr($previous_btw_clockInOut_time['att_checkout'], 11, 5);

                $diffTime = differenceInHours($time1, $time2);
                $timestamp = strtotime($value['come_late_time']) + 60 * 60 * $diffTime;

                $time = date('H:i:s', $timestamp);

                $firstTime = strtotime($time1);
                $secondTime = strtotime($time2);
                $thirdTime = $secondTime - $firstTime;

                $find_time_stamp_to_Min = strtotime($time1) + 60 * 60 * $thirdTime / 60 / 60;
                $timeMin = date('H:i', $find_time_stamp_to_Min);


                $value['come_late_time'] = $time;
                $value['reduce_time'] = $timeMin;
            }
            $shift_work[] = $value;
        }

        foreach ($user_data as $value) {
            $empty_clock = Attendance::find()->where(['user_id' => $value['id']])->andWhere(['att_date' => date('Y-m-d')])->one();
            if (empty($empty_clock)) {
                $user_name[] = $value;
            }
        }

        $user_leave = (new Query())
            ->select('"user".id, "user".full_name, "leave".user_id, "leave".username, "leave".leave_start, "leave".description, "leave".leave_type, "leave_type".id, "leave_type".leave_type_name')
            ->from('user')
            ->leftJoin('leave', '"user".id = "leave".user_id')
            ->leftJoin('leave_type', '"leave".leave_type = "leave_type".id')
            ->where(['leave_start' => date('Y-m-d')])
            ->andWhere(['approve_status' => 2])
            ->all();

        return $this->render('monitor-scan', [
            'timescan' => $shift_work,
            'no_scan_user' => $user_name,
            'user_leave' => $user_leave,
        ]);
    }
}

function differenceInHours($startdate, $enddate)
{
    $starttimestamp = strtotime($startdate);
    $endtimestamp = strtotime($enddate);
    $difference = abs($endtimestamp - $starttimestamp) / 3600;
    return $difference;
}

if (!empty($_POST["submit"])) {
    $hours_difference = differenceInHours($_POST["startdate"], $_POST["enddate"]);
    $message = "The Difference is " . $hours_difference . " hours";
}
?>