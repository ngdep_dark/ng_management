<?php

namespace app\modules\attd\controllers;

use Yii;
use app\modules\attd\models\Holiday;
use app\modules\attd\models\HolidaySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;

/**
 * HolidayController implements the CRUD actions for Holiday model.
 */
class HolidayController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Holiday models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HolidaySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Holiday model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Holiday model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $new_year = $this->findHoliday();
        if (empty($new_year)) {
            $holiday = [
                ['holiday_name' => 'วันหยุดปีใหม่', 'description' => 'วันหยุดปีใหม่', 'holiday_date' => date('Y') . '-01-01'],
            ];
            foreach ($holiday as $item) {
                $holiday_model = new Holiday();
                $holiday_model->holiday_name = $item['holiday_name'];
                $holiday_model->description = $item['description'];
                $holiday_model->holiday_date = $item['holiday_date'];
                $holiday_model->save();
            }
        }

        $model = new Holiday();
        if ($model->load(Yii::$app->request->post())) {
            $check_exist = Holiday::find()->where(['holiday_date' => $model->holiday_date])->scalar();
            if (!empty($check_exist)){
                $this->actionAlert('warning', 'แจ้งเตือน', 'มีวันหยุดนี้อยู่ในระบบแล้ว');
                return $this->render('create',[
                    'model' => $model
                ]);

            }else{
                $model->save();
                return $this->redirect(['holiday/index']);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionAlert($type, $title, $message)
    {
        return Yii::$app->getSession()->setFlash('alert', [
            'type' => $type,
            'duration' => 3000,
            'icon' => ' glyphicon glyphicon-th-large',
            'title' => Yii::t('app', Html::encode($title)),
            'message' => Yii::t('app', Html::encode($message)),
            'positonY' => 'top',
            'positonX' => 'right'
        ]);
    }

    /**
     * Updates an existing Holiday model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Holiday model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Holiday model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Holiday the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Holiday::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private function findHoliday()
    {
        return Holiday::find()->where(['holiday_date' => date('Y') . '-01-01'])->scalar();
    }
}
