<?php

namespace app\modules\attd\controllers;

use app\models\User;
use app\modules\attd\models\Attendance;
use app\modules\attd\models\Shift;
use app\modules\attd\models\AttendanceCronLog;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;
use yii\web\Response;

class  ApiController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
//        $behaviors['authenticator'] = [
//            'class' => HttpBasicAuth::className(),
//        ];

        return ArrayHelper::merge([
            [
                'class' => Cors::className(),
                'cors' => [
                    'Access-Control-Request-Method' => ['GET', 'HEAD', 'OPTIONS'],
                ],
            ],
        ], $behaviors);
        // return $behaviors;
    }

    public function actions()
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Headers: authorization, content-type');
        return parent::actions();
    }

    public function actionSetAtd()
    {
        $post = \Yii::$app->request->post();
        $minTime = '00:00:01';
        $maxTime = '05:00:01';
        $preOut = '12:00:00';
        
        $cronLog = new AttendanceCronLog();
        $cronLog->record = count($post);
        $cronLog->save();
        
        foreach ($post as $model) {
            $dateServer = substr($model['date_time'], 0, 10);
            $timeServer = substr($model['date_time'], 11);
            //สูตรเช็คเวลาออกหลังเที่ยงคืน
            if ($timeServer >= $minTime && $timeServer <= $maxTime) {
                $date = date_create($dateServer);
                date_modify($date, '-1 day');
                $trueDate = date_format($date, 'Y-m-d');
                $check_clockOut = Attendance::find()->where(['user_id' => $model['user_id']])->andWhere(['att_date' => $trueDate])->one();
                if (!empty($check_clockOut)) {
                    if (empty($check_clockOut->att_checkout)) {
                        $check_clockOut->att_checkout = $model['date_time'];
                        $check_clockOut->log_id = $cronLog->id;
                        $check_clockOut->save(false);
                    }
                }
            } //สูตรลงเวลาปกติ
            else {
                $data = \Yii::$app->db->createCommand("SELECT * from attendance att WHERE att.user_id =:id  and DATE(att.att_check) =:date ")->bindValues(['id' => $model['user_id'], 'date' => $dateServer])->queryOne();
                if (empty($data)) {
                    $att = new  Attendance();
                    $att->att_check = $model['date_time'];
                    $att->user_id = $model['user_id'];
                    $att->att_date = $dateServer;
                    $att->log_id = $cronLog->id;
                    $att->save(false);

                } else {
                    $check_att = Attendance::find()->where(['user_id' => $model['user_id']])->andWhere(['att_date' => $dateServer])->one();
                    if (!empty($check_att->att_check)) {
                        if ($check_att->att_check != $model['date_time'] && substr($model['date_time'], 11) <= $preOut) {
                            $check_att->att_check = $model['date_time'];
                            $check_att->log_id = $cronLog->id;
                            $check_att->save(false);
                        } elseif ($check_att->att_check != $model['date_time'] && substr($model['date_time'], 11) >= $preOut) {
                            $check_att->att_checkout = $model['date_time'];
                            $check_att->log_id = $cronLog->id;
                            $check_att->save(false);
                        }
                    }
                }
            }
        }
    }

    public function actionCheckClockOutPreviousDay($id, $date)
    {
        $date = date_create($date);
        date_modify($date, '-1 day');
        $trueDate = date_format($date, 'Y-m-d');
        $check_ClockOut_Previous_date = Attendance::find()->where(['user_id' => $id])->andWhere(['att_date' => $trueDate])->one();
        if ($check_ClockOut_Previous_date['att_checkout'] == null) {
            $user_data = User::find()->where(['id' => $id])->one();
            $check_shift = Shift::find()->where(['id' => $user_data->role_shift])->one();
            $check_ClockOut_Previous_date['att_checkout'] = $trueDate . ' ' . $check_shift->back_early_time;
            $check_ClockOut_Previous_date->save(false);
        }
    }

    public function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function actionSetUsr()
    {
        $post = \Yii::$app->request->post();
        foreach ($post as $user) {
            $rndPass = $this->generateRandomString();
            $data = User::findOne($user['id']);
            if (empty($data)) {
                $users = new User();
                $users->id = $user['id'];
                $users->username = $user['username'];
                $users->full_name = $user['username'];
                $users->password = 123456789;
                $users->email = $user['username'] . "@gmail.com";
                $users->status = 10;
                $users->role_shift = 1;
                $users->setPassword($users->password);
                $users->generateAuthKey();
                $users->save(false);
            }
            var_dump($user);
            var_dump($rndPass);
        }
        exit();
    }
}