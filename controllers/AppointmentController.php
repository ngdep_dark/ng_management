<?php

namespace app\controllers;

use Yii;
use app\models\Appointment;
use app\models\AppointmentDetail;
use app\models\AppointmentType;
use app\models\User;
use app\models\AppointmentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\data\ArrayDataProvider;

/**
 * AppointmentController implements the CRUD actions for Appointment model.
 */
class AppointmentController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Appointment models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AppointmentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new Appointment();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'schoolName' => $model->getSchoolData(),
            'appointmentType' => $model->getAppointmentData()
        ]);
    }

    /**
     * Displays a single Appointment model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Appointment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Appointment();

        $post = Yii::$app->request->post();
        if ($model->load($post)) {
            if ($model->save()) {
                $this->saveWorker($model, 'create');
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionMyAppointment($id)
    {
        $model = AppointmentDetail::find()
            ->leftJoin('appointment', '"appointment".id = "appointment_detail".appointment_id')
            ->where(['>=', '"appointment".appointment_start', 'NOW()'])
            ->andWhere(['user_id' => $id])
            ->orderBy(['"appointment".appointment_start' => SORT_ASC])
            ->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $model,
            'pagination' => FALSE,
        ]);
        
        return $this->render('my-appointment', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Updates an existing Appointment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();
        if ($model->load($post)) {
            if ($model->save()) {
                $this->saveWorker($model, 'update');
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    private function saveWorker($model, $scenario) {
        if ($scenario === 'update') {
            AppointmentDetail::deleteAll([
                'appointment_id' => $model->id 
            ]);
            foreach ($model->workers as $workerId) {
                $appointmentDetail = new AppointmentDetail();
                $appointmentDetail->appointment_id = $model->id;
                $appointmentDetail->user_id = $workerId;
                $appointmentDetail->save();
            }
        } else {
            foreach ($model->workers as $workerId) {
                $appointmentDetail = new AppointmentDetail();
                $appointmentDetail->appointment_id = $model->id;
                $appointmentDetail->user_id = $workerId;
                $appointmentDetail->save();
            }
        }
        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * Deletes an existing Appointment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public static function getAppointment($list = false) {
        $get = Yii::$app->request->get();
        // Yii::$app->response->format = Response::FORMAT_JSON;
        $appointment = new Appointment;
        $schoolData = $appointment->getSchoolData();
        
        $appointmentTypes = Appointment::find()
            ->select('appointment_type_id')
            ->distinct()
            ->orderBy('appointment_type_id')
            ->all();

        $data = [];

        foreach($appointmentTypes as $appointmentTypeId) {
            $events = [];
            $model = Appointment::find()
                ->where([
                    'appointment_type_id' => $appointmentTypeId
                ])
                ->all();
            
            foreach($model as $appointment) {
                $schoolName = $schoolData[$appointment->school_id];
                $title = $schoolName;
                
                $nicknameList = '';
                foreach($appointment->appointmentDetails as $appointmentDetails) 
                {
                    $nicknameList .= ' ' . $appointmentDetails->user->nickname;
                }

                $event = [                    
                    'title' => $title,
                    'start' => $appointment->appointment_start, 
                    'end' => $appointment->appointment_end,
                ];
                
                if ($list == true) {
                    // if (isset($get) && array_key_exists('list', $get) && $get['list'] == true) {
                    $event['title'] .= $nicknameList;
                } else {
                    $event['description'] = '<div><i class="fa fa-user"></i> : '. $nicknameList .'</div><div><i class="fa fa-tags"></i> : '.$appointment->appointmentType->name.'</div><div><i class="fa fa-map"></i> : '. $appointment->schoolName .'</div><div><i class="fa fa fa-calendar"></i> : '. $appointment->appointment_start .' - '. $appointment->appointment_end .'</div><div>รายละเอียด</div>' . $appointment->description;
                }
                
                $events[] = $event; 
            }

            $color = AppointmentType::find()
                ->select('color')
                ->where([
                    'id' => $appointmentTypeId
                ])
                ->scalar();
            
            $data['eventSources'][] = ['events' => $events, 'color' => $color]; 
        }

        return $data;
    }

    /**
     * Finds the Appointment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Appointment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Appointment::findOne($id)) !== null) {
            $model->workers = $model->workersValue;
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}