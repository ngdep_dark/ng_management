<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class StoreAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    'css/toastr.min.css',
       // 'https://unpkg.com/vue-multiselect@2.1.0/dist/vue-multiselect.min.css'
//     'js/vue/select2/vue-multiselect.min.css',
    ];
    public $js = [
        'js/vue/vuejs.js',
         'js/vue/axios.min.js',
       // 'https://unpkg.com/axios/dist/axios.min.js',
        'js/vue/toastr.min.js',
        'js/vue/select2/vue-multiselect.min.js',
        'js/vue/select2/vue-select.js',
        'js/vue/app-store.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'dmstr\web\AdminLteAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
