<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ThemesAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
//        'css/site.css',
//        'themes/css/bootstrap.min.css',
//    'themes/css/bootstrap.min.css',
        'themes/css/icons.css',
        'themes/css/style.css'
    ];
    public $js = [
//        'themes/js/bootstrap.min.js',
       // 'themes/js/jquery.min.js',
        'themes/js/bootstrap.min.js',
        'themes/js/fastclick.js',
        'themes/js/jquery.slimscroll.js',
        'themes/js/jquery.blockUI.js',
        'themes/js/waves.js',
        'themes/js/wow.min.js',
        'themes/js/jquery.nicescroll.js',
        'themes/js/jquery.scrollTo.min.js',
        'themes/js/rs-app.js',
    ];
    public $depends = [
         'yii\web\YiiAsset',

        'yii\bootstrap\BootstrapAsset',
    ];
}
