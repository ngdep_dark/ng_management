<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        // 'css/bootstrap.min.css',
        'css/croppie.css',
        'css/fullcalendar.min.css',
        'css/summernote2.css',
    ];
    public $js = [
        'js/croppie/croppie.js',
        'js/moment.min.js',
        'js/fullcalendar.min.js',
        'js/locale-all.js',
        'js/summernote.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}