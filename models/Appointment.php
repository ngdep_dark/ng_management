<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;

/**
 * This is the model class for table "appointment".
 *
 * @property int $id
 * @property int $school_id
 * @property int $appointment_type_id
 * @property string $appointment_start
 * @property string $appointment_end
 * @property string $created_at
 * @property string $updated_at
 * @property string $created_by
 * @property string $updated_by
 *
 * @property AppointmentDetail[] $appointmentDetails
 */
class Appointment extends \yii\db\ActiveRecord
{
    public $workers;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'appointment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['school_id', 'appointment_type_id', 'workers', 'appointment_start', 'appointment_end'], 'required'],
            [['school_id', 'appointment_type_id'], 'default', 'value' => null],
            [['school_id', 'appointment_type_id'], 'integer'],
            [['created_at', 'updated_at', 'created_by', 'updated_by', 'description'], 'safe'],
        ];
    }

    public function behaviors(){
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className()
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'school_id' => 'School ID',
            'appointment_type_id' => 'Appointment Type',
            'appointment_start' => 'Appointment Start',
            'appointment_end' => 'Appointment End',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

   
    public function getSchoolName()
    {
        return ArrayHelper::getValue($this->getSchoolData(),$this->school_id);
    }

    public function getSchoolData()
    {   
        return [];
        $url = "http://scb.futuremlm.com/api/schools";

        $client = new Client(['baseUrl' => $url]);
        $response = $client->createRequest()
            ->setFormat(Client::FORMAT_JSON)
            ->send();

        $schoolRaw = json_decode($response->content, true);
       
        $school = ArrayHelper::map($schoolRaw, 'id', 'name');
        unset($school[64]);

        return $school;
    }

    // public function getAppointmentName()
    // {
    //     return ArrayHelper::getValue($this->getAppointmentTypes(),$this->appointment_type);
    // }

    public function getAppointmentData()
    {
        return ArrayHelper::map(AppointmentType::find(['id','name'])->orderBy('id')->all(), 'id', 'name');
    }

    // public function getAppointmentType()
    // {
    //     return [
    //         1 => 'ถ่ายรูป',
    //         2 => 'ถ่ายรูป2',
    //         3 => 'ถ่ายรูป3',
    //         4 => 'ถ่ายรูป4',
    //     ];
    // }

    // query for select2 multiple value
    public function getWorkersValue(){
        return ArrayHelper::map($this->appointmentDetails, 'id', 'user_id');
    }

    public function getWorkersData()
    {
        return ArrayHelper::map(User::find(['id','nickname'])->orderBy('id')->all(), 'id', 'nickname');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppointmentDetails()
    {
        return $this->hasMany(AppointmentDetail::className(), ['appointment_id' => 'id']);
    }

    public function getAppointmentType()
    {
        return $this->hasOne(AppointmentType::className(), ['id' => 'appointment_type_id']);
    }
}