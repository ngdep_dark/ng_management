<?php

use yii\db\Migration;

/**
 * Handles adding description to table `{{%appointment}}`.
 */
class m190418_094603_add_description_column_to_appointment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%appointment}}', 'description', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%appointment}}', 'description');
    }
}
