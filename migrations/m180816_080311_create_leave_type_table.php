<?php

use yii\db\Migration;

/**
 * Handles the creation of table `leave_type`.
 */
class m180816_080311_create_leave_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('leave_type', [
            'id' => $this->primaryKey(),
            'leave_type_name' => $this->string(),
            'created_at' => $this->dateTime(),
            'created_by' => $this->dateTime()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('leave_type');
    }
}
