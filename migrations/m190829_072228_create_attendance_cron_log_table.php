<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%attendance_cron_log}}`.
 */
class m190829_072228_create_attendance_cron_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%attendance_cron_log}}', [
            'id' => $this->primaryKey(),
            'type' => $this->integer(),
            'created_at' => $this->text(),
            'updated_at' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%attendance_cron_log}}');
    }
}
