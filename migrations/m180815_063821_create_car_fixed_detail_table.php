<?php

use yii\db\Migration;

/**
 * Handles the creation of table `car_fixed_detail`.
 * Has foreign keys to the tables:
 *
 * - `car_fixed`
 */
class m180815_063821_create_car_fixed_detail_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('car_fixed_detail', [
            'id' => $this->primaryKey(),
            'car_fixed_id' => $this->integer()->notNull(),
            'name' => $this->string(),
            'amount' => $this->integer(),
            'price' => $this->integer(),
        ]);

        // creates index for column `car_fixed_id`
        $this->createIndex(
            'idx-car_fixed_detail-car_fixed_id',
            'car_fixed_detail',
            'car_fixed_id'
        );

        // add foreign key for table `car_fixed`
        $this->addForeignKey(
            'fk-car_fixed_detail-car_fixed_id',
            'car_fixed_detail',
            'car_fixed_id',
            'car_fixed',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `car_fixed`
        $this->dropForeignKey(
            'fk-car_fixed_detail-car_fixed_id',
            'car_fixed_detail'
        );

        // drops index for column `car_fixed_id`
        $this->dropIndex(
            'idx-car_fixed_detail-car_fixed_id',
            'car_fixed_detail'
        );

        $this->dropTable('car_fixed_detail');
    }
}
