<?php

use yii\db\Migration;

/**
 * Handles adding record to table `{{%attendance_cron_log}}`.
 */
class m191119_061249_add_record_column_to_attendance_cron_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%attendance_cron_log}}', 'record', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%attendance_cron_log}}', 'record');
    }
}
