<?php

use yii\db\Migration;

/**
 * Handles the creation of table `attendance_group_description`.
 */
class m180828_052919_create_attendance_group_description_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('attendance_group_description', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'group_id' =>$this->integer(),
            'group_leader' => $this->boolean()
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-attendance_group_description-user_id',
            'attendance_group_description',
            'user_id'
        );

        // add foreign key for table `attendance_group_description`
        $this->addForeignKey(
            'fk-attendance_group_description-user_id',
            'attendance_group_description',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `group_id`
        $this->createIndex(
            'idx-attendance_group_description-group_id',
            'attendance_group_description',
            'group_id'
        );

        // add foreign key for table `attendance_group_description`
        $this->addForeignKey(
            'fk-attendance_group_description-group_id',
            'attendance_group_description',
            'group_id',
            'attendance_group',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `post`
        $this->dropForeignKey(
            'fk-attendance_group_description-user_id',
            'attendance_group_description'
        );

        // drops index for column `post_id`
        $this->dropIndex(
            'idx-attendance_group_description-user_id',
            'attendance_group_description'
        );

        // drops foreign key for table `post`
        $this->dropForeignKey(
            'fk-attendance_group_description-group_id',
            'attendance_group_description'
        );

        // drops index for column `post_id`
        $this->dropIndex(
            'idx-attendance_group_description-group_id',
            'attendance_group_description'
        );

        $this->dropTable('attendance_group_description');
    }
}
