<?php

use yii\db\Migration;

/**
 * Class m180719_094412_add_cloum_user
 */
class m180719_094412_add_cloum_user extends Migration
{


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('user', 'full_name', $this->string());
        $this->addColumn('user', 'role_shift', $this->integer());
        $this->addColumn('user', 'phone', $this->string());
    }

    public function down()
    {
        $this->dropColumn('user', 'full_name');
        $this->dropColumn('user', 'role_shift');
        $this->dropColumn('user', 'phone');

    }
}
