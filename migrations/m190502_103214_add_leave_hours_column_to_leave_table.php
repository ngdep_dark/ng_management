<?php

use yii\db\Migration;

/**
 * Handles adding leave_hours to table `{{%leave}}`.
 */
class m190502_103214_add_leave_hours_column_to_leave_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('leave', 'leave_hours', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('leave', 'leave_hours');

    }
}
