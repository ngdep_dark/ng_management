<?php

use yii\db\Migration;

/**
 * Handles the creation of table `attendance_overtime`.
 * Has foreign keys to the tables:
 *
 * - `attendance`
 */
class m180904_094054_create_attendance_overtime_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('attendance_overtime', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'clock_in' => $this->datetime(),
            'clock_out' => $this->datetime(),
            'retire_time' => $this->datetime(),
            'ot_duration' => $this->integer(),
            'status' => $this->integer(),
            'attendance_overtime_id' => $this->integer()->notNull(),
            'approve_by' => $this->string(),
            'created_at' => $this->integer(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('attendance_overtime');
    }
}
