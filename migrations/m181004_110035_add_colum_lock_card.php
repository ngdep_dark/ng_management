<?php

use yii\db\Migration;

/**
 * Class m181004_110035_add_colum_lock_card
 */
class m181004_110035_add_colum_lock_card extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product', 'lock_card_bad', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product','lock_card_bad');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181004_110035_add_colum_lock_card cannot be reverted.\n";

        return false;
    }
    */
}
