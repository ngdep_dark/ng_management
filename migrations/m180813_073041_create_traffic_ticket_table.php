<?php

use yii\db\Migration;

/**
 * Handles the creation of table `traffic_ticket`.
 * Has foreign keys to the tables:
 *
 * - `car_booking`
 * - `car_booking_location`
 * - `allegation_type`
 */
class m180813_073041_create_traffic_ticket_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('traffic_ticket', [
            'id' => $this->primaryKey(),
            'car_booking_id' => $this->integer()->notNull(),
            'tid' => $this->string(),
            'accuse_at' => $this->datetime(),
            'due_pay_at' => $this->datetime(),
            'car_booking_location_id' => $this->integer()->notNull(),
            'allegation_type_id' => $this->integer()->notNull(),
            'price' => $this->integer(),
            'payment_at' => $this->datetime(),
            'detail' => $this->string(),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

//        // creates index for column `car_booking_id`
        $this->createIndex(
            'idx-traffic_ticket-car_booking_id',
            'traffic_ticket',
            'car_booking_id'
        );

        // add foreign key for table `car_booking`
        $this->addForeignKey(
            'fk-traffic_ticket-car_booking_id',
            'traffic_ticket',
            'car_booking_id',
            'car_booking',
            'id',
            'CASCADE'
        );

        // creates index for column `car_booking_location_id`
        $this->createIndex(
            'idx-traffic_ticket-car_booking_location_id',
            'traffic_ticket',
            'car_booking_location_id'
        );

        // add foreign key for table `car_booking_location`
        $this->addForeignKey(
            'fk-traffic_ticket-car_booking_location_id',
            'traffic_ticket',
            'car_booking_location_id',
            'car_booking_location',
            'id',
            'CASCADE'
        );

        // creates index for column `allegation_type_id`
        $this->createIndex(
            'idx-traffic_ticket-allegation_type_id',
            'traffic_ticket',
            'allegation_type_id'
        );

        // add foreign key for table `allegation_type`
        $this->addForeignKey(
            'fk-traffic_ticket-allegation_type_id',
            'traffic_ticket',
            'allegation_type_id',
            'allegation_type',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `car_booking`
        $this->dropForeignKey(
            'fk-traffic_ticket-car_booking_id',
            'traffic_ticket'
        );

        // drops index for column `car_booking_id`
        $this->dropIndex(
            'idx-traffic_ticket-car_booking_id',
            'traffic_ticket'
        );

        // drops foreign key for table `car_booking_location`
        $this->dropForeignKey(
            'fk-traffic_ticket-car_booking_location_id',
            'traffic_ticket'
        );

        // drops index for column `car_booking_location_id`
        $this->dropIndex(
            'idx-traffic_ticket-car_booking_location_id',
            'traffic_ticket'
        );

        // drops foreign key for table `allegation_type`
        $this->dropForeignKey(
            'fk-traffic_ticket-allegation_type_id',
            'traffic_ticket'
        );

        // drops index for column `allegation_type_id`
        $this->dropIndex(
            'idx-traffic_ticket-allegation_type_id',
            'traffic_ticket'
        );

        $this->dropTable('traffic_ticket');
    }
}
