<?php

use yii\db\Migration;

/**
 * Class m181001_083053_change_price_type_car_fixed_detail
 */
class m181001_083053_change_price_type_car_fixed_detail extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('car_fixed_detail', 'price', 'real');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('car_fixed_detail', 'price', 'integer');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181001_083053_change_price_type_car_fixed_detail cannot be reverted.\n";

        return false;
    }
    */
}
