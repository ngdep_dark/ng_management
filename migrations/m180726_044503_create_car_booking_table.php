<?php

use yii\db\Migration;

/**
 * Handles the creation of table `car_booking`.
 * Has foreign keys to the tables:
 *
 * - `car`
 * - `car_booking_location`
 * - `car_booking_category`
 */
class m180726_044503_create_car_booking_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('car_booking', [
            'id' => $this->primaryKey(),
            'datetime_start' => $this->dateTime(),
            'datetime_end' => $this->dateTime(),
            'miles_start' => $this->integer(),
            'miles_end' => $this->integer(),
            'status' => $this->integer(),
            'car_id' => $this->integer()->notNull(),
            'car_booking_location_id' => $this->integer()->notNull(),
            'car_booking_category_id' => $this->integer()->notNull(),
            'detail' => $this->string(),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

        // creates index for column `car_id`
        $this->createIndex(
            'idx-car_booking-car_id',
            'car_booking',
            'car_id'
        );

        // add foreign key for table `car`
        $this->addForeignKey(
            'fk-car_booking-car_id',
            'car_booking',
            'car_id',
            'car',
            'id',
            'CASCADE'
        );

        // creates index for column `car_booking_location_id`
        $this->createIndex(
            'idx-car_booking-car_booking_location_id',
            'car_booking',
            'car_booking_location_id'
        );

        // add foreign key for table `car_booking_location`
        $this->addForeignKey(
            'fk-car_booking-car_booking_location_id',
            'car_booking',
            'car_booking_location_id',
            'car_booking_location',
            'id',
            'CASCADE'
        );

        // creates index for column `car_booking_category_id`
        $this->createIndex(
            'idx-car_booking-car_booking_category_id',
            'car_booking',
            'car_booking_category_id'
        );

        // add foreign key for table `car_booking_category`
        $this->addForeignKey(
            'fk-car_booking-car_booking_category_id',
            'car_booking',
            'car_booking_category_id',
            'car_booking_category',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `car`
        $this->dropForeignKey(
            'fk-car_booking-car_id',
            'car_booking'
        );

        // drops index for column `car_id`
        $this->dropIndex(
            'idx-car_booking-car_id',
            'car_booking'
        );

        // drops foreign key for table `car_booking_location`
        $this->dropForeignKey(
            'fk-car_booking-car_booking_location_id',
            'car_booking'
        );

        // drops index for column `car_booking_location_id`
        $this->dropIndex(
            'idx-car_booking-car_booking_location_id',
            'car_booking'
        );

        // drops foreign key for table `car_booking_category`
        $this->dropForeignKey(
            'fk-car_booking-car_booking_category_id',
            'car_booking'
        );

        // drops index for column `car_booking_category_id`
        $this->dropIndex(
            'idx-car_booking-car_booking_category_id',
            'car_booking'
        );

        $this->dropTable('car_booking');
    }
}
