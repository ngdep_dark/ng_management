<?php

use yii\db\Migration;

/**
 * Handles adding last_check_miles to table `car_fixed`.
 */
class m181021_055313_add_last_check_miles_column_to_car_fixed_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('car_fixed', 'last_check_miles', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('car_fixed', 'last_check_miles');
    }
}
