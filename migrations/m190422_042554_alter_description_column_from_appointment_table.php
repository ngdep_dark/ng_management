<?php

use yii\db\Migration;

/**
 * Class m190422_042554_alter_description_column_from_appointment_table
 */
class m190422_042554_alter_description_column_from_appointment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('appointment', 'description', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('appointment', 'description', $this->string());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190422_042554_alter_description_column_from_appointment_table cannot be reverted.\n";

        return false;
    }
    */
}