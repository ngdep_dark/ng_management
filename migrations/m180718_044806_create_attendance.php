<?php

use yii\db\Migration;

/**
 * Class m180718_044806_create_attendance
 */
class m180718_044806_create_attendance extends Migration
{
    /**
     * {@inheritdoc}
     */
//    public function safeUp()
//    {
//
//    }
//
//    /**
//     * {@inheritdoc}
//     */
//    public function safeDown()
//    {
//        echo "m180718_044806_create_attendance cannot be reverted.\n";
//
//        return false;
//    }


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('attendance', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'att_date'=> $this->date(),
            'att_check' => $this->datetime(),
            'att_checkout' => $this->datetime(),
            'overtime' => $this->string(),
            'att_status' => $this->string(),
            'updated_at' => $this->dateTime(),
            'updated_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'created_by' => $this->integer(),
            'leave_id' => $this->integer(),
            'overtime_id' => $this->integer(),
        ]);
    }

    public function down()
    {
        $this->DropTable('attendance');
    }

}
