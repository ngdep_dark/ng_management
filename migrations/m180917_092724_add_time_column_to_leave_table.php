<?php

use yii\db\Migration;

/**
 * Handles adding time to table `leave`.
 */
class m180917_092724_add_time_column_to_leave_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('leave', 'start_time', $this->time());
        $this->addColumn('leave', 'end_time', $this->time());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('leave', 'start_time');
        $this->dropColumn('leave', 'end_time');
    }
}
