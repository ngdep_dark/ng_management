<?php

use yii\db\Migration;

/**
 * Handles adding driver to table `car_booking`.
 * Has foreign keys to the tables:
 *
 * - `user`
 */
class m180916_163046_add_driver_column_to_car_booking_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('car_booking', 'driver', $this->integer());

        // creates index for column `driver`
        $this->createIndex(
            'idx-car_booking-driver',
            'car_booking',
            'driver'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-car_booking-driver',
            'car_booking',
            'driver',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-car_booking-driver',
            'car_booking'
        );

        // drops index for column `driver`
        $this->dropIndex(
            'idx-car_booking-driver',
            'car_booking'
        );

        $this->dropColumn('car_booking', 'driver');
    }
}
