<?php

use yii\db\Migration;

/**
 * Handles the creation of table `car_booking_passenger`.
 * Has foreign keys to the tables:
 *
 * - `car_booking`
 * - `user`
 */
class m180726_044649_create_car_booking_passenger_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('car_booking_passenger', [
            'id' => $this->primaryKey(),
            'car_booking_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

        // creates index for column `car_booking_id`
        $this->createIndex(
            'idx-car_booking_passenger-car_booking_id',
            'car_booking_passenger',
            'car_booking_id'
        );

        // add foreign key for table `car_booking`
        $this->addForeignKey(
            'fk-car_booking_passenger-car_booking_id',
            'car_booking_passenger',
            'car_booking_id',
            'car_booking',
            'id',
            'CASCADE'
        );

        // creates index for column `user_id`
        $this->createIndex(
            'idx-car_booking_passenger-user_id',
            'car_booking_passenger',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-car_booking_passenger-user_id',
            'car_booking_passenger',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `car_booking`
        $this->dropForeignKey(
            'fk-car_booking_passenger-car_booking_id',
            'car_booking_passenger'
        );

        // drops index for column `car_booking_id`
        $this->dropIndex(
            'idx-car_booking_passenger-car_booking_id',
            'car_booking_passenger'
        );

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-car_booking_passenger-user_id',
            'car_booking_passenger'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-car_booking_passenger-user_id',
            'car_booking_passenger'
        );

        $this->dropTable('car_booking_passenger');
    }
}
