<?php

use yii\db\Migration;

/**
 * Handles the creation of table `leave`.
 */
class m180816_022303_create_leave_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('leave', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'username' =>$this->string(),
            'leave_type' => $this->integer(),
            'leave_start' => $this->date(),
            'leave_end' => $this->date(),
            'description' => $this->string(),
            'approve_status' => $this->integer(),
            'created_at' => $this->date(),
            'create_by' => $this->date(),
            'approve_by' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('leave');
    }


}
