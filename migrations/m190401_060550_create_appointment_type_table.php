<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%appointment_type}}`.
 */
class m190401_060550_create_appointment_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%appointment_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'color' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%appointment_type}}');
    }
}