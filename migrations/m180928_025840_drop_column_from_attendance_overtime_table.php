<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `column_from_attendance_overtime`.
 */
class m180928_025840_drop_column_from_attendance_overtime_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('attendance_overtime', 'user_id');
        $this->dropColumn('attendance_overtime', 'clock_in');
        $this->dropColumn('attendance_overtime', 'clock_out');
        $this->dropColumn('attendance_overtime', 'retire_time');
        $this->dropColumn('attendance_overtime', 'created_at');

        $this->addColumn('attendance_overtime', 'overtime_date' , $this->date());
        $this->addColumn('attendance_overtime', 'start_time' , $this->time());
        $this->addColumn('attendance_overtime', 'end_time' , $this->time());
        $this->addColumn('attendance_overtime', 'created_time' , $this->dateTime());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('attendance_overtime', 'user_id' , $this->integer());
        $this->addColumn('attendance_overtime', 'clock_in' , $this->datetime());
        $this->addColumn('attendance_overtime', 'clock_out' , $this->datetime());
        $this->addColumn('attendance_overtime', 'retire_time' , $this->datetime());
        $this->addColumn('attendance_overtime', 'created_at' , $this->integer());

        $this->dropColumn('attendance_overtime', 'overtime_date');
        $this->dropColumn('attendance_overtime', 'start_time');
        $this->dropColumn('attendance_overtime', 'end_time');
        $this->dropColumn('attendance_overtime', 'created_time');
    }
}
