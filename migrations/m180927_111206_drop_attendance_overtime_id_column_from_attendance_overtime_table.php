<?php

use yii\db\Migration;

/**
 * Handles dropping attendance_overtime_id from table `attendance_overtime`.
 */
class m180927_111206_drop_attendance_overtime_id_column_from_attendance_overtime_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('attendance_overtime', 'attendance_overtime_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('attendance_overtime', 'attendance_overtime_id' , $this->integer()->notNull());
    }
}
