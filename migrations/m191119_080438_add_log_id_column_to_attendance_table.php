<?php

use yii\db\Migration;

/**
 * Handles adding log_id to table `{{%attendance}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%attendance_cron_log}}`
 */
class m191119_080438_add_log_id_column_to_attendance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%attendance}}', 'log_id', $this->integer());

        // creates index for column `log_id`
        $this->createIndex(
            '{{%idx-attendance-log_id}}',
            '{{%attendance}}',
            'log_id'
        );

        // add foreign key for table `{{%attendance_cron_log}}`
        $this->addForeignKey(
            '{{%fk-attendance-log_id}}',
            '{{%attendance}}',
            'log_id',
            '{{%attendance_cron_log}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%attendance_cron_log}}`
        $this->dropForeignKey(
            '{{%fk-attendance-log_id}}',
            '{{%attendance}}'
        );

        // drops index for column `log_id`
        $this->dropIndex(
            '{{%idx-attendance-log_id}}',
            '{{%attendance}}'
        );

        $this->dropColumn('{{%attendance}}', 'log_id');
    }
}