<?php

use yii\db\Migration;

/**
 * Handles adding userid to table `attendance_overtime`.
 */
class m181004_065755_add_userid_column_to_attendance_overtime_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('attendance_overtime', 'user_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('attendance_overtime', 'user_id');
    }
}
