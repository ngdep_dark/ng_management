<?php

use yii\db\Migration;

/**
 * Handles the creation of table `appointment`.
 */
class m190402_034037_create_appointment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('appointment', [
            'id' => $this->primaryKey(),
            'school_id' => $this->integer(),
            'appointment_type_id' => $this->integer()->notNull(),
            'appointment_start' => $this->dateTime(),
            'appointment_end' => $this->dateTime(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

        // creates index for column `appointment_id`
        $this->createIndex(
            'idx-appointment-appointment_type_id',
            'appointment',
            'appointment_type_id'
        );

        // add foreign key for table `appointment`
        $this->addForeignKey(
            'fk-appointment-appointment_type_id',
            'appointment',
            'appointment_type_id',
            'appointment_type',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `appointment`
        $this->dropForeignKey(
            'fk-appointment-appointment_type_id',
            'appointment'
        );

        // drops index for column `appointment_id`
        $this->dropIndex(
            'idx-appointment-appointment_type_id',
            'appointment'
        );

        $this->dropTable('appointment');
    }
}