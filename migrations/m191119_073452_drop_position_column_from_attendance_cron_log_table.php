<?php

use yii\db\Migration;

/**
 * Handles dropping position from table `{{%attendance_cron_log}}`.
 */
class m191119_073452_drop_position_column_from_attendance_cron_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%attendance_cron_log}}', 'updated_at');
        $this->dropColumn('{{%attendance_cron_log}}', 'type');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%attendance_cron_log}}', 'updated_at', $this->datetime());
        $this->addColumn('{{%attendance_cron_log}}', 'type', $this->integer());
    }
}