<?php

use yii\db\Migration;

/**
 * Class m180827_074443_add_calculator_order
 */
class m180827_074443_add_calculator_order extends Migration
{

    public function up()
    {
            $this->createTable('calculator_order',[
                'id'=>$this->primaryKey(),
                'amount'=>$this->integer(),
                'percent'=>$this->integer(),
                'cal_id'=>$this->integer(),
                'product_id'=>$this->integer(),
                'balance'=>$this->integer(),
                'amount_use'=>$this->integer(),
                'amount_take'=>$this->integer(),
                'amount_order'=>$this->integer(),
                'created_at' => $this->dateTime(),
                'updated_at' => $this->dateTime(),
            ]);
    }

    public function down()
    {
        $this->dropTable('calculator_order');
    }

}
