<?php

use yii\db\Migration;

/**
 * Handles adding fixed_count to table `car`.
 */
class m180815_061711_add_fixed_count_column_to_car_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('car', 'miles_last_check', $this->integer());
        $this->addColumn('car', 'insurance_date', $this->datetime());
        $this->addColumn('car', 'car_act_date', $this->datetime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('car', 'miles_last_check');
        $this->dropColumn('car', 'insurance_date');
        $this->dropColumn('car', 'car_act_date');
    }
}
