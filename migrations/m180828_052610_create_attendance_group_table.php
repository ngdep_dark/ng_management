<?php

use yii\db\Migration;

/**
 * Handles the creation of table `attendance_group`.
 */
class m180828_052610_create_attendance_group_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('attendance_group', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('attendance_group');
    }
}
