<?php

use yii\db\Migration;

/**
 * Class m180713_045609_shift
 */
class m180713_045609_shift extends Migration
{


    public function up()
    {
        $this->createTable('shift', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'come_late_time' => $this->time(),
            'back_early_time' => $this->time(),
            'no_attend_time' => $this->time(),
            'day_time_details' => $this->text(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

        $this->insert('shift', [
            'name' => 'ค่าเริ่มต้น',
            'come_late_time' => '09:00:00',
            'back_early_time' => '18:00:00',
            'no_attend_time' => '23:50:00',
            'day_time_details'=>''
        ]);
    }

    public function down()
    {
        $this->dropTable('shift');
    }
}
