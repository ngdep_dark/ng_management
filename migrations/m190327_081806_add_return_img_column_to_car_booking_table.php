<?php

use yii\db\Migration;

/**
 * Handles adding return_img to table `car_booking`.
 */
class m190327_081806_add_return_img_column_to_car_booking_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('car_booking', 'return_img', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('car_booking', 'return_img');
    }
}
