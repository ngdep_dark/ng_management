<?php

use yii\db\Migration;

/**
 * Handles the creation of table `car_fixed`.
 * Has foreign keys to the tables:
 *
 * - `car`
 * - `car_fixed_type`
 * - `garage`
 * - `user`
 */
class m180814_102307_create_car_fixed_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('car_fixed', [
            'id' => $this->primaryKey(),
            'car_id' => $this->integer()->notNull(),
            'car_fixed_type_id' => $this->integer()->notNull(),
            'garage_id' => $this->integer()->notNull(),
            //'price' => $this->integer(),
            'sender_fixed_by' => $this->integer()->notNull(),
            'fix_date' => $this->datetime(),
            'fixed_date' => $this->datetime(),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

        // creates index for column `car_id`
        $this->createIndex(
            'idx-car_fixed-car_id',
            'car_fixed',
            'car_id'
        );

        // add foreign key for table `car`
        $this->addForeignKey(
            'fk-car_fixed-car_id',
            'car_fixed',
            'car_id',
            'car',
            'id',
            'CASCADE'
        );

        // creates index for column `car_fixed_type_id`
        $this->createIndex(
            'idx-car_fixed-car_fixed_type_id',
            'car_fixed',
            'car_fixed_type_id'
        );

        // add foreign key for table `car_fixed_type`
        $this->addForeignKey(
            'fk-car_fixed-car_fixed_type_id',
            'car_fixed',
            'car_fixed_type_id',
            'car_fixed_type',
            'id',
            'CASCADE'
        );

        // creates index for column `garage_id`
        $this->createIndex(
            'idx-car_fixed-garage_id',
            'car_fixed',
            'garage_id'
        );

        // add foreign key for table `garage`
        $this->addForeignKey(
            'fk-car_fixed-garage_id',
            'car_fixed',
            'garage_id',
            'garage',
            'id',
            'CASCADE'
        );

        // creates index for column `sender_fixed_by`
        $this->createIndex(
            'idx-car_fixed-sender_fixed_by',
            'car_fixed',
            'sender_fixed_by'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-car_fixed-sender_fixed_by',
            'car_fixed',
            'sender_fixed_by',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `car`
        $this->dropForeignKey(
            'fk-car_fixed-car_id',
            'car_fixed'
        );

        // drops index for column `car_id`
        $this->dropIndex(
            'idx-car_fixed-car_id',
            'car_fixed'
        );

        // drops foreign key for table `car_fixed_type`
        $this->dropForeignKey(
            'fk-car_fixed-car_fixed_type_id',
            'car_fixed'
        );

        // drops index for column `car_fixed_type_id`
        $this->dropIndex(
            'idx-car_fixed-car_fixed_type_id',
            'car_fixed'
        );

        // drops foreign key for table `garage`
        $this->dropForeignKey(
            'fk-car_fixed-garage_id',
            'car_fixed'
        );

        // drops index for column `garage_id`
        $this->dropIndex(
            'idx-car_fixed-garage_id',
            'car_fixed'
        );

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-car_fixed-sender_fixed_by',
            'car_fixed'
        );

        // drops index for column `sender_fixed_by`
        $this->dropIndex(
            'idx-car_fixed-sender_fixed_by',
            'car_fixed'
        );

        $this->dropTable('car_fixed');
    }
}
