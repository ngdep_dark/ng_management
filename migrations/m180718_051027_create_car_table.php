<?php

use yii\db\Migration;

/**
 * Handles the creation of table `car`.
 * Has foreign keys to the tables:
 *
 * - `car_brand`
 */
class m180718_051027_create_car_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('car', [
            'id' => $this->primaryKey(),
            'license_id' => $this->string(),
            'car_brand_id' => $this->integer()->notNull(),
            'car_model' => $this->string(),
            'miles' => $this->integer(),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

        // creates index for column `car_brand_id`
        $this->createIndex(
            'idx-car-car_brand_id',
            'car',
            'car_brand_id'
        );

        // add foreign key for table `car_brand`
        $this->addForeignKey(
            'fk-car-car_brand_id',
            'car',
            'car_brand_id',
            'car_brand',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `car_brand`
        $this->dropForeignKey(
            'fk-car-car_brand_id',
            'car'
        );

        // drops index for column `car_brand_id`
        $this->dropIndex(
            'idx-car-car_brand_id',
            'car'
        );

        $this->dropTable('car');
    }
}
