<?php

use yii\db\Migration;

/**
 * Class m180802_090530_table_stock
 */
class m180802_090530_table_stock extends Migration
{


    public function up()
    {
        $this->createTable('types', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);
        $this->createTable('product',[
            'id'=>$this->primaryKey(),
            'code'=>$this->string(),
            'types_id'=>$this->integer(),
            'name'=>$this->string(),
            'detail'=>$this->string(),
            'group_type'=>$this->string(),
            'group_per'=>$this->string(),
            'photos'=>$this->string(),
            'status'=>$this->integer(),
            'lock_cal_products'=>$this->integer(),
            'use'=>$this->integer(),
            'price'=>$this->float(6,2),
            'locations'=>$this->string(),
            'created_by'=>$this->integer(),
            'updated_by'=>$this->integer(),
            'created_at'=>$this->dateTime(),
            'updated_at'=>$this->dateTime(),
        ]);
        $this->createTable('items', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'details' => $this->string(),
            'use_check' => $this->integer(),
            'amount' => $this->integer(),
            'status' => $this->integer(),
            'status_order' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);

    }
    public function down()
    {
        $this->dropTable('items');
        $this->dropTable('product');
        $this->dropTable('types');
    }
}