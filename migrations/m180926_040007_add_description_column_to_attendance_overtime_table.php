<?php

use yii\db\Migration;

/**
 * Handles adding description to table `attendance_overtime`.
 */
class m180926_040007_add_description_column_to_attendance_overtime_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('attendance_overtime', 'description', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('attendance_overtime', 'description');
    }
}
