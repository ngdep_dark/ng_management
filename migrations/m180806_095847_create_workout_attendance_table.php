<?php

use yii\db\Migration;

/**
 * Handles the creation of table `workout_attendance`.
 */
class m180806_095847_create_workout_attendance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('workout_attendance', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'clock_date'=> $this->date(),
            'clock_in' => $this->datetime(),
            'clock_out' => $this->datetime(),
            'description' =>$this->string('500'),
            'note' => $this->string(),
            'status' => $this->string()
        ]);
    }
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('workout_attendance');
    }
}
