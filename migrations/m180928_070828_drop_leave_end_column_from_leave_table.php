<?php

use yii\db\Migration;

/**
 * Handles dropping leave_end from table `leave`.
 */
class m180928_070828_drop_leave_end_column_from_leave_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('leave', 'leave_end');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('leave', 'leave_end' , $this->datetime());

    }
}
