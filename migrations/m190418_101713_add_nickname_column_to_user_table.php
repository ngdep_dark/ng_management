<?php

use yii\db\Migration;

/**
 * Handles adding nickname to table `{{%user}}`.
 */
class m190418_101713_add_nickname_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'nickname', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'nickname');
    }
}