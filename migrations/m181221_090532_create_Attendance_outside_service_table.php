<?php

use yii\db\Migration;

/**
 * Handles the creation of table `outside_service`.
 */
class m181221_090532_create_attendance_outside_service_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('attendance_outside_service', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'outside_date_start' => $this->date(),
            'outside_date_end' => $this->date(),
            'outside_type' => $this->integer(),
            'outside_province' => $this->string(),
            'detail' => $this->string(),
            'status' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('attendance_outside_service');
    }
}
