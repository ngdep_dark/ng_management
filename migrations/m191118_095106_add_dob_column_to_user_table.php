<?php

use yii\db\Migration;

/**
 * Handles adding dob to table `{{%user}}`.
 */
class m191118_095106_add_dob_column_to_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'dob', $this->datetime());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'dob');
    }
}
