<?php

use yii\db\Migration;

/**
 * Handles dropping car_booking_id from table `traffic_ticket`.
 */
class m180913_024715_drop_car_booking_id_column_from_traffic_ticket_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey(
            'fk-traffic_ticket-car_booking_id',
            'traffic_ticket'
        );

        // drops index for column `car_booking_id`
        $this->dropIndex(
            'idx-traffic_ticket-car_booking_id',
            'traffic_ticket'
        );

        $this->dropColumn('traffic_ticket', 'car_booking_id');

        $this->addColumn('traffic_ticket', 'car_booking_id', $this->integer());

        $this->addColumn('traffic_ticket', 'car_id', $this->integer());

        $this->createIndex(
            'idx-traffic_ticket-car_id',
            'traffic_ticket',
            'car_id'
        );

        // add foreign key for table `car_booking`
        $this->addForeignKey(
            'fk-traffic_ticket-car_id',
            'traffic_ticket',
            'car_id',
            'car',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-traffic_ticket-car_id',
            'traffic_ticket'
        );

        // drops index for column `car_booking_id`
        $this->dropIndex(
            'idx-traffic_ticket-car_id',
            'traffic_ticket'
        );

        $this->dropColumn('traffic_ticket', 'car_id');

        $this->dropColumn('traffic_ticket', 'car_booking_id');

        $this->addColumn('traffic_ticket', 'car_booking_id', $this->integer());

        $this->createIndex(
            'idx-traffic_ticket-car_booking_id',
            'traffic_ticket',
            'car_booking_id'
        );

        // add foreign key for table `car_booking`
        $this->addForeignKey(
            'fk-traffic_ticket-car_booking_id',
            'traffic_ticket',
            'car_booking_id',
            'car_booking',
            'id',
            'CASCADE'
        );
    }
}
