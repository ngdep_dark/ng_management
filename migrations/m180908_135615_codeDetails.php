<?php

use yii\db\Migration;

/**
 * Class m180908_135615_codeDetails
 */
class m180908_135615_codeDetails extends Migration
{


    public function up()
    {
        $this->createTable('code_details',[
            'id'=>$this->primaryKey(),
            'code'=>$this->string()->null()->unique(),
            'detail'=>$this->string(),
            'status'=>$this->integer(),
            'created_by'=>$this->integer(),
            'updated_by'=>$this->integer(),
            'created_at'=>$this->dateTime(),
            'updated_at'=>$this->dateTime(),
        ]);
    }

    public function down()
    {
        $this->dropTable('code_details');
    }

}
