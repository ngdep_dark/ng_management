<?php

use yii\db\Migration;

/**
 * Handles the creation of table `holiday`.
 */
class m181122_031048_create_holiday_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('holiday', [
            'id' => $this->primaryKey(),
            'holiday_name' => $this->string(),
            'description' => $this->string(),
            'holiday_date' => $this->date(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('holiday');
    }
}
