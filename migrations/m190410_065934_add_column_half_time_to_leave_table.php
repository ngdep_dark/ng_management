<?php

use yii\db\Migration;

/**
 * Class m190410_065934_add_column_halftime_to_leave_table
 */
class m190410_065934_add_column_half_time_to_leave_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('leave', 'half_time', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('leave', 'half_time');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190410_065934_add_column_halftime_to_leave_table cannot be reverted.\n";

        return false;
    }
    */
}
