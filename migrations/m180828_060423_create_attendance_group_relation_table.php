<?php

use yii\db\Migration;

/**
 * Handles the creation of table `attendance_group_relation`.
 * Has foreign keys to the tables:
 *
 * - `attendance_group_description`
 * - `attendance_group_description`
 */
class m180828_060423_create_attendance_group_relation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('attendance_group_relation', [
            'id' => $this->primaryKey(),
            'parent' => $this->integer()->notNull(),
            'child' => $this->integer()->notNull(),
        ]);

        // creates index for column `parent`
        $this->createIndex(
            'idx-attendance_group_relation-parent',
            'attendance_group_relation',
            'parent'
        );

        // add foreign key for table `attendance_group_description`
        $this->addForeignKey(
            'fk-attendance_group_relation-parent',
            'attendance_group_relation',
            'parent',
            'attendance_group_description',
            'id',
            'CASCADE'
        );

        // creates index for column `child`
        $this->createIndex(
            'idx-attendance_group_relation-child',
            'attendance_group_relation',
            'child'
        );

        // add foreign key for table `attendance_group_description`
        $this->addForeignKey(
            'fk-attendance_group_relation-child',
            'attendance_group_relation',
            'child',
            'attendance_group_description',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `attendance_group_description`
        $this->dropForeignKey(
            'fk-attendance_group_relation-parent',
            'attendance_group_relation'
        );

        // drops index for column `parent`
        $this->dropIndex(
            'idx-attendance_group_relation-parent',
            'attendance_group_relation'
        );

        // drops foreign key for table `attendance_group_description`
        $this->dropForeignKey(
            'fk-attendance_group_relation-child',
            'attendance_group_relation'
        );

        // drops index for column `child`
        $this->dropIndex(
            'idx-attendance_group_relation-child',
            'attendance_group_relation'
        );

        $this->dropTable('attendance_group_relation');
    }
}
