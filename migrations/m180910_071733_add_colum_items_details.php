<?php

use yii\db\Migration;

/**
 * Class m180910_071733_add_colum_items_details
 */
class m180910_071733_add_colum_items_details extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('item_details','order_id',$this->string());
        $this->addColumn('item_details','card_use',$this->string());
    }
    public function down()
    {
        $this->dropColumn('item_details','card_use');
        $this->dropColumn('item_details', 'order_id');
    }
}
