<?php

use yii\db\Migration;

/**
 * Handles the creation of table `appointment_detail`.
 * Has foreign keys to the tables:
 *
 * - `appointment`
 * - `user`
 */
class m190402_035652_create_appointment_detail_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('appointment_detail', [
            'id' => $this->primaryKey(),
            'appointment_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
        ]);

        // creates index for column `appointment_id`
        $this->createIndex(
            'idx-appointment_detail-appointment_id',
            'appointment_detail',
            'appointment_id'
        );

        // add foreign key for table `appointment`
        $this->addForeignKey(
            'fk-appointment_detail-appointment_id',
            'appointment_detail',
            'appointment_id',
            'appointment',
            'id',
            'CASCADE'
        );

        // creates index for column `user_id`
        $this->createIndex(
            'idx-appointment_detail-user_id',
            'appointment_detail',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-appointment_detail-user_id',
            'appointment_detail',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `appointment`
        $this->dropForeignKey(
            'fk-appointment_detail-appointment_id',
            'appointment_detail'
        );

        // drops index for column `appointment_id`
        $this->dropIndex(
            'idx-appointment_detail-appointment_id',
            'appointment_detail'
        );

        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-appointment_detail-user_id',
            'appointment_detail'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-appointment_detail-user_id',
            'appointment_detail'
        );

        $this->dropTable('appointment_detail');
    }
}
