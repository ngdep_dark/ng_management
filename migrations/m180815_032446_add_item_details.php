<?php

use yii\db\Migration;

/**
 * Class m180815_032446_add_item_details
 */
class m180815_032446_add_item_details extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('item_details',[
           'id'=>$this->primaryKey(),
            'item_id'=>$this->integer(),
            'amount'=>$this->integer(),
            'product_id'=>$this->integer(),
            'school_id'=>$this->integer(),
            'status'=>$this->integer(),
            'card_lose'=>$this->string(),
            'card_lose_2'=>$this->string(),
            'created_by'=>$this->integer(),
            'updated_by'=>$this->integer(),
            'causes'=>$this->string(),
            'created_at'=>$this->dateTime(),
            'updated_at'=>$this->dateTime(),
        ]);
        $this->createTable('school',[
            'id'=>$this->primaryKey(),
            'name'=>$this->string(),
        ]);
    }

    public function down()
    {
        $this->dropTable('item_details');
        $this->dropTable('school');
    }
}
