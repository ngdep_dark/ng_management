<?php

use yii\db\Migration;

/**
 * Class m181224_034336_add_roles_users
 */
class m181224_034336_add_roles_users extends Migration
{



    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('user', 'roles', $this->integer());
    }

    public function down()
    {
        $this->dropColumn('user', 'roles');
    }

}
