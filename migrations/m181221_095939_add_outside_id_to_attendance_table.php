<?php

use yii\db\Migration;

/**
 * Class m181221_095939_add_outside_id_to_attendance_table
 */
class m181221_095939_add_outside_id_to_attendance_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('attendance', 'outside_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('attendance','outside_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181221_095939_add_outside_id_to_attendance_table cannot be reverted.\n";

        return false;
    }
    */
}
