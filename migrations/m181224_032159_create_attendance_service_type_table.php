<?php

use yii\db\Migration;

/**
 * Handles the creation of table `attendance_service_type`.
 */
class m181224_032159_create_attendance_service_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('attendance_service_type', [
            'id' => $this->primaryKey(),
            'type_name' => $this->string(),
            'created_at' => $this->date(),
            'created_by' => $this->integer(),
            'updated_at' => $this->date(),
            'updated_by' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('attendance_service_type');
    }
}
